﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_MaeAlmacen
    {
        public short siCodAlm { get; set; }
        public string vDescripcion { get; set; }
        public string vAbreviatura { get; set; }
        public string vDireccion { get; set; }
        public short siCodUsuCre { get; set; }
        public short? siCodUsuAct { get; set; }
    }
}
