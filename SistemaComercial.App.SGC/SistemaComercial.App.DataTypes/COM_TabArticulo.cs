﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_TabArticulo
    {
        public int iCodArt { get; set; }
        public string vDescripcion { get; set; }
        public string vCodigoOriginal { get; set; }
        public string vCodigoAlterno { get; set; }
        public short siCodUMed { get; set; }
        public int iCodMarca { get; set; }
        public short siCodUsuCre { get; set; }
        public short? siCodUsuAct { get; set; }
        public decimal dPesoKg { get; set; }
        public decimal dPrecio { get; set; }
    }
}
