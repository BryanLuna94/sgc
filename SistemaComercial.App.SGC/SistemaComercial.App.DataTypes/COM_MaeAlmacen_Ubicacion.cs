﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_MaeAlmacen_Ubicacion
    {
        public int iCodUbi { get; set; }
        public short siCodAlm { get; set; }
        public byte tiPiso { get; set; }
        public string vRack { get; set; }
        public string vColumna { get; set; }
        public string vFila { get; set; }
        public short siCodUsuCre { get; set; }
        public short? siCodUsuAct { get; set; }
        public decimal dVolumen { get; set; }
        public short? siCodUMed { get; set; }
    }
}
