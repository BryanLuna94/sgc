﻿namespace SistemaComercial.App.DataTypes.Others
{
    public class AppSettings
    {
        public PathsFiles PathsFiles { get; set; }
        public string storagefileaccount { get; set; }
        public string UriContainer { get; set; }
        public string UploadAzure { get; set; }
    }

    public class PathsFiles
    {
        public string PathImgTemplate { get; set; }
        public string PathImgTemplateDetail { get; set; }
        public string PathImgInspection { get; set; }
        public string PathImgInspectionDetail { get; set; }
        public string PathImgUser { get; set; }
        public string PathFileComponentPerformance { get; set; }
    }
}
