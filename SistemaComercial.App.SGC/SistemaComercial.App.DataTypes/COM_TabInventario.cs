﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_TabInventario
    {
        public int iCodInv { get; set; }
        public string vDescripcion { get; set; }
        public short siCodAlm { get; set; }
        public short siCodUsuCre { get; set; }
        public string vAlertaId { get; set; }
        public int iCodPartner { get; set; }
    }
}
