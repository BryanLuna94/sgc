﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_TabPersona
    {
        public int iCodPer { get; set; }
        public byte tiCodTipDocIde { get; set; }
        public string vNroDocIde { get; set; }
        public string vRazonSocial { get; set; }
        public string vApPaterno { get; set; }
        public string vApMaterno { get; set; }
        public string vNombres { get; set; }
        public string vNombreComercial { get; set; }
        public short siCodUsuCre { get; set; }
        public short? siCodUsuAct { get; set; }
        public short siCodPais { get; set; }
        public string vNumero { get; set; }
        public string vPaginaWeb { get; set; }
        public bool bPartner { get; set; }
        public bool bCliente { get; set; }
        public bool bProveedor { get; set; }
        public bool bDistribuidor { get; set; }
        public bool bAgenteRetenedor { get; set; }
        public bool bBuenContribuyente { get; set; }
        public bool bExportador { get; set; }
        public System.DateTime? sdFecNac { get; set; }
    }
}
