﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_TabPersona_Oficina
    {
        public int iCodPerOfi { get; set; }
        public int iCodPer { get; set; }
        public string vDireccion { get; set; }
        public string vDescripcion { get; set; }
        public string vNumero { get; set; }
        public short siCodPais { get; set; }
        public short siCodUsuCre { get; set; }
        public short? siCodUsuAct { get; set; }
    }
}
