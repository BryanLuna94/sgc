﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_TabStock
    {
        public int iCodSto { get; set; }
        public int iCodArt { get; set; }
        public short siCodAlm { get; set; }
        public decimal dCantidad { get; set; }
    }
}
