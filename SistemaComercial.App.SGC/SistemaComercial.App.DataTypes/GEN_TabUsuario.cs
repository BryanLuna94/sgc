﻿namespace SistemaComercial.App.DataTypes
{
    public class GEN_TabUsuario
    {
        public short siCodUsu { get; set; }
        public string vNombre { get; set; }
        public string vUsuario { get; set; }
        public string vClave { get; set; }
        public short siCodPer { get; set; }
        public bool bEliminado { get; set; }
        public short siCodUsuAct { get; set; }
        public string vCorreo { get; set; }
    }
}
