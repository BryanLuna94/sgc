﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_CabFactura
    {
        public int iCodCabMov { get; set; }
        public string vSerie { get; set; }
        public string vNumero { get; set; }
        public int iCodPer { get; set; }
        public int iCodFactura { get; set; }
        public decimal dTotal { get; set; }
        public int? iTipoDoc { get; set; }
        public int? iMoneda { get; set; }
        public System.DateTime? sdFecha { get; set; }
        public string vObservacion { get; set; }
        public decimal dSubTotal { get; set; }
        public decimal dImpuesto { get; set; }
        public decimal dOtrosCargos { get; set; }
        public string vDireccion { get; set; }
    }
}
