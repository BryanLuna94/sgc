﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_DetStock
    {
        public int iCodDetSto { get; set; }
        public int iCodSto { get; set; }
        public string vSerie { get; set; }
        public int iCodEstArt { get; set; }
        public int iCodUbi { get; set; }
        public decimal dCostoIngreso { get; set; }
        public int iCodDetMov { get; set; }
        public decimal dPesoKg { get; set; }
    }
}
