﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_TabPersona_Contacto
    {
        public int iCodPerCon { get; set; }
        public int iCodPer { get; set; }
        public string vNombre { get; set; }
        public string vComentarios { get; set; }
        public string vNumero { get; set; }
        public string vCorreo { get; set; }
        public short siCodUsuCre { get; set; }
        public short? siCodUsuAct { get; set; }
    }
}
