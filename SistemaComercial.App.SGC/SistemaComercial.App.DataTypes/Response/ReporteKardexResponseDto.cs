﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class ReporteKardexResponseDto
    {
        [DataMember]
        public List<ReporteKardexListaDto> ListaKardex { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaAlmacenes { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaEmpresas { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaMarcas { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaMeses { get; set; }
    }
}
