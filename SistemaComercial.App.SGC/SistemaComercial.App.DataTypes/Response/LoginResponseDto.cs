﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class LoginResponseDto
    {
        [DataMember]
        public LoginListaDto Usuario { get; set; }
        [DataMember]
        public List<OpcionListaDto> ListaOpcionesPorPerfil { get; set; }
        [DataMember]
        public ArchivoListaDto FotoUsuario { get; set; }
    }
}
