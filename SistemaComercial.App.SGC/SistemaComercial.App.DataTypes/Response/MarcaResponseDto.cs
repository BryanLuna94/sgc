﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class MarcaResponseDto
    {
        [DataMember]
        public List<MarcaListaDto> ListaMarcas { get; set; }
        [DataMember]
        public COM_MaeMarca Marca { get; set; }
    }
}
