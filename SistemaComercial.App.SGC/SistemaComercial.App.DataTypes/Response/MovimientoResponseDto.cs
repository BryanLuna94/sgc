﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class MovimientoResponseDto
    {
        [DataMember]
        public List<MovimientoListaDto> ListaMovimientos { get; set; }
        [DataMember]
        public MovimientoListaDto MovimientoCabeceraDto { get; set; }
        [DataMember]
        public COM_CabMovArt MovimientoCabecera { get; set; }
        [DataMember]
        public COM_DetMovArt MovimientoDetalle { get; set; }
        [DataMember]
        public COM_TabStock Stock { get; set; }
        [DataMember]
        public COM_DetStock DetalleStock { get; set; }
        [DataMember]
        public COM_CabMovArt_Ext MovimientoCabecera_Ext { get; set; }
        [DataMember]
        public List<DetalleMovimientoListaDto> ListaMovimientoDetalle { get; set; }
        [DataMember]
        public DetalleMovimientoListaDto MovimientoDetalleDto { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaTiposMov { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaTiposDoc { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaMonedas { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaUsuarios { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaAlmacenes { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaEstadosArticulo { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaUbicaciones { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaPisos { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaRacks { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaDirecciones { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaEmpresas { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaEmpresasPartner { get; set; }
        [DataMember]
        public List<ArchivoListaDto> ListaArchivos { get; set; }
        [DataMember]
        public string Piso { get; set; }
        [DataMember]
        public string Rack { get; set; }
        [DataMember]
        public COM_CabFactura Factura { get; set; }
        [DataMember]
        public COM_CabGuia Guia { get; set; }
        [DataMember]
        public List<COM_Liquidacion> ListaLiquidacion { get; set; }
        [DataMember]
        public COM_Liquidacion Liquidacion { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaOficinas { get; set; }
    }
}
