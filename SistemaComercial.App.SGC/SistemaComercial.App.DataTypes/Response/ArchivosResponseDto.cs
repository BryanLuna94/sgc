﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class ArchivosResponseDto
    {
        [DataMember]
        public List<ArchivoListaDto> ListaArchivos { get; set; }
        [DataMember]
        public ArchivoListaDto Archivo { get; set; }
    }
}
