﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class PersonaResponseDto
    {
        [DataMember]
        public List<PersonaListaDto> ListaPersonas { get; set; }
        [DataMember]
        public List<PersonaOficinaListaDto> ListaOficinas { get; set; }
        [DataMember]
        public List<PersonaContactoListaDto> ListaContactos { get; set; }
        [DataMember]
        public COM_TabPersona Persona { get; set; }
        [DataMember]
        public COM_TabPersona_Contacto ContactoPersona { get; set; }
        [DataMember]
        public COM_TabPersona_Oficina OficinaPersona { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaTipDocIdentidad { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaPaises { get; set; }
    }
}
