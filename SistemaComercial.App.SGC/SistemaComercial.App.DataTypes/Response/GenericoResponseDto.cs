﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class GenericoResponseDto
    {
        [DataMember]
        public List<GenericoListaDto> ListaGenerica { get; set; }
    }
}
