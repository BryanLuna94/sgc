﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class InventarioResponseDto
    {
        [DataMember]
        public List<InventarioListaDto> ListaInventarios { get; set; }

        [DataMember]
        public COM_TabInventario Inventario { get; set; }

        [DataMember]
        public List<GenericoListaDto> ListaAlmacenes { get; set; }

        [DataMember]
        public List<GenericoListaDto> ListaEstados { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaEmpresasPartner { get; set; }
    }
}
