﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class UsuarioResponseDto
    {
        [DataMember]
        public List<UsuarioListaDto> ListaUsuarios { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaPerfiles { get; set; }
        [DataMember]
        public GEN_TabUsuario Usuario { get; set; }
        [DataMember]
        public ArchivoListaDto Archivo { get; set; }
    }
}
