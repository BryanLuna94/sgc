﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class MovimientoLiquidacionResponseDto
    {
        [DataMember]
        public COM_Liquidacion Liquidacion { get; set; }
    }
}
