﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class ArticuloResponseDto
    {
        [DataMember]
        public List<ArticuloListaDto> ListaArticulos { get; set; }
        [DataMember]
        public COM_TabArticulo Articulo { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaUnidadesMedida { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaMarcas { get; set; }
    }
}
