﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class GuiaRemisionResponseDto
    {
        [DataMember]
        public ReporteGuiaRemCabListaDto GuiaRemisionCab { get; set; }
        [DataMember]
        public List<ReporteGuiaRemDetListaDto> ListaGuiaRemisionDet { get; set; }
    }
}
