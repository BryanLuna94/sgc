﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class AlmacenUbicacionResponseDto
    {
        [DataMember]
        public List<AlmacenUbicacionListaDto> ListaUbicacionesAlmacen { get; set; }
        [DataMember]
        public COM_MaeAlmacen_Ubicacion AlmacenUbicacion { get; set; }
        [DataMember]
        public AlmacenListaDto AlmacenDto { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaUnidadesMedida { get; set; }
    }
}
