﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class PerfilResponseDto
    {
        [DataMember]
        public List<PerfilListaDto> ListaPerfiles { get; set; }
        [DataMember]
        public GEN_MaePerfil Perfil { get; set; }
        [DataMember]
        public List<OpcionesPorPerfilListaDto> ListaOpciones { get; set; }
        [DataMember]
        public List<OpcionListaDto> ListaOpcionesMenu { get; set; }
    }
}
