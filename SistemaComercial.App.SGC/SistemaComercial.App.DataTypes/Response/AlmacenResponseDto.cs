﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class AlmacenResponseDto
    {
        [DataMember]
        public List<AlmacenListaDto> ListaAlmacenes { get; set; }
        [DataMember]
        public COM_MaeAlmacen Almacen { get; set; }
    }
}
