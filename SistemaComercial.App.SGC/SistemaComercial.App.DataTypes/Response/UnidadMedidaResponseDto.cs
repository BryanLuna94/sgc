﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class UnidadMedidaResponseDto
    {
        [DataMember]
        public List<UnidadMedidaListaDto> ListaUnidadesMedida { get; set; }
        [DataMember]
        public COM_MaeUnidadMedida UnidadMedida { get; set; }
    }
}
