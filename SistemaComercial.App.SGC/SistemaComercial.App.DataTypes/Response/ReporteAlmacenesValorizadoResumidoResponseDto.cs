﻿using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Response
{
    [DataContract]
    public class ReporteAlmacenesValorizadoResumidoResponseDto
    {
        [DataMember]
        public List<ReporteAlmacenesValorizadoResumidoListaDto> ListaAlmacenesValorizados { get; set; }
        [DataMember]
        public List<ReporteAlmacenesValorizadoDetalladoListaDto> ListaAlmacenesValorizadosDet { get; set; }
        [DataMember]
        public List<GenericoListaDto> ListaMarcas { get; set; }
    }
}
