﻿namespace SistemaComercial.App.DataTypes.Filtros
{
    public class PersonaContactoFiltroDto
    {
        public string Nombre { get; set; }
        public int CodigoPersona { get; set; }
    }
}
