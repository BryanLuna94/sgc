﻿namespace SistemaComercial.App.DataTypes.Filtros
{
    public class ReporteKardexFiltroDto
    {
        public short CodigoAlmacen { get; set; }
        public int CodigoEmpresa { get; set; }
        public int Anio { get; set; }
        public int IdMes { get; set; }
        public short CodigoMarca { get; set; }
        public int CodigoProducto { get; set; }
    }
}
