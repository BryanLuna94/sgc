﻿namespace SistemaComercial.App.DataTypes.Filtros
{
    public class AlmacenFiltroDto
    {
        public string Descripcion { get; set; }
        public string Direccion { get; set; }
    }
}
