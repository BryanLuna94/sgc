﻿namespace SistemaComercial.App.DataTypes.Filtros
{
    public class ReporteAlmacenesValorizadoResumidoFiltroDto
    {
        public int CodigoArticulo { get; set; }
        public int CodigoMarca { get; set; }
    }
}
