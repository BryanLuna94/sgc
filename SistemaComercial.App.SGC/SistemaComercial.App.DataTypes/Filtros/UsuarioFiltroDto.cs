﻿namespace SistemaComercial.App.DataTypes.Filtros
{
    public class UsuarioFiltroDto
    {
        public string Usuario { get; set; }
        public string Perfil { get; set; }
    }
}
