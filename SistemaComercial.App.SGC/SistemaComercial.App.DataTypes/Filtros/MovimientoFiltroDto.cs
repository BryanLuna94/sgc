﻿namespace SistemaComercial.App.DataTypes.Filtros
{
    public class MovimientoFiltroDto
    {
        public string Correlativo { get; set; }
        public byte TipoMovimiento { get; set; }
        public string Factura { get; set; }
        public string FechaDesde { get; set; }
        public string FechaHasta { get; set; }
        public string Persona { get; set; }
        public short UsuarioCrea { get; set; }
        public string AlertaId { get; set; }
        public string GuiaArea { get; set; }
        public string RMA { get; set; }
        public string OrdExterior { get; set; }
        public string FacturaComercial { get; set; }
        public byte Canal { get; set; }
        public string Articulo { get; set; }
        public string CodigoOriginal { get; set; }
        public string Serie { get; set; }
        public int Codigo { get; set; }
        public string NumeroDoc { get; set; }
    }
}