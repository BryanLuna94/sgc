﻿namespace SistemaComercial.App.DataTypes.Filtros
{
    public class PersonaOficinaFiltroDto
    {
        public string Nombre { get; set; }
        public int CodigoPersona { get; set; }
    }
}
