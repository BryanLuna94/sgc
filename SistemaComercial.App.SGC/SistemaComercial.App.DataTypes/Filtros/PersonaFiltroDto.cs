﻿namespace SistemaComercial.App.DataTypes.Filtros
{
    public class PersonaFiltroDto
    {
        public string Persona { get; set; }
        public string NroDocumento{ get; set; }
        public string Pais { get; set; }
        public bool EsPartner { get; set; }
        public bool EsCliente { get; set; }
        public bool EsProveedor { get; set; }
        public bool EsDistribuidor { get; set; }
        public bool EsExportador { get; set; }
    }
}
