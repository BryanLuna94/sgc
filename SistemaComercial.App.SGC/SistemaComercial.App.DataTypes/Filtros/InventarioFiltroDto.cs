﻿namespace SistemaComercial.App.DataTypes.Filtros
{
    public class InventarioFiltroDto
    {
        public short CodigoAlmacen { get; set; }
        public string Descripcion { get; set; }
        public int Estado { get; set; }

    }
}
