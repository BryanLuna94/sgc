﻿namespace SistemaComercial.App.DataTypes.Filtros
{
    public class PerfilFiltroDto
    {
        public string Nombre { get; set; }
    }
}
