﻿namespace SistemaComercial.App.DataTypes.Filtros
{
    public class UnidadMedidaFiltroDto
    {
        public string Descripcion { get; set; }
    }
}
