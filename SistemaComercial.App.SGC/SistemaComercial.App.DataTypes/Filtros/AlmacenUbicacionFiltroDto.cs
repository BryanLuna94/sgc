﻿namespace SistemaComercial.App.DataTypes.Filtros
{
    public class AlmacenUbicacionFiltroDto
    {
        public short CodigoAlmacen { get; set; }
        public byte Piso { get; set; }
        public string Rack { get; set; }
        public string Fila { get; set; }
        public string Columna { get; set; }
    }
}
