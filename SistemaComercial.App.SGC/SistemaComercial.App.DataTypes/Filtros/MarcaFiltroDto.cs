﻿namespace SistemaComercial.App.DataTypes.Filtros
{
    public class MarcaFiltroDto
    {
        public string Descripcion { get; set; }
    }
}
