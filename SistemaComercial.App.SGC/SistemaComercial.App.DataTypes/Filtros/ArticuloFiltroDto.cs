﻿namespace SistemaComercial.App.DataTypes.Filtros
{
    public class ArticuloFiltroDto
    {
        public string Descripcion { get; set; }
        public string CodigoOriginal { get; set; }
        public string CodigoAlterno{ get; set; }
        public string Marca { get; set; }
    }
}
