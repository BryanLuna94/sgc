﻿namespace SistemaComercial.App.DataTypes
{
    public class GEN_MaePerfil
    {
        public short siCodPer { get; set; }
        public string vNombre { get; set; }
        public bool bEliminado { get; set; }
        public short siCodUsuAct { get; set; }
    }
}
