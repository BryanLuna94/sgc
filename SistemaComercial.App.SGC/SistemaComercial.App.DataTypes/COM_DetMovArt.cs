﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_DetMovArt
    {
        public int iCodDetMov { get; set; }
        public int iCodCabMov { get; set; }
        public int iCodArt { get; set; }
        public string vSerie { get; set; }
        public int? iCodDetMovIng { get; set; }
        public string vNombreProducto { get; set; }
        public decimal dCosto { get; set; }
        public string vAlertaId { get; set; }
        public decimal dPeso { get; set; }
        public string vLFPN { get; set; }
    }
}
