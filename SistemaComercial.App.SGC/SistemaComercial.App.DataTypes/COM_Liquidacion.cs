﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_Liquidacion
    {
        public int iCodLiquidacion { get; set; }
        public int iCodCabMov { get; set; }
        public string vDescripcion { get; set; }
        public decimal dCosto { get; set; }
    }
}
