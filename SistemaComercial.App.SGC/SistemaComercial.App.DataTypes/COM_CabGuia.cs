﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_CabGuia
    {
        public int iCodCabMov { get; set; }
        public string vSerie { get; set; }
        public string vNumero { get; set; }
        public int iCodPer { get; set; }
        public int iGuiaId { get; set; }
        public string vDireccion { get; set; }
        public int iCodOficina { get; set; }
        public string vObservacion { get; set; }
    }
}
