﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_CabMovArt_Ext
    {
        public int iCodCabMovExt { get; set; }
        public int iCodCabMov { get; set; }
        public string vAlertaId { get; set; }
        public string vGuiaAerea { get; set; }
        public string vRMA { get; set; }
        public string vOrdExt { get; set; }
        public int? iCodPerOperador { get; set; }
        public string vFacCom { get; set; }
        public short? tiCodCan { get; set; }
        public short? tiCodCanAct { get; set; }
        public bool bConValorComercial { get; set; }
        public int iCodPartner { get; set; }
        public bool bTuvoAjuste { get; set; }
        public bool bCarga { get; set; }
        public string vDUA { get; set; }
        public System.DateTime? sdFechaETA { get; set; }
    }
}
