﻿namespace SistemaComercial.App.DataTypes
{
    public class GEN_TabOpcion
    {
        public int iCodOpc { get; set; }
        public string vNombre { get; set; }
        public int iCodOpcPadre { get; set; }
        public string vRuta { get; set; }
        public short siOrden { get; set; }
        public int iCodTipOpc { get; set; }
        public string vClase { get; set; }
    }
}
