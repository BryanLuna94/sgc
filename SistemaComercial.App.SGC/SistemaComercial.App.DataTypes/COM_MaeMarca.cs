﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_MaeMarca
    {
        public int iCodMarca { get; set; }
        public string vDescripcion { get; set; }
        public string vAbreviatura { get; set; }
        public short siCodUsuCre { get; set; }
        public short? siCodUsuAct { get; set; }
    }
}
