﻿namespace SistemaComercial.App.DataTypes
{
    public class GEN_MaeArchivo
    {
        public string Tabla { get; set; }
        public string Codigo { get; set; }
        public byte[] Archivo { get; set; }
        public string Descripcion { get; set; }
        public string Nombre { get; set; }
        public string Extension { get; set; }
    }
}
