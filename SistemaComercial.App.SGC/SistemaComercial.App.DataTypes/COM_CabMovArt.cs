﻿namespace SistemaComercial.App.DataTypes
{
    public class COM_CabMovArt
    {
        public int iCodCabMov { get; set; }
        public string vCorrelativo { get; set; }
        public byte tiCodTipMov { get; set; }
        public short siCodTipDoc { get; set; }
        public string vNumeroDoc { get; set; }
        public System.DateTime sdFechaHoraRecep { get; set; }
        public int iCodPer { get; set; }
        public bool bImpExp { get; set; }
        public short siCodUsuCre { get; set; }
        public short? siCodUsuAct { get; set; }
        public bool bCerrado { get; set; }
        public string vTipoMov { get; set; }
        public string vObservacion { get; set; }
    }
}
