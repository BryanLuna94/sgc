﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Request
{
    public class MarcaRequestDto
    {
        [DataMember]
        public MarcaFiltroDto Filtro { get; set; }

        [DataMember]
        public COM_MaeMarca Marca { get; set; }
    }
}
