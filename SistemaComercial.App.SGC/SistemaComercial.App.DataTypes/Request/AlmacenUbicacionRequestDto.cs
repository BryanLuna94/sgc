﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Request
{
    [DataContract]
    public class AlmacenUbicacionRequestDto
    {
        [DataMember]
        public AlmacenUbicacionFiltroDto Filtro { get; set; }

        [DataMember]
        public COM_MaeAlmacen_Ubicacion AlmacenUbicacion { get; set; }
    }
}
