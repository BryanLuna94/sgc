﻿using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Request
{
    [DataContract]
    public class LoginRequestDto
    {
        [DataMember]
        public string Usuario { get; set; }
        [DataMember]
        public string Clave { get; set; }
    }
}
