﻿using SistemaComercial.App.DataTypes.Filtros;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Request
{
    [DataContract]
    public class InventarioRequestDto
    {
        [DataMember]
        public InventarioFiltroDto Filtro { get; set; }

        [DataMember]
        public COM_TabInventario Inventario { get; set; }
    }
}
