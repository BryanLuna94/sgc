﻿using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace SistemaComercial.App.DataTypes.Request
{
    [DataContract]
    public class MovimientoRequestDto
    {
        [DataMember]
        public MovimientoFiltroDto Filtro { get; set; }
        [DataMember]
        public COM_CabMovArt MovimientoCabecera { get; set; }
        [DataMember]
        public MovimientoListaDto MovimientoCabeceraDto { get; set; }
        [DataMember]
        public COM_CabMovArt_Ext MovimientoCabeceraExt { get; set; }
        [DataMember]
        public COM_DetMovArt MovimientoDetalle { get; set; }
        [DataMember]
        public COM_DetStock DetalleStock { get; set; }
        [DataMember]
        public COM_TabStock CabeceraStock { get; set; }
        [DataMember]
        public COM_CabFactura Factura { get; set; }
        [DataMember]
        public COM_CabGuia Guia { get; set; }
        [DataMember]
        public COM_Liquidacion Liquidacion { get; set; }
        [DataMember]
        public List<COM_Liquidacion> ListaLiquidacion { get; set; }
    }
}
