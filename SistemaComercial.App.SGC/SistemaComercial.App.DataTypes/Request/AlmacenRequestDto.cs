﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Request
{
    [DataContract]
    public class AlmacenRequestDto
    {
        [DataMember]
        public AlmacenFiltroDto Filtro { get; set; }

        [DataMember]
        public COM_MaeAlmacen Almacen { get; set; }
    }
}
