﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using System.Runtime.Serialization;
using SistemaComercial.App.DataTypes.Listas;
using System.Collections.Generic;

namespace SistemaComercial.App.DataTypes.Request
{
    [DataContract]
    public class PerfilRequestDto
    {
        [DataMember]
        public PerfilFiltroDto Filtro { get; set; }
        [DataMember]
        public GEN_MaePerfil Perfil { get; set; }
        [DataMember]
        public List<OpcionesPorPerfilListaDto> ListaOpciones { get; set; }
        [DataMember]
        public short IdPerfil { get; set; }
    }
}
