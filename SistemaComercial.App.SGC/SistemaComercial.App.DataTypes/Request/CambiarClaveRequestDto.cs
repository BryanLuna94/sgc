﻿using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Request
{
    [DataContract]
    public class CambiarClaveRequestDto
    {
        [DataMember]
        public string ClaveAnterior { get; set; }
        [DataMember]
        public string ClaveNueva { get; set; }
        [DataMember]
        public short CodigoUsuario { get; set; }
    }
}
