﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Request
{
    [DataContract]
    public class ArticuloRequestDto
    {
        [DataMember]
        public ArticuloFiltroDto Filtro { get; set; }

        [DataMember]
        public COM_TabArticulo Articulo { get; set; }
    }
}
