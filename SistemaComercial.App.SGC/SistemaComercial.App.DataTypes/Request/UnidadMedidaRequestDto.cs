﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Request
{
    [DataContract]
    public class UnidadMedidaRequestDto
    {
        [DataMember]
        public UnidadMedidaFiltroDto Filtro { get; set; }

        [DataMember]
        public COM_MaeUnidadMedida UnidadMedida { get; set; }
    }
}
