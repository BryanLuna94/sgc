﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Request
{
    [DataContract]
    public class ArchivosRequestDto
    {
        [DataMember]
        public GEN_MaeArchivo Archivo { get; set; }
    }
}
