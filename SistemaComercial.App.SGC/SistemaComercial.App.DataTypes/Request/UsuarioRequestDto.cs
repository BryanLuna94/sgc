﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Request
{
    [DataContract]
    public class UsuarioRequestDto
    {
        [DataMember]
        public UsuarioFiltroDto Filtro { get; set; }
        [DataMember]
        public GEN_TabUsuario Usuario { get; set; }
        [DataMember]
        public byte[] Archivo { get; set; }
    }
}
