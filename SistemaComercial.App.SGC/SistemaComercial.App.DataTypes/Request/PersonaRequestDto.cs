﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using System.Runtime.Serialization;

namespace SistemaComercial.App.DataTypes.Request
{
    [DataContract]
    public class PersonaRequestDto
    {
        [DataMember]
        public PersonaFiltroDto Filtro { get; set; }
        [DataMember]
        public PersonaOficinaFiltroDto FiltroOficina { get; set; }
        [DataMember]
        public PersonaContactoFiltroDto FiltroContacto { get; set; }
        [DataMember]
        public COM_TabPersona Persona { get; set; }
        [DataMember]
        public COM_TabPersona_Contacto ContactoPersona { get; set; }
        [DataMember]
        public COM_TabPersona_Oficina OficinaPersona { get; set; }
    }
}
