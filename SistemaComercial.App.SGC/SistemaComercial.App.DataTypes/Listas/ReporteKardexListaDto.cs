﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class ReporteKardexListaDto
    {
        public string TipoMov { get; set; }
        public string TipoDoc { get; set; }
        public string NumeroDoc { get; set; }
        public string UnMed { get; set; }
        public string Codigo { get; set; }
        public string Fecha { get; set; }
        public int Cantidad { get; set; }
        public string Articulo { get; set; }
        public string Empresa { get; set; }
    }
}
