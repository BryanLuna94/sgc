﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class ArticuloListaDto
    {
        public int Codigo { get; set; }
        public string CodigoOriginal { get; set; }
        public string CodigoAlterno { get; set; }
        public string Descripcion { get; set; }
        public string UnidadMedida { get; set; }
        public string Marca { get; set; }
        public string UsuarioCrea { get; set; }
        public string UsuarioActualiza { get; set; }
        public string FechaCrea { get; set; }
        public string FechaActualiza { get; set; }

    }
}
