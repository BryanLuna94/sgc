﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class MovimientoListaDto
    {
        public int Codigo { get; set; }
        public string Correlativo { get; set; }
        public string TipoMov { get; set; }
        public string TipoDoc { get; set; }
        public string NumeroDoc { get; set; }
        public string UsuarioCrea { get; set; }
        public string UsuarioActualiza { get; set; }
        public string FechaCrea { get; set; }
        public string FechaActualiza { get; set; }
        public string Persona { get; set; }
        public string PersonaExt { get; set; }
        public string FechaHoraRecep { get; set; }
        public string FechaRecepcion { get; set; }
        public string HoraRecepcion { get; set; }
        public string Estado { get; set; }
        public string SerieFactura { get; set; }
        public string NumeroFactura { get; set; }
    }
}
