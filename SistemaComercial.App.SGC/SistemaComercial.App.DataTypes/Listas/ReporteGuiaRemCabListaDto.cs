﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class ReporteGuiaRemCabListaDto
    {
        public string DireccionLlegada { get; set; }
        public string Ruc { get; set; }
        public string Fecha { get; set; }
        public string Serie { get; set; }
        public string Numero { get; set; }
        public string MotivoTraslado { get; set; }
        public string Marca { get; set; }
        public string Origen { get; set; }
        public string Destino { get; set; }
        public string RMA { get; set; }
        public string Estado { get; set; }
        public string NOrden { get; set; }
        public string Peso { get; set; }
        public string DireccionPartida { get; set; }
        public string Cliente { get; set; }
    }
}
