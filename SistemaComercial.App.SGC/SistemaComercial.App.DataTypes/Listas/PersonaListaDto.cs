﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class PersonaListaDto
    {
        public int Codigo { get; set; }
        public string NroDocumento { get; set; }
        public string NombreComercial { get; set; }
        public string RazonSocial { get; set; }
        public string UsuarioCrea { get; set; }
        public string UsuarioActualiza { get; set; }
        public string FechaCrea { get; set; }
        public string FechaActualiza { get; set; }
    }
}
