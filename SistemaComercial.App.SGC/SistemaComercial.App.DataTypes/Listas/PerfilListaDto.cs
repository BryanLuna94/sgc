﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class PerfilListaDto
    {
        public short Codigo { get; set; }
        public string Nombre { get; set; }
    }
}
