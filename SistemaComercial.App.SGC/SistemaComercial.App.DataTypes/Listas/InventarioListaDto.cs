﻿
namespace SistemaComercial.App.DataTypes.Listas
{
    public class InventarioListaDto
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
        public string Persona { get; set; }
        public string Alerta { get; set; }
    }
}
