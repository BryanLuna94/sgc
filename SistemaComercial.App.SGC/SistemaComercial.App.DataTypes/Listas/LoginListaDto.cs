﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class LoginListaDto
    {
        public short Codigo { get; set; }
        public string Nombre { get; set; }
        public string Usuario { get; set; }
        public string Clave { get; set; }
        public short CodigoPerfil { get; set; }
        public string Correo { get; set; }
    }
}
