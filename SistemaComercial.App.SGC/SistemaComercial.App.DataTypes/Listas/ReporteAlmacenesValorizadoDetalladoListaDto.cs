﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class ReporteAlmacenesValorizadoDetalladoListaDto
    {
        public string Almacen { get; set; }
        public string CodigoOriginal { get; set; }
        public string Articulo { get; set; }
        public string Serie { get; set; }
        public decimal Costo { get; set; }
    }
}
