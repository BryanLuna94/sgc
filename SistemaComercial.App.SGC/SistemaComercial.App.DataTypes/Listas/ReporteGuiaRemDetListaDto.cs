﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class ReporteGuiaRemDetListaDto
    {
        public int Cantidad { get; set; }
        public string Descripcion { get; set; }
        public string PartNumber { get; set; }
        public string Serie { get; set; }
        public string Codigo { get; set; }
        public decimal Peso { get; set; }
        public string Marca { get; set; }
        public string Estado { get; set; }
    }
}
