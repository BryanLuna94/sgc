﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class ArchivoListaDto
    {
        public int Longitud { get; set; }
        public byte[] Archivo { get; set; }
        public string Id { get; set; }
        public string Descripcion { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Extension { get; set; }
    }
}
