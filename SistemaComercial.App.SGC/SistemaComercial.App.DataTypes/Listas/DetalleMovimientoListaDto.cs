﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class DetalleMovimientoListaDto
    {
        public int Codigo { get; set; }
        public string Articulo { get; set; }
        public string CodigoOriginal { get; set; }
        public string CodigoAlterno { get; set; }
        public string UnidadMedida { get; set; }
        public string Marca { get; set; }
        public string Serie { get; set; }
        public string Almacen { get; set; }
        public string EstadoArticulo { get; set; }
        public string Ubicacion { get; set; }
        public decimal CostoIngreso { get; set; }
        public decimal PrecioIngreso { get; set; }
        public decimal Peso { get; set; }
        public string FLPN { get; set; }
        public string AlertaId { get; set; }
        public string NombreProducto { get; set; }
    }
}
