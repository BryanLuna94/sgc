﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class ReporteAlmacenesValorizadoResumidoListaDto
    {
        public string Almacen { get; set; }
        public decimal Suma { get; set; }
    }
}
