﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class OpcionListaDto
    {
        public int Codigo { get; set; }
        public string Nombre { get; set; }
        public short Orden { get; set; }
        public int CodigoPadre { get; set; }
        public int TipoOpcion { get; set; }
        public string Ruta { get; set; }
        public string Clase { get; set; }
    }
}
