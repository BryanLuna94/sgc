﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class PersonaContactoListaDto
    {
        public int Codigo { get; set; }
        public string Nombre { get; set; }
        public string Numero { get; set; }
        public string Comentario { get; set; }
        public string UsuarioCrea { get; set; }
        public string UsuarioActualiza { get; set; }
        public string FechaCrea { get; set; }
        public string FechaActualiza { get; set; }
    }
}
