﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class AlmacenUbicacionListaDto
    {
        public int Codigo { get; set; }
        public string Almacen { get; set; }
        public byte Piso { get; set; }
        public string Rack { get; set; }
        public string Fila { get; set; }
        public string Columna { get; set; }
        public string Volumen { get; set; }
        public string UsuarioCrea { get; set; }
        public string UsuarioActualiza { get; set; }
        public string FechaCrea { get; set; }
        public string FechaActualiza { get; set; }
    }
}
