﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class UsuarioListaDto
    {
        public short Codigo { get; set; }
        public string Nombre { get; set; }
        public string Usuario { get; set; }
        public string Perfil { get; set; }
        public string Correo { get; set; }
    }
}
