﻿namespace SistemaComercial.App.DataTypes.Listas
{
    public class AlmacenListaDto
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
        public string Direccion { get; set; }
        public string Abreviatura { get; set; }
        public string UsuarioCrea { get; set; }
        public string UsuarioActualiza { get; set; }
        public string FechaCrea { get; set; }
        public string FechaActualiza { get; set; }
    }
}
