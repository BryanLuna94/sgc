﻿using Newtonsoft.Json;

namespace SistemaComercial.App.RESTApi.Models
{
    public class Inventario
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "descripcion")]
        public string Descripcion { get; set; }

        [JsonProperty(PropertyName = "estado")]
        public string Estado { get; set; }
    }
}