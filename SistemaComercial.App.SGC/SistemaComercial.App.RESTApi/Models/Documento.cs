﻿using Newtonsoft.Json;

namespace SistemaComercial.App.RESTApi.Models
{
    public class Documento
    {
        [JsonProperty(PropertyName = "nroDocumento")]
        public string NroDocumento { get; set; }
        
        [JsonProperty(PropertyName = "tipoDocumento")]
        public string TipoDocumento { get; set; }

        [JsonProperty(PropertyName = "cantidad")]
        public string Cantidad { get; set; }

        [JsonProperty(PropertyName = "precio")]
        public string Precio { get; set; }

    }
}