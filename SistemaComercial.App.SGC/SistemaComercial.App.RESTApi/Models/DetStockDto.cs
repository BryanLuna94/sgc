﻿using Newtonsoft.Json;

namespace SistemaComercial.App.RESTApi.Models
{
    public class DetStockDto
    {
        [JsonProperty(PropertyName = "iCodDetSto")]
        public int iCodDetSto { get; set; }
        
        [JsonProperty(PropertyName = "vDescripcion")]
        public string vDescripcion { get; set; }

        [JsonProperty(PropertyName = "vSerie")]
        public string vSerie { get; set; }

        [JsonProperty(PropertyName = "vCodigoOriginal")]
        public string vCodigoOriginal { get; set; }

        [JsonProperty(PropertyName = "tiPiso")]
        public int tiPiso { get; set; }

        [JsonProperty(PropertyName = "vRack")]
        public string vRack { get; set; }

        [JsonProperty(PropertyName = "vColumna")]
        public string vColumna { get; set; }

        [JsonProperty(PropertyName = "vFila")]
        public string vFila { get; set; }

    }
}