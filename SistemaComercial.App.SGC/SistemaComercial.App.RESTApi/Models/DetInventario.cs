﻿namespace SistemaComercial.App.RESTApi.Models
{
    public class DetInventario
    {
        public int iCodInv { get; set; }
        public int iCodDetSto { get; set; }
    }
}