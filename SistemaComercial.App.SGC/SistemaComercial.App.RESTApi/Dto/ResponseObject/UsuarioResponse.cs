﻿namespace SistemaComercial.App.RESTApi.Dto.ResponseObject
{
    public class UsuarioResponse
    {
        public string name { get; set; }
        public string UsuNom { get; set; }
        public string UsuApePat { get; set; }
        public string UsuApeMat { get; set; }
        public string Email { get; set; }
        public string Perfil { get; set; }
        public string cliente { get; set; }
        public string codigoArea { get; set; }
        public string nombreArea { get; set; }
        public string centroCosto { get; set; }
    }

    public class UsuarioAutenticado
    {
        public string token { get; set; }
        public string username { get; set; }
        public string nombre { get; set; }
        public string perfil { get; set; }
    }


    public class Usuario
    {
        public string username { get; set; }
        public string nombre { get; set; }
        public string perfil { get; set; }
        public string clave { get; set; }
    }
}