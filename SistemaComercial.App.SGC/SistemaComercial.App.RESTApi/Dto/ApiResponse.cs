﻿namespace SistemaComercial.App.RESTApi.Dto
{
    public class ApiResponse
    {
        public int status { get; set; }
        public object data { get; set; }
        public string msg { get; set; }
    }
}