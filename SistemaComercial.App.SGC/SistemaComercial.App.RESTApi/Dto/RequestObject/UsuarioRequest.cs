﻿namespace SistemaComercial.App.RESTApi.Dto.RequestObject
{
    public class UsuarioRequest
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}