﻿using Newtonsoft.Json;
using SistemaComercial.App.RESTApi.Data;
using SistemaComercial.App.RESTApi.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SistemaComercial.App.RESTApi.Controllers
{
    [RoutePrefix("api/inventario")]
    public class InventarioController : ApiController
    {
        DInventario oData = new DInventario();

        [Authorize]
        [HttpGet]
        [Route("")]
        public HttpResponseMessage GetAll()
        {
            try
            {
                List<Inventario> listInventario = new List<Inventario>();
                listInventario = oData.Listar();
                string jsonResult = JsonConvert.SerializeObject(listInventario, Formatting.Indented);
                var json = Request.CreateResponse(HttpStatusCode.OK);
                json.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");

                return json;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(ex); ;
            }
        }

        [Authorize]
        [HttpGet]
        [Route("{id}")]
        public HttpResponseMessage GetById(string id)
        {
            try
            {
                DetStockDto DetStockDto = new DetStockDto();
                DetStockDto = oData.Obtener(Convert.ToInt32(id));
                string jsonResult = JsonConvert.SerializeObject(DetStockDto, Formatting.Indented);
                var json = Request.CreateResponse(HttpStatusCode.OK);
                json.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");

                return json;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(ex); ;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("")]
        public HttpResponseMessage Create(DetInventario detInventario)
        {
            try
            {

                List<DetInventario> detInventarioaux = new List<DetInventario>();
                detInventarioaux = oData.Validar(detInventario);
                if (detInventarioaux.Count > 0)
                {
                    string jsonResult = JsonConvert.SerializeObject(detInventario, Formatting.Indented);
                    var json = Request.CreateResponse(HttpStatusCode.Found);
                    json.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
                    return json;
                }
                else {
                    oData.Crear(detInventario);
                    string jsonResult = JsonConvert.SerializeObject(detInventario, Formatting.Indented);
                    var json = Request.CreateResponse(HttpStatusCode.Created);
                    json.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
                    return json;
                }
                
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(ex); ;
            }
        }

    }
}