﻿using Newtonsoft.Json;
using SistemaComercial.App.RESTApi.Data;
using SistemaComercial.App.RESTApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SistemaComercial.App.RESTApi.Controllers
{
    [RoutePrefix("api/documento")]
    public class DocumentoController : ApiController
    {
        DDocumento oData = new DDocumento();

        [Authorize]
        [HttpPost]
        [Route("{nroDocumento}")]
        public HttpResponseMessage Create(string nroDocumento)
        {
            try
            {
                List<Documento> listDocumento = new List<Documento>();
                listDocumento = oData.Listar(nroDocumento);
                if (listDocumento.Count>0)
                {
                    return Request.CreateResponse(HttpStatusCode.Conflict);
                    // guardar
                }

                var json = Request.CreateResponse(HttpStatusCode.Created);
                return json;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(ex);
            }
        }
    }
}
