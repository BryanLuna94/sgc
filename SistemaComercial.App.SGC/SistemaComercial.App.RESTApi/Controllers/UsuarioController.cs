﻿using SistemaComercial.App.RESTApi.Authentication;
using SistemaComercial.App.RESTApi.Data;
using SistemaComercial.App.RESTApi.Dto.RequestObject;
using SistemaComercial.App.RESTApi.Dto.ResponseObject;
using SistemaComercial.App.Utility;
using System.Web.Http;

namespace SistemaComercial.App.RESTApi.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/user")]
    public class UsuarioController : ApiController
    {
        DUsuario dUsuario = new DUsuario();
        [HttpPost]
        [Route("login")]

        public IHttpActionResult Authenticate([FromBody] UsuarioRequest usuario)
        {
            if (!ModelState.IsValid) return BadRequest("Inválido");

            var u = dUsuario.Obtener(usuario.username, usuario.password);

            if (u.clave != Funciones.Encriptar(usuario.password.ToUpper()))
            {
                return Ok(HL.ApiResponse(null, -1, "Clave de usuario incorrecta."));
            }
            if (u.nombre != null)
            {
                var token = TokenGenerator.GenerateToken(u.username, u.nombre);
                UsuarioAutenticado user = new UsuarioAutenticado();
                user.token = token;
                user.username = u.username;
                user.nombre = u.nombre;
                user.perfil = u.perfil;

                return Ok(HL.ApiResponse(user));
            }
            return Ok(HL.ApiResponse(null, -1, "Usuario no tiene permisos de acceso"));

        }

    }
}