﻿using System;
using System.Collections.Generic;
using System.Data;
using SistemaComercial.App.RESTApi.Data.Base;
using SistemaComercial.App.RESTApi.Models;

namespace SistemaComercial.App.RESTApi.Data
{
    public class DInventario
    {
        public List<Inventario> Listar()
        {
            ConexionManager oTrfCon = new ConexionManager();
            oTrfCon.Conectar();

            DataTable dtArea = oTrfCon.ejecutarDataTable("usp_COM_TabInventario_Listar", "",null,1);
            List<Inventario> listInventario = new List<Inventario>();

            foreach (DataRow item in dtArea.Rows)
            {
                Inventario Inventario = new Inventario();
                Inventario.Id = (int)item[0];
                Inventario.Descripcion = item[1].ToString().Trim();
                Inventario.Estado = item[2].ToString().Trim();
                listInventario.Add(Inventario);
            }

            return listInventario;
        }

        public DetStockDto Obtener(int id)
        {
            ConexionManager oTrfCon = new ConexionManager();
            oTrfCon.Conectar();

            DataTable dtDetStockDto = oTrfCon.ejecutarDataTable("usp_COM_DetStock_Obtener", id);
            DetStockDto detStockDto = new DetStockDto();
            if (dtDetStockDto.Rows.Count > 0)
            {
                detStockDto.iCodDetSto = (int)dtDetStockDto.Rows[0]["iCodDetSto"];
                detStockDto.vDescripcion = dtDetStockDto.Rows[0]["vDescripcion"].ToString().Trim();
                detStockDto.vSerie = dtDetStockDto.Rows[0]["vSerie"].ToString().Trim();
                detStockDto.vCodigoOriginal = dtDetStockDto.Rows[0]["vCodigoOriginal"].ToString().Trim();
                detStockDto.tiPiso = Convert.ToInt32(dtDetStockDto.Rows[0]["tiPiso"]);
                detStockDto.vRack = dtDetStockDto.Rows[0]["vRack"].ToString().Trim();
                detStockDto.vColumna = dtDetStockDto.Rows[0]["vColumna"].ToString().Trim();
                detStockDto.vFila = dtDetStockDto.Rows[0]["vFila"].ToString().Trim();
            }   

            return detStockDto;
        }

        public void Crear(DetInventario detInventario)
        {
            ConexionManager oTrfCon = new ConexionManager();
            oTrfCon.Conectar();
            oTrfCon.ejecutar("usp_COM_DetInventario_Crear", detInventario.iCodInv, detInventario.iCodDetSto);
        }

        public List<DetInventario> Validar(DetInventario detInventario)
        {
            ConexionManager oTrfCon = new ConexionManager();
            oTrfCon.Conectar();

            DataTable dtDetInventario = oTrfCon.ejecutarDataTable("usp_COM_DetStock_Validar", detInventario.iCodInv, detInventario.iCodDetSto);
            List<DetInventario> listdetInventario = new List<DetInventario>();
            foreach (DataRow item in dtDetInventario.Rows)
            {
                DetInventario DetInventarioAux = new DetInventario();
                DetInventarioAux.iCodInv = (int)item[0];
                DetInventarioAux.iCodDetSto = (int)item[1];

                listdetInventario.Add(DetInventarioAux);
            }

            return listdetInventario;
        }

    }
}