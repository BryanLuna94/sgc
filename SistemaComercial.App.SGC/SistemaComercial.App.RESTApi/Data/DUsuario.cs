﻿using System.Data;
using SistemaComercial.App.RESTApi.Data.Base;
using SistemaComercial.App.RESTApi.Dto.ResponseObject;

namespace SistemaComercial.App.RESTApi.Data
{
    public class DUsuario
    {
        public Usuario Obtener(string userName, string password)
        {
            ConexionManager oTrfCon = new ConexionManager();
            oTrfCon.Conectar();

            DataTable dtUsuario = oTrfCon.ejecutarDataTable("usp_GEN_TabUsuario_Login", userName);
            Usuario usuario = new Usuario();
            if (dtUsuario.Rows.Count > 0)
            {
                usuario.username = dtUsuario.Rows[0]["vUsuario"].ToString();
                usuario.nombre = dtUsuario.Rows[0]["vNombre"].ToString().Trim();
                usuario.perfil = dtUsuario.Rows[0]["siCodPer"].ToString();
                usuario.clave = dtUsuario.Rows[0]["vClave"].ToString();
            }

            return usuario;
        }

    }
}