﻿using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using MPED = Microsoft.Practices.EnterpriseLibrary.Data;

namespace SistemaComercial.App.RESTApi.Data.Base
{
    public class ConexionManager
    {
        private MPED.Sql.SqlDatabase loSqlConnIn;
        public ConexionManager()
        {
            Conectar();
        }


        public void Conectar()
        {
            string sConexion = obtenerCadenaConexion();
            loSqlConnIn = new Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase(sConexion);
        }

        public DataSet ejecutarDataSet(string sProcedure, params object[] valores)
        {
            DbCommand cmd = loSqlConnIn.GetStoredProcCommand(sProcedure, valores);
            cmd.CommandTimeout = 0;

            return loSqlConnIn.ExecuteDataSet(cmd);
        }

        public DataTable ejecutarDataTable(string sProcedure, params object[] valores)
        {
            DbCommand cmd = loSqlConnIn.GetStoredProcCommand(sProcedure, valores);
            cmd.CommandTimeout = 0;

            DataSet ds = loSqlConnIn.ExecuteDataSet(cmd);
            DataTable dt = null;
            if (ds.Tables.Count > 0) dt = ds.Tables[0].Copy();

            return dt;
        }

        public void ejecutar(string sProcedure, params object[] valores)
        {
            DbCommand cmd = loSqlConnIn.GetStoredProcCommand(sProcedure, valores);
            cmd.CommandTimeout = 0;
            loSqlConnIn.ExecuteNonQuery(cmd);
        }

        public int ejecutarReturn(string sProcedure, params object[] valores)
        {
            DbCommand cmd = loSqlConnIn.GetStoredProcCommand(sProcedure, valores);
            cmd.CommandTimeout = 0;
            loSqlConnIn.ExecuteScalar(cmd);
            int newId = (int)cmd.Parameters["@newIdProducto"].Value;
            return newId;
        }


        public IDbConnection GetConnection
        {
            get
            {
                var sqlConnection = new SqlConnection();
                sqlConnection.ConnectionString = obtenerCadenaConexion(); 
                sqlConnection.Open();
                return sqlConnection;
            }
        }

        private string obtenerCadenaConexion()
        {
            string Aplicacion = ConfigurationManager.AppSettings["AppName"].ToString();
            string DataBaseCore = ConfigurationManager.AppSettings["DataBase"].ToString();
            string ServerCore = ConfigurationManager.AppSettings["Server"].ToString();
            string UsuarioCore = ConfigurationManager.AppSettings["Usuario"].ToString();
            string ClaveCore = ConfigurationManager.AppSettings["Clave"].ToString();
            return $"Database={DataBaseCore};Server={ServerCore};User ID={UsuarioCore};Password={ClaveCore};Application Name={Aplicacion};Connect Timeout=0";
            //return $"workstation id=BDCOMERCIAL.mssql.somee.com;packet size=4096;user id=appinventario_SQLLogin_1;pwd=fa5duswbbe;data source=BDCOMERCIAL.mssql.somee.com;persist security info=False;initial catalog=BDCOMERCIAL";
        }
    }
}