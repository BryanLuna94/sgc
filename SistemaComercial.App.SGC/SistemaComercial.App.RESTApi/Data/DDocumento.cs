﻿using System.Collections.Generic;
using SistemaComercial.App.RESTApi.Data.Base;
using SistemaComercial.App.RESTApi.Models;

namespace SistemaComercial.App.RESTApi.Data
{
    public class DDocumento
    {
        public List<Documento> Listar(string nroDocumento)
        {
            ConexionManager oTrfCon = new ConexionManager();
            oTrfCon.Conectar();

            //DataTable dtDocumento = oTrfCon.ejecutarDataTable("up_listar_documento",nroDocumento);
            //List<Documento> listDocumento = new List<Documento>();

            //foreach (DataRow item in dtArea.Rows)
            //{
            //    Documento documento = new Documento();
            //    documento.NroDocumento = item[0].ToString().Trim();
            //    documento.TipoDocumento = item[1].ToString().Trim();
            //    documento.Cantidad = item[2].ToString().Trim();
            //    documento.Precio = item[3].ToString().Trim();
            //    listDocumento.Add(documento);
            //}

            List<Documento> listDocumento = new List<Documento>();
            if(nroDocumento == "78762345") { 
                listDocumento.Add(new Documento { NroDocumento="78762345",TipoDocumento="DNI",Cantidad="7",Precio="1000" });
            }

            return listDocumento;
        }

    }
}