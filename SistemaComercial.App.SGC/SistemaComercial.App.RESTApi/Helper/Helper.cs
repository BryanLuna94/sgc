﻿using SistemaComercial.App.RESTApi.Dto;

namespace SistemaComercial.App.RESTApi
{
    public class HL
    {
        public static ApiResponse ApiResponse(object D, int status = 0, string msg = null)
        {
            return new ApiResponse() { status = status, data = D, msg = msg };
        }
    }
}