﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Request;
using SistemaComercial.App.DataTypes.Response;
using SistemaComercial.App.Utility;
using System;
using System.Collections.Generic;
using SistemaComercial.App.DataAccess;
using SistemaComercial.App.DataTypes;
using System.Transactions;
using JBLV.Log;

namespace SistemaComercial.App.BusinessLayer
{
    public class PersonaLogic
    {
        /// <summary>Invoca al Procedimiento Personaado que lista Personas.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Personas</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static PersonaResponseDto ObtenerIndexPersonas(PersonaRequestDto request)
        {
            try
            {
                PersonaResponseDto response;
                PersonaFiltroDto objPersonaFiltro;
                List<PersonaListaDto> listaPersonas;

                objPersonaFiltro = request.Filtro;
                listaPersonas = COM_TabPersonaData.ListaPersonas(objPersonaFiltro);

                response = new PersonaResponseDto
                {
                    ListaPersonas = listaPersonas
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Personaado que lista Personas.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Personas</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static PersonaResponseDto ListaPersonas(PersonaRequestDto request)
        {
            try
            {
                PersonaResponseDto response;
                PersonaFiltroDto objPersonaFiltro;
                List<PersonaListaDto> listaPersonas;

                objPersonaFiltro = request.Filtro;
                listaPersonas = COM_TabPersonaData.ListaPersonas(objPersonaFiltro);

                response = new PersonaResponseDto
                {
                    ListaPersonas = listaPersonas
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static PersonaResponseDto ObtenerEditorPersona(int IdPersona = 0)
        {
            try
            {
                PersonaResponseDto response;
                COM_TabPersona objPersona;
                List<PersonaContactoListaDto> listaContactos;
                List<PersonaOficinaListaDto> listaOficinas;
                PersonaContactoFiltroDto objFiltroContacto;
                PersonaOficinaFiltroDto objFiltroOficina;
                List<GenericoListaDto> listaTipDocIdentidad;
                List<GenericoListaDto> listaPaises;
                int codigoTablaTipDocIdentidad;

                objPersona = COM_TabPersonaData.ObtenerPersona(IdPersona);
                listaContactos = new List<PersonaContactoListaDto>();
                listaOficinas = new List<PersonaOficinaListaDto>();
                if (IdPersona > 0)
                {
                    objFiltroContacto = new PersonaContactoFiltroDto { CodigoPersona = IdPersona };
                    objFiltroOficina = new PersonaOficinaFiltroDto { CodigoPersona = IdPersona };
                    listaContactos = COM_TabPersona_ContactoData.ListaPersonaContacto(objFiltroContacto);
                    listaOficinas = COM_TabPersona_OficinaData.ListaPersonaOficina(objFiltroOficina);
                }
                codigoTablaTipDocIdentidad = Convert.ToInt32(Constantes.Tablas.TIPO_DOCIDENTIDAD);
                listaTipDocIdentidad = GEN_TabParametroData.ListaParametros(codigoTablaTipDocIdentidad);
                listaPaises = GEN_MaePais.ListaPaises_Combo();

                response = new PersonaResponseDto
                {
                    Persona = objPersona,
                    ListaContactos = listaContactos,
                    ListaOficinas = listaOficinas,
                    ListaPaises = listaPaises,
                    ListaTipDocIdentidad = listaTipDocIdentidad
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarPersona(PersonaRequestDto request)
        {
            COM_TabPersona objPersona;
            int resultado;

            objPersona = request.Persona;

            try
            {
                if (COM_TabPersonaData.ValidaExiste(objPersona))
                {
                    BusinessException.Generar(string.Format(Constantes.Mensajes.PERSONA_YA_EXISTE, objPersona.vNroDocIde));
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    resultado = COM_TabPersonaData.RegistrarPersona(objPersona);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return resultado;
        }

        /// <summary>Método que Actualiza maestro de Persona.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarPersona(PersonaRequestDto request)
        {
            COM_TabPersona objPersona;
            COM_TabPersona objPersonaAnterior;

            objPersona = request.Persona;
            objPersonaAnterior = COM_TabPersonaData.ObtenerPersona(objPersona.iCodPer);

            try
            {
                if (objPersona.tiCodTipDocIde != objPersonaAnterior.tiCodTipDocIde && objPersona.vNroDocIde != objPersonaAnterior.vNroDocIde)
                {
                    if (COM_TabPersonaData.ValidaExiste(objPersona))
                    {
                        BusinessException.Generar(string.Format(Constantes.Mensajes.PERSONA_YA_EXISTE, objPersona.vNroDocIde));
                    }
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_TabPersonaData.ActualizarPersona(objPersona);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que elimina Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la Persona.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarPersona(PersonaRequestDto request)
        {
            COM_TabPersona objPersona;

            objPersona = request.Persona;

            try
            {
                if (COM_TabPersonaData.ValidaEsMarca(objPersona.iCodPer))
                {
                    BusinessException.Generar(Constantes.Mensajes.PERSONA_TIENEMARCA);
                }

                if (COM_TabPersonaData.ValidaTieneMovimientos(objPersona.iCodPer) || COM_TabPersonaData.ValidaTieneMovimientosExt(objPersona.iCodPer))
                {
                    BusinessException.Generar(Constantes.Mensajes.PERSONA_TIENEMOVIMIENTOS);
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_TabPersonaData.EliminarPersona(objPersona);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Nivels.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Nivels</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static GenericoResponseDto ListaPersona_Autocomplete(MovimientoRequestDto request)
        {
            try
            {
                List<GenericoListaDto> listaPersonas;
                GenericoResponseDto response;
                PersonaFiltroDto objFiltro;
                COM_CabMovArt objMovimiento;
                byte codigoTipoMovimientoIngreso;

                codigoTipoMovimientoIngreso = Convert.ToByte(Constantes.Tablas.TipoMovimiento.INGRESO);
                objMovimiento = request.MovimientoCabecera;

                if (codigoTipoMovimientoIngreso == objMovimiento.tiCodTipMov)
                {

                }

                objFiltro = new PersonaFiltroDto();

                objFiltro.EsCliente = true;
                objFiltro.EsDistribuidor = true;
                objFiltro.EsExportador = true;
                objFiltro.EsPartner = true;
                objFiltro.EsProveedor = true;
                objFiltro.Persona = request.MovimientoCabeceraDto.Persona;

                listaPersonas = COM_TabPersonaData.ListaEmpresas_Auto(objFiltro);

                response = new GenericoResponseDto
                {
                    ListaGenerica = listaPersonas
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }
    }
}
