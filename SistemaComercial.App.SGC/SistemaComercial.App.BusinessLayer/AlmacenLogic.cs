﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Request;
using SistemaComercial.App.DataTypes.Response;
using SistemaComercial.App.Utility;
using System;
using System.Collections.Generic;
using SistemaComercial.App.DataAccess;
using SistemaComercial.App.DataTypes;
using System.Transactions;
using JBLV.Log;

namespace SistemaComercial.App.BusinessLayer
{
    public class AlmacenLogic
    {
        /// <summary>Invoca al Procedimiento almacenado que lista Almacenes.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Almacenes</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static AlmacenResponseDto ObtenerIndexAlmacenes(AlmacenRequestDto request)
        {
            try
            {
                AlmacenResponseDto response;
                AlmacenFiltroDto objAlmacenFiltro;
                List<AlmacenListaDto> listaAlmacenes;

                objAlmacenFiltro = request.Filtro;
                listaAlmacenes = COM_MaeAlmacenData.ListaAlmacenes(objAlmacenFiltro);

                response = new AlmacenResponseDto
                {
                    ListaAlmacenes = listaAlmacenes
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Almacenes.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Almacenes</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static AlmacenResponseDto ListaAlmacenes(AlmacenRequestDto request)
        {
            try
            {
                AlmacenResponseDto response;
                AlmacenFiltroDto objAlmacenFiltro;
                List<AlmacenListaDto> listaAlmacenes;

                objAlmacenFiltro = request.Filtro;
                listaAlmacenes = COM_MaeAlmacenData.ListaAlmacenes(objAlmacenFiltro);

                response = new AlmacenResponseDto
                {
                    ListaAlmacenes = listaAlmacenes
                };

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>Método que registra Almacenes.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static AlmacenResponseDto ObtenerEditorAlmacen(short IdAlmacen = 0)
        {
            try
            {
                AlmacenResponseDto response;
                COM_MaeAlmacen objAlmacen;

                objAlmacen = COM_MaeAlmacenData.ObtenerAlmacen(IdAlmacen);

                response = new AlmacenResponseDto
                {
                    Almacen = objAlmacen
                };

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Almacenes.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarAlmacen(AlmacenRequestDto request)
        {
            COM_MaeAlmacen objAlmacen;
            List<COM_TabArticulo> listaArticulosActivos;
            COM_TabStock objStock;
            int resultado;

            objAlmacen = request.Almacen;

            try
            {
                if (COM_MaeAlmacenData.ValidaExiste(objAlmacen))
                {
                    BusinessException.Generar(string.Format(Constantes.Mensajes.ALMACEN_YA_EXISTE, objAlmacen.vDescripcion));
                }

                listaArticulosActivos = COM_TabArticuloData.ListaArticulosActivos();

                using (TransactionScope tran = new TransactionScope())
                {
                    resultado = COM_MaeAlmacenData.RegistrarAlmacen(objAlmacen);

                    foreach (var item in listaArticulosActivos)
                    {
                        objStock = new COM_TabStock
                        {
                            dCantidad = 0,
                            iCodArt = item.iCodArt,
                            siCodAlm = Convert.ToInt16(resultado)
                        };
                        COM_TabStockData.RegistrarStock(objStock);
                    }

                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return resultado;
        }

        /// <summary>Método que Actualiza maestro de Almacen.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarAlmacen(AlmacenRequestDto request)
        {
            COM_MaeAlmacen objAlmacen;
            COM_MaeAlmacen objAlmacenAnterior;

            objAlmacen = request.Almacen;
            objAlmacenAnterior = COM_MaeAlmacenData.ObtenerAlmacen(objAlmacen.siCodAlm);

            try
            {
                if (objAlmacen.vDescripcion.ToUpper() != objAlmacenAnterior.vDescripcion.ToUpper())
                {
                    if (COM_MaeAlmacenData.ValidaExiste(objAlmacen))
                    {
                        BusinessException.Generar(string.Format(Constantes.Mensajes.ALMACEN_YA_EXISTE, objAlmacen.vDescripcion));
                    }
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_MaeAlmacenData.ActualizarAlmacen(objAlmacen);
                    tran.Complete();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>Método que elimina Almacenes.</summary>
        /// <param name="objPersona">Entidad con los datos de la Almacen.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarAlmacen(AlmacenRequestDto request)
        {
            COM_MaeAlmacen objAlmacen;

            objAlmacen = request.Almacen;

            try
            {
                if (COM_MaeAlmacenData.ValidaTieneStock(objAlmacen.siCodAlm))
                {
                    BusinessException.Generar(Constantes.Mensajes.ALMACEN_TIENESTOCK);
                }

                if (COM_MaeAlmacenData.ValidaTieneStock(objAlmacen.siCodAlm))
                {
                    BusinessException.Generar(Constantes.Mensajes.ALMACEN_TIENEUBICACIONES);
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_MaeAlmacenData.EliminarAlmacen(objAlmacen);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        #region UBICACION ALMACEN

        /// <summary>Invoca al Procedimiento almacenado que lista Almacenes.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Almacenes</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static AlmacenUbicacionResponseDto ObtenerIndexUbicacionAlmacen(AlmacenUbicacionRequestDto request)
        {
            try
            {
                AlmacenUbicacionResponseDto response;
                AlmacenUbicacionFiltroDto objAlmacenUbicacionFiltro;
                List<AlmacenUbicacionListaDto> listaUbicaciones;
                AlmacenListaDto objAlmacenDto;
                COM_MaeAlmacen objAlmacen;

                objAlmacenUbicacionFiltro = request.Filtro;
                listaUbicaciones = COM_MaeAlmacen_UbicacionData.ListaUbicaciones(objAlmacenUbicacionFiltro);
                objAlmacen = COM_MaeAlmacenData.ObtenerAlmacen(objAlmacenUbicacionFiltro.CodigoAlmacen);
                objAlmacenDto = new AlmacenListaDto { Descripcion = objAlmacen.vDescripcion };

                response = new AlmacenUbicacionResponseDto
                {
                    ListaUbicacionesAlmacen = listaUbicaciones,
                    AlmacenDto = objAlmacenDto
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento AlmacenUbicacionado que lista AlmacenUbicaciones.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las AlmacenUbicaciones</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static AlmacenUbicacionResponseDto ListaUbicacionesAlmacen(AlmacenUbicacionRequestDto request)
        {
            try
            {
                AlmacenUbicacionResponseDto response;
                AlmacenUbicacionFiltroDto objAlmacenUbicacionFiltro;
                List<AlmacenUbicacionListaDto> listaUbicaciones;

                objAlmacenUbicacionFiltro = request.Filtro;
                listaUbicaciones = COM_MaeAlmacen_UbicacionData.ListaUbicaciones(objAlmacenUbicacionFiltro);

                response = new AlmacenUbicacionResponseDto
                {
                    ListaUbicacionesAlmacen = listaUbicaciones
                };

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>Método que registra AlmacenUbicaciones.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static AlmacenUbicacionResponseDto ObtenerEditorUbicacionAlmacen(short IdAlmacenUbicacion = 0)
        {
            try
            {
                AlmacenUbicacionResponseDto response;
                COM_MaeAlmacen_Ubicacion objAlmacenUbicacion;
                List<GenericoListaDto> listaUnidadesMedida;

                objAlmacenUbicacion = COM_MaeAlmacen_UbicacionData.ObtenerUbicacionAlmacen(IdAlmacenUbicacion);
                listaUnidadesMedida = COM_MaeUnidadMedidaData.ListaUnidadMedida_Combo();

                response = new AlmacenUbicacionResponseDto
                {
                    AlmacenUbicacion = objAlmacenUbicacion,
                    ListaUnidadesMedida = listaUnidadesMedida
                };

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra AlmacenUbicaciones.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarUbicacionAlmacen(AlmacenUbicacionRequestDto request)
        {
            COM_MaeAlmacen_Ubicacion objAlmacenUbicacion;
            int resultado;

            objAlmacenUbicacion = request.AlmacenUbicacion;

            try
            {
                if (COM_MaeAlmacen_UbicacionData.ValidaExiste(objAlmacenUbicacion))
                {
                    BusinessException.Generar(string.Format(Constantes.Mensajes.UBICACIONALMACEN_YA_EXISTE));
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    resultado = COM_MaeAlmacen_UbicacionData.RegistrarUbicacionAlmacen(objAlmacenUbicacion);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return resultado;
        }

        /// <summary>Método que Actualiza maestro de AlmacenUbicacion.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarUbicacionAlmacen(AlmacenUbicacionRequestDto request)
        {
            COM_MaeAlmacen_Ubicacion objAlmacenUbicacion;
            COM_MaeAlmacen_Ubicacion objAlmacenUbicacionAnterior;

            objAlmacenUbicacion = request.AlmacenUbicacion;
            objAlmacenUbicacionAnterior = COM_MaeAlmacen_UbicacionData.ObtenerUbicacionAlmacen(objAlmacenUbicacion.iCodUbi);

            try
            {
                if (objAlmacenUbicacion.siCodAlm != objAlmacenUbicacionAnterior.siCodAlm || objAlmacenUbicacion.tiPiso != objAlmacenUbicacionAnterior.tiPiso ||
                    objAlmacenUbicacion.vRack.ToUpper() != objAlmacenUbicacionAnterior.vRack.ToUpper() || objAlmacenUbicacion.vFila.ToUpper() != objAlmacenUbicacionAnterior.vFila.ToUpper() ||
                    objAlmacenUbicacion.vColumna.ToUpper() != objAlmacenUbicacionAnterior.vColumna.ToUpper())
                {
                    if (COM_MaeAlmacen_UbicacionData.ValidaExiste(objAlmacenUbicacion))
                    {
                        BusinessException.Generar(string.Format(Constantes.Mensajes.UBICACIONALMACEN_YA_EXISTE));
                    }
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_MaeAlmacen_UbicacionData.ActualizarUbicacionAlmacen(objAlmacenUbicacion);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que elimina AlmacenUbicaciones.</summary>
        /// <param name="objPersona">Entidad con los datos de la AlmacenUbicacion.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarUbicacionAlmacen(AlmacenUbicacionRequestDto request)
        {
            COM_MaeAlmacen_Ubicacion objAlmacenUbicacion;

            objAlmacenUbicacion = request.AlmacenUbicacion;

            try
            {
                //if (COM_MaeAlmacenUbicacionData.ValidaTieneProblemas(objAlmacenUbicacion.siCodAlm))
                //{
                //    BusinessException.Generar(Constantes.Mensajes.AlmacenUbicacion_TIENE_PROBLEMAS);
                //}

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_MaeAlmacen_UbicacionData.EliminarUbicacionAlmacen(objAlmacenUbicacion);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento AlmacenUbicacionado que lista AlmacenUbicaciones.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las AlmacenUbicaciones</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static GenericoResponseDto ListaUbicacionesAlmacen_Combo(short idAlmacen)
        {
            try
            {
                GenericoResponseDto response;
                List<GenericoListaDto> listaUbicaciones;

                listaUbicaciones = COM_MaeAlmacen_UbicacionData.ListaUbicaciones_Combo(idAlmacen);

                response = new GenericoResponseDto
                {
                    ListaGenerica = listaUbicaciones
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento AlmacenUbicacionado que lista AlmacenUbicaciones.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las AlmacenUbicaciones</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static GenericoResponseDto ListaUbicaciones_Pisos_Combo(short idAlmacen)
        {
            try
            {
                GenericoResponseDto response;
                List<GenericoListaDto> listaUbicaciones;

                listaUbicaciones = COM_MaeAlmacen_UbicacionData.ListaUbicaciones_Pisos_Combo(idAlmacen);

                response = new GenericoResponseDto
                {
                    ListaGenerica = listaUbicaciones
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static GenericoResponseDto ListaUbicaciones_Racks_Combo(COM_MaeAlmacen_Ubicacion objUbicacion)
        {
            try
            {
                GenericoResponseDto response;
                List<GenericoListaDto> listaUbicaciones;

                listaUbicaciones = COM_MaeAlmacen_UbicacionData.ListaUbicaciones_Racks_Combo(objUbicacion);

                response = new GenericoResponseDto
                {
                    ListaGenerica = listaUbicaciones
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static GenericoResponseDto ListaUbicaciones_Combo_PorRack(COM_MaeAlmacen_Ubicacion objUbicacion)
        {
            try
            {
                GenericoResponseDto response;
                List<GenericoListaDto> listaUbicaciones;

                listaUbicaciones = COM_MaeAlmacen_UbicacionData.ListaUbicaciones_Combo_PorRack(objUbicacion);

                response = new GenericoResponseDto
                {
                    ListaGenerica = listaUbicaciones
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        #endregion
    }
}
