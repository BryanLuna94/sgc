﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Request;
using SistemaComercial.App.DataTypes.Response;
using SistemaComercial.App.Utility;
using System;
using System.Collections.Generic;
using SistemaComercial.App.DataAccess;
using SistemaComercial.App.DataTypes;
using System.Transactions;
using JBLV.Log;

namespace SistemaComercial.App.BusinessLayer
{
    public class PersonaContactoLogic
    {
        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static PersonaResponseDto ObtenerEditorPersonaContacto(int IdPersonaContacto = 0)
        {
            try
            {
                PersonaResponseDto response;
                COM_TabPersona_Contacto objPersonaContacto;

                objPersonaContacto = COM_TabPersona_ContactoData.ObtenerPersonaContacto(IdPersonaContacto);

                response = new PersonaResponseDto
                {
                    ContactoPersona = objPersonaContacto
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarPersonaContacto(PersonaRequestDto request)
        {
            COM_TabPersona_Contacto objPersonaContacto;
            int resultado;

            objPersonaContacto = request.ContactoPersona;

            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    resultado = COM_TabPersona_ContactoData.RegistrarPersonaContacto(objPersonaContacto);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return resultado;
        }

        /// <summary>Método que Actualiza maestro de Persona.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarPersonaContacto(PersonaRequestDto request)
        {
            COM_TabPersona_Contacto objPersonaContacto;
            COM_TabPersona_Contacto objPersonaContactoAnterior;

            objPersonaContacto = request.ContactoPersona;
            objPersonaContactoAnterior = COM_TabPersona_ContactoData.ObtenerPersonaContacto(objPersonaContacto.iCodPerCon);

            try
            {
                //if (objPersonaContacto.tiCodTipDocIde != objPersonaAnterior.tiCodTipDocIde && objPersona.vNroDocIde != objPersonaAnterior.vNroDocIde)
                //{
                //    if (COM_TabPersonaData.ValidaExiste(objPersona))
                //    {
                //        BusinessException.Generar(string.Format(Constantes.Mensajes.PERSONA_YA_EXISTE, objPersona.vNroDocIde));
                //    }
                //}

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_TabPersona_ContactoData.ActualizarPersonaContacto(objPersonaContacto);
                    tran.Complete();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>Método que elimina Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la Persona.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int EliminarPersonaContacto(PersonaRequestDto request)
        {
            COM_TabPersona_Contacto objPersonaContacto;
            COM_TabPersona_Contacto objPersonaContactoAnterior;

            objPersonaContacto = request.ContactoPersona;

            try
            {
                //if (COM_TabPersonaData.ValidaTieneProblemas(objPersona.siCodAlm))
                //{
                //    BusinessException.Generar(Constantes.Mensajes.Persona_TIENE_PROBLEMAS);
                //}

                objPersonaContactoAnterior = COM_TabPersona_ContactoData.ObtenerPersonaContacto(objPersonaContacto.iCodPerCon);

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_TabPersona_ContactoData.EliminarPersonaContacto(objPersonaContacto);
                    tran.Complete();
                }

                return objPersonaContactoAnterior.iCodPer;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }
    }
}
