﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Request;
using SistemaComercial.App.DataTypes.Response;
using SistemaComercial.App.Utility;
using System;
using System.Collections.Generic;
using SistemaComercial.App.DataAccess;
using SistemaComercial.App.DataTypes;
using System.Transactions;
using JBLV.Log;
using System.Linq;
using System.Data;

namespace SistemaComercial.App.BusinessLayer
{
    public class MovimientoLogic
    {
        #region CABECERA

        /// <summary>Invoca al Procedimiento Movimientoado que lista Movimientos.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Movimientos</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static MovimientoResponseDto ObtenerIndexMovimientos(MovimientoRequestDto request)
        {
            try
            {
                MovimientoResponseDto response;
                MovimientoFiltroDto objMovimientoFiltro;
                List<MovimientoListaDto> listaMovimientos;
                List<GenericoListaDto> listaTiposMov;
                List<GenericoListaDto> listaTiposDoc;
                List<GenericoListaDto> listaUsuarios;
                int codigoTablaTipoMovimiento;
                int codigoTablaTipoDocumento;

                objMovimientoFiltro = request.Filtro;
                listaMovimientos = COM_CabMovArtData.ListaMovimientos(objMovimientoFiltro);
                codigoTablaTipoMovimiento = Convert.ToInt32(Constantes.Tablas.TIPO_MOVIMIENTO);
                codigoTablaTipoDocumento = Convert.ToInt32(Constantes.Tablas.TIPO_DOCUMENTO);
                listaTiposMov = GEN_TabParametroData.ListaParametros(codigoTablaTipoMovimiento);
                listaTiposDoc = GEN_TabParametroData.ListaParametros(codigoTablaTipoDocumento);
                listaUsuarios = GEN_TabUsuarioData.ListaUsuarios_Combo();

                response = new MovimientoResponseDto
                {
                    ListaMovimientos = listaMovimientos,
                    ListaTiposMov = listaTiposMov,
                    ListaTiposDoc = listaTiposDoc,
                    ListaUsuarios = listaUsuarios
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Movimientoado que lista Movimientos.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Movimientos</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static MovimientoResponseDto ListaMovimientos(MovimientoRequestDto request)
        {
            try
            {
                MovimientoResponseDto response;
                MovimientoFiltroDto objMovimientoFiltro;
                List<MovimientoListaDto> listaMovimientos;

                objMovimientoFiltro = request.Filtro;
                listaMovimientos = COM_CabMovArtData.ListaMovimientos(objMovimientoFiltro);

                response = new MovimientoResponseDto
                {
                    ListaMovimientos = listaMovimientos
                };

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static MovimientoResponseDto ObtenerEditorMovimiento(int IdMovimiento = 0)
        {
            try
            {
                MovimientoResponseDto response;
                COM_CabMovArt objMovimiento;
                COM_CabMovArt_Ext objMovimientoExt;
                MovimientoListaDto objMovimientoDto;
                List<DetalleMovimientoListaDto> listaMovimientosDetalle;
                List<GenericoListaDto> listaTiposMov;
                List<GenericoListaDto> listaTiposDoc;
                List<GenericoListaDto> listaDirecciones;
                List<GenericoListaDto> listaEmpresas;
                List<GenericoListaDto> listaEmpresasPartner;
                List<GenericoListaDto> listaMonedas;
                List<GenericoListaDto> listaOficinas;
                int codigoTablaTipoMovimiento;
                int codigoTablaTipoDocumento;
                int codigoTablaMoneda;
                COM_CabGuia objGuia;
                COM_CabFactura objFactura;
                List<COM_Liquidacion> listaLiquidacion;

                codigoTablaTipoMovimiento = Convert.ToInt32(Constantes.Tablas.TIPO_MOVIMIENTO);
                codigoTablaTipoDocumento = Convert.ToInt32(Constantes.Tablas.TIPO_DOCUMENTO);
                codigoTablaMoneda = Convert.ToInt32(Constantes.Tablas.MONEDA);
                objMovimiento = COM_CabMovArtData.ObtenerMovimiento(IdMovimiento);
                objMovimientoExt = new COM_CabMovArt_Ext();
                listaMovimientosDetalle = new List<DetalleMovimientoListaDto>();
                listaDirecciones = new List<GenericoListaDto>();
                listaTiposMov = GEN_TabParametroData.ListaParametros(codigoTablaTipoMovimiento);
                listaTiposDoc = GEN_TabParametroData.ListaParametros(codigoTablaTipoDocumento);
                listaMonedas = GEN_TabParametroData.ListaParametros(codigoTablaMoneda);
                objMovimientoDto = COM_CabMovArtData.ListaMovimiento(IdMovimiento);
                listaEmpresas = COM_TabPersonaData.ListaEmpresas_Combo(new PersonaFiltroDto());
                listaEmpresasPartner = COM_TabPersonaData.ListaEmpresas_Combo(new PersonaFiltroDto { EsPartner = true });
                listaOficinas = GEN_OficinaData.ListaOficina_Combo();
                objGuia = new COM_CabGuia();
                objFactura = new COM_CabFactura();
                listaLiquidacion = new List<COM_Liquidacion>();

                if (objMovimiento != null)
                {
                    objMovimientoDto.FechaRecepcion = Funciones.Check.FechaCorta(objMovimiento.sdFechaHoraRecep);
                    objMovimientoDto.HoraRecepcion = objMovimiento.sdFechaHoraRecep.ToShortTimeString();
                    objGuia = COM_CabGuiaData.ObtenerGuia(IdMovimiento);
                    objFactura = COM_CabFacturaData.ObtenerFactura(IdMovimiento);
                    listaLiquidacion = COM_LiquidacionData.ListaLiquidacion_PorCab(IdMovimiento);
                }

                if (objMovimiento != null)
                {
                    objMovimientoExt = COM_CabMovArt_ExtData.ObtenerMovimientoExt_Mov(IdMovimiento);
                    if (objMovimientoExt != null)
                    {
                        if (objMovimientoExt.iCodPerOperador != null)
                        {
                            objMovimientoDto.PersonaExt = COM_TabPersonaData.ObtenerPersona((int)objMovimientoExt.iCodPerOperador).vNombreComercial;
                        }
                    }
                    listaDirecciones = COM_TabPersona_OficinaData.ListaPersonaOficina_Combo(objMovimiento.iCodPer);
                    listaMovimientosDetalle = COM_DetMovArtData.ListaDetalleMovimiento(IdMovimiento);
                }

                response = new MovimientoResponseDto
                {
                    MovimientoCabecera = objMovimiento,
                    MovimientoCabeceraDto = objMovimientoDto,
                    MovimientoCabecera_Ext = objMovimientoExt,
                    ListaMovimientoDetalle = listaMovimientosDetalle,
                    ListaTiposMov = listaTiposMov,
                    ListaTiposDoc = listaTiposDoc,
                    ListaDirecciones = listaDirecciones,
                    ListaEmpresas = listaEmpresas,
                    Factura = objFactura,
                    Guia = objGuia,
                    ListaEmpresasPartner = listaEmpresasPartner,
                    ListaLiquidacion = listaLiquidacion,
                    ListaMonedas = listaMonedas,
                    ListaOficinas = listaOficinas
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static MovimientoResponseDto RegistrarMovimiento(MovimientoRequestDto request)
        {
            MovimientoResponseDto response;
            COM_CabMovArt objMovimiento;
            MovimientoListaDto objMovimientoDto;
            COM_CabMovArt_Ext objMovimientoExt;
            COM_CabMovArt resultado;
            int nuevoId;
            string fechaHora;
            DateTime fechaValidar;
            DataTable dtListLiquidacion;
            decimal porcentajeImpuesto;

            porcentajeImpuesto = 18;
            objMovimiento = request.MovimientoCabecera;
            objMovimientoExt = request.MovimientoCabeceraExt;
            objMovimientoDto = request.MovimientoCabeceraDto;
            dtListLiquidacion = CreateColumnsDatatableLiquidacion();

            #region LLENANDO DATOS

            if (objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.IMPORTACION)
            {
                objMovimiento.siCodTipDoc = Convert.ToInt16(Constantes.Tablas.TipoDocumento.DUA);
                objMovimiento.vNumeroDoc = objMovimientoExt.vDUA;
                request.Factura.iCodPer = objMovimientoExt.iCodPartner;
                if (request.Guia != null) request.Guia.iCodPer = objMovimiento.iCodPer;
            }
            else if (objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.DEVOLUCION)
            {
                objMovimiento.siCodTipDoc = Convert.ToInt16(Constantes.Tablas.TipoDocumento.GUIAREMISION);
            }
            else if (objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.EXPORTACION)
            {
                objMovimiento.siCodTipDoc = Convert.ToInt16(Constantes.Tablas.TipoDocumento.FACTURA);
                objMovimiento.vNumeroDoc = $"{request.Factura.vSerie}-{request.Factura.vNumero}";
                request.Factura.iCodPer = objMovimiento.iCodPer;
            }
            else if (objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.DELIVERY)
            {
                objMovimiento.siCodTipDoc = Convert.ToInt16(Constantes.Tablas.TipoDocumento.GUIAREMISION);
                objMovimiento.vNumeroDoc = $"{request.Guia.vSerie}-{request.Guia.vNumero}";
            }
            else if (objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.VENTA)
            {
                objMovimiento.siCodTipDoc = Convert.ToInt16(request.Factura.iTipoDoc);
                request.Factura.iCodPer = objMovimiento.iCodPer;
                request.Guia.iCodPer = objMovimiento.iCodPer;
                objMovimiento.vNumeroDoc = $"{request.Factura.vSerie}-{request.Factura.vNumero}";
            }

            #endregion

            try
            {
                fechaHora = objMovimientoDto.FechaRecepcion + " " + objMovimientoDto.HoraRecepcion;

                if (!DateTime.TryParse(fechaHora, out fechaValidar))
                {
                    BusinessException.Generar("El formato de hora y/o fecha ingresada es incorrecto");
                }

                objMovimiento.sdFechaHoraRecep = Convert.ToDateTime(fechaHora);

                //if (COM_CabMovArtData.ValidaExiste(objMovimiento))
                //{
                //    BusinessException.Generar(string.Format(Constantes.Mensajes.MOVIMIENTO_YA_EXISTE));
                //}

                using (TransactionScope tran = new TransactionScope())
                {
                    nuevoId = COM_CabMovArtData.RegistrarMovimiento(objMovimiento);

                    if (objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.EXPORTACION
                        || objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.IMPORTACION
                        || objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.DELIVERY
                        || objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.DEVOLUCION)
                    {
                        objMovimientoExt.iCodCabMov = nuevoId;
                        COM_CabMovArt_ExtData.RegistrarActualizarMovimientoExt(objMovimientoExt);
                    }

                    #region FACTURA
                    if (request.Factura != null)
                    {
                        //obtener total de liquidacion
                        if (request.ListaLiquidacion != null)
                        {
                            decimal total, subTotal, impuesto;
                            subTotal = request.ListaLiquidacion.Sum(x => x.dCosto);
                            impuesto = subTotal * porcentajeImpuesto / 100;
                            total = subTotal + impuesto;

                            request.Factura.dSubTotal = subTotal;
                            request.Factura.dImpuesto = impuesto;
                            request.Factura.dTotal = total;
                        }
                        request.Factura.iCodCabMov = nuevoId;
                        COM_CabFacturaData.RegistrarFactura(request.Factura);
                    }
                    #endregion

                    //datos guia
                    if (request.Guia != null)
                    {
                        if (request.Guia.iCodPer > 0)
                        {
                            request.Guia.iCodCabMov = nuevoId;
                            COM_CabGuiaData.RegistrarGuia(request.Guia);
                        }
                    }

                    //liquidacion
                    if (request.ListaLiquidacion != null)
                    {
                        COM_LiquidacionData.EliminarLiquidacionPorCab(nuevoId);
                        foreach (var itemTemplate in request.ListaLiquidacion) dtListLiquidacion.Rows.Add(nuevoId, itemTemplate.vDescripcion, itemTemplate.dCosto);
                        COM_LiquidacionData.RegistrarLiquidacion(dtListLiquidacion);
                    }

                    tran.Complete();
                }

                resultado = COM_CabMovArtData.ObtenerMovimiento(nuevoId);

                response = new MovimientoResponseDto
                {
                    MovimientoCabecera = resultado
                };
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return response;
        }

        /// <summary>Método que Actualiza maestro de Movimiento.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarMovimiento(MovimientoRequestDto request)
        {
            COM_CabMovArt objMovimiento;
            COM_CabMovArt objMovimientoAnterior;
            MovimientoListaDto objMovimientoDto;
            COM_CabMovArt_Ext objMovimientoExt;
            string fechaHora;
            DateTime fechaValidar;
            DataTable dtListLiquidacion;
            decimal porcentajeImpuesto;

            porcentajeImpuesto = 18;
            objMovimiento = request.MovimientoCabecera;
            //objMovimientoAnterior = COM_CabMovArtData.ObtenerMovimiento(objMovimiento.iCodCabMov);
            objMovimientoExt = request.MovimientoCabeceraExt;
            objMovimientoDto = request.MovimientoCabeceraDto;
            dtListLiquidacion = CreateColumnsDatatableLiquidacion();

            #region LLENANDO DATOS

            if (objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.IMPORTACION)
            {
                objMovimiento.siCodTipDoc = Convert.ToInt16(Constantes.Tablas.TipoDocumento.DUA);
                objMovimiento.vNumeroDoc = objMovimientoExt.vDUA;
                request.Factura.iCodPer = objMovimientoExt.iCodPartner;
                if (request.Guia != null) request.Guia.iCodPer = objMovimiento.iCodPer;
            }
            else if (objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.DEVOLUCION)
            {
                objMovimiento.siCodTipDoc = Convert.ToInt16(Constantes.Tablas.TipoDocumento.GUIAREMISION);
            }
            else if (objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.EXPORTACION)
            {
                objMovimiento.siCodTipDoc = Convert.ToInt16(Constantes.Tablas.TipoDocumento.FACTURA);
                objMovimiento.vNumeroDoc = $"{request.Factura.vSerie}-{request.Factura.vNumero}";
                request.Factura.iCodPer = objMovimiento.iCodPer;
            }
            else if (objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.DELIVERY)
            {
                objMovimiento.siCodTipDoc = Convert.ToInt16(Constantes.Tablas.TipoDocumento.GUIAREMISION);
                objMovimiento.vNumeroDoc = $"{request.Guia.vSerie}-{request.Guia.vNumero}";
            }
            else if (objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.VENTA)
            {
                objMovimiento.siCodTipDoc = Convert.ToInt16(request.Factura.iTipoDoc);
                request.Factura.iCodPer = objMovimiento.iCodPer;
                request.Guia.iCodPer = objMovimiento.iCodPer;
                objMovimiento.vNumeroDoc = $"{request.Factura.vSerie}-{request.Factura.vNumero}";
            }

            #endregion

            try
            {
                fechaHora = objMovimientoDto.FechaRecepcion + " " + objMovimientoDto.HoraRecepcion;

                if (!DateTime.TryParse(fechaHora, out fechaValidar))
                {
                    BusinessException.Generar("El formato de hora y/o fecha ingresada es incorrecto");
                }

                //if (Funciones.Check.Cadena(objMovimiento.vSerieDoc).ToUpper() != Funciones.Check.Cadena(objMovimientoAnterior.vSerieDoc).ToUpper() ||
                //    objMovimiento.vNumeroDoc.ToUpper() != objMovimientoAnterior.vNumeroDoc.ToUpper() ||
                //    objMovimiento.siCodTipDoc != objMovimientoAnterior.siCodTipDoc ||
                //    objMovimiento.iCodPer != objMovimientoAnterior.iCodPer)
                //{
                //    if (COM_CabMovArtData.ValidaExiste(objMovimiento))
                //    {
                //        BusinessException.Generar(string.Format(Constantes.Mensajes.MOVIMIENTO_YA_EXISTE));
                //    }
                //}

                objMovimiento.sdFechaHoraRecep = Convert.ToDateTime(fechaHora);

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_CabMovArtData.ActualizarMovimiento(objMovimiento);

                    if (objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.EXPORTACION
                        || objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.IMPORTACION
                        || objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.DELIVERY
                        || objMovimiento.vTipoMov == Constantes.Tablas.TipoMov.DEVOLUCION)
                    {
                        objMovimientoExt.iCodCabMov = objMovimiento.iCodCabMov;
                        COM_CabMovArt_ExtData.RegistrarActualizarMovimientoExt(objMovimientoExt);
                    }
                    else
                    {
                        if (objMovimientoExt != null) COM_CabMovArt_ExtData.EliminarMovimientoExt(objMovimientoExt);
                    }

                    if (request.Guia != null)
                    {
                        if (request.Guia.iCodPer > 0)
                        {
                            request.Guia.iCodCabMov = objMovimiento.iCodCabMov;
                            CrearActualizarGuia(request.Guia);
                        }
                    }


                    #region FACTURA
                    if (request.Factura != null)
                    {
                        //obtener total de liquidacion
                        if (request.ListaLiquidacion != null)
                        {
                            decimal total, subTotal, impuesto;
                            subTotal = request.ListaLiquidacion.Sum(x => x.dCosto);
                            impuesto = subTotal * porcentajeImpuesto / 100;
                            total = subTotal + impuesto;

                            request.Factura.dSubTotal = subTotal;
                            request.Factura.dImpuesto = impuesto;
                            request.Factura.dTotal = total;
                        }
                        request.Factura.iCodCabMov = objMovimiento.iCodCabMov;
                        CrearActualizarFactura(request.Factura);
                    }
                    #endregion

                    //liquidacion
                    if (request.ListaLiquidacion != null)
                    {
                        COM_LiquidacionData.EliminarLiquidacionPorCab(objMovimiento.iCodCabMov);
                        foreach (var itemTemplate in request.ListaLiquidacion) dtListLiquidacion.Rows.Add(objMovimiento.iCodCabMov, itemTemplate.vDescripcion, itemTemplate.dCosto);
                        COM_LiquidacionData.RegistrarLiquidacion(dtListLiquidacion);
                    }


                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que elimina Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la Movimiento.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void CerrarMovimiento(MovimientoRequestDto request)
        {
            COM_CabMovArt objMovimiento;
            List<COM_DetMovArt> listaMovimientosDetalle;
            COM_TabStock objStock;
            COM_DetStock objDetalleStock;
            int codigoMovimientoAnterior;
            byte codigoTipoMovimientoIngreso;

            objMovimiento = request.MovimientoCabecera;

            try
            {
                objMovimiento = COM_CabMovArtData.ObtenerMovimiento(objMovimiento.iCodCabMov);
                listaMovimientosDetalle = COM_DetMovArtData.ListaDetalleMovimiento_PorCab(objMovimiento.iCodCabMov);
                codigoTipoMovimientoIngreso = Convert.ToByte(Constantes.Tablas.TipoMovimiento.INGRESO);

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_CabMovArtData.CerrarMovimiento(objMovimiento);
                    tran.Complete();
                }

                foreach (var item in listaMovimientosDetalle)
                {
                    //si es salida
                    if (objMovimiento.tiCodTipMov != codigoTipoMovimientoIngreso)
                    {
                        codigoMovimientoAnterior = (int)item.iCodDetMovIng;
                        objDetalleStock = COM_DetStockData.ObtenerDetalleStockPorMovDet(codigoMovimientoAnterior);
                        objStock = COM_TabStockData.ObtenerStock(objDetalleStock.iCodSto);
                        objStock.dCantidad = 1;

                        COM_TabStockData.DisminuirStock(objStock);
                        COM_DetStockData.QuitarDeStock(codigoMovimientoAnterior);
                    }
                    else //si es ingreso
                    {
                        objDetalleStock = COM_DetStockData.ObtenerDetalleStockPorMovDet(item.iCodDetMov);
                        objStock = COM_TabStockData.ObtenerStock(objDetalleStock.iCodSto);
                        objStock.dCantidad = 1;

                        COM_TabStockData.AumentarStock(objStock);
                        COM_DetStockData.PonerEnStock(item.iCodDetMov);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que elimina Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la Movimiento.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarMovimiento(MovimientoRequestDto request)
        {
            COM_CabMovArt objMovimiento;
            COM_CabMovArt objMovimientoAnterior;
            List<COM_DetMovArt> listaDetalle;
            byte tipoMovimientoIngreso;

            tipoMovimientoIngreso = Convert.ToByte(Constantes.Tablas.TipoMovimiento.INGRESO);
            objMovimiento = request.MovimientoCabecera;

            try
            {
                objMovimientoAnterior = COM_CabMovArtData.ObtenerMovimiento(objMovimiento.iCodCabMov);
                listaDetalle = COM_DetMovArtData.ListaDetalleMovimiento_PorCab(objMovimiento.iCodCabMov);

                if (objMovimientoAnterior.tiCodTipMov == tipoMovimientoIngreso)
                {
                    if (COM_CabMovArtData.ValidaSalioDetalle(objMovimiento.iCodCabMov))
                    {
                        BusinessException.Generar(Constantes.Mensajes.MOVIMIENTOCABECERA_SALIO);
                    }
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_CabMovArtData.EliminarMovimiento(objMovimiento);
                    tran.Complete();
                }

                foreach (var item in listaDetalle)
                {
                    EliminarMovimientoDetalle(item.iCodDetMov);
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Articuloado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static GenericoResponseDto ListaConceptosLiquidacion(string iCodPartner, string vTipoMov)
        {
            try
            {
                GenericoResponseDto response;
                List<GenericoListaDto> listaConceptosLiquidacion;

                listaConceptosLiquidacion = COM_ConceptosLiquidacionData.ListaConceptosLiquidacion_PorCab(Convert.ToInt32(iCodPartner), vTipoMov);

                response = new GenericoResponseDto
                {
                    ListaGenerica = listaConceptosLiquidacion
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Articuloado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static GenericoResponseDto ListaArticulosParaSalida_Auto(string filtro)
        {
            try
            {
                GenericoResponseDto response;
                List<GenericoListaDto> listaArticulos;

                listaArticulos = COM_DetMovArtData.ListaArticulosParaSalida_Auto(filtro);

                response = new GenericoResponseDto
                {
                    ListaGenerica = listaArticulos
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        #endregion

        #region DETALLE

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static MovimientoResponseDto ObtenerEditorMovimientoDetalle(int IdMovimiento, int IdMovimientoDetalle = 0)
        {
            try
            {
                MovimientoResponseDto response;
                COM_CabMovArt objMovimiento;
                COM_DetMovArt objMovimientoDetalle;
                COM_TabStock objStock;
                COM_DetStock objDetalleStock;
                COM_MaeUnidadMedida objUnidadMedida;
                DetalleMovimientoListaDto objMovimientoDetalleDto;
                List<GenericoListaDto> listaAlmacenes;
                List<GenericoListaDto> listaUbicaciones;
                List<GenericoListaDto> listaPisos;
                List<GenericoListaDto> listaRacks;
                List<GenericoListaDto> listaEstadosArticulos;
                COM_TabArticulo objArticulo;
                COM_MaeAlmacen_Ubicacion objUbicacion;
                int codigoTablaEstadoArticulo;
                byte codigoTipoMovimientoIngreso;
                string descripcionArticulo;
                string piso;
                string rack;

                codigoTipoMovimientoIngreso = Convert.ToByte(Constantes.Tablas.TipoMovimiento.INGRESO);
                objMovimiento = COM_CabMovArtData.ObtenerMovimiento(IdMovimiento);
                objMovimientoDetalle = COM_DetMovArtData.ObtenerDetalleMovimiento(IdMovimientoDetalle);
                listaAlmacenes = COM_MaeAlmacenData.ListaAlmacenes_Combo();
                codigoTablaEstadoArticulo = Convert.ToInt32(Constantes.Tablas.ESTADO_ARTICULO);
                listaEstadosArticulos = GEN_TabParametroData.ListaParametros(codigoTablaEstadoArticulo);
                objDetalleStock = new COM_DetStock();
                objStock = new COM_TabStock();
                objMovimientoDetalleDto = new DetalleMovimientoListaDto();
                listaPisos = new List<GenericoListaDto>();
                listaRacks = new List<GenericoListaDto>();
                listaUbicaciones = new List<GenericoListaDto>();
                piso = string.Empty;
                rack = string.Empty;

                if (IdMovimientoDetalle > 0)
                {
                    objArticulo = COM_TabArticuloData.ObtenerArticulo(objMovimientoDetalle.iCodArt);
                    objUnidadMedida = COM_MaeUnidadMedidaData.ObtenerUnidadMedida(objArticulo.siCodUMed);
                    descripcionArticulo = objArticulo.vCodigoOriginal + " - " + objArticulo.vDescripcion + " " + objUnidadMedida.vAbreviatura;
                    if (objMovimiento.tiCodTipMov == codigoTipoMovimientoIngreso)
                    {
                        objDetalleStock = COM_DetStockData.ObtenerDetalleStockPorMovDet(IdMovimientoDetalle);
                        objStock = COM_TabStockData.ObtenerStock(objDetalleStock.iCodSto);
                        objMovimientoDetalleDto = new DetalleMovimientoListaDto
                        {
                            Articulo = descripcionArticulo
                        };
                        objUbicacion = COM_MaeAlmacen_UbicacionData.ObtenerUbicacionAlmacen(objDetalleStock.iCodUbi);
                        listaPisos = COM_MaeAlmacen_UbicacionData.ListaUbicaciones_Pisos_Combo(objStock.siCodAlm);
                        listaRacks = COM_MaeAlmacen_UbicacionData.ListaUbicaciones_Racks_Combo(objUbicacion);
                        listaUbicaciones = COM_MaeAlmacen_UbicacionData.ListaUbicaciones_Combo_PorRack(objUbicacion);
                        piso = objUbicacion.tiPiso.ToString();
                        rack = objUbicacion.vRack;
                    }
                    else
                    {
                        objMovimientoDetalleDto = COM_DetMovArtData.ObtenerDetalleMovimientoParaSalida((int)objMovimientoDetalle.iCodDetMovIng);
                        descripcionArticulo += " " + objMovimientoDetalleDto.Serie;
                        objMovimientoDetalleDto.Articulo = descripcionArticulo;
                    }
                }

                response = new MovimientoResponseDto
                {
                    MovimientoCabecera = objMovimiento,
                    MovimientoDetalle = objMovimientoDetalle,
                    ListaAlmacenes = listaAlmacenes,
                    ListaEstadosArticulo = listaEstadosArticulos,
                    DetalleStock = objDetalleStock,
                    Stock = objStock,
                    MovimientoDetalleDto = objMovimientoDetalleDto,
                    ListaUbicaciones = listaUbicaciones,
                    ListaPisos = listaPisos,
                    ListaRacks = listaRacks,
                    Piso = piso,
                    Rack = rack
                };

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarMovimientoDetalle(MovimientoRequestDto request)
        {
            COM_DetMovArt objMovimientoDetalle;
            COM_DetMovArt objMovimientoDetalleAnterior;
            COM_CabMovArt objMovimiento;
            COM_TabStock objStock;
            COM_DetStock objDetalleStock;
            COM_DetStock objDetalleStockAnterior;
            int codigoStock;
            int resultado;
            int codigoMovimientoAnterior;
            byte codigoTipoMovimientoIngreso;

            objMovimiento = request.MovimientoCabecera;
            objMovimientoDetalle = request.MovimientoDetalle;
            objMovimientoDetalle.iCodCabMov = objMovimiento.iCodCabMov;
            objStock = request.CabeceraStock;
            objDetalleStock = request.DetalleStock;

            #region LLENAR DATOS

            objMovimiento = COM_CabMovArtData.ObtenerMovimiento(objMovimiento.iCodCabMov);
            if (objMovimiento.vTipoMov != Constantes.Tablas.TipoMov.EXPORTACION && 
                objMovimiento.vTipoMov != Constantes.Tablas.TipoMov.DELIVERY &&
                objMovimiento.vTipoMov != Constantes.Tablas.TipoMov.VENTA
                )
            {
                objDetalleStock.dCostoIngreso = objMovimientoDetalle.dCosto;
                objDetalleStock.dPesoKg = objMovimientoDetalle.dPeso;
            }

            #endregion

            codigoTipoMovimientoIngreso = Convert.ToByte(Constantes.Tablas.TipoMovimiento.INGRESO);

            try
            {
                if (COM_DetMovArtData.ValidaExiste(objMovimientoDetalle))
                {
                    BusinessException.Generar(string.Format(Constantes.Mensajes.MOVIMIENTODETALLE_YA_EXISTE));
                }

                objMovimiento = COM_CabMovArtData.ObtenerMovimiento(objMovimiento.iCodCabMov);

                //si es salida
                if (objMovimiento.tiCodTipMov != codigoTipoMovimientoIngreso)
                {
                    codigoMovimientoAnterior = (int)objMovimientoDetalle.iCodDetMovIng;
                    objMovimientoDetalleAnterior = COM_DetMovArtData.ObtenerDetalleMovimiento(codigoMovimientoAnterior);
                    objDetalleStockAnterior = COM_DetStockData.ObtenerDetalleStockPorMovDet(codigoMovimientoAnterior);
                    objMovimientoDetalle.iCodArt = objMovimientoDetalleAnterior.iCodArt;
                    objMovimientoDetalle.vSerie = objMovimientoDetalleAnterior.vSerie;

                    if (COM_DetMovArtData.ValidaEsDiferenteEstado(objMovimientoDetalle.iCodCabMov, objDetalleStockAnterior.iCodEstArt))
                    {
                        BusinessException.Generar(string.Format(Constantes.Mensajes.MOVIMIENTO_ESTADO_DIFERENTE));
                    }

                    objStock = new COM_TabStock
                    {
                        dCantidad = 1,
                        iCodSto = objDetalleStockAnterior.iCodSto
                    };

                    using (TransactionScope tran = new TransactionScope())
                    {
                        resultado = COM_DetMovArtData.RegistrarDetalleMovimiento(objMovimientoDetalle);

                        tran.Complete();
                    }
                }
                else //es ingreso
                {
                    objStock.dCantidad = 1;
                    objStock.iCodArt = objMovimientoDetalle.iCodArt;

                    codigoStock = COM_TabStockData.ObtenerStockPorArticuloAlmacen(objStock).iCodSto;
                    objStock.iCodSto = codigoStock;
                    objDetalleStock.iCodSto = codigoStock;
                    objDetalleStock.vSerie = objMovimientoDetalle.vSerie;

                    using (TransactionScope tran = new TransactionScope())
                    {
                        resultado = COM_DetMovArtData.RegistrarDetalleMovimiento(objMovimientoDetalle);

                        objDetalleStock.iCodDetMov = resultado;

                        COM_DetStockData.RegistrarDetalleStock(objDetalleStock);

                        tran.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return resultado;
        }

        /// <summary>Método que Actualiza maestro de Movimiento.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarMovimientoDetalle(MovimientoRequestDto request)
        {
            COM_DetMovArt objMovimientoDetalle;
            COM_DetMovArt objMovimientoDetalleAnterior;
            COM_DetMovArt objMovimientoDetalleIngreso;
            COM_CabMovArt objMovimiento;
            COM_TabStock objStock;
            COM_TabStock objStockAnterior;
            COM_DetStock objDetalleStock;
            COM_DetStock objDetalleStockAnterior;
            COM_DetStock objDetalleStockIngreso;
            int codigoStock;
            int codigoMovimientoAnterior;
            byte codigoTipoMovimientoIngreso;

            objMovimiento = request.MovimientoCabecera;
            objMovimientoDetalle = request.MovimientoDetalle;
            objMovimientoDetalle.iCodCabMov = objMovimiento.iCodCabMov;
            objStock = request.CabeceraStock;
            objDetalleStock = request.DetalleStock;

            #region LLENAR DATOS

            objMovimiento = COM_CabMovArtData.ObtenerMovimiento(objMovimiento.iCodCabMov);
            if (objMovimiento.vTipoMov != Constantes.Tablas.TipoMov.EXPORTACION &&
                objMovimiento.vTipoMov != Constantes.Tablas.TipoMov.DELIVERY &&
                objMovimiento.vTipoMov != Constantes.Tablas.TipoMov.VENTA
                )
            {
                objDetalleStock.dCostoIngreso = objMovimientoDetalle.dCosto;
                objDetalleStock.dPesoKg = objMovimientoDetalle.dPeso;
            }

            #endregion

            codigoTipoMovimientoIngreso = Convert.ToByte(Constantes.Tablas.TipoMovimiento.INGRESO);

            try
            {
                objMovimientoDetalleAnterior = COM_DetMovArtData.ObtenerDetalleMovimiento(objMovimientoDetalle.iCodDetMov);


                objMovimiento = COM_CabMovArtData.ObtenerMovimiento(objMovimiento.iCodCabMov);

                //si es salida
                if (objMovimiento.tiCodTipMov != codigoTipoMovimientoIngreso)
                {

                    if (objMovimientoDetalle.iCodDetMovIng != objMovimientoDetalleAnterior.iCodDetMovIng)
                    {
                        if (COM_DetMovArtData.ValidaExiste(objMovimientoDetalle))
                        {
                            BusinessException.Generar(string.Format(Constantes.Mensajes.MOVIMIENTODETALLE_YA_EXISTE));
                        }
                    }

                    codigoMovimientoAnterior = (int)objMovimientoDetalle.iCodDetMovIng;
                    objMovimientoDetalleIngreso = COM_DetMovArtData.ObtenerDetalleMovimiento(codigoMovimientoAnterior);
                    objDetalleStockIngreso = COM_DetStockData.ObtenerDetalleStockPorMovDet(codigoMovimientoAnterior);
                    objMovimientoDetalle.iCodArt = objMovimientoDetalleIngreso.iCodArt;
                    objMovimientoDetalle.vSerie = objMovimientoDetalleIngreso.vSerie;

                    if (objMovimientoDetalle.iCodDetMovIng != objMovimientoDetalleAnterior.iCodDetMovIng)
                    {
                        if (COM_DetMovArtData.ValidaEsDiferenteEstado(objMovimientoDetalle.iCodCabMov, objDetalleStockIngreso.iCodEstArt))
                        {
                            BusinessException.Generar(string.Format(Constantes.Mensajes.MOVIMIENTO_ESTADO_DIFERENTE));
                        }
                    }

                    objStock = new COM_TabStock
                    {
                        dCantidad = 1,
                        iCodSto = objDetalleStockIngreso.iCodSto
                    };

                    using (TransactionScope tran = new TransactionScope())
                    {
                        COM_DetMovArtData.ActualizarDetalleMovimiento(objMovimientoDetalle);

                        if (objMovimientoDetalle.iCodArt != objMovimientoDetalleAnterior.iCodArt)
                        {
                            if (objMovimiento.bCerrado)
                            {
                                objDetalleStockAnterior = COM_DetStockData.ObtenerDetalleStockPorMovDet(objMovimientoDetalleAnterior.iCodDetMov);
                                objStockAnterior = COM_TabStockData.ObtenerStock(objDetalleStockAnterior.iCodSto);

                                COM_TabStockData.AumentarStock(objStockAnterior);
                                COM_DetStockData.PonerEnStock(objMovimientoDetalleAnterior.iCodDetMov);

                                COM_TabStockData.DisminuirStock(objStock);
                                COM_DetStockData.QuitarDeStock(codigoMovimientoAnterior);
                            }
                        }

                        tran.Complete();
                    }
                }
                else
                {
                    if (objMovimientoDetalle.iCodArt != objMovimientoDetalleAnterior.iCodArt)
                    {
                        if (COM_DetMovArtData.ValidaExiste(objMovimientoDetalle))
                        {
                            BusinessException.Generar(string.Format(Constantes.Mensajes.MOVIMIENTODETALLE_YA_EXISTE));
                        }
                    }

                    objStock.dCantidad = 1;
                    objStock.iCodArt = objMovimientoDetalle.iCodArt;

                    codigoStock = COM_TabStockData.ObtenerStockPorArticuloAlmacen(objStock).iCodSto;
                    objStock.iCodSto = codigoStock;
                    objDetalleStock.iCodSto = codigoStock;
                    objDetalleStock.vSerie = objMovimientoDetalle.vSerie;

                    if (COM_DetMovArtData.ValidaSalio(objMovimientoDetalle.iCodDetMov))
                    {
                        BusinessException.Generar(Constantes.Mensajes.MOVIMIENTODETALLE_SALIO);
                    }

                    using (TransactionScope tran = new TransactionScope())
                    {
                        COM_DetMovArtData.ActualizarDetalleMovimiento(objMovimientoDetalle);

                        objDetalleStock.iCodDetMov = objMovimientoDetalle.iCodDetMov;
                        //si ha cambiado el artículo, regresarlo a su estado original
                        if (objMovimientoDetalle.iCodArt != objMovimientoDetalleAnterior.iCodArt)
                        {
                            if (objMovimiento.bCerrado)
                            {
                                objDetalleStockAnterior = COM_DetStockData.ObtenerDetalleStockPorMovDet(objMovimientoDetalleAnterior.iCodDetMov);
                                objStockAnterior = COM_TabStockData.ObtenerStock(objDetalleStockAnterior.iCodSto);

                                COM_TabStockData.DisminuirStock(objStockAnterior);
                                COM_TabStockData.AumentarStock(objStock);
                            }
                            COM_DetStockData.Eliminar(objMovimientoDetalleAnterior.iCodDetMov);
                            COM_DetStockData.RegistrarDetalleStock(objDetalleStock);

                        }
                        else
                        {
                            COM_DetStockData.Eliminar(objMovimientoDetalleAnterior.iCodDetMov);
                            COM_DetStockData.RegistrarDetalleStock(objDetalleStock);
                        }

                        tran.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int EliminarMovimientoDetalle(int idMovimientoDetalle)
        {
            COM_DetMovArt objMovimientoDetalle;
            COM_CabMovArt objMovimiento;
            COM_TabStock objStock;
            COM_DetStock objDetalleStockAnterior;
            byte codigoTipoMovimientoIngreso;

            codigoTipoMovimientoIngreso = Convert.ToByte(Constantes.Tablas.TipoMovimiento.INGRESO);

            try
            {
                objMovimientoDetalle = COM_DetMovArtData.ObtenerDetalleMovimiento(idMovimientoDetalle);
                objMovimiento = COM_CabMovArtData.ObtenerMovimiento(objMovimientoDetalle.iCodCabMov);

                //si es salida
                if (objMovimiento.tiCodTipMov != codigoTipoMovimientoIngreso)
                {

                    objDetalleStockAnterior = COM_DetStockData.ObtenerDetalleStockPorMovDet((int)objMovimientoDetalle.iCodDetMovIng);

                    objStock = new COM_TabStock
                    {
                        dCantidad = 1,
                        iCodSto = objDetalleStockAnterior.iCodSto
                    };

                    using (TransactionScope tran = new TransactionScope())
                    {
                        COM_DetMovArtData.EliminarDetalleMovimiento(objMovimientoDetalle);

                        if (objMovimiento.bCerrado)
                        {
                            COM_TabStockData.AumentarStock(objStock);
                            COM_DetStockData.PonerEnStock((int)objMovimientoDetalle.iCodDetMovIng);
                        }

                        tran.Complete();
                    }
                }
                else
                {
                    if (COM_DetMovArtData.ValidaSalio(idMovimientoDetalle))
                    {
                        BusinessException.Generar(Constantes.Mensajes.MOVIMIENTODETALLE_SALIO);
                    }

                    objDetalleStockAnterior = COM_DetStockData.ObtenerDetalleStockPorMovDet(idMovimientoDetalle);

                    objStock = new COM_TabStock
                    {
                        dCantidad = 1,
                        iCodSto = objDetalleStockAnterior.iCodSto
                    };

                    using (TransactionScope tran = new TransactionScope())
                    {
                        COM_DetMovArtData.EliminarDetalleMovimiento(objMovimientoDetalle);

                        if (objMovimiento.bCerrado)
                        {
                            COM_TabStockData.DisminuirStock(objStock);
                            COM_DetStockData.Eliminar(idMovimientoDetalle);
                        }

                        tran.Complete();
                    }
                }


            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return objMovimiento.iCodCabMov;
        }

        /// <summary>Invoca al Procedimiento DetalleMovimientoado que lista Descripcion Base.</summary>
        /// <param name="idDetalleMovimiento">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static MovimientoResponseDto ObtenerDetalleMovimientoParaSalida(int idDetalleMovimiento)
        {
            try
            {
                MovimientoResponseDto response;
                DetalleMovimientoListaDto objDetalle;

                objDetalle = COM_DetMovArtData.ObtenerDetalleMovimientoParaSalida(idDetalleMovimiento);

                response = new MovimientoResponseDto
                {
                    MovimientoDetalleDto = objDetalle
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        #endregion

        #region REPORTES

        public static GuiaRemisionResponseDto ObtenerGuiaRemision(int idMovimiento)
        {
            try
            {
                GuiaRemisionResponseDto response;
                ReporteGuiaRemCabListaDto objGuiaRemCab;
                List<ReporteGuiaRemDetListaDto> listaGuiaRemDet;
                string marca;
                string puntoPartida;
                string estadoArticulo;
                decimal peso;
                string motivoTraslado;
                string origen;
                string destino;
                COM_MaeAlmacen objAlmacen;

                objGuiaRemCab = COM_CabMovArtData.ObtenerReporteGuiaRemCabecera(idMovimiento);
                listaGuiaRemDet = COM_CabMovArtData.ListaReporteGuiaRemDetalle(idMovimiento);

                objAlmacen = COM_MaeAlmacenData.ObtenerAlmacen(1);
                destino = "PERU";
                origen = objAlmacen.vDescripcion;
                motivoTraslado = "TRASLADO DE EQUIPOS";
                puntoPartida = objAlmacen.vDireccion;
                estadoArticulo = listaGuiaRemDet.FirstOrDefault().Estado;
                marca = listaGuiaRemDet.FirstOrDefault().Marca;
                peso = 0;
                foreach (var item in listaGuiaRemDet) peso += item.Peso;

                objGuiaRemCab.Destino = destino;
                objGuiaRemCab.Origen = origen;
                objGuiaRemCab.MotivoTraslado = motivoTraslado;
                objGuiaRemCab.DireccionPartida = puntoPartida;
                objGuiaRemCab.Estado = estadoArticulo;
                objGuiaRemCab.Peso = peso.ToString("#.#0");
                objGuiaRemCab.Marca = marca;

                response = new GuiaRemisionResponseDto
                {
                    GuiaRemisionCab = objGuiaRemCab,
                    ListaGuiaRemisionDet = listaGuiaRemDet
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        public static ReporteAlmacenesValorizadoResumidoResponseDto ObtenerIndexReporteAlmacenesValorizadosResumido()
        {
            try
            {
                ReporteAlmacenesValorizadoResumidoResponseDto response;
                List<GenericoListaDto> listaMarcas;

                listaMarcas = COM_MaeMarcaData.ListaMarca_Combo();

                response = new ReporteAlmacenesValorizadoResumidoResponseDto
                {
                    ListaMarcas = listaMarcas
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        public static ReporteAlmacenesValorizadoResumidoResponseDto ListaReporteAlmacenesValorizadosResumido(ReporteAlmacenesValorizadosResumidoRequestDto request)
        {
            try
            {
                ReporteAlmacenesValorizadoResumidoResponseDto response;
                ReporteAlmacenesValorizadoResumidoFiltroDto objFiltro;
                List<ReporteAlmacenesValorizadoResumidoListaDto> listaReporte;

                objFiltro = request.Filtro;
                listaReporte = COM_CabMovArtData.ListaReporteAlmacenValorizadoResumido(objFiltro);

                response = new ReporteAlmacenesValorizadoResumidoResponseDto
                {
                    ListaAlmacenesValorizados = listaReporte
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        public static ReporteAlmacenesValorizadoResumidoResponseDto ListaReporteAlmacenesValorizadosDetallado(ReporteAlmacenesValorizadosResumidoRequestDto request)
        {
            try
            {
                ReporteAlmacenesValorizadoResumidoResponseDto response;
                ReporteAlmacenesValorizadoResumidoFiltroDto objFiltro;
                List<ReporteAlmacenesValorizadoDetalladoListaDto> listaReporte;

                objFiltro = request.Filtro;
                listaReporte = COM_CabMovArtData.ListaReporteAlmacenValorizadoDetallado(objFiltro);

                response = new ReporteAlmacenesValorizadoResumidoResponseDto
                {
                    ListaAlmacenesValorizadosDet = listaReporte
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        public static ReporteKardexResponseDto ObtenerIndexReporteKardex()
        {
            try
            {
                ReporteKardexResponseDto response;
                List<GenericoListaDto> listaAlmacenes;
                List<GenericoListaDto> listaEmpresas;
                List<GenericoListaDto> listaMarcas;
                List<GenericoListaDto> listaMeses;

                listaAlmacenes = COM_MaeAlmacenData.ListaAlmacenes_Combo();
                listaMarcas = COM_MaeMarcaData.ListaMarca_Combo();
                listaEmpresas = COM_TabPersonaData.ListaEmpresas_Combo(new PersonaFiltroDto());

                listaMeses = new List<GenericoListaDto>();
                foreach (var item in Funciones.ListaMeses()) listaMeses.Add(new GenericoListaDto { Codigo = item.Codigo, Descripcion = item.Descripcion });

                response = new ReporteKardexResponseDto
                {
                    ListaMarcas = listaMarcas,
                    ListaAlmacenes = listaAlmacenes,
                    ListaEmpresas = listaEmpresas,
                    ListaMeses = listaMeses
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        public static ReporteKardexResponseDto ListaReporteKardex(ReporteKardexRequestDto request)
        {
            try
            {
                ReporteKardexResponseDto response;
                List<ReporteKardexListaDto> listaKardex;
                ReporteKardexFiltroDto objFiltro;

                objFiltro = request.Filtro;
                listaKardex = COM_CabMovArtData.ListaReporteKardex(objFiltro);

                response = new ReporteKardexResponseDto
                {
                    ListaKardex = listaKardex
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        #endregion

        #region LIQUIDACION

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static MovimientoLiquidacionResponseDto ObtenerEditorMovimientoLiquidacion(int IdLiquidacion = 0)
        {
            try
            {
                MovimientoLiquidacionResponseDto response;
                COM_Liquidacion objLiquidacion;

                objLiquidacion = COM_LiquidacionData.ObtenerLiquidacion(IdLiquidacion);

                response = new MovimientoLiquidacionResponseDto
                {
                    Liquidacion = objLiquidacion
                };

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static DataTable CreateColumnsDatatableLiquidacion()
        {
            DataTable dt = new DataTable();
            //Add columns  
            dt.Columns.Add(new DataColumn("iCodCabMov", typeof(int)));
            dt.Columns.Add(new DataColumn("vDescripcion", typeof(string)));
            dt.Columns.Add(new DataColumn("dCosto", typeof(decimal)));

            return dt;
        }

        #endregion

        private static int CrearActualizarFactura(COM_CabFactura objFactura)
        {
            var existe = COM_CabFacturaData.ObtenerFactura(objFactura.iCodCabMov);
            if (existe == null)
            {
                return COM_CabFacturaData.RegistrarFactura(objFactura);
            }
            else
            {
                COM_CabFacturaData.ActualizarFactura(objFactura);
                return existe.iCodCabMov;
            }
        }

        private static int CrearActualizarGuia(COM_CabGuia objGuia)
        {
            var existe = COM_CabGuiaData.ObtenerGuia(objGuia.iCodCabMov);
            if (existe == null)
            {
                return COM_CabGuiaData.RegistrarGuia(objGuia);
            }
            else
            {
                COM_CabGuiaData.ActualizarGuia(objGuia);
                return existe.iCodCabMov;
            }
        }

    }
}
