﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Request;
using SistemaComercial.App.DataTypes.Response;
using SistemaComercial.App.Utility;
using System;
using System.Collections.Generic;
using SistemaComercial.App.DataAccess;
using SistemaComercial.App.DataTypes;
using System.Transactions;
using JBLV.Log;

namespace SistemaComercial.App.BusinessLayer
{
    public class PaisLogic
    {
        /// <summary>Invoca al Procedimiento Personaado que lista Personas.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Personas</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static GenericoResponseDto ListaPaises(string filtro)
        {
            try
            {
                GenericoResponseDto response;
                List<GenericoListaDto> listaPaises;

                listaPaises = GEN_MaePais.ListaPaises_Autocomplete(filtro);

                response = new GenericoResponseDto
                {
                    ListaGenerica = listaPaises
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }
    }
}
