﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Request;
using SistemaComercial.App.DataTypes.Response;
using SistemaComercial.App.Utility;
using System;
using System.Collections.Generic;
using SistemaComercial.App.DataAccess;
using SistemaComercial.App.DataTypes;
using System.Transactions;
using JBLV.Log;

namespace SistemaComercial.App.BusinessLayer
{
    public class UnidadMedidaLogic
    {
        /// <summary>Invoca al Procedimiento UnidadMedidaado que lista UnidadMedida.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las UnidadMedida</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static UnidadMedidaResponseDto ObtenerIndexUnidadMedida(UnidadMedidaRequestDto request)
        {
            try
            {
                UnidadMedidaResponseDto response;
                UnidadMedidaFiltroDto objUnidadMedidaFiltro;
                List<UnidadMedidaListaDto> listaUnidadMedida;

                objUnidadMedidaFiltro = request.Filtro;
                listaUnidadMedida = COM_MaeUnidadMedidaData.ListaUnidadMedida(objUnidadMedidaFiltro);

                response = new UnidadMedidaResponseDto
                {
                    ListaUnidadesMedida = listaUnidadMedida
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento UnidadMedidaado que lista UnidadMedida.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las UnidadMedida</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static UnidadMedidaResponseDto ListaUnidadMedida(UnidadMedidaRequestDto request)
        {
            try
            {
                UnidadMedidaResponseDto response;
                UnidadMedidaFiltroDto objUnidadMedidaFiltro;
                List<UnidadMedidaListaDto> listaUnidadMedida;

                objUnidadMedidaFiltro = request.Filtro;
                listaUnidadMedida = COM_MaeUnidadMedidaData.ListaUnidadMedida(objUnidadMedidaFiltro);

                response = new UnidadMedidaResponseDto
                {
                    ListaUnidadesMedida = listaUnidadMedida
                };

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>Método que registra UnidadMedida.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static UnidadMedidaResponseDto ObtenerEditorUnidadMedida(short IdUnidadMedida = 0)
        {
            try
            {
                UnidadMedidaResponseDto response;
                COM_MaeUnidadMedida objUnidadMedida;

                objUnidadMedida = COM_MaeUnidadMedidaData.ObtenerUnidadMedida(IdUnidadMedida);

                response = new UnidadMedidaResponseDto
                {
                    UnidadMedida = objUnidadMedida
                };

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra UnidadMedida.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarUnidadMedida(UnidadMedidaRequestDto request)
        {
            COM_MaeUnidadMedida objUnidadMedida;
            int resultado;

            objUnidadMedida = request.UnidadMedida;

            try
            {
                if (COM_MaeUnidadMedidaData.ValidaExiste(objUnidadMedida))
                {
                    BusinessException.Generar(string.Format(Constantes.Mensajes.UNIDADMEDIDA_YA_EXISTE, objUnidadMedida.vDescripcion));
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    resultado = COM_MaeUnidadMedidaData.RegistrarUnidadMedida(objUnidadMedida);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return resultado;
        }

        /// <summary>Método que Actualiza maestro de UnidadMedida.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarUnidadMedida(UnidadMedidaRequestDto request)
        {
            COM_MaeUnidadMedida objUnidadMedida;
            COM_MaeUnidadMedida objUnidadMedidaAnterior;

            objUnidadMedida = request.UnidadMedida;
            objUnidadMedidaAnterior = COM_MaeUnidadMedidaData.ObtenerUnidadMedida(objUnidadMedida.siCodUMed);

            try
            {
                if (objUnidadMedida.vDescripcion.ToUpper() != objUnidadMedidaAnterior.vDescripcion.ToUpper())
                {
                    if (COM_MaeUnidadMedidaData.ValidaExiste(objUnidadMedida))
                    {
                        BusinessException.Generar(string.Format(Constantes.Mensajes.UNIDADMEDIDA_YA_EXISTE, objUnidadMedida.vDescripcion));
                    }
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_MaeUnidadMedidaData.ActualizarUnidadMedida(objUnidadMedida);
                    tran.Complete();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>Método que elimina UnidadMedida.</summary>
        /// <param name="objPersona">Entidad con los datos de la UnidadMedida.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarUnidadMedida(UnidadMedidaRequestDto request)
        {
            COM_MaeUnidadMedida objUnidadMedida;

            objUnidadMedida = request.UnidadMedida;

            try
            {
                //if (COM_MaeUnidadMedidaData.ValidaTieneProblemas(objUnidadMedida.siCodUMed))
                //{
                //    BusinessException.Generar(Constantes.Mensajes.UnidadMedida_TIENE_PROBLEMAS);
                //}

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_MaeUnidadMedidaData.EliminarUnidadMedida(objUnidadMedida);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }
    }
}
