﻿using System.ServiceModel;
using SistemaComercial.App.DataTypes.Response;

namespace SistemaComercial.App.BusinessLayer
{
    public class BusinessException
    {
        public static FaultException<ServiceErrorResponseType> Generar(string strMensaje)
        {
            dynamic serviceErrorResponse = new ServiceErrorResponseType();
            serviceErrorResponse.Message = strMensaje;
            throw new FaultException<ServiceErrorResponseType>(serviceErrorResponse, new FaultReason("Advertencia del lado del servidor"));
        }
    }
}
