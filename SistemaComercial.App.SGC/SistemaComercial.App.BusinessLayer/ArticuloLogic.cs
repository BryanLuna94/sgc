﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Request;
using SistemaComercial.App.DataTypes.Response;
using SistemaComercial.App.Utility;
using System;
using System.Collections.Generic;
using SistemaComercial.App.DataAccess;
using SistemaComercial.App.DataTypes;
using System.Transactions;
using JBLV.Log;

namespace SistemaComercial.App.BusinessLayer
{
    public class ArticuloLogic
    {
        /// <summary>Invoca al Procedimiento Articuloado que lista Articulos.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Articulos</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static ArticuloResponseDto ObtenerIndexArticulos(ArticuloRequestDto request)
        {
            try
            {
                ArticuloResponseDto response;
                ArticuloFiltroDto objArticuloFiltro;
                List<ArticuloListaDto> listaArticulos;

                objArticuloFiltro = request.Filtro;
                listaArticulos = COM_TabArticuloData.ListaArticulos(objArticuloFiltro);

                response = new ArticuloResponseDto
                {
                    ListaArticulos = listaArticulos
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Articuloado que lista Articulos.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Articulos</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static ArticuloResponseDto ListaArticulos(ArticuloRequestDto request)
        {
            try
            {
                ArticuloResponseDto response;
                ArticuloFiltroDto objArticuloFiltro;
                List<ArticuloListaDto> listaArticulos;

                objArticuloFiltro = request.Filtro;
                listaArticulos = COM_TabArticuloData.ListaArticulos(objArticuloFiltro);

                response = new ArticuloResponseDto
                {
                    ListaArticulos = listaArticulos
                };

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>Método que registra Articulos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static ArticuloResponseDto ObtenerEditorArticulo(int IdArticulo = 0)
        {
            try
            {
                ArticuloResponseDto response;
                COM_TabArticulo objArticulo;
                List<GenericoListaDto> listaUnidadesMedida;
                List<GenericoListaDto> listaMarcas;

                objArticulo = COM_TabArticuloData.ObtenerArticulo(IdArticulo);
                listaUnidadesMedida = COM_MaeUnidadMedidaData.ListaUnidadMedida_Combo();
                listaMarcas = COM_MaeMarcaData.ListaMarca_Combo();

                response = new ArticuloResponseDto
                {
                    Articulo = objArticulo,
                    ListaUnidadesMedida = listaUnidadesMedida,
                    ListaMarcas = listaMarcas
                };

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Articulos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarArticulo(ArticuloRequestDto request)
        {
            COM_TabArticulo objArticulo;
            COM_TabStock objStock;
            List<GenericoListaDto> listaAlmacenes;
            int resultado;

            objArticulo = request.Articulo;

            try
            {
                if (COM_TabArticuloData.ValidaExiste(objArticulo))
                {
                    BusinessException.Generar(string.Format(Constantes.Mensajes.ARTICULO_YA_EXISTE, objArticulo.vDescripcion));
                }

                listaAlmacenes = COM_MaeAlmacenData.ListaAlmacenes_Combo();

                using (TransactionScope tran = new TransactionScope())
                {
                    resultado = COM_TabArticuloData.RegistrarArticulo(objArticulo);

                    foreach (var item in listaAlmacenes)
                    {
                        objStock = new COM_TabStock
                        {
                            dCantidad = 0,
                            iCodArt = resultado,
                            siCodAlm = Convert.ToInt16(item.Codigo)
                        };
                        COM_TabStockData.RegistrarStock(objStock);
                    }

                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return resultado;
        }

        /// <summary>Método que Actualiza maestro de Articulo.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarArticulo(ArticuloRequestDto request)
        {
            COM_TabArticulo objArticulo;
            COM_TabArticulo objArticuloAnterior;

            objArticulo = request.Articulo;
            objArticuloAnterior = COM_TabArticuloData.ObtenerArticulo(objArticulo.iCodArt);

            try
            {
                if (COM_TabArticuloData.ValidaTieneMovimientos(objArticulo.iCodArt))
                {
                    BusinessException.Generar(Constantes.Mensajes.ARTICULO_TIENEMOVIMIENTOS);
                }

                if (objArticulo.vDescripcion.ToUpper() != objArticuloAnterior.vDescripcion.ToUpper())
                {
                    if (COM_TabArticuloData.ValidaExiste(objArticulo))
                    {
                        BusinessException.Generar(string.Format(Constantes.Mensajes.ARTICULO_YA_EXISTE, objArticulo.vDescripcion));
                    }
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_TabArticuloData.ActualizarArticulo(objArticulo);
                    tran.Complete();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>Método que elimina Articulos.</summary>
        /// <param name="objPersona">Entidad con los datos de la Articulo.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarArticulo(ArticuloRequestDto request)
        {
            COM_TabArticulo objArticulo;

            objArticulo = request.Articulo;

            try
            {
                if (COM_TabArticuloData.ValidaTieneMovimientos(objArticulo.iCodArt))
                {
                    BusinessException.Generar(Constantes.Mensajes.ARTICULO_TIENEMOVIMIENTOS);
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_TabArticuloData.EliminarArticulo(objArticulo);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Articuloado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static GenericoResponseDto ListaArticulos_Auto(string filtro)
        {
            try
            {
                GenericoResponseDto response;
                List<GenericoListaDto> listaUbicaciones;

                listaUbicaciones = COM_TabArticuloData.ListaArticulos_Auto(filtro);

                response = new GenericoResponseDto
                {
                    ListaGenerica = listaUbicaciones
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        
    }
}
