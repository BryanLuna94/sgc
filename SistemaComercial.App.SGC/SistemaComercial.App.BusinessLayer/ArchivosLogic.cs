﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Request;
using SistemaComercial.App.DataTypes.Response;
using SistemaComercial.App.Utility;
using System;
using System.Collections.Generic;
using SistemaComercial.App.DataAccess;
using SistemaComercial.App.DataTypes;
using System.Transactions;
using JBLV.Log;
using System.Linq;

namespace SistemaComercial.App.BusinessLayer
{
    public class ArchivosLogic
    {
        /// <summary>Método que registra MaestroStopees.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Castañeda</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void GuardarArchivo(ArchivosRequestDto request)
        {
            try
            {
                GEN_MaeArchivo objArchivo;

                objArchivo = request.Archivo;

                using (TransactionScope tran = new TransactionScope())
                {
                    GEN_MaeArchivoData.GuardarArchivo(objArchivo);

                    tran.Complete();
                }
            }
            catch(Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que registra MaestroStopees.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Castañeda</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int EliminarArchivo(string id)
        {
            try
            {
                ArchivoListaDto objArchivo;

                objArchivo = GEN_MaeArchivoData.ObtenerArchivoPorId(id);

                using (TransactionScope tran = new TransactionScope())
                {
                    GEN_MaeArchivoData.EliminarArchivo(id);

                    tran.Complete();
                }

                return Convert.ToInt32(objArchivo.Codigo);
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static ArchivosResponseDto ListadoArchivos(string tabla, int id)
        {
            try
            {
                ArchivosResponseDto response;
                List<ArchivoListaDto> listaArchivos;

                listaArchivos = GEN_MaeArchivoData.ListarArchivos(tabla, id.ToString());

                response = new ArchivosResponseDto
                {
                    ListaArchivos = listaArchivos
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static ArchivosResponseDto ObtenerArchivoPorId(string id)
        {
            try
            {
                ArchivosResponseDto response;
                ArchivoListaDto objArchivo;

                objArchivo = GEN_MaeArchivoData.ObtenerArchivoPorId(id);

                response = new ArchivosResponseDto
                {
                    Archivo = objArchivo
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }
    }
}
