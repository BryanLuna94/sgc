﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Request;
using SistemaComercial.App.DataTypes.Response;
using SistemaComercial.App.Utility;
using System;
using System.Collections.Generic;
using SistemaComercial.App.DataAccess;
using SistemaComercial.App.DataTypes;
using System.Transactions;
using JBLV.Log;

namespace SistemaComercial.App.BusinessLayer
{
    public class UsuarioLogic
    {
        /// <summary>Invoca al Procedimiento almacenado que lista Usuarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Usuarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>19/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static LoginResponseDto Login(LoginRequestDto request)
        {
            try
            {
                LoginResponseDto response;
                LoginListaDto objLogin;
                ArchivoListaDto objArchivo;
                List<OpcionListaDto> listaOpcionesPerfil;
                string usuario;
                string clave;

                usuario = request.Usuario;
                clave = request.Clave;
                objLogin = GEN_TabUsuarioData.Login(usuario);

                if (objLogin == null)
                {
                    BusinessException.Generar(Constantes.Mensajes.USUARIO_NO_EXISTE);
                }

                if (objLogin.Clave != Funciones.Encriptar(clave.ToUpper()))
                {
                    BusinessException.Generar(Constantes.Mensajes.CLAVE_USUARIO_INCORRECTA);
                }

                objArchivo = GEN_MaeArchivoData.ObtenerArchivo("USUARIO", objLogin.Codigo.ToString());
                listaOpcionesPerfil = GEN_TabOpcionData.ListaOpcionesPorPerfil(objLogin.CodigoPerfil);

                response = new LoginResponseDto
                {
                    ListaOpcionesPorPerfil = listaOpcionesPerfil,
                    Usuario = objLogin,
                    FotoUsuario = objArchivo,
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Usuarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Usuarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>19/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static UsuarioResponseDto ObtenerIndexUsuarios(UsuarioRequestDto request)
        {
            try
            {
                UsuarioResponseDto response;
                UsuarioFiltroDto objUsuarioFiltro;
                List<UsuarioListaDto> listaUsuarios;

                objUsuarioFiltro = request.Filtro;
                listaUsuarios = GEN_TabUsuarioData.ListaUsuarios(objUsuarioFiltro);

                response = new UsuarioResponseDto
                {
                    ListaUsuarios = listaUsuarios
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Usuarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Usuarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>19/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static UsuarioResponseDto ListaUsuarios(UsuarioRequestDto request)
        {
            try
            {
                UsuarioResponseDto response;
                UsuarioFiltroDto objUsuarioFiltro;
                List<UsuarioListaDto> listaUsuarios;

                objUsuarioFiltro = request.Filtro;
                listaUsuarios = GEN_TabUsuarioData.ListaUsuarios(objUsuarioFiltro);

                response = new UsuarioResponseDto
                {
                    ListaUsuarios = listaUsuarios
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que registra Usuarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static UsuarioResponseDto ObtenerEditorUsuario(short IdUsuario = 0)
        {
            try
            {
                UsuarioResponseDto response;
                GEN_TabUsuario objUsuario;
                ArchivoListaDto objArchivo;
                List<GenericoListaDto> listaPerfiles;

                objUsuario = GEN_TabUsuarioData.ObtenerUsuario(IdUsuario);
                objArchivo = GEN_MaeArchivoData.ObtenerArchivo("USUARIO", IdUsuario.ToString());
                listaPerfiles = GEN_MaePerfilData.ListaPerfiles_Combo();

                response = new UsuarioResponseDto
                {
                    Usuario = objUsuario,
                    ListaPerfiles = listaPerfiles,
                    Archivo = objArchivo
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que registra Usuarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarUsuario(UsuarioRequestDto request)
        {
            GEN_TabUsuario objUsuario;
            GEN_MaeArchivo objArchivo;
            byte[] archivo;
            int resultado;

            objUsuario = request.Usuario;
            archivo = request.Archivo;

            try
            {
                if (GEN_TabUsuarioData.ValidaExiste(objUsuario))
                {
                    BusinessException.Generar(Constantes.Mensajes.NOMBREUSUARIO_YA_EXISTE);
                }

                if (GEN_TabUsuarioData.ValidaExisteNombreUsuario(objUsuario))
                {
                    BusinessException.Generar(Constantes.Mensajes.USUARIO_YA_EXISTE);
                }

                objArchivo = new GEN_MaeArchivo
                {
                    Archivo = archivo,
                    Codigo = objUsuario.siCodUsu.ToString(),
                    Tabla = "USUARIO"
                };

                objUsuario.vClave = Funciones.Encriptar(objUsuario.vClave);

                using (TransactionScope tran = new TransactionScope())
                {
                    resultado = GEN_TabUsuarioData.RegistrarUsuario(objUsuario);
                    if (archivo != null)
                    {
                        objArchivo.Codigo = resultado.ToString();
                        GEN_MaeArchivoData.GuardarArchivo(objArchivo);
                    }

                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return resultado;
        }

        /// <summary>Método que Actualiza maestro de Usuario.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int ActualizarUsuario(UsuarioRequestDto request)
        {
            GEN_TabUsuario objUsuario;
            GEN_MaeArchivo objArchivo;
            byte[] archivo;
            GEN_TabUsuario objUsuarioAnterior;
            int resultado;

            objUsuario = request.Usuario;
            archivo = request.Archivo;
            objUsuarioAnterior = GEN_TabUsuarioData.ObtenerUsuario(objUsuario.siCodUsu);

            try
            {
                if (objUsuario.vNombre != objUsuarioAnterior.vNombre)
                {
                    if (GEN_TabUsuarioData.ValidaExiste(objUsuario))
                    {
                        BusinessException.Generar(Constantes.Mensajes.NOMBREUSUARIO_YA_EXISTE);
                    }
                }

                if (objUsuario.vUsuario != objUsuarioAnterior.vUsuario)
                {
                    if (GEN_TabUsuarioData.ValidaExisteNombreUsuario(objUsuario))
                    {
                        BusinessException.Generar(Constantes.Mensajes.USUARIO_YA_EXISTE);
                    }
                }

                objArchivo = new GEN_MaeArchivo
                {
                    Archivo = archivo,
                    Codigo = objUsuario.siCodUsu.ToString(),
                    Tabla = "USUARIO"
                };

                using (TransactionScope tran = new TransactionScope())
                {
                    resultado = GEN_TabUsuarioData.ActualizarUsuario(objUsuario);
                    if (archivo != null) GEN_MaeArchivoData.GuardarArchivo(objArchivo);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return resultado;
        }

        /// <summary>Método que Actualiza maestro de Usuario.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int CambiarClave(CambiarClaveRequestDto request)
        {
            GEN_TabUsuario objUsuario;
            GEN_TabUsuario objUsuarioAnterior;
            string claveAnterior;
            int resultado;

            claveAnterior = request.ClaveAnterior;

            try
            {
                objUsuarioAnterior = GEN_TabUsuarioData.ObtenerUsuario(request.CodigoUsuario);

                if (objUsuarioAnterior.vClave != Funciones.Encriptar(claveAnterior.ToUpper()))
                {
                    BusinessException.Generar("La clave anterior ingresada es incorrecta");
                }

                objUsuario = new GEN_TabUsuario
                {
                    vClave = Funciones.Encriptar(request.ClaveNueva.ToUpper()),
                    siCodUsu = request.CodigoUsuario
                };

                using (TransactionScope tran = new TransactionScope())
                {
                    resultado = GEN_TabUsuarioData.CambiarClave(objUsuario);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return resultado;
        }

        /// <summary>Método que elimina Usuarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la Usuario.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int EliminarUsuario(UsuarioRequestDto request)
        {
            GEN_TabUsuario objUsuario;
            int resultado;

            objUsuario = request.Usuario;

            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    resultado = GEN_TabUsuarioData.EliminarUsuario(objUsuario);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return resultado;
        }
    }
}
