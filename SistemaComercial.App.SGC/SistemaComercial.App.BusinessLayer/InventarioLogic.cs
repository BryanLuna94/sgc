﻿using JBLV.Log;
using SistemaComercial.App.DataAccess;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Request;
using SistemaComercial.App.DataTypes.Response;
using SistemaComercial.App.Utility;
using System;
using System.Collections.Generic;
using System.Transactions;

namespace SistemaComercial.App.BusinessLayer
{
    public class InventarioLogic
    {
        /// <summary>Invoca al Procedimiento Inventario que lista Inventarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>David Godoy Huaman</CreadoPor></item>
        ///<item><FecCrea>07/09/2021</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        /// 
        public static InventarioResponseDto ObtenerIndexInventarios(InventarioRequestDto request)
        {
            try
            {
                InventarioResponseDto response;
                InventarioFiltroDto objArticuloFiltro;
                List<InventarioListaDto> ListaInventarios;
                List<GenericoListaDto> listaAlmacenes;
                List<GenericoListaDto> listaEstadoInventario;
                int codigoTablaEstadoInventario;

                codigoTablaEstadoInventario = Convert.ToInt32(Constantes.Tablas.ESTADO_INVENTARIO);


                listaAlmacenes = COM_MaeAlmacenData.ListaAlmacenes_Combo();

                objArticuloFiltro = request.Filtro;
                ListaInventarios = COM_TabInventarioData.ListaInventarios(objArticuloFiltro);
                listaEstadoInventario = GEN_TabParametroData.ListaParametros(codigoTablaEstadoInventario);

                response = new InventarioResponseDto
                {
                    ListaInventarios = ListaInventarios,
                    ListaAlmacenes = listaAlmacenes,
                    ListaEstados = listaEstadoInventario,
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que obtiene Inventarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy</CreadoPor></item>
        /// <item><FecCrea>13/09/2021.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static InventarioResponseDto ObtenerEditorInventario(int IdInventario = 0)
        {
            try
            {
                InventarioResponseDto response;
                COM_TabInventario objInventario;
                List<GenericoListaDto> listaAlmacenes;
                List<GenericoListaDto> listaEmpresasPartner;

                listaAlmacenes = COM_MaeAlmacenData.ListaAlmacenes_Combo();
                objInventario = COM_TabInventarioData.ObtenerInventario(IdInventario);
                listaEmpresasPartner = COM_TabPersonaData.ListaEmpresas_Combo(new PersonaFiltroDto { EsPartner = true });

                response = new InventarioResponseDto
                {
                    Inventario = objInventario,
                    ListaAlmacenes = listaAlmacenes,
                    ListaEmpresasPartner = listaEmpresasPartner
                };

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Inventario.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy</CreadoPor></item>
        /// <item><FecCrea>14/09/2021.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarInventario(InventarioRequestDto request)
        {
            COM_TabInventario objInventario;
            int resultado;

            objInventario = request.Inventario;

            try
            {
                if (COM_TabInventarioData.ValidaExiste(objInventario))
                {
                    BusinessException.Generar(string.Format(Constantes.Mensajes.INVENTARIO_YA_EXISTE, objInventario.vDescripcion));
                }

                

                using (TransactionScope tran = new TransactionScope())
                {
                    resultado = COM_TabInventarioData.RegistrarInventario(objInventario);

                  

                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return resultado;
        }

        /// <summary>Método que Actualiza maestro de Inventarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy</CreadoPor></item>
        /// <item><FecCrea>14/09/2021.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarInventario(InventarioRequestDto request)
        {
            COM_TabInventario objInventario;
            COM_TabInventario objInventarioAnterior;

            objInventario = request.Inventario;
            objInventarioAnterior = COM_TabInventarioData.ObtenerInventario(objInventario.iCodInv);

            try
            {

                if (objInventario.vDescripcion.ToUpper() != objInventarioAnterior.vDescripcion.ToUpper())
                {
                    if (COM_TabInventarioData.ValidaExiste(objInventario))
                    {
                        BusinessException.Generar(string.Format(Constantes.Mensajes.INVENTARIO_YA_EXISTE, objInventario.vDescripcion));
                    }
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_TabInventarioData.ActualizarInventario(objInventario);
                    tran.Complete();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>Método que elimina Inventario.</summary>
        /// <param name="objPersona">Entidad con los datos de la Inventario.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy</CreadoPor></item>
        /// <item><FecCrea>14/09/2021.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarInventario(InventarioRequestDto request)
        {
            COM_TabInventario objInventario;

            objInventario = request.Inventario;

            try
            {

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_TabInventarioData.EliminarInventario(objInventario);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que activar Inventario.</summary>
        /// <param name="objPersona">Entidad con los datos de la Inventario.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy</CreadoPor></item>
        /// <item><FecCrea>14/09/2021.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActivarInventario(InventarioRequestDto request)
        {
            COM_TabInventario objInventario;

            objInventario = request.Inventario;

            try
            {

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_TabInventarioData.ActivarInventario(objInventario);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

    }
}
