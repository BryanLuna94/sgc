﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Request;
using SistemaComercial.App.DataTypes.Response;
using SistemaComercial.App.Utility;
using System;
using System.Collections.Generic;
using SistemaComercial.App.DataAccess;
using SistemaComercial.App.DataTypes;
using System.Transactions;
using JBLV.Log;

namespace SistemaComercial.App.BusinessLayer
{
    public class PersonaOficinaLogic
    {
        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static PersonaResponseDto ObtenerEditorPersonaOficina(int IdPersonaOficina = 0)
        {
            try
            {
                PersonaResponseDto response;
                COM_TabPersona_Oficina objPersonaOficina;
                List<GenericoListaDto> listaPaises;

                objPersonaOficina = COM_TabPersona_OficinaData.ObtenerPersonaOficina(IdPersonaOficina);
                listaPaises = GEN_MaePais.ListaPaises_Combo();

                response = new PersonaResponseDto
                {
                    OficinaPersona = objPersonaOficina,
                    ListaPaises = listaPaises
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarPersonaOficina(PersonaRequestDto request)
        {
            COM_TabPersona_Oficina objPersonaOficina;
            int resultado;

            objPersonaOficina = request.OficinaPersona;

            try
            {
                //if (objPersonaOficina.tiCodTipDocIde != objPersonaAnterior.tiCodTipDocIde && objPersona.vNroDocIde != objPersonaAnterior.vNroDocIde)
                //{
                //    if (COM_TabPersonaData.ValidaExiste(objPersona))
                //    {
                //        BusinessException.Generar(string.Format(Constantes.Mensajes.PERSONA_YA_EXISTE, objPersona.vNroDocIde));
                //    }
                //}

                using (TransactionScope tran = new TransactionScope())
                {
                    resultado = COM_TabPersona_OficinaData.RegistrarPersonaOficina(objPersonaOficina);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return resultado;
        }

        /// <summary>Método que Actualiza maestro de Persona.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarPersonaOficina(PersonaRequestDto request)
        {
            COM_TabPersona_Oficina objPersonaOficina;
            COM_TabPersona_Oficina objPersonaOficinaAnterior;

            objPersonaOficina = request.OficinaPersona;
            objPersonaOficinaAnterior = COM_TabPersona_OficinaData.ObtenerPersonaOficina(objPersonaOficina.iCodPerOfi);

            try
            {
                //if (objPersonaOficina.tiCodTipDocIde != objPersonaAnterior.tiCodTipDocIde && objPersona.vNroDocIde != objPersonaAnterior.vNroDocIde)
                //{
                //    if (COM_TabPersonaData.ValidaExiste(objPersona))
                //    {
                //        BusinessException.Generar(string.Format(Constantes.Mensajes.PERSONA_YA_EXISTE, objPersona.vNroDocIde));
                //    }
                //}

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_TabPersona_OficinaData.ActualizarPersonaOficina(objPersonaOficina);
                    tran.Complete();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>Método que elimina Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la Persona.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int EliminarPersonaOficina(PersonaRequestDto request)
        {
            COM_TabPersona_Oficina objPersonaOficina;
            COM_TabPersona_Oficina objPersonaOficinaAnterior;

            objPersonaOficina = request.OficinaPersona;

            try
            {
                //if (COM_TabPersonaData.ValidaTieneProblemas(objPersona.siCodAlm))
                //{
                //    BusinessException.Generar(Constantes.Mensajes.Persona_TIENE_PROBLEMAS);
                //}

                objPersonaOficinaAnterior = COM_TabPersona_OficinaData.ObtenerPersonaOficina(objPersonaOficina.iCodPerOfi);

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_TabPersona_OficinaData.EliminarPersonaOficina(objPersonaOficina);
                    tran.Complete();
                }

                return objPersonaOficinaAnterior.iCodPer;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento AlmacenUbicacionado que lista AlmacenUbicaciones.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las AlmacenUbicaciones</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static GenericoResponseDto ListaDirecciones_Combo(int idPersona)
        {
            try
            {
                GenericoResponseDto response;
                List<GenericoListaDto> listaPersonas;

                listaPersonas = COM_TabPersona_OficinaData.ListaPersonaOficina_Combo(idPersona);

                response = new GenericoResponseDto
                {
                    ListaGenerica = listaPersonas
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }
    }
}
