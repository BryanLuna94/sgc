﻿using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Request;
using SistemaComercial.App.DataTypes.Response;
using SistemaComercial.App.Utility;
using System;
using System.Collections.Generic;
using SistemaComercial.App.DataAccess;
using SistemaComercial.App.DataTypes;
using System.Transactions;
using JBLV.Log;

namespace SistemaComercial.App.BusinessLayer
{
    public class MarcaLogic
    {
        /// <summary>Invoca al Procedimiento Marcaado que lista Marca.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Marca</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static MarcaResponseDto ObtenerIndexMarca(MarcaRequestDto request)
        {
            try
            {
                MarcaResponseDto response;
                MarcaFiltroDto objMarcaFiltro;
                List<MarcaListaDto> listaMarca;

                objMarcaFiltro = request.Filtro;
                listaMarca = COM_MaeMarcaData.ListaMarca(objMarcaFiltro);

                response = new MarcaResponseDto
                {
                    ListaMarcas = listaMarca
                };

                return response;
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Marcaado que lista Marca.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Marca</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static MarcaResponseDto ListaMarca(MarcaRequestDto request)
        {
            try
            {
                MarcaResponseDto response;
                MarcaFiltroDto objMarcaFiltro;
                List<MarcaListaDto> listaMarca;

                objMarcaFiltro = request.Filtro;
                listaMarca = COM_MaeMarcaData.ListaMarca(objMarcaFiltro);

                response = new MarcaResponseDto
                {
                    ListaMarcas = listaMarca
                };

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>Método que registra Marca.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static MarcaResponseDto ObtenerEditorMarca(int IdMarca = 0)
        {
            try
            {
                MarcaResponseDto response;
                COM_MaeMarca objMarca;

                objMarca = COM_MaeMarcaData.ObtenerMarca(IdMarca);

                response = new MarcaResponseDto
                {
                    Marca = objMarca
                };

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Marca.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarMarca(MarcaRequestDto request)
        {
            DataTypes.COM_MaeMarca objMarca;
            int resultado;

            objMarca = request.Marca;

            try
            {
                if (COM_MaeMarcaData.ValidaExiste(objMarca))
                {
                    BusinessException.Generar(string.Format(Constantes.Mensajes.MARCA_YA_EXISTE, objMarca.vDescripcion));
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    resultado = COM_MaeMarcaData.RegistrarMarca(objMarca);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
            return resultado;
        }

        /// <summary>Método que Actualiza maestro de Marca.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarMarca(MarcaRequestDto request)
        {
            DataTypes.COM_MaeMarca objMarca;
            DataTypes.COM_MaeMarca objMarcaAnterior;

            objMarca = request.Marca;
            objMarcaAnterior = COM_MaeMarcaData.ObtenerMarca(objMarca.iCodMarca);

            try
            {
                if (objMarca.vDescripcion.ToUpper() != objMarcaAnterior.vDescripcion.ToUpper())
                {
                    if (COM_MaeMarcaData.ValidaExiste(objMarca))
                    {
                        BusinessException.Generar(string.Format(Constantes.Mensajes.MARCA_YA_EXISTE, objMarca.vDescripcion));
                    }
                }

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_MaeMarcaData.ActualizarMarca(objMarca);
                    tran.Complete();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>Método que elimina Marca.</summary>
        /// <param name="objPersona">Entidad con los datos de la Marca.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarMarca(MarcaRequestDto request)
        {
            DataTypes.COM_MaeMarca objMarca;

            objMarca = request.Marca;

            try
            {
                //if (COM_MaeMarcaData.ValidaTieneProblemas(objMarca.siCodUMed))
                //{
                //    BusinessException.Generar(Constantes.Mensajes.Marca_TIENE_PROBLEMAS);
                //}

                using (TransactionScope tran = new TransactionScope())
                {
                    COM_MaeMarcaData.EliminarMarca(objMarca);
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.RegistrarLog(NivelLog.Error, ex);
                throw;
            }
        }
    }
}
