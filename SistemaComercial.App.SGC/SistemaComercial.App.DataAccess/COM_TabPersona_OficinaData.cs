﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.Utility;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class COM_TabPersona_OficinaData
    {
        /// <summary>Invoca al Procedimiento PersonaOficinaado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<PersonaOficinaListaDto> ListaPersonaOficina(PersonaOficinaFiltroDto objFiltro)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<PersonaOficinaListaDto> ListaPersonaOficina = new List<PersonaOficinaListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Oficina_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvNombre", SqlDbType.VarChar, objFiltro.Nombre);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, objFiltro.CodigoPersona);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaPersonaOficina.Add(new PersonaOficinaListaDto
                                {
                                    Codigo = Convert.ToInt16(dr["iCodPerOfi"]),
                                    Descripcion = Convert.ToString(dr["vDescripcion"]),
                                    Direccion = Convert.ToString(dr["vDireccion"]),
                                    UsuarioCrea = Convert.ToString(dr["UsuCrea"]),
                                    UsuarioActualiza = Funciones.Check.Cadena(dr["UsuActualiza"]),
                                    FechaCrea = Funciones.Check.FechaCorta(dr["sdFechaCre"]),
                                    FechaActualiza = Funciones.Check.FechaCorta(dr["sdFechaAct"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaPersonaOficina;
        }

        /// <summary>Invoca al Procedimiento PersonaOficinaado que lista Descripcion Base.</summary>
        /// <param name="idPersonaOficina">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static COM_TabPersona_Oficina ObtenerPersonaOficina(int idPersonaOficina)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Oficina_Obtener");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodPerOfi", SqlDbType.Int, idPersonaOficina);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new COM_TabPersona_Oficina
                                {
                                    iCodPerOfi = Convert.ToInt16(dr["iCodPerOfi"]),
                                    vDescripcion = Convert.ToString(dr["vDescripcion"]),
                                    iCodPer = Convert.ToInt32(dr["iCodPer"]),
                                    siCodPais = Convert.ToInt16(dr["siCodPais"]),
                                    vDireccion = Funciones.Check.Cadena(dr["vDireccion"]),
                                    vNumero = Funciones.Check.Cadena(dr["vNumero"]),
                                    siCodUsuAct = Funciones.Check.Int16Null(dr["siCodUsuAct"]),
                                    siCodUsuCre = Convert.ToInt16(dr["siCodUsuCre"]),
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Método que registra Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarPersonaOficina(COM_TabPersona_Oficina objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Oficina_Insertar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, objParametros.iCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objParametros.vDescripcion);
                        objDatabase.AgregarParametro(ref cmd, "@pvDireccion", SqlDbType.VarChar, objParametros.vDireccion);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumero", SqlDbType.VarChar, objParametros.vNumero);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodPais", SqlDbType.SmallInt, objParametros.siCodPais);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuCre", SqlDbType.SmallInt, objParametros.siCodUsuCre);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPerOfi", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piCodPerOfi"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que Actualiza maestro de PersonaOficina.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarPersonaOficina(COM_TabPersona_Oficina objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Oficina_Actualizar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodPerOfi", SqlDbType.Int, objParametros.iCodPerOfi);
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objParametros.vDescripcion);
                        objDatabase.AgregarParametro(ref cmd, "@pvDireccion", SqlDbType.VarChar, objParametros.vDireccion);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumero", SqlDbType.VarChar, objParametros.vNumero);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodPais", SqlDbType.SmallInt, objParametros.siCodPais);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que elimina Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la Descripcion Base.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarPersonaOficina(COM_TabPersona_Oficina objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Oficina_Eliminar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodPerOfi", SqlDbType.Int, objParametros.iCodPerOfi);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaExiste(COM_TabPersona_Oficina objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Oficina_ValidaExiste");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objParametros.vDescripcion);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Invoca al Procedimiento PersonaOficinaado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<GenericoListaDto> ListaPersonaOficina_Combo(int idPersona)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<GenericoListaDto> ListaPersonaOficina = new List<GenericoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Oficina_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvNombre", SqlDbType.VarChar, string.Empty);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, idPersona);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaPersonaOficina.Add(new GenericoListaDto
                                {
                                    Codigo = Convert.ToString(dr["iCodPerOfi"]),
                                    Descripcion = Convert.ToString(dr["vDireccion"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaPersonaOficina;
        }
    }
}
