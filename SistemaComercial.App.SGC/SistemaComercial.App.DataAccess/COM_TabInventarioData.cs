﻿using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class COM_TabInventarioData
    {
        /// <summary>Invoca al Procedimiento Inventario que lista Inventarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>David Godoy Huaman</CreadoPor></item>
        ///<item><FecCrea>07/09/2021</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        /// 
        public static List<InventarioListaDto> ListaInventarios(InventarioFiltroDto objFiltro)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<InventarioListaDto> ListaInventario = new List<InventarioListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabInventario_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objFiltro.Descripcion);
                        objDatabase.AgregarParametro(ref cmd, "@piCodAlm", SqlDbType.SmallInt, objFiltro.CodigoAlmacen);
                        objDatabase.AgregarParametro(ref cmd, "@piEstado", SqlDbType.SmallInt, objFiltro.Estado);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaInventario.Add(new InventarioListaDto
                                {
                                    Id = Convert.ToInt16(dr["iCodInv"]),
                                    Descripcion = Convert.ToString(dr["vDescripcion"]),
                                    Estado = Convert.ToString(dr["Estado"]),
                                    Alerta = Funciones.Check.Cadena(dr["vAlertaId"]),
                                    Persona = Convert.ToString(dr["Persona"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaInventario;
        }

        /// <summary>Invoca al Procedimiento Inventario que obtener Inventarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>David Godoy Huaman</CreadoPor></item>
        ///<item><FecCrea>07/09/2021</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        /// 
        public static COM_TabInventario ObtenerInventario(int idInventario)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabInventario_Obtener");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodInv", SqlDbType.Int, idInventario);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new COM_TabInventario
                                {
                                    iCodInv = Convert.ToInt16(dr["iCodInv"]),
                                    siCodAlm = Convert.ToInt16(dr["siCodAlm"]),
                                    vDescripcion = Convert.ToString(dr["vDescripcion"]),
                                    vAlertaId = Funciones.Check.Cadena(dr["vAlertaId"]),
                                    iCodPartner = Funciones.Check.Int32(dr["iCodPartner"])
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Método que registra Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy</CreadoPor></item>
        /// <item><FecCrea>14/09/2021</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarInventario(COM_TabInventario objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabInventario_Insertar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objParametros.vDescripcion);
                        objDatabase.AgregarParametro(ref cmd, "@piCodAlm", SqlDbType.SmallInt, objParametros.siCodAlm);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuCre", SqlDbType.SmallInt, objParametros.siCodUsuCre);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPartner", SqlDbType.Int, objParametros.iCodPartner);
                        objDatabase.AgregarParametro(ref cmd, "@pvAlertaId", SqlDbType.VarChar, objParametros.vAlertaId);
                        objDatabase.AgregarParametro(ref cmd, "@piCodInv", SqlDbType.SmallInt, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piCodInv"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que Actualiza maestro de Articulo.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy</CreadoPor></item>
        /// <item><FecCrea>14/09/2021</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarInventario(COM_TabInventario objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabInventario_Actualizar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodInv", SqlDbType.SmallInt, objParametros.iCodInv);
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objParametros.vDescripcion);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPartner", SqlDbType.Int, objParametros.iCodPartner);
                        objDatabase.AgregarParametro(ref cmd, "@pvAlertaId", SqlDbType.VarChar, objParametros.vAlertaId);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que elimina Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la Descripcion Base.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy</CreadoPor></item>
        /// <item><FecCrea>14/09/2021</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarInventario(COM_TabInventario objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabInventario_Eliminar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodInv", SqlDbType.SmallInt, objParametros.iCodInv);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy</CreadoPor></item>
        /// <item><FecCrea>14/09/2021</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaExiste(COM_TabInventario objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabInventario_ValidaExiste");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objParametros.vDescripcion);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Método que activar Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la Descripcion Base.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy</CreadoPor></item>
        /// <item><FecCrea>14/09/2021</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActivarInventario(COM_TabInventario objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabInventario_Activar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodInv", SqlDbType.SmallInt, objParametros.iCodInv);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
