﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.Utility;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class COM_CabMovArtData
    {
        /// <summary>Invoca al Procedimiento Movimientoado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<MovimientoListaDto> ListaMovimientos(MovimientoFiltroDto objFiltro)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<MovimientoListaDto> ListaMovimiento = new List<MovimientoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvCorrelativo", SqlDbType.VarChar, objFiltro.Correlativo);
                        objDatabase.AgregarParametro(ref cmd, "@ptiCodTipMov", SqlDbType.TinyInt, objFiltro.TipoMovimiento);
                        objDatabase.AgregarParametro(ref cmd, "@pvFactura", SqlDbType.SmallInt, objFiltro.Factura);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumeroDoc", SqlDbType.VarChar, objFiltro.NumeroDoc);
                        objDatabase.AgregarParametro(ref cmd, "@pvFechaDesde", SqlDbType.VarChar, objFiltro.FechaDesde);
                        objDatabase.AgregarParametro(ref cmd, "@pvFechaHasta", SqlDbType.VarChar, objFiltro.FechaHasta);
                        objDatabase.AgregarParametro(ref cmd, "@pvPersona", SqlDbType.VarChar, objFiltro.Persona);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuCre", SqlDbType.SmallInt, objFiltro.UsuarioCrea);
                        objDatabase.AgregarParametro(ref cmd, "@pvAlertaId", SqlDbType.VarChar, objFiltro.AlertaId);
                        objDatabase.AgregarParametro(ref cmd, "@pvGuiaAerea", SqlDbType.VarChar, objFiltro.GuiaArea);
                        objDatabase.AgregarParametro(ref cmd, "@pvRMA", SqlDbType.VarChar, objFiltro.RMA);
                        objDatabase.AgregarParametro(ref cmd, "@pvOrdExt", SqlDbType.VarChar, objFiltro.OrdExterior);
                        objDatabase.AgregarParametro(ref cmd, "@pvFacCom", SqlDbType.VarChar, objFiltro.FacturaComercial);
                        objDatabase.AgregarParametro(ref cmd, "@ptiCodCan", SqlDbType.TinyInt, objFiltro.Canal);
                        objDatabase.AgregarParametro(ref cmd, "@pvArtDesc", SqlDbType.VarChar, objFiltro.Articulo);
                        objDatabase.AgregarParametro(ref cmd, "@pvArtCodO", SqlDbType.VarChar, objFiltro.CodigoOriginal);
                        objDatabase.AgregarParametro(ref cmd, "@pvSerie", SqlDbType.VarChar, objFiltro.Serie);
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, objFiltro.Codigo);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaMovimiento.Add(new MovimientoListaDto
                                {
                                    Codigo = Convert.ToInt32(dr["iCodCabMov"]),
                                    TipoMov = Convert.ToString(dr["TipoMov"]),
                                    TipoDoc = Convert.ToString(dr["TipoDoc"]),
                                    NumeroDoc = Convert.ToString(dr["vNumeroDoc"]),
                                    FechaHoraRecep = Funciones.Check.FechaLarga(dr["sdFechaHoraRecep"]),
                                    Persona = Convert.ToString(dr["Marca"]),
                                    UsuarioCrea = Funciones.Check.Cadena(dr["UsuCrea"]),
                                    UsuarioActualiza = Funciones.Check.Cadena(dr["UsuActualiza"]),
                                    FechaCrea = Funciones.Check.FechaCorta(dr["sdFechaCre"]),
                                    FechaActualiza = Funciones.Check.FechaCorta(dr["sdFechaAct"]),
                                    Correlativo = Convert.ToString(dr["vCorrelativo"]),
                                    Estado = (Convert.ToBoolean(dr["bCerrado"]) ? "CERRADO" : "ABIERTO")
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaMovimiento;
        }

        /// <summary>Invoca al Procedimiento Movimientoado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static MovimientoListaDto ListaMovimiento(int codigo)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_ObtenerDto");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, codigo);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new MovimientoListaDto
                                {
                                    Codigo = Convert.ToInt32(dr["iCodCabMov"]),
                                    TipoMov = Convert.ToString(dr["TipoMov"]),
                                    TipoDoc = Convert.ToString(dr["TipoDoc"]),
                                    FechaHoraRecep = Funciones.Check.FechaLarga(dr["sdFechaHoraRecep"]),
                                    Persona = Convert.ToString(dr["Marca"]),
                                    UsuarioCrea = Funciones.Check.Cadena(dr["UsuCrea"]),
                                    UsuarioActualiza = Funciones.Check.Cadena(dr["UsuActualiza"]),
                                    FechaCrea = Funciones.Check.FechaCorta(dr["sdFechaCre"]),
                                    FechaActualiza = Funciones.Check.FechaCorta(dr["sdFechaAct"]),
                                    Correlativo = Convert.ToString(dr["vCorrelativo"])
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Invoca al Procedimiento Movimientoado que lista Descripcion Base.</summary>
        /// <param name="idMovimiento">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static COM_CabMovArt ObtenerMovimiento(int idMovimiento)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_Obtener");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, idMovimiento);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new COM_CabMovArt
                                {
                                    iCodCabMov = Convert.ToInt32(dr["iCodCabMov"]),
                                    tiCodTipMov = Convert.ToByte(dr["tiCodTipMov"]),
                                    siCodTipDoc = Convert.ToInt16(dr["siCodTipDoc"]),
                                    sdFechaHoraRecep = Convert.ToDateTime(dr["sdFechaHoraRecep"]),
                                    iCodPer = Convert.ToInt32(dr["iCodPer"]),
                                    bImpExp = Convert.ToBoolean(dr["bImpExp"]),
                                    vCorrelativo = Convert.ToString(dr["vCorrelativo"]),
                                    siCodUsuAct = Funciones.Check.Int16Null(dr["siCodUsuAct"]),
                                    siCodUsuCre = Convert.ToInt16(dr["siCodUsuCre"]),
                                    bCerrado = Convert.ToBoolean(dr["bCerrado"]),
                                    vTipoMov = Convert.ToString(dr["vTipoMov"]),
                                    vObservacion = Convert.ToString(dr["vObservacion"]),
                                    vNumeroDoc = Convert.ToString(dr["vNumeroDoc"])
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Método que registra Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarMovimiento(COM_CabMovArt objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_Insertar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@ptiCodTipMov", SqlDbType.TinyInt, objParametros.tiCodTipMov);
                        objDatabase.AgregarParametro(ref cmd, "@pbImpExp", SqlDbType.Bit, objParametros.bImpExp);
                        objDatabase.AgregarParametro(ref cmd, "@psdFechaHoraRecep", SqlDbType.DateTime, objParametros.sdFechaHoraRecep);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, objParametros.iCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuCre", SqlDbType.SmallInt, objParametros.siCodUsuCre);
                        objDatabase.AgregarParametro(ref cmd, "@pvTipoMov", SqlDbType.VarChar, objParametros.vTipoMov);
                        objDatabase.AgregarParametro(ref cmd, "@pvObservacion", SqlDbType.VarChar, objParametros.vObservacion);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodTipDoc", SqlDbType.TinyInt, objParametros.siCodTipDoc);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumeroDoc", SqlDbType.VarChar, objParametros.vNumeroDoc);
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piCodCabMov"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que Actualiza maestro de Movimiento.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarMovimiento(COM_CabMovArt objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_Actualizar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.SmallInt, objParametros.iCodCabMov);
                        objDatabase.AgregarParametro(ref cmd, "@pvObservacion", SqlDbType.VarChar, objParametros.vObservacion);
                        objDatabase.AgregarParametro(ref cmd, "@psdFechaHoraRecep", SqlDbType.DateTime, objParametros.sdFechaHoraRecep);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, objParametros.iCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumeroDoc", SqlDbType.VarChar, objParametros.vNumeroDoc);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que elimina Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la Descripcion Base.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void CerrarMovimiento(COM_CabMovArt objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_Cerrar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, objParametros.iCodCabMov);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que elimina Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la Descripcion Base.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarMovimiento(COM_CabMovArt objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_Eliminar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, objParametros.iCodCabMov);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaExiste(COM_CabMovArt objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_ValidaExiste");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodTipDoc", SqlDbType.SmallInt, objParametros.siCodTipDoc);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, objParametros.iCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaSalioDetalle(int iCodCabMov)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_ValidaSalioDetalle");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, iCodCabMov);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        #region REPORTES

        /// <summary>Invoca al Procedimiento Movimientoado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static ReporteGuiaRemCabListaDto ObtenerReporteGuiaRemCabecera(int codigo)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_ObtenerGuiaRem");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, codigo);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new ReporteGuiaRemCabListaDto
                                {
                                    DireccionLlegada = Convert.ToString(dr["DireccionLlegada"]),
                                    Fecha = Convert.ToString(dr["Fecha"]),
                                    NOrden = Convert.ToString(dr["Orden"]),
                                    Numero = Convert.ToString(dr["vNumeroDoc"]),
                                    Serie = Convert.ToString(dr["vSerieDoc"]),
                                    RMA = Convert.ToString(dr["RMA"]),
                                    Ruc = Convert.ToString(dr["Ruc"]),
                                    Cliente = Convert.ToString(dr["Cliente"])
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Invoca al Procedimiento Movimientoado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<ReporteGuiaRemDetListaDto> ListaReporteGuiaRemDetalle(int codigo)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<ReporteGuiaRemDetListaDto> ListaMovimiento = new List<ReporteGuiaRemDetListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_DetMovArt_ObtenerGuiaRem");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, codigo);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaMovimiento.Add(new ReporteGuiaRemDetListaDto
                                {
                                    Cantidad = 1,
                                    Serie = Convert.ToString(dr["vSerie"]),
                                    Codigo = Convert.ToString(dr["iCodDetMov"]),
                                    Descripcion = Convert.ToString(dr["Descripcion"]),
                                    PartNumber = Convert.ToString(dr["Part"]),
                                    Peso = Convert.ToDecimal(dr["Peso"]),
                                    Estado = Convert.ToString(dr["EstadoArticulo"]),
                                    Marca = Convert.ToString(dr["Marca"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaMovimiento;
        }

        /// <summary>Invoca al Procedimiento Movimientoado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<ReporteAlmacenesValorizadoResumidoListaDto> ListaReporteAlmacenValorizadoResumido(ReporteAlmacenesValorizadoResumidoFiltroDto request)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<ReporteAlmacenesValorizadoResumidoListaDto> ListaMovimiento = new List<ReporteAlmacenesValorizadoResumidoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_Reporte_AlmacenValorizadoResumido");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodArt", SqlDbType.Int, request.CodigoArticulo);
                        objDatabase.AgregarParametro(ref cmd, "@piCodMarca", SqlDbType.Int, request.CodigoMarca);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaMovimiento.Add(new ReporteAlmacenesValorizadoResumidoListaDto
                                {
                                    Almacen = Convert.ToString(dr["Almacen"]),
                                    Suma = Convert.ToDecimal(dr["Suma"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaMovimiento;
        }

        /// <summary>Invoca al Procedimiento Movimientoado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<ReporteAlmacenesValorizadoDetalladoListaDto> ListaReporteAlmacenValorizadoDetallado(ReporteAlmacenesValorizadoResumidoFiltroDto request)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<ReporteAlmacenesValorizadoDetalladoListaDto> ListaMovimiento = new List<ReporteAlmacenesValorizadoDetalladoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_Reporte_AlmacenValorizadoDetallado");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodArt", SqlDbType.Int, request.CodigoArticulo);
                        objDatabase.AgregarParametro(ref cmd, "@piCodMarca", SqlDbType.Int, request.CodigoMarca);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaMovimiento.Add(new ReporteAlmacenesValorizadoDetalladoListaDto
                                {
                                    Almacen = Convert.ToString(dr["Almacen"]),
                                    CodigoOriginal = Convert.ToString(dr["vCodigoOriginal"]),
                                    Articulo = Convert.ToString(dr["vDescripcion"]),
                                    Serie = Convert.ToString(dr["vSerie"]),
                                    Costo = Convert.ToDecimal(dr["dCostoIngreso"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaMovimiento;
        }

        /// <summary>Invoca al Procedimiento Movimientoado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<ReporteKardexListaDto> ListaReporteKardex(ReporteKardexFiltroDto objFiltro)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<ReporteKardexListaDto> ListaMovimiento = new List<ReporteKardexListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_ListaKardex");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodAlm", SqlDbType.SmallInt, objFiltro.CodigoAlmacen);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, objFiltro.CodigoEmpresa);
                        objDatabase.AgregarParametro(ref cmd, "@piAnio", SqlDbType.Int, objFiltro.Anio);
                        objDatabase.AgregarParametro(ref cmd, "@piMes", SqlDbType.Int, objFiltro.IdMes);
                        objDatabase.AgregarParametro(ref cmd, "@piCodMarca", SqlDbType.Int, objFiltro.CodigoMarca);
                        objDatabase.AgregarParametro(ref cmd, "@piCodArt", SqlDbType.Int, objFiltro.CodigoProducto);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaMovimiento.Add(new ReporteKardexListaDto
                                {
                                    Codigo = Convert.ToString(dr["Codigo"]),
                                    TipoMov = Convert.ToString(dr["TipoMov"]),
                                    TipoDoc = Convert.ToString(dr["TipoDoc"]),
                                    NumeroDoc = Convert.ToString(dr["NumeroDoc"]),
                                    UnMed = Convert.ToString(dr["UnMed"]),
                                    Fecha = Convert.ToString(dr["Fecha"]),
                                    Cantidad = Funciones.Check.Int32(dr["Cantidad"]),
                                    Articulo = Funciones.Check.Cadena(dr["Articulo"]),
                                    Empresa = Funciones.Check.Cadena(dr["Empresa"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaMovimiento;
        }

        #endregion
    }
}
