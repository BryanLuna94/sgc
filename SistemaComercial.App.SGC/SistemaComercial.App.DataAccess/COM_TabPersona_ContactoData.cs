﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.Utility;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class COM_TabPersona_ContactoData
    {
        /// <summary>Invoca al Procedimiento PersonaContactoado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<PersonaContactoListaDto> ListaPersonaContacto(PersonaContactoFiltroDto objFiltro)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<PersonaContactoListaDto> ListaPersonaContacto = new List<PersonaContactoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Contacto_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvNombre", SqlDbType.VarChar, objFiltro.Nombre);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, objFiltro.CodigoPersona);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaPersonaContacto.Add(new PersonaContactoListaDto
                                {
                                    Codigo = Convert.ToInt16(dr["iCodPerCon"]),
                                    Nombre = Convert.ToString(dr["vNombre"]),
                                    Numero = Funciones.Check.Cadena(dr["vNumero"]),
                                    Comentario = Funciones.Check.Cadena("vComentario"),
                                    UsuarioCrea = Convert.ToString(dr["UsuCrea"]),
                                    UsuarioActualiza = Funciones.Check.Cadena(dr["UsuActualiza"]),
                                    FechaCrea = Funciones.Check.FechaCorta(dr["sdFechaCre"]),
                                    FechaActualiza = Funciones.Check.FechaCorta(dr["sdFechaAct"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaPersonaContacto;
        }

        /// <summary>Invoca al Procedimiento PersonaContactoado que lista Descripcion Base.</summary>
        /// <param name="idPersonaContacto">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static COM_TabPersona_Contacto ObtenerPersonaContacto(int idPersonaContacto)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Contacto_Obtener");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodPerCon", SqlDbType.Int, idPersonaContacto);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new COM_TabPersona_Contacto
                                {
                                    iCodPerCon = Convert.ToInt16(dr["iCodPerCon"]),
                                    vComentarios = Convert.ToString(dr["vComentarios"]),
                                    iCodPer = Convert.ToInt32(dr["iCodPer"]),
                                    vCorreo = Funciones.Check.Cadena(dr["vCorreo"]),
                                    vNombre = Funciones.Check.Cadena(dr["vNombre"]),
                                    vNumero = Funciones.Check.Cadena(dr["vNumero"]),
                                    siCodUsuAct = Funciones.Check.Int16Null(dr["siCodUsuAct"]),
                                    siCodUsuCre = Convert.ToInt16(dr["siCodUsuCre"]),
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Método que registra Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarPersonaContacto(COM_TabPersona_Contacto objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Contacto_Insertar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, objParametros.iCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@pvNombre", SqlDbType.VarChar, objParametros.vNombre);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumero", SqlDbType.VarChar, objParametros.vNumero);
                        objDatabase.AgregarParametro(ref cmd, "@pvComentarios", SqlDbType.VarChar, objParametros.vComentarios);
                        objDatabase.AgregarParametro(ref cmd, "@pvCorreo", SqlDbType.VarChar, objParametros.vCorreo);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuCre", SqlDbType.SmallInt, objParametros.siCodUsuCre);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPerCon", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piCodPerCon"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que Actualiza maestro de PersonaContacto.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarPersonaContacto(COM_TabPersona_Contacto objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Contacto_Actualizar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodPerCon", SqlDbType.Int, objParametros.iCodPerCon);
                        objDatabase.AgregarParametro(ref cmd, "@pvNombre", SqlDbType.VarChar, objParametros.vNombre);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumero", SqlDbType.VarChar, objParametros.vNumero);
                        objDatabase.AgregarParametro(ref cmd, "@pvComentarios", SqlDbType.VarChar, objParametros.vComentarios);
                        objDatabase.AgregarParametro(ref cmd, "@pvCorreo", SqlDbType.VarChar, objParametros.vCorreo);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que elimina Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la Descripcion Base.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarPersonaContacto(COM_TabPersona_Contacto objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Contacto_Eliminar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodPerCon", SqlDbType.Int, objParametros.iCodPerCon);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento PersonaContactoado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<GenericoListaDto> ListaPersonaContacto_Combo(int idPersona)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<GenericoListaDto> ListaPersonaContacto = new List<GenericoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Contacto_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, string.Empty);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, idPersona);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaPersonaContacto.Add(new GenericoListaDto
                                {
                                    Codigo = Convert.ToString(dr["iCodPerCon"]),
                                    Descripcion = Convert.ToString(dr["vDescripcion"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaPersonaContacto;
        }
    }
}
