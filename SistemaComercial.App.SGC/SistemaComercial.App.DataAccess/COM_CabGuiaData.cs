﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.Utility;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class COM_CabGuiaData
    {


        /// <summary>Invoca al Procedimiento Movimientoado que lista Descripcion Base.</summary>
        /// <param name="idMovimiento">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static COM_CabGuia ObtenerGuia(int idMovimiento)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabGuia_Obtener");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, idMovimiento);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new COM_CabGuia
                                {
                                    iCodCabMov = Convert.ToInt32(dr["iCodCabMov"]),
                                    iCodPer = Convert.ToInt32(dr["iCodPer"]),
                                    iGuiaId = Convert.ToInt32(dr["iGuiaId"]),
                                    vDireccion = Convert.ToString(dr["vDireccion"]),
                                    vNumero = Convert.ToString(dr["vNumero"]),
                                    vSerie = Convert.ToString(dr["vSerie"]),
                                    iCodOficina= Convert.ToInt32(dr["iCodOficina"]),
                                    vObservacion = Funciones.Check.Cadena(dr["vObservacion"])
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Método que registra Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarGuia(COM_CabGuia objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabGuia_Insertar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, objParametros.iCodCabMov);
                        objDatabase.AgregarParametro(ref cmd, "@pvSerie", SqlDbType.VarChar, objParametros.vSerie);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumero", SqlDbType.VarChar, objParametros.vNumero);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, objParametros.iCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@pvDireccion", SqlDbType.VarChar, objParametros.vDireccion);
                        objDatabase.AgregarParametro(ref cmd, "@piCodOficina", SqlDbType.Int, objParametros.iCodOficina);
                        objDatabase.AgregarParametro(ref cmd, "@pvObservacion", SqlDbType.VarChar, objParametros.vObservacion);
                        objDatabase.AgregarParametro(ref cmd, "@piGuiaId", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piGuiaId"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que Actualiza maestro de Movimiento.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarGuia(COM_CabGuia objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabGuia_Actualizar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, objParametros.iCodCabMov);
                        objDatabase.AgregarParametro(ref cmd, "@pvSerie", SqlDbType.VarChar, objParametros.vSerie);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumero", SqlDbType.VarChar, objParametros.vNumero);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, objParametros.iCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@pvDireccion", SqlDbType.VarChar, objParametros.vDireccion);
                        objDatabase.AgregarParametro(ref cmd, "@piCodOficina", SqlDbType.Int, objParametros.iCodOficina);
                        objDatabase.AgregarParametro(ref cmd, "@pvObservacion", SqlDbType.VarChar, objParametros.vObservacion);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaExiste(COM_CabGuia objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabGuia_ValidaExiste");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvSerieDoc", SqlDbType.VarChar, objParametros.vSerie);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumeroDoc", SqlDbType.VarChar, objParametros.vNumero);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

    }
}
