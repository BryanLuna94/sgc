﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.Utility;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class COM_CabMovArt_ExtData
    {
        /// <summary>Invoca al Procedimiento MovimientoExtado que lista Descripcion Base.</summary>
        /// <param name="idMovimientoExt">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static COM_CabMovArt_Ext ObtenerMovimientoExt_Mov(int idMovimiento)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_Ext_Obtener_PorMov");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, idMovimiento);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new COM_CabMovArt_Ext
                                {
                                    iCodCabMov = Convert.ToInt32(dr["iCodCabMov"]),
                                    iCodCabMovExt = Convert.ToInt32(dr["iCodCabMovExt"]),
                                    iCodPerOperador = Funciones.Check.Int32Null(dr["iCodPerOperador"]),
                                    tiCodCan = Funciones.Check.Int16Null(dr["tiCodCan"]),
                                    tiCodCanAct = Funciones.Check.Int16Null(dr["tiCodCanAct"]),
                                    vAlertaId = Funciones.Check.Cadena(dr["vAlertaId"]),
                                    vFacCom = Funciones.Check.Cadena(dr["vFacCom"]),
                                    vGuiaAerea = Funciones.Check.Cadena(dr["vGuiaAerea"]),
                                    vOrdExt = Funciones.Check.Cadena(dr["vOrdExt"]),
                                    vRMA = Funciones.Check.Cadena(dr["vRMA"]),
                                    bConValorComercial= Funciones.Check.Boolean(dr["bConValorComercial"]),
                                    sdFechaETA = Funciones.Check.Datetime(dr["sdFechaETA"]),
                                    vDUA = Funciones.Check.Cadena(dr["vDUA"]),
                                    iCodPartner = Funciones.Check.Int32(dr["iCodPartner"]),
                                    bCarga = Funciones.Check.Boolean(dr["bCarga"]),
                                    bTuvoAjuste= Funciones.Check.Boolean(dr["bTuvoAjuste"]),
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Método que registra Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarActualizarMovimientoExt(COM_CabMovArt_Ext objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_Ext_InsertarActualizar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, objParametros.iCodCabMov);
                        objDatabase.AgregarParametro(ref cmd, "@pvAlertaId", SqlDbType.VarChar, objParametros.vAlertaId);
                        objDatabase.AgregarParametro(ref cmd, "@pvGuiaAerea", SqlDbType.VarChar, objParametros.vGuiaAerea);
                        objDatabase.AgregarParametro(ref cmd, "@pvRMA", SqlDbType.VarChar, objParametros.vRMA);
                        objDatabase.AgregarParametro(ref cmd, "@pvOrdExt", SqlDbType.VarChar, objParametros.vOrdExt);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPerOperador", SqlDbType.Int, objParametros.iCodPerOperador);
                        objDatabase.AgregarParametro(ref cmd, "@pvFacCom", SqlDbType.VarChar, objParametros.vFacCom);
                        objDatabase.AgregarParametro(ref cmd, "@ptiCodCan", SqlDbType.SmallInt, objParametros.tiCodCan);
                        objDatabase.AgregarParametro(ref cmd, "@ptiCodCanAct", SqlDbType.SmallInt, objParametros.tiCodCanAct);
                        objDatabase.AgregarParametro(ref cmd, "@pbConValorComercial", SqlDbType.Bit, objParametros.bConValorComercial);
                        objDatabase.AgregarParametro(ref cmd, "@pvDUA", SqlDbType.VarChar, objParametros.vDUA);
                        objDatabase.AgregarParametro(ref cmd, "@psdFechaETA", SqlDbType.SmallDateTime, objParametros.sdFechaETA);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPartner", SqlDbType.Int, objParametros.iCodPartner);
                        objDatabase.AgregarParametro(ref cmd, "@pbTuvoAjuste", SqlDbType.Bit, objParametros.bTuvoAjuste);
                        objDatabase.AgregarParametro(ref cmd, "@pbCarga", SqlDbType.Bit, objParametros.bCarga);
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMovExt", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piCodCabMovExt"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que elimina Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la Descripcion Base.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarMovimientoExt(COM_CabMovArt_Ext objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabMovArt_Ext_Eliminar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, objParametros.iCodCabMov);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

    }
}
