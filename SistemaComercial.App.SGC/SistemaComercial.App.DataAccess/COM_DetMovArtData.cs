﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.Utility;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class COM_DetMovArtData
    {
        /// <summary>Invoca al Procedimiento DetalleMovimientoado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<DetalleMovimientoListaDto> ListaDetalleMovimiento(int CodigoMov)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<DetalleMovimientoListaDto> ListaDetalleMovimiento = new List<DetalleMovimientoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_DetMovArt_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, CodigoMov);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaDetalleMovimiento.Add(new DetalleMovimientoListaDto
                                {
                                    Codigo = Convert.ToInt32(dr["iCodDetMov"]),
                                    Articulo = Convert.ToString(dr["Descripcion"]),
                                    CodigoOriginal = Convert.ToString(dr["vCodigoOriginal"]),
                                    CodigoAlterno = Convert.ToString(dr["vCodigoAlterno"]),
                                    UnidadMedida = Convert.ToString(dr["UnidadMedida"]),
                                    Marca = Convert.ToString(dr["Persona"]),
                                    Serie = Funciones.Check.Cadena(dr["vSerie"]),
                                    FLPN = Funciones.Check.Cadena(dr["vFLPN"]),
                                    AlertaId = Funciones.Check.Cadena(dr["vAlertaId"]),
                                    Peso = Funciones.Check.Decimal(dr["dPeso"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaDetalleMovimiento;
        }

        /// <summary>Invoca al Procedimiento DetalleMovimientoado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<COM_DetMovArt> ListaDetalleMovimiento_PorCab(int CodigoMov)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<COM_DetMovArt> ListaDetalleMovimiento = new List<COM_DetMovArt>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_DetMovArt_Listar_PorCab");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, CodigoMov);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaDetalleMovimiento.Add(new COM_DetMovArt
                                {
                                    iCodArt = Convert.ToInt32(dr["iCodArt"]),
                                    iCodCabMov = Convert.ToInt32(dr["iCodCabMov"]),
                                    iCodDetMov = Convert.ToInt32(dr["iCodDetMov"]),
                                    vSerie = Convert.ToString(dr["vSerie"]),
                                    iCodDetMovIng = Funciones.Check.Int32Null(dr["iCodDetMovIng"]),
                                    dCosto = Funciones.Check.Decimal(dr["dCosto"]),
                                    vNombreProducto = Funciones.Check.Cadena(dr["vNombreProducto"]),
                                    vLFPN = Funciones.Check.Cadena(dr["vFLPN"]),
                                    vAlertaId = Funciones.Check.Cadena(dr["vAlertaId"]),
                                    dPeso = Funciones.Check.Decimal(dr["dPeso"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaDetalleMovimiento;
        }

        /// <summary>Invoca al Procedimiento DetalleMovimientoado que lista Descripcion Base.</summary>
        /// <param name="idDetalleMovimiento">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static COM_DetMovArt ObtenerDetalleMovimiento(int idDetalleMovimiento)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_DetMovArt_Obtener");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodDetMov", SqlDbType.Int, idDetalleMovimiento);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new COM_DetMovArt
                                {
                                    iCodArt = Convert.ToInt32(dr["iCodArt"]),
                                    iCodCabMov = Convert.ToInt32(dr["iCodCabMov"]),
                                    iCodDetMov = Convert.ToInt32(dr["iCodDetMov"]),
                                    vSerie = Convert.ToString(dr["vSerie"]),
                                    iCodDetMovIng = Funciones.Check.Int32Null(dr["iCodDetMovIng"]),
                                    dCosto = Funciones.Check.Decimal(dr["dCosto"]),
                                    vNombreProducto = Funciones.Check.Cadena(dr["vNombreProducto"]),
                                    vLFPN = Funciones.Check.Cadena(dr["vFLPN"]),
                                    vAlertaId = Funciones.Check.Cadena(dr["vAlertaId"]),
                                    dPeso = Funciones.Check.Decimal(dr["dPeso"]),
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Método que registra Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarDetalleMovimiento(COM_DetMovArt objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_DetMovArt_Insertar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, objParametros.iCodCabMov);
                        objDatabase.AgregarParametro(ref cmd, "@piCodArt", SqlDbType.Int, objParametros.iCodArt);
                        objDatabase.AgregarParametro(ref cmd, "@pvSerie", SqlDbType.VarChar, objParametros.vSerie);
                        objDatabase.AgregarParametro(ref cmd, "@piCodDetMovIng", SqlDbType.Int, objParametros.iCodDetMovIng);
                        objDatabase.AgregarParametro(ref cmd, "@pvNombreProducto", SqlDbType.VarChar, objParametros.vNombreProducto);
                        objDatabase.AgregarParametro(ref cmd, "@pdCosto", SqlDbType.Decimal, objParametros.dCosto);
                        objDatabase.AgregarParametro(ref cmd, "@pvAlertaId", SqlDbType.VarChar, objParametros.vAlertaId);
                        objDatabase.AgregarParametro(ref cmd, "@pvFLPN", SqlDbType.VarChar, objParametros.vLFPN);
                        objDatabase.AgregarParametro(ref cmd, "@pdPeso", SqlDbType.Decimal, objParametros.dPeso);
                        objDatabase.AgregarParametro(ref cmd, "@piCodDetMov", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piCodDetMov"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que Actualiza maestro de DetalleMovimiento.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarDetalleMovimiento(COM_DetMovArt objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_DetMovArt_Actualizar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodDetMov", SqlDbType.Int, objParametros.iCodDetMov);
                        objDatabase.AgregarParametro(ref cmd, "@piCodArt", SqlDbType.Int, objParametros.iCodArt);
                        objDatabase.AgregarParametro(ref cmd, "@pvSerie", SqlDbType.VarChar, objParametros.vSerie);
                        objDatabase.AgregarParametro(ref cmd, "@pdCosto", SqlDbType.Decimal, objParametros.dCosto);
                        objDatabase.AgregarParametro(ref cmd, "@pvAlertaId", SqlDbType.VarChar, objParametros.vAlertaId);
                        objDatabase.AgregarParametro(ref cmd, "@pvFLPN", SqlDbType.VarChar, objParametros.vLFPN);
                        objDatabase.AgregarParametro(ref cmd, "@pdPeso", SqlDbType.Decimal, objParametros.dPeso);
                        objDatabase.AgregarParametro(ref cmd, "@pvNombreProducto", SqlDbType.VarChar, objParametros.vNombreProducto);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que elimina Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la Descripcion Base.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarDetalleMovimiento(COM_DetMovArt objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_DetMovArt_Eliminar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodDetMov", SqlDbType.SmallInt, objParametros.iCodDetMov);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaExiste(COM_DetMovArt objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_DetMovArt_ValidaExiste");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, objParametros.iCodCabMov);
                        objDatabase.AgregarParametro(ref cmd, "@piCodArt", SqlDbType.Int, objParametros.iCodArt);
                        objDatabase.AgregarParametro(ref cmd, "@pvSerie", SqlDbType.VarChar, objParametros.vSerie);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaEsDiferenteEstado(int iCodCabMov, int iCodEstArt)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_DetMovArt_ValidaProductoEsDiferenteEstado");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, iCodCabMov);
                        objDatabase.AgregarParametro(ref cmd, "@piCodEstArt", SqlDbType.Int, iCodEstArt);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));
                        iResultado = 0;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaSalio(int iCodDetMov)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_DetMovArt_ValidaSalio");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodDetMov", SqlDbType.Int, iCodDetMov);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Invoca al Procedimiento Articuloado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<GenericoListaDto> ListaArticulosParaSalida_Auto(string filtro)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<GenericoListaDto> ListaArticulo = new List<GenericoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_DetMovArt_ListaParaSalida_Autocomplete");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, filtro);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaArticulo.Add(new GenericoListaDto
                                {
                                    Codigo = Convert.ToString(dr["iCodDetMov"]),
                                    Descripcion = Convert.ToString(dr["vCodigoOriginal"]) + " - " + Convert.ToString(dr["vDescripcion"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaArticulo;
        }

        /// <summary>Invoca al Procedimiento DetalleMovimientoado que lista Descripcion Base.</summary>
        /// <param name="idDetalleMovimiento">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static DetalleMovimientoListaDto ObtenerDetalleMovimientoParaSalida(int idDetalleMovimiento)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_DetMovArt_ObtenerParaMovSal");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodDetMov", SqlDbType.Int, idDetalleMovimiento);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new DetalleMovimientoListaDto
                                {
                                    Almacen = Convert.ToString(dr["Almacen"]),
                                    Ubicacion = string.Format("{0} - {1} - {2} - {3}", Funciones.Check.Cadena(dr["tiPiso"]), Funciones.Check.Cadena(dr["vRack"]), Funciones.Check.Cadena(dr["vFila"]), Funciones.Check.Cadena(dr["vColumna"])),
                                    EstadoArticulo = Convert.ToString(dr["EstadoArt"]),
                                    Serie = Convert.ToString(dr["vSerie"]),
                                    CostoIngreso = Convert.ToDecimal(dr["dCostoIngreso"]),
                                    NombreProducto = Convert.ToString(dr["vNombreProducto"])
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }
    }
}
