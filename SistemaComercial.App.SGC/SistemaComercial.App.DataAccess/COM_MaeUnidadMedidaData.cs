﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.Utility;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class COM_MaeUnidadMedidaData
    {
        /// <summary>Invoca al Procedimiento UnidadMedidaado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<UnidadMedidaListaDto> ListaUnidadMedida(UnidadMedidaFiltroDto objFiltro)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<UnidadMedidaListaDto> ListaUnidadMedida = new List<UnidadMedidaListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeUnidadMedida_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objFiltro.Descripcion);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaUnidadMedida.Add(new UnidadMedidaListaDto
                                {
                                    Codigo = Convert.ToInt16(dr["siCodUMed"]),
                                    Descripcion = Convert.ToString(dr["vDescripcion"]),
                                    Abreviatura = Convert.ToString(dr["vAbreviatura"]),
                                    UsuarioCrea = Convert.ToString(dr["UsuCrea"]),
                                    UsuarioActualiza = Funciones.Check.Cadena(dr["UsuActualiza"]),
                                    FechaCrea = Funciones.Check.FechaCorta(dr["sdFechaCre"]),
                                    FechaActualiza = Funciones.Check.FechaCorta(dr["sdFechaAct"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaUnidadMedida;
        }

        /// <summary>Invoca al Procedimiento UnidadMedidaado que lista Descripcion Base.</summary>
        /// <param name="idUnidadMedida">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static COM_MaeUnidadMedida ObtenerUnidadMedida(int idUnidadMedida)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeUnidadMedida_Obtener");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUMed", SqlDbType.SmallInt, idUnidadMedida);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new COM_MaeUnidadMedida
                                {
                                    siCodUMed = Convert.ToInt16(dr["siCodUMed"]),
                                    vDescripcion = Convert.ToString(dr["vDescripcion"]),
                                    vAbreviatura = Convert.ToString(dr["vAbreviatura"]),
                                    siCodUsuAct = Funciones.Check.Int16Null(dr["siCodUsuAct"]),
                                    siCodUsuCre = Convert.ToInt16(dr["siCodUsuCre"]),
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Método que registra Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarUnidadMedida(COM_MaeUnidadMedida objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeUnidadMedida_Insertar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objParametros.vDescripcion);
                        objDatabase.AgregarParametro(ref cmd, "@pvAbreviatura", SqlDbType.VarChar, objParametros.vAbreviatura);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuCre", SqlDbType.SmallInt, objParametros.siCodUsuCre);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUMed", SqlDbType.SmallInt, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@psiCodUMed"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que Actualiza maestro de UnidadMedida.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarUnidadMedida(COM_MaeUnidadMedida objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeUnidadMedida_Actualizar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUMed", SqlDbType.SmallInt, objParametros.siCodUMed);
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objParametros.vDescripcion);
                        objDatabase.AgregarParametro(ref cmd, "@pvAbreviatura", SqlDbType.VarChar, objParametros.vAbreviatura);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que elimina Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la Descripcion Base.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarUnidadMedida(COM_MaeUnidadMedida objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeUnidadMedida_Eliminar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUMed", SqlDbType.SmallInt, objParametros.siCodUMed);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaExiste(COM_MaeUnidadMedida objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeUnidadMedida_ValidaExiste");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objParametros.vDescripcion);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Invoca al Procedimiento UnidadMedidaado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<GenericoListaDto> ListaUnidadMedida_Combo()
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<GenericoListaDto> ListaUnidadMedida = new List<GenericoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeUnidadMedida_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, string.Empty);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaUnidadMedida.Add(new GenericoListaDto
                                {
                                    Codigo = Convert.ToString(dr["siCodUMed"]),
                                    Descripcion = Convert.ToString(dr["vDescripcion"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaUnidadMedida;
        }
    }
}
