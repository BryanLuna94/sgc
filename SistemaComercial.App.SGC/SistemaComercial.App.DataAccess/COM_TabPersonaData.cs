﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.Utility;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class COM_TabPersonaData
    {
        /// <summary>Invoca al Procedimiento Personaado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<PersonaListaDto> ListaPersonas(PersonaFiltroDto objFiltro)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<PersonaListaDto> ListaPersona = new List<PersonaListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvPersona", SqlDbType.VarChar, objFiltro.Persona);
                        objDatabase.AgregarParametro(ref cmd, "@pvNroDocIde", SqlDbType.VarChar, objFiltro.NroDocumento);
                        objDatabase.AgregarParametro(ref cmd, "@pvPais", SqlDbType.VarChar, objFiltro.Pais);
                        objDatabase.AgregarParametro(ref cmd, "@pbPartner", SqlDbType.Bit, objFiltro.EsPartner);
                        objDatabase.AgregarParametro(ref cmd, "@pbCliente", SqlDbType.Bit, objFiltro.EsCliente);
                        objDatabase.AgregarParametro(ref cmd, "@pbProveedor", SqlDbType.Bit, objFiltro.EsProveedor);
                        objDatabase.AgregarParametro(ref cmd, "@pbDistribuidor", SqlDbType.Bit, objFiltro.EsDistribuidor);
                        objDatabase.AgregarParametro(ref cmd, "@pbExportador", SqlDbType.Bit, objFiltro.EsExportador);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaPersona.Add(new PersonaListaDto
                                {
                                    Codigo = Convert.ToInt32(dr["iCodPer"]),
                                    NroDocumento = Convert.ToString(dr["vNroDocIde"]),
                                    NombreComercial = Convert.ToString(dr["vNombreComercial"]),
                                    RazonSocial = Convert.ToString(dr["Persona"]),
                                    UsuarioCrea = Convert.ToString(dr["UsuCrea"]),
                                    UsuarioActualiza = Funciones.Check.Cadena(dr["UsuActualiza"]),
                                    FechaCrea = Funciones.Check.FechaCorta(dr["sdFechaCre"]),
                                    FechaActualiza = Funciones.Check.FechaCorta(dr["sdFechaAct"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaPersona;
        }

        /// <summary>Invoca al Procedimiento Personaado que lista Descripcion Base.</summary>
        /// <param name="idPersona">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static COM_TabPersona ObtenerPersona(int idPersona)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Obtener");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.SmallInt, idPersona);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new COM_TabPersona
                                {
                                    iCodPer = Convert.ToInt16(dr["iCodPer"]),
                                    vNumero = Convert.ToString(dr["vNumero"]),
                                    vRazonSocial = Funciones.Check.Cadena(dr["vRazonSocial"]),
                                    bAgenteRetenedor = Convert.ToBoolean(dr["bAgenteRetenedor"]),
                                    bBuenContribuyente = Convert.ToBoolean(dr["bBuenContribuyente"]),
                                    bCliente = Convert.ToBoolean(dr["bCliente"]),
                                    bDistribuidor = Convert.ToBoolean(dr["bDistribuidor"]),
                                    bExportador = Convert.ToBoolean(dr["bExportador"]),
                                    bPartner = Convert.ToBoolean(dr["bPartner"]),
                                    bProveedor = Convert.ToBoolean(dr["bProveedor"]),
                                    siCodPais = Funciones.Check.Int16(dr["siCodPais"]),
                                    tiCodTipDocIde = Convert.ToByte(dr["tiCodTipDocIde"]),
                                    vApMaterno = Funciones.Check.Cadena(dr["vApMaterno"]),
                                    vApPaterno = Funciones.Check.Cadena(dr["vApPaterno"]),
                                    vNombreComercial = Funciones.Check.Cadena(dr["vNombreComercial"]),
                                    vNombres = Funciones.Check.Cadena(dr["vNombres"]),
                                    vNroDocIde = Funciones.Check.Cadena(dr["vNroDocIde"]),
                                    vPaginaWeb = Funciones.Check.Cadena(dr["vPaginaWeb"]),
                                    siCodUsuAct = Funciones.Check.Int16Null(dr["siCodUsuAct"]),
                                    siCodUsuCre = Convert.ToInt16(dr["siCodUsuCre"]),
                                    sdFecNac = Funciones.Check.Datetime(dr["sdFecNac"])
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Método que registra Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarPersona(COM_TabPersona objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Insertar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@ptiCodTipDocIde", SqlDbType.TinyInt, objParametros.tiCodTipDocIde);
                        objDatabase.AgregarParametro(ref cmd, "@pvNroDocIde", SqlDbType.VarChar, objParametros.vNroDocIde);
                        objDatabase.AgregarParametro(ref cmd, "@pvRazonSocial", SqlDbType.VarChar, objParametros.vRazonSocial);
                        objDatabase.AgregarParametro(ref cmd, "@pvApPaterno", SqlDbType.VarChar, objParametros.vApPaterno);
                        objDatabase.AgregarParametro(ref cmd, "@pvApMaterno", SqlDbType.VarChar, objParametros.vApMaterno);
                        objDatabase.AgregarParametro(ref cmd, "@pvNombres", SqlDbType.VarChar, objParametros.vNombres);
                        objDatabase.AgregarParametro(ref cmd, "@pvNombreComercial", SqlDbType.VarChar, objParametros.vNombreComercial);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuCre", SqlDbType.SmallInt, objParametros.siCodUsuCre);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodPais", SqlDbType.SmallInt, objParametros.siCodPais);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumero", SqlDbType.VarChar, objParametros.vNumero);
                        objDatabase.AgregarParametro(ref cmd, "@pvPaginaWeb", SqlDbType.VarChar, objParametros.vPaginaWeb);
                        objDatabase.AgregarParametro(ref cmd, "@pbPartner", SqlDbType.Bit, objParametros.bPartner);
                        objDatabase.AgregarParametro(ref cmd, "@pbCliente", SqlDbType.Bit, objParametros.bCliente);
                        objDatabase.AgregarParametro(ref cmd, "@pbProveedor", SqlDbType.Bit, objParametros.bProveedor);
                        objDatabase.AgregarParametro(ref cmd, "@pbDistribuidor", SqlDbType.Bit, objParametros.bDistribuidor);
                        objDatabase.AgregarParametro(ref cmd, "@pbAgenteRetenedor", SqlDbType.Bit, objParametros.bAgenteRetenedor);
                        objDatabase.AgregarParametro(ref cmd, "@pbBuenContribuyente", SqlDbType.Bit, objParametros.bBuenContribuyente);
                        objDatabase.AgregarParametro(ref cmd, "@pbExportador", SqlDbType.Bit, objParametros.bExportador);
                        objDatabase.AgregarParametro(ref cmd, "@psdFecNac", SqlDbType.SmallDateTime, objParametros.sdFecNac);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piCodPer"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que Actualiza maestro de Persona.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarPersona(COM_TabPersona objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Actualizar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, objParametros.iCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@ptiCodTipDocIde", SqlDbType.TinyInt, objParametros.tiCodTipDocIde);
                        objDatabase.AgregarParametro(ref cmd, "@pvNroDocIde", SqlDbType.VarChar, objParametros.vNroDocIde);
                        objDatabase.AgregarParametro(ref cmd, "@pvRazonSocial", SqlDbType.VarChar, objParametros.vRazonSocial);
                        objDatabase.AgregarParametro(ref cmd, "@pvApPaterno", SqlDbType.VarChar, objParametros.vApPaterno);
                        objDatabase.AgregarParametro(ref cmd, "@pvApMaterno", SqlDbType.VarChar, objParametros.vApMaterno);
                        objDatabase.AgregarParametro(ref cmd, "@pvNombres", SqlDbType.VarChar, objParametros.vNombres);
                        objDatabase.AgregarParametro(ref cmd, "@pvNombreComercial", SqlDbType.VarChar, objParametros.vNombreComercial);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodPais", SqlDbType.SmallInt, objParametros.siCodPais);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumero", SqlDbType.VarChar, objParametros.vNumero);
                        objDatabase.AgregarParametro(ref cmd, "@pvPaginaWeb", SqlDbType.VarChar, objParametros.vPaginaWeb);
                        objDatabase.AgregarParametro(ref cmd, "@pbPartner", SqlDbType.Bit, objParametros.bPartner);
                        objDatabase.AgregarParametro(ref cmd, "@pbCliente", SqlDbType.Bit, objParametros.bCliente);
                        objDatabase.AgregarParametro(ref cmd, "@pbProveedor", SqlDbType.Bit, objParametros.bProveedor);
                        objDatabase.AgregarParametro(ref cmd, "@pbDistribuidor", SqlDbType.Bit, objParametros.bDistribuidor);
                        objDatabase.AgregarParametro(ref cmd, "@pbAgenteRetenedor", SqlDbType.Bit, objParametros.bAgenteRetenedor);
                        objDatabase.AgregarParametro(ref cmd, "@pbBuenContribuyente", SqlDbType.Bit, objParametros.bBuenContribuyente);
                        objDatabase.AgregarParametro(ref cmd, "@pbExportador", SqlDbType.Bit, objParametros.bExportador);
                        objDatabase.AgregarParametro(ref cmd, "@psdFecNac", SqlDbType.SmallDateTime, objParametros.sdFecNac);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que elimina Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la Descripcion Base.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarPersona(COM_TabPersona objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Eliminar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, objParametros.iCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaExiste(COM_TabPersona objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_ValidaExiste");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@ptiCodTipDocIde", SqlDbType.TinyInt, objParametros.tiCodTipDocIde);
                        objDatabase.AgregarParametro(ref cmd, "@pvNroDocIde", SqlDbType.VarChar, objParametros.vNroDocIde);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaTieneMovimientos(int iCodPer)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_ValidaTieneMovimientos");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, iCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaTieneMovimientosExt(int iCodPer)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_ValidaTieneMovimientosExt");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, iCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaEsMarca(int iCodPer)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_ValidaEsMarca");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, iCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Invoca al Procedimiento Personaado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<GenericoListaDto> ListaEmpresas_Auto(PersonaFiltroDto entidad)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<GenericoListaDto> ListaPersona = new List<GenericoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Listar_Auto");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvPersona", SqlDbType.VarChar, entidad.Persona);
                        objDatabase.AgregarParametro(ref cmd, "@pbPartner", SqlDbType.Bit, entidad.EsPartner);
                        objDatabase.AgregarParametro(ref cmd, "@pbCliente", SqlDbType.Bit, entidad.EsCliente);
                        objDatabase.AgregarParametro(ref cmd, "@pbProveedor", SqlDbType.Bit, entidad.EsProveedor);
                        objDatabase.AgregarParametro(ref cmd, "@pbDistribuidor", SqlDbType.Bit, entidad.EsDistribuidor);
                        objDatabase.AgregarParametro(ref cmd, "@pbExportador", SqlDbType.Bit, entidad.EsExportador);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaPersona.Add(new GenericoListaDto
                                {
                                    Codigo = Convert.ToString(dr["iCodPer"]),
                                    Descripcion = Convert.ToString(dr["vNroDocIde"]) + " - " + Convert.ToString(dr["Persona"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaPersona;
        }

        /// <summary>Invoca al Procedimiento Personaado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<GenericoListaDto> ListaEmpresas_Combo(PersonaFiltroDto entidad)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<GenericoListaDto> ListaPersona = new List<GenericoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_TabPersona_Listar_Combo");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pbPartner", SqlDbType.Bit, entidad.EsPartner);
                        objDatabase.AgregarParametro(ref cmd, "@pbCliente", SqlDbType.Bit, entidad.EsCliente);
                        objDatabase.AgregarParametro(ref cmd, "@pbProveedor", SqlDbType.Bit, entidad.EsProveedor);
                        objDatabase.AgregarParametro(ref cmd, "@pbDistribuidor", SqlDbType.Bit, entidad.EsDistribuidor);
                        objDatabase.AgregarParametro(ref cmd, "@pbExportador", SqlDbType.Bit, entidad.EsExportador);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaPersona.Add(new GenericoListaDto
                                {
                                    Codigo = Convert.ToString(dr["iCodPer"]),
                                    Descripcion = Convert.ToString(dr["vNroDocIde"]) + " - " + Convert.ToString(dr["Persona"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaPersona;
        }
    }
}
