﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.Utility;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class GEN_TabOpcionData
    {
        /// <summary>Invoca al Procedimiento almacenado que lista Secciones.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Secciones</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Reusp_>Reusp_onsable.</Reusp_></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<OpcionListaDto> ListaOpcionesPorModulo(int idModulo)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<OpcionListaDto> lista = new List<OpcionListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_GEN_TabOpcion_ListarPorModulo");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodOpcModulo", SqlDbType.Int, idModulo);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                lista.Add(new OpcionListaDto
                                {
                                    Nombre = dr["vNombre"].ToString(),
                                    Codigo = Convert.ToInt32(dr["iCodOpc"]),
                                    CodigoPadre = Convert.ToInt32(dr["iCodOpcPadre"]),
                                    Orden = Convert.ToInt16(dr["siOrden"]),
                                    Ruta = Convert.ToString(dr["vRuta"]),
                                    Clase = Convert.ToString(dr["vClase"]),
                                    TipoOpcion = Convert.ToInt32(dr["iCodTipOpc"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return lista;
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Secciones.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Secciones</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Reusp_>Reusp_onsable.</Reusp_></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<OpcionListaDto> ListaOpcionesPorPerfil(short idPerfil)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<OpcionListaDto> lista = new List<OpcionListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_GEN_TabOpcion_ListarPorPerfil");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodPer", SqlDbType.SmallInt, idPerfil);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                lista.Add(new OpcionListaDto
                                {
                                    Nombre = dr["vNombre"].ToString(),
                                    Codigo = Convert.ToInt32(dr["iCodOpc"]),
                                    CodigoPadre = Convert.ToInt32(dr["iCodOpcPadre"]),
                                    Orden = Convert.ToInt16(dr["siOrden"]),
                                    Ruta = Convert.ToString(dr["vRuta"]),
                                    Clase = Convert.ToString(dr["vClase"]),
                                    TipoOpcion = Convert.ToInt32(dr["iCodTipOpc"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return lista;
        }
    }
}
