﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.Utility;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class COM_MaeAlmacen_UbicacionData
    {
        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<AlmacenUbicacionListaDto> ListaUbicaciones(AlmacenUbicacionFiltroDto objFiltro)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<AlmacenUbicacionListaDto> ListaAlmacen = new List<AlmacenUbicacionListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Ubicacion_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodAlm", SqlDbType.SmallInt, objFiltro.CodigoAlmacen);
                        objDatabase.AgregarParametro(ref cmd, "@ptiPiso", SqlDbType.TinyInt, objFiltro.Piso);
                        objDatabase.AgregarParametro(ref cmd, "@pvRack", SqlDbType.VarChar, objFiltro.Rack);
                        objDatabase.AgregarParametro(ref cmd, "@pvColumna", SqlDbType.VarChar, objFiltro.Fila);
                        objDatabase.AgregarParametro(ref cmd, "@pvFila", SqlDbType.VarChar, objFiltro.Columna);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaAlmacen.Add(new AlmacenUbicacionListaDto
                                {
                                    Codigo = Convert.ToInt16(dr["iCodUbi"]),
                                    Almacen = Funciones.Check.Cadena(dr["vDescripcion"]),
                                    Columna = Funciones.Check.Cadena(dr["vColumna"]),
                                    Fila = Funciones.Check.Cadena(dr["vFila"]),
                                    Piso = Convert.ToByte(dr["tiPiso"]),
                                    Rack = Funciones.Check.Cadena(dr["vRack"]),
                                    Volumen = Funciones.Check.Cadena("dVolumen") + " " + Funciones.Check.Cadena("AbreUmed"),
                                    UsuarioCrea = Funciones.Check.Cadena(dr["UsuCrea"]),
                                    UsuarioActualiza = Funciones.Check.Cadena(dr["UsuActualiza"]),
                                    FechaCrea = Funciones.Check.FechaCorta(dr["sdFechaCre"]),
                                    FechaActualiza = Funciones.Check.FechaCorta(dr["sdFechaAct"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaAlmacen;
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="idAlmacen">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static COM_MaeAlmacen_Ubicacion ObtenerUbicacionAlmacen(int idUbicacion)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Ubicacion_Obtener");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodUbi", SqlDbType.SmallInt, idUbicacion);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new COM_MaeAlmacen_Ubicacion
                                {
                                    siCodAlm = Convert.ToInt16(dr["siCodAlm"]),
                                    iCodUbi = Convert.ToInt32(dr["iCodUbi"]),
                                    tiPiso = Convert.ToByte(dr["tiPiso"]),
                                    vColumna = Convert.ToString(dr["vColumna"]),
                                    vFila = Convert.ToString(dr["vFila"]),
                                    vRack = Convert.ToString(dr["vRack"]),
                                    siCodUsuAct = Funciones.Check.Int16Null(dr["siCodUsuAct"]),
                                    siCodUsuCre = Convert.ToInt16(dr["siCodUsuCre"]),
                                    dVolumen = Convert.ToDecimal(dr["dVolumen"]),
                                    siCodUMed = Funciones.Check.Int16Null(dr["siCodUMed"])
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Método que registra Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarUbicacionAlmacen(COM_MaeAlmacen_Ubicacion objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Ubicacion_Insertar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodAlm", SqlDbType.SmallInt, objParametros.siCodAlm);
                        objDatabase.AgregarParametro(ref cmd, "@ptiPiso", SqlDbType.TinyInt, objParametros.tiPiso);
                        objDatabase.AgregarParametro(ref cmd, "@pvRack", SqlDbType.VarChar, objParametros.vRack);
                        objDatabase.AgregarParametro(ref cmd, "@pvColumna", SqlDbType.VarChar, objParametros.vColumna);
                        objDatabase.AgregarParametro(ref cmd, "@pvFila", SqlDbType.VarChar, objParametros.vFila);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuCre", SqlDbType.SmallInt, objParametros.siCodUsuCre);
                        objDatabase.AgregarParametro(ref cmd, "@pdVolumen", SqlDbType.Decimal, objParametros.dVolumen);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUMed", SqlDbType.SmallInt, objParametros.siCodUMed);
                        objDatabase.AgregarParametro(ref cmd, "@piCodUbi", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@psiCodAlm"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que Actualiza maestro de Almacen.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarUbicacionAlmacen(COM_MaeAlmacen_Ubicacion objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Ubicacion_Actualizar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodUbi", SqlDbType.Int, objParametros.iCodUbi);
                        objDatabase.AgregarParametro(ref cmd, "@ptiPiso", SqlDbType.TinyInt, objParametros.tiPiso);
                        objDatabase.AgregarParametro(ref cmd, "@pvRack", SqlDbType.VarChar, objParametros.vRack);
                        objDatabase.AgregarParametro(ref cmd, "@pvColumna", SqlDbType.VarChar, objParametros.vColumna);
                        objDatabase.AgregarParametro(ref cmd, "@pvFila", SqlDbType.VarChar, objParametros.vFila);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.AgregarParametro(ref cmd, "@pdVolumen", SqlDbType.Decimal, objParametros.dVolumen);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUMed", SqlDbType.SmallInt, objParametros.siCodUMed);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que elimina Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la Descripcion Base.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarUbicacionAlmacen(COM_MaeAlmacen_Ubicacion objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Ubicacion_Eliminar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodUbi", SqlDbType.Int, objParametros.iCodUbi);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaExiste(COM_MaeAlmacen_Ubicacion objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Ubicacion_ValidaExiste");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodAlm", SqlDbType.SmallInt, objParametros.siCodAlm);
                        objDatabase.AgregarParametro(ref cmd, "@ptiPiso", SqlDbType.TinyInt, objParametros.tiPiso);
                        objDatabase.AgregarParametro(ref cmd, "@pvRack", SqlDbType.VarChar, objParametros.vRack);
                        objDatabase.AgregarParametro(ref cmd, "@pvColumna", SqlDbType.VarChar, objParametros.vColumna);
                        objDatabase.AgregarParametro(ref cmd, "@pvFila", SqlDbType.VarChar, objParametros.vFila);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<GenericoListaDto> ListaUbicaciones_Combo(short idAlmacen)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<GenericoListaDto> ListaAlmacen = new List<GenericoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Ubicacion_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodAlm", SqlDbType.SmallInt, idAlmacen);
                        objDatabase.AgregarParametro(ref cmd, "@ptiPiso", SqlDbType.TinyInt, 0);
                        objDatabase.AgregarParametro(ref cmd, "@pvRack", SqlDbType.VarChar, string.Empty);
                        objDatabase.AgregarParametro(ref cmd, "@pvColumna", SqlDbType.VarChar, string.Empty);
                        objDatabase.AgregarParametro(ref cmd, "@pvFila", SqlDbType.VarChar, string.Empty);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaAlmacen.Add(new GenericoListaDto
                                {
                                    Codigo = Convert.ToString(dr["iCodUbi"]),
                                    Descripcion = string.Format("{0} - {1} - {2} - {3}", Funciones.Check.Cadena(dr["tiPiso"]), Funciones.Check.Cadena(dr["vRack"]), Funciones.Check.Cadena(dr["vFila"]), Funciones.Check.Cadena(dr["vColumna"]))
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaAlmacen;
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<GenericoListaDto> ListaUbicaciones_Pisos_Combo(short idAlmacen)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<GenericoListaDto> ListaAlmacen = new List<GenericoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Ubicacion_Listar_Pisos");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodAlm", SqlDbType.SmallInt, idAlmacen);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaAlmacen.Add(new GenericoListaDto
                                {
                                    Codigo = Convert.ToString(dr["tiPiso"]),
                                    Descripcion = Funciones.Check.Cadena(dr["tiPiso"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaAlmacen;
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<GenericoListaDto> ListaUbicaciones_Racks_Combo(COM_MaeAlmacen_Ubicacion objFiltro)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<GenericoListaDto> ListaAlmacen = new List<GenericoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Ubicacion_ListarRacks_PorPiso");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodAlm", SqlDbType.SmallInt, objFiltro.siCodAlm);
                        objDatabase.AgregarParametro(ref cmd, "@ptiPiso", SqlDbType.TinyInt, objFiltro.tiPiso);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaAlmacen.Add(new GenericoListaDto
                                {
                                    Codigo = Convert.ToString(dr["vRack"]),
                                    Descripcion = Funciones.Check.Cadena(dr["vRack"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaAlmacen;
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<GenericoListaDto> ListaUbicaciones_Combo_PorRack(COM_MaeAlmacen_Ubicacion objFiltro)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<GenericoListaDto> ListaAlmacen = new List<GenericoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Ubicacion_Listar_PorRack");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodAlm", SqlDbType.SmallInt, objFiltro.siCodAlm);
                        objDatabase.AgregarParametro(ref cmd, "@ptiPiso", SqlDbType.TinyInt, objFiltro.tiPiso);
                        objDatabase.AgregarParametro(ref cmd, "@pvRack", SqlDbType.VarChar, objFiltro.vRack);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaAlmacen.Add(new GenericoListaDto
                                {
                                    Codigo = Convert.ToString(dr["iCodUbi"]),
                                    Descripcion = string.Format("{0} - {1}", Funciones.Check.Cadena(dr["vFila"]), Funciones.Check.Cadena(dr["vColumna"]))
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaAlmacen;
        }
    }
}
