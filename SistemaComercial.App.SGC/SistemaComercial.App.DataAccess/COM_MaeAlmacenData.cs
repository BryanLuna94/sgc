﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.Utility;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class COM_MaeAlmacenData
    {
        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<AlmacenListaDto> ListaAlmacenes(AlmacenFiltroDto objFiltro)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<AlmacenListaDto> ListaAlmacen = new List<AlmacenListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objFiltro.Descripcion);
                        objDatabase.AgregarParametro(ref cmd, "@pvDireccion", SqlDbType.VarChar, objFiltro.Direccion);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaAlmacen.Add(new AlmacenListaDto
                                {
                                    Codigo = Convert.ToInt16(dr["siCodAlm"]),
                                    Descripcion = Convert.ToString(dr["vDescripcion"]),
                                    Abreviatura = Convert.ToString(dr["vAbreviatura"]),
                                    Direccion = Convert.ToString(dr["vDireccion"]),
                                    UsuarioCrea = Convert.ToString(dr["UsuCrea"]),
                                    UsuarioActualiza = Funciones.Check.Cadena(dr["UsuActualiza"]),
                                    FechaCrea = Funciones.Check.FechaCorta(dr["sdFechaCre"]),
                                    FechaActualiza = Funciones.Check.FechaCorta(dr["sdFechaAct"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaAlmacen;
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="idAlmacen">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static COM_MaeAlmacen ObtenerAlmacen(int idAlmacen)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Obtener");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodAlm", SqlDbType.SmallInt, idAlmacen);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new COM_MaeAlmacen
                                {
                                    siCodAlm = Convert.ToInt16(dr["siCodAlm"]),
                                    vDescripcion = Convert.ToString(dr["vDescripcion"]),
                                    vDireccion = Convert.ToString(dr["vDireccion"]),
                                    vAbreviatura = Convert.ToString(dr["vAbreviatura"]),
                                    siCodUsuAct = Funciones.Check.Int16Null(dr["siCodUsuAct"]),
                                    siCodUsuCre = Convert.ToInt16(dr["siCodUsuCre"]),
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Método que registra Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarAlmacen(COM_MaeAlmacen objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Insertar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objParametros.vDescripcion);
                        objDatabase.AgregarParametro(ref cmd, "@pvDireccion", SqlDbType.VarChar, objParametros.vDireccion);
                        objDatabase.AgregarParametro(ref cmd, "@pvAbreviatura", SqlDbType.VarChar, objParametros.vAbreviatura);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuCre", SqlDbType.SmallInt, objParametros.siCodUsuCre);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodAlm", SqlDbType.SmallInt, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@psiCodAlm"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que Actualiza maestro de Almacen.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarAlmacen(COM_MaeAlmacen objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Actualizar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodAlm", SqlDbType.SmallInt, objParametros.siCodAlm);
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objParametros.vDescripcion);
                        objDatabase.AgregarParametro(ref cmd, "@pvDireccion", SqlDbType.VarChar, objParametros.vDireccion);
                        objDatabase.AgregarParametro(ref cmd, "@pvAbreviatura", SqlDbType.VarChar, objParametros.vAbreviatura);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que elimina Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la Descripcion Base.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void EliminarAlmacen(COM_MaeAlmacen objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Eliminar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodAlm", SqlDbType.SmallInt, objParametros.siCodAlm);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaExiste(COM_MaeAlmacen objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_ValidaExiste");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, objParametros.vDescripcion);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaTieneUbicaciones(int siCodAlm)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_ValidaTieneUbicaciones");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodAlm", SqlDbType.VarChar, siCodAlm);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaTieneStock(short siCodAlm)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_ValidaTieneStock");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodAlm", SqlDbType.VarChar, siCodAlm);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<GenericoListaDto> ListaAlmacenes_Combo()
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<GenericoListaDto> ListaAlmacen = new List<GenericoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_MaeAlmacen_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvDescripcion", SqlDbType.VarChar, string.Empty);
                        objDatabase.AgregarParametro(ref cmd, "@pvDireccion", SqlDbType.VarChar, string.Empty);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaAlmacen.Add(new GenericoListaDto
                                {
                                    Codigo = Convert.ToString(dr["siCodAlm"]),
                                    Descripcion = Convert.ToString(dr["vDescripcion"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaAlmacen;
        }
    }
}
