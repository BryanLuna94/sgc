﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.Utility;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class COM_CabFacturaData
    {
        /// <summary>Invoca al Procedimiento Movimientoado que lista Descripcion Base.</summary>
        /// <param name="idMovimiento">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static COM_CabFactura ObtenerFactura(int idMovimiento)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabFactura_Obtener");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, idMovimiento);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new COM_CabFactura
                                {
                                    iCodCabMov = Convert.ToInt32(dr["iCodCabMov"]),
                                    iCodPer = Convert.ToInt32(dr["iCodPer"]),
                                    iCodFactura = Convert.ToInt32(dr["iCodFactura"]),
                                    dTotal = Convert.ToDecimal(dr["dTotal"]),
                                    vNumero = Convert.ToString(dr["vNumero"]),
                                    vSerie = Convert.ToString(dr["vSerie"]),
                                    dImpuesto = Funciones.Check.Decimal(dr["dImpuesto"]),
                                    dOtrosCargos= Funciones.Check.Decimal(dr["dOtrosCargos"]),
                                    dSubTotal = Funciones.Check.Decimal(dr["dSubTotal"]),
                                    iMoneda= Funciones.Check.Int32(dr["iMoneda"]),
                                    iTipoDoc = Funciones.Check.Int32(dr["iTipoDoc"]),
                                    sdFecha= Funciones.Check.Datetime(dr["sdFecha"]),
                                    vDireccion = Funciones.Check.Cadena(dr["vDireccion"]),
                                    vObservacion = Funciones.Check.Cadena(dr["vObservacion"])
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Método que registra Descripcion Base.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarFactura(COM_CabFactura objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabFactura_Insertar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, objParametros.iCodCabMov);
                        objDatabase.AgregarParametro(ref cmd, "@pvSerie", SqlDbType.VarChar, objParametros.vSerie);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumero", SqlDbType.VarChar, objParametros.vNumero);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, objParametros.iCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@pdTotal", SqlDbType.Decimal, objParametros.dTotal);
                        objDatabase.AgregarParametro(ref cmd, "@pdSubTotal", SqlDbType.Decimal, objParametros.dSubTotal);
                        objDatabase.AgregarParametro(ref cmd, "@pdImpuesto", SqlDbType.Decimal, objParametros.dImpuesto);
                        objDatabase.AgregarParametro(ref cmd, "@pdOtrosCargos", SqlDbType.Decimal, objParametros.dOtrosCargos);
                        objDatabase.AgregarParametro(ref cmd, "@piTipoDoc", SqlDbType.Int, objParametros.iTipoDoc);
                        objDatabase.AgregarParametro(ref cmd, "@piMoneda", SqlDbType.Int, objParametros.iMoneda);
                        objDatabase.AgregarParametro(ref cmd, "@psdFecha", SqlDbType.SmallDateTime, objParametros.sdFecha);
                        objDatabase.AgregarParametro(ref cmd, "@pvObservacion", SqlDbType.VarChar, objParametros.vObservacion);
                        objDatabase.AgregarParametro(ref cmd, "@pvDireccion", SqlDbType.VarChar, objParametros.vDireccion);
                        objDatabase.AgregarParametro(ref cmd, "@piCodFactura", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piCodFactura"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que Actualiza maestro de Movimiento.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Julio Luna</CreadoPor></item>
        /// <item><FecCrea>06/05/2018</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static void ActualizarFactura(COM_CabFactura objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabFactura_Actualizar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodCabMov", SqlDbType.Int, objParametros.iCodCabMov);
                        objDatabase.AgregarParametro(ref cmd, "@pvSerie", SqlDbType.VarChar, objParametros.vSerie);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumero", SqlDbType.VarChar, objParametros.vNumero);
                        objDatabase.AgregarParametro(ref cmd, "@piCodPer", SqlDbType.Int, objParametros.iCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@pdTotal", SqlDbType.VarChar, objParametros.dTotal);
                        objDatabase.AgregarParametro(ref cmd, "@pdSubTotal", SqlDbType.Decimal, objParametros.dSubTotal);
                        objDatabase.AgregarParametro(ref cmd, "@pdImpuesto", SqlDbType.Decimal, objParametros.dImpuesto);
                        objDatabase.AgregarParametro(ref cmd, "@pdOtrosCargos", SqlDbType.Decimal, objParametros.dOtrosCargos);
                        objDatabase.AgregarParametro(ref cmd, "@piTipoDoc", SqlDbType.Int, objParametros.iTipoDoc);
                        objDatabase.AgregarParametro(ref cmd, "@piMoneda", SqlDbType.Int, objParametros.iMoneda);
                        objDatabase.AgregarParametro(ref cmd, "@psdFecha", SqlDbType.SmallDateTime, objParametros.sdFecha);
                        objDatabase.AgregarParametro(ref cmd, "@pvDireccion", SqlDbType.VarChar, objParametros.vDireccion);
                        objDatabase.AgregarParametro(ref cmd, "@pvObservacion", SqlDbType.VarChar, objParametros.vObservacion);
                        objDatabase.ExecuteNonQuery(cmd);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        /// <summary>Método que valida si existe detalle stope.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaExiste(COM_CabFactura objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_COM_CabFactura_ValidaExiste");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvSerieDoc", SqlDbType.VarChar, objParametros.vSerie);
                        objDatabase.AgregarParametro(ref cmd, "@pvNumeroDoc", SqlDbType.VarChar, objParametros.vNumero);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }
    }
}
