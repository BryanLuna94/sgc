﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.Utility;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class GEN_TabParametroData
    {
        /// <summary>Invoca al Procedimiento almacenado que lista problemas.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Problemas</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<GenericoListaDto> ListaParametros(int idTabla)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<GenericoListaDto> ListaGenerica = new List<GenericoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_GEN_TabParametro_ListarPorTabla");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@piCodTab", SqlDbType.Int, idTabla);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaGenerica.Add(new GenericoListaDto
                                {
                                    Codigo = Convert.ToString(dr["iCodPar"]),
                                    Descripcion = Convert.ToString(dr["vParametro"]),
                                    Descripcion2 = Convert.ToString(dr["vParametro2"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaGenerica;
        }
    }
}
