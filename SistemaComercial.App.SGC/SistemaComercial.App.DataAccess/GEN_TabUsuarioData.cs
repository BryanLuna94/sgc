﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBLV.BaseDatos;
using SistemaComercial.App.DataTypes.Listas;
using SistemaComercial.App.DataTypes.Filtros;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.Utility;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SistemaComercial.App.DataAccess
{
    public class GEN_TabUsuarioData
    {
        /// <summary>Invoca al Procedimiento almacenado que lista Nivels.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Nivels</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<GenericoListaDto> ListaUsuarios_Combo()
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<GenericoListaDto> ListaUsuarios = new List<GenericoListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_GEN_TabUsuario_Listar_Combo");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaUsuarios.Add(new GenericoListaDto
                                {
                                    Codigo = Convert.ToString(dr["siCodUsu"]),
                                    Descripcion = Convert.ToString(dr["vNombre"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaUsuarios;
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Usuarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Usuarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static List<UsuarioListaDto> ListaUsuarios(UsuarioFiltroDto objFiltro)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            List<UsuarioListaDto> ListaUsuario = new List<UsuarioListaDto>();
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_GEN_TabUsuario_Listar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvNombre", SqlDbType.VarChar, objFiltro.Usuario);
                        objDatabase.AgregarParametro(ref cmd, "@pvPerfil", SqlDbType.VarChar, objFiltro.Perfil);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                ListaUsuario.Add(new UsuarioListaDto
                                {
                                    Nombre = dr["vNombre"].ToString(),
                                    Codigo = Convert.ToInt16(dr["siCodUsu"]),
                                    Perfil = Convert.ToString(dr["Perfil"]),
                                    Usuario = Convert.ToString(dr["vUsuario"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return ListaUsuario;
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Usuarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Usuarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static GEN_TabUsuario ObtenerUsuario(short IdUsuario)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_GEN_TabUsuario_Obtener");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsu", SqlDbType.SmallInt, IdUsuario);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new GEN_TabUsuario
                                {
                                    vNombre = dr["vNombre"].ToString(),
                                    siCodPer = Convert.ToInt16(dr["siCodPer"]),
                                    vClave = Convert.ToString(dr["vClave"]),
                                    vUsuario = Convert.ToString(dr["vUsuario"]),
                                    bEliminado = Convert.ToBoolean(dr["bEliminado"]),
                                    siCodUsu = Convert.ToInt16(dr["siCodUsu"]),
                                    siCodUsuAct = Convert.ToInt16(dr["siCodUsuAct"]),
                                    vCorreo = Convert.ToString(dr["vCorreo"]),
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Usuarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Usuarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static LoginListaDto Login(string usuario)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            SqlCommand cmd;

            try
            {
                using (SqlConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_GEN_TabUsuario_Login");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvUsuario", SqlDbType.VarChar, usuario);
                        using (var dr = objDatabase.ExecuteReader(cmd))
                        {
                            while (dr.Read())
                            {
                                return new LoginListaDto
                                {
                                    Clave = Convert.ToString(dr["vClave"]),
                                    Nombre = dr["vNombre"].ToString(),
                                    CodigoPerfil = Convert.ToInt16(dr["siCodPer"]),
                                    Codigo = Convert.ToInt16(dr["siCodUsu"]),
                                    Usuario = Convert.ToString(dr["vUsuario"]),
                                    Correo = Convert.ToString(dr["vCorreo"])
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }


        /// <summary>Método que registra Usuarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Castañeda</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int RegistrarUsuario(GEN_TabUsuario objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_GEN_TabUsuario_Insertar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvNombre", SqlDbType.VarChar, objParametros.vNombre);
                        objDatabase.AgregarParametro(ref cmd, "@pvUsuario", SqlDbType.VarChar, objParametros.vUsuario);
                        objDatabase.AgregarParametro(ref cmd, "@pvClave", SqlDbType.VarChar, objParametros.vClave);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodPer", SqlDbType.SmallInt, objParametros.siCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.AgregarParametro(ref cmd, "@pvCorreo", SqlDbType.VarChar, objParametros.vCorreo);
                        objDatabase.AgregarParametro(ref cmd, "@piGrabado", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piGrabado"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que Actualiza maestro de Usuario.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Castañeda</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int ActualizarUsuario(GEN_TabUsuario objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_GEN_TabUsuario_Actualizar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsu", SqlDbType.SmallInt, objParametros.siCodUsu);
                        objDatabase.AgregarParametro(ref cmd, "@pvNombre", SqlDbType.VarChar, objParametros.vNombre);
                        objDatabase.AgregarParametro(ref cmd, "@pvUsuario", SqlDbType.VarChar, objParametros.vUsuario);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodPer", SqlDbType.SmallInt, objParametros.siCodPer);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.AgregarParametro(ref cmd, "@pvCorreo", SqlDbType.VarChar, objParametros.vCorreo);
                        objDatabase.AgregarParametro(ref cmd, "@piGrabado", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piGrabado"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que Actualiza maestro de Usuario.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Castañeda</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int CambiarClave(GEN_TabUsuario objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_GEN_TabUsuario_CambiarClave");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsu", SqlDbType.SmallInt, objParametros.siCodUsu);
                        objDatabase.AgregarParametro(ref cmd, "@pvClave", SqlDbType.VarChar, objParametros.vClave);
                        objDatabase.AgregarParametro(ref cmd, "@piGrabado", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piGrabado"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que elimina Usuarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la Usuario.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Castañeda</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static int EliminarUsuario(GEN_TabUsuario objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_GEN_TabUsuario_Eliminar");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsu", SqlDbType.SmallInt, objParametros.siCodUsu);
                        objDatabase.AgregarParametro(ref cmd, "@psiCodUsuAct", SqlDbType.SmallInt, objParametros.siCodUsuAct);
                        objDatabase.AgregarParametro(ref cmd, "@piGrabado", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piGrabado"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return iResultado;
        }

        /// <summary>Método que valida si existe Usuario.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaExiste(GEN_TabUsuario objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_GEN_TabUsuario_ValidaExiste");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvNombre", SqlDbType.VarChar, objParametros.vNombre);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }

        /// <summary>Método que valida si existe Usuario.</summary>
        /// <param name="objParametros">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>22/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public static bool ValidaExisteNombreUsuario(GEN_TabUsuario objParametros)
        {
            var objDatabase = new BaseDatos(BD.CadenaConexion.BDSGM);
            int iResultado;
            SqlCommand cmd;

            try
            {
                using (DbConnection con = objDatabase.CrearConexion())
                {
                    con.Open();
                    try
                    {
                        cmd = objDatabase.ObtenerProcedimiento("dbo.usp_GEN_TabUsuario_ValidaNomUsuario");
                        cmd.CommandTimeout = BaseDatos.Timeout();
                        objDatabase.AgregarParametro(ref cmd, "@pvUsuario", SqlDbType.VarChar, objParametros.vUsuario);
                        objDatabase.AgregarParametro(ref cmd, "@piExiste", SqlDbType.Int, 0, true);
                        objDatabase.ExecuteNonQuery(cmd);
                        iResultado = Convert.ToInt32(objDatabase.ObtenerParametro(cmd, "@piExiste"));

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            return (iResultado > 0);
        }
    }
}
