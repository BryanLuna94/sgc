﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.ServiceModel;
using SistemaComercial.App.DataTypes.Response;
using SistemaComercial.App.DataTypes.Request;
using System.ServiceModel.Web;
using SistemaComercial.App.DataTypes;

namespace SistemaComercial.App.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IServicioSistemaComercial" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IServicioSistemaComercial
    {
        #region ALMACEN

        /// <summary>Invoca al Procedimiento almacenado que lista Almacenes.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Almacenes</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerIndexAlmacenes")]
        AlmacenResponseDto ObtenerIndexAlmacenes(AlmacenRequestDto request);

        /// <summary>Invoca al Procedimiento almacenado que lista Almacenes.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Almacenes</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaAlmacenes")]
        AlmacenResponseDto ListaAlmacenes(AlmacenRequestDto request);

        /// <summary>Método que registra Almacenes.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerEditorAlmacen")]
        AlmacenResponseDto ObtenerEditorAlmacen(short IdAlmacen = 0);

        /// <summary>Método que registra Almacenes.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "RegistrarAlmacen")]
        int RegistrarAlmacen(AlmacenRequestDto request);

        /// <summary>Método que Actualiza maestro de Almacen.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ActualizarAlmacen")]
        void ActualizarAlmacen(AlmacenRequestDto request);

        /// <summary>Método que elimina Almacenes.</summary>
        /// <param name="objPersona">Entidad con los datos de la Almacen.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "EliminarAlmacen")]
        void EliminarAlmacen(AlmacenRequestDto request);

        #endregion

        #region UBICACION ALMACEN

        /// <summary>Invoca al Procedimiento almacenado que lista Almacenes.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Almacenes</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerIndexUbicacionAlmacen")]
        AlmacenUbicacionResponseDto ObtenerIndexUbicacionAlmacen(AlmacenUbicacionRequestDto request);

        /// <summary>Invoca al Procedimiento AlmacenUbicacionado que lista AlmacenUbicaciones.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las AlmacenUbicaciones</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaUbicacionesAlmacen")]
        AlmacenUbicacionResponseDto ListaUbicacionesAlmacen(AlmacenUbicacionRequestDto request);

        /// <summary>Método que registra AlmacenUbicaciones.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerEditorUbicacionAlmacen")]
        AlmacenUbicacionResponseDto ObtenerEditorUbicacionAlmacen(short IdAlmacenUbicacion = 0);

        /// <summary>Método que registra AlmacenUbicaciones.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "RegistrarUbicacionAlmacen")]
        int RegistrarUbicacionAlmacen(AlmacenUbicacionRequestDto request);

        /// <summary>Método que Actualiza maestro de AlmacenUbicacion.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ActualizarUbicacionAlmacen")]
        void ActualizarUbicacionAlmacen(AlmacenUbicacionRequestDto request);

        /// <summary>Método que elimina AlmacenUbicaciones.</summary>
        /// <param name="objPersona">Entidad con los datos de la AlmacenUbicacion.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "EliminarUbicacionAlmacen")]
        void EliminarUbicacionAlmacen(AlmacenUbicacionRequestDto request);

        /// <summary>Invoca al Procedimiento AlmacenUbicacionado que lista AlmacenUbicaciones.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las AlmacenUbicaciones</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaUbicacionesAlmacen_Combo")]
        GenericoResponseDto ListaUbicacionesAlmacen_Combo(short idAlmacen);

        /// <summary>Invoca al Procedimiento AlmacenUbicacionado que lista AlmacenUbicaciones.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las AlmacenUbicaciones</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaUbicaciones_Pisos_Combo")]
        GenericoResponseDto ListaUbicaciones_Pisos_Combo(short idAlmacen);

        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaUbicaciones_Racks_Combo")]
        GenericoResponseDto ListaUbicaciones_Racks_Combo(COM_MaeAlmacen_Ubicacion objUbicacion);

        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaUbicaciones_Combo_PorRack")]
        GenericoResponseDto ListaUbicaciones_Combo_PorRack(COM_MaeAlmacen_Ubicacion objUbicacion);

        #endregion

        #region UNIDAD MEDIDA

        /// <summary>Invoca al Procedimiento UnidadMedidaado que lista UnidadMedida.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las UnidadMedida</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerIndexUnidadMedida")]
        UnidadMedidaResponseDto ObtenerIndexUnidadMedida(UnidadMedidaRequestDto request);

        /// <summary>Invoca al Procedimiento UnidadMedidaado que lista UnidadMedida.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las UnidadMedida</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaUnidadMedida")]
        UnidadMedidaResponseDto ListaUnidadMedida(UnidadMedidaRequestDto request);

        /// <summary>Método que registra UnidadMedida.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerEditorUnidadMedida")]
        UnidadMedidaResponseDto ObtenerEditorUnidadMedida(short IdUnidadMedida = 0);

        /// <summary>Método que registra UnidadMedida.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "RegistrarUnidadMedida")]
        int RegistrarUnidadMedida(UnidadMedidaRequestDto request);

        /// <summary>Método que Actualiza maestro de UnidadMedida.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ActualizarUnidadMedida")]
        void ActualizarUnidadMedida(UnidadMedidaRequestDto request);

        /// <summary>Método que elimina UnidadMedida.</summary>
        /// <param name="objPersona">Entidad con los datos de la UnidadMedida.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "EliminarUnidadMedida")]
        void EliminarUnidadMedida(UnidadMedidaRequestDto request);

        #endregion

        #region MARCA

        /// <summary>Invoca al Procedimiento Marcaado que lista Marca.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Marca</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerIndexMarca")]
        MarcaResponseDto ObtenerIndexMarca(MarcaRequestDto request);

        /// <summary>Invoca al Procedimiento Marcaado que lista Marca.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Marca</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaMarca")]
        MarcaResponseDto ListaMarca(MarcaRequestDto request);

        /// <summary>Método que registra Marca.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerEditorMarca")]
        MarcaResponseDto ObtenerEditorMarca(short IdMarca = 0);

        /// <summary>Método que registra Marca.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "RegistrarMarca")]
        int RegistrarMarca(MarcaRequestDto request);

        /// <summary>Método que Actualiza maestro de Marca.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ActualizarMarca")]
        void ActualizarMarca(MarcaRequestDto request);

        /// <summary>Método que elimina Marca.</summary>
        /// <param name="objPersona">Entidad con los datos de la Marca.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "EliminarMarca")]
        void EliminarMarca(MarcaRequestDto request);

        #endregion

        #region PERSONA

        /// <summary>Invoca al Procedimiento Personaado que lista Personas.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Personas</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerIndexPersonas")]
        PersonaResponseDto ObtenerIndexPersonas(PersonaRequestDto request);

        /// <summary>Invoca al Procedimiento Personaado que lista Personas.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Personas</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaPersonas")]
        PersonaResponseDto ListaPersonas(PersonaRequestDto request);

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerEditorPersona")]
        PersonaResponseDto ObtenerEditorPersona(int IdPersona = 0);

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "RegistrarPersona")]
        int RegistrarPersona(PersonaRequestDto request);

        /// <summary>Método que Actualiza maestro de Persona.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ActualizarPersona")]
        void ActualizarPersona(PersonaRequestDto request);

        /// <summary>Método que elimina Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la Persona.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "EliminarPersona")]
        void EliminarPersona(PersonaRequestDto request);

        /// <summary>Invoca al Procedimiento almacenado que lista Nivels.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Nivels</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaPersona_Autocomplete")]
        GenericoResponseDto ListaPersona_Autocomplete(MovimientoRequestDto request);

        #endregion

        #region PERSONA OFICINA

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerEditorPersonaOficina")]
        PersonaResponseDto ObtenerEditorPersonaOficina(int IdPersonaOficina = 0);

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "RegistrarPersonaOficina")]
        int RegistrarPersonaOficina(PersonaRequestDto request);

        /// <summary>Método que Actualiza maestro de Persona.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ActualizarPersonaOficina")]
        void ActualizarPersonaOficina(PersonaRequestDto request);

        /// <summary>Método que elimina Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la Persona.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "EliminarPersonaOficina")]
        int EliminarPersonaOficina(PersonaRequestDto request);

        /// <summary>Invoca al Procedimiento AlmacenUbicacionado que lista AlmacenUbicaciones.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las AlmacenUbicaciones</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaDirecciones_Combo")]
        GenericoResponseDto ListaDirecciones_Combo(int idPersona);

        #endregion

        #region PERSONA CONTACTO

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerIndexPersonas")]
        PersonaResponseDto ObtenerEditorPersonaContacto(int IdPersonaContacto = 0);

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "RegistrarPersonaContacto")]
        int RegistrarPersonaContacto(PersonaRequestDto request);

        /// <summary>Método que Actualiza maestro de Persona.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ActualizarPersonaContacto")]
        void ActualizarPersonaContacto(PersonaRequestDto request);

        /// <summary>Método que elimina Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la Persona.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "EliminarPersonaContacto")]
        int EliminarPersonaContacto(PersonaRequestDto request);

        #endregion

        #region ARTICULO

        /// <summary>Invoca al Procedimiento Articuloado que lista Articulos.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Articulos</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerIndexArticulos")]
        ArticuloResponseDto ObtenerIndexArticulos(ArticuloRequestDto request);

        /// <summary>Invoca al Procedimiento Articuloado que lista Articulos.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Articulos</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaArticulos")]
        ArticuloResponseDto ListaArticulos(ArticuloRequestDto request);

        /// <summary>Método que registra Articulos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerEditorArticulo")]
        ArticuloResponseDto ObtenerEditorArticulo(int IdArticulo = 0);


        /// <summary>Método que registra Articulos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "RegistrarArticulo")]
        int RegistrarArticulo(ArticuloRequestDto request);

        /// <summary>Método que Actualiza maestro de Articulo.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ActualizarArticulo")]
        void ActualizarArticulo(ArticuloRequestDto request);

        /// <summary>Método que elimina Articulos.</summary>
        /// <param name="objPersona">Entidad con los datos de la Articulo.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "EliminarArticulo")]
        void EliminarArticulo(ArticuloRequestDto request);

        /// <summary>Invoca al Procedimiento Articuloado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaArticulos_Auto")]
        GenericoResponseDto ListaArticulos_Autocomplete(string filtro);

        #endregion

        #region PERFIL

        /// <summary>Invoca al Procedimiento almacenado que lista Perfiles.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Perfiles</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>19/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerIndexPerfiles")]
        PerfilResponseDto ObtenerIndexPerfiles(PerfilRequestDto request);

        /// <summary>Invoca al Procedimiento almacenado que lista Perfiles.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Perfiles</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>19/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaPerfiles")]
        PerfilResponseDto ListaPerfiles(PerfilRequestDto request);

        /// <summary>Método que registra Perfiles.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerEditorPerfil")]
        PerfilResponseDto ObtenerEditorPerfil(short IdPerfil = 0);

        /// <summary>Método que registra Perfiles.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "RegistrarPerfil")]
        int RegistrarPerfil(PerfilRequestDto request);

        /// <summary>Método que Actualiza maestro de Perfil.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ActualizarPerfil")]
        int ActualizarPerfil(PerfilRequestDto request);

        /// <summary>Método que elimina Perfiles.</summary>
        /// <param name="objPersona">Entidad con los datos de la Perfil.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "EliminarPerfil")]
        int EliminarPerfil(PerfilRequestDto request);

        /// <summary>Método que devuelve el response para la pantalla index de ruta</summary>
        /// <param name="request">Objeto request</param>
        /// <returns>Response de index para ruta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Wilmer Gómez Prado</CreadoPor></item>
        /// <item><FecCrea>02/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaOpcionesPorPerfil")]
        PerfilResponseDto ListaOpcionesPorPerfil(short idPerfil);

        /// <summary>Método que devuelve el response para la pantalla index de ruta</summary>
        /// <param name="request">Objeto request</param>
        /// <returns>Response de index para ruta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Wilmer Gómez Prado</CreadoPor></item>
        /// <item><FecCrea>02/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaOpcionesPorPerfil_Menu")]
        PerfilResponseDto ListaOpcionesPorPerfil_Menu(short idPerfil);

        /// <summary>Método que devuelve el response para la pantalla index de ruta</summary>
        /// <param name="request">Objeto request</param>
        /// <returns>Response de index para ruta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Wilmer Gómez Prado</CreadoPor></item>
        /// <item><FecCrea>02/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "GrabarOpcionesPorPerfil")]
        int GrabarOpcionesPorPerfil(PerfilRequestDto request);

        #endregion

        #region USUARIO

        /// <summary>Invoca al Procedimiento almacenado que lista Usuarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Usuarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>19/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerIndexUsuarios")]
        UsuarioResponseDto ObtenerIndexUsuarios(UsuarioRequestDto request);

        /// <summary>Invoca al Procedimiento almacenado que lista Usuarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Usuarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>19/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaUsuarios")]
        UsuarioResponseDto ListaUsuarios(UsuarioRequestDto request);

        /// <summary>Método que registra Usuarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerEditorUsuario")]
        UsuarioResponseDto ObtenerEditorUsuario(short IdUsuario);

        /// <summary>Método que registra Usuarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "RegistrarUsuario")]
        int RegistrarUsuario(UsuarioRequestDto request);

        /// <summary>Método que Actualiza maestro de Usuario.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ActualizarUsuario")]
        int ActualizarUsuario(UsuarioRequestDto request);

        /// <summary>Método que Actualiza maestro de Usuario.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "CambiarClave")]
        int CambiarClave(CambiarClaveRequestDto request);

        /// <summary>Método que elimina Usuarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la Usuario.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "EliminarUsuario")]
        int EliminarUsuario(UsuarioRequestDto request);

        /// <summary>Invoca al Procedimiento almacenado que lista Usuarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Usuarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>19/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "Login")]
        LoginResponseDto Login(LoginRequestDto request);

        #endregion

        #region CABECERA MOVIMIENTO

        /// <summary>Invoca al Procedimiento Movimientoado que lista Movimientos.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Movimientos</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerIndexMovimientos")]
        MovimientoResponseDto ObtenerIndexMovimientos(MovimientoRequestDto request);

        /// <summary>Invoca al Procedimiento Movimientoado que lista Movimientos.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Movimientos</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaMovimientos")]
        MovimientoResponseDto ListaMovimientos(MovimientoRequestDto request);

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerEditorMovimiento")]
        MovimientoResponseDto ObtenerEditorMovimiento(int IdMovimiento = 0);

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "RegistrarMovimiento")]
        MovimientoResponseDto RegistrarMovimiento(MovimientoRequestDto request);

        /// <summary>Método que Actualiza maestro de Movimiento.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ActualizarMovimiento")]
        void ActualizarMovimiento(MovimientoRequestDto request);

        /// <summary>Método que elimina Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la Movimiento.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "CerrarMovimiento")]
        void CerrarMovimiento(MovimientoRequestDto request);

        /// <summary>Método que elimina Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la Movimiento.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "EliminarMovimiento")]
        void EliminarMovimiento(MovimientoRequestDto request);

        #endregion

        #region DETALLE MOVIMIENTO

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerEditorMovimientoDetalle")]
        MovimientoResponseDto ObtenerEditorMovimientoDetalle(int IdMovimiento, int IdMovimientoDetalle = 0);

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "RegistrarMovimientoDetalle")]
        int RegistrarMovimientoDetalle(MovimientoRequestDto request);

        /// <summary>Método que Actualiza maestro de Movimiento.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ActualizarMovimientoDetalle")]
        void ActualizarMovimientoDetalle(MovimientoRequestDto request);

        /// <summary>Invoca al Procedimiento Articuloado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaArticulosParaSalida_Auto")]
        GenericoResponseDto ListaArticulosParaSalida_Auto(string filtro);

        /// <summary>Invoca al Procedimiento Articuloado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaConceptosLiquidacion")]
        GenericoResponseDto ListaConceptosLiquidacion(string iCodPartner, string vTipoMov);

        

        /// <summary>Invoca al Procedimiento DetalleMovimientoado que lista Descripcion Base.</summary>
        /// <param name="idDetalleMovimiento">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerDetalleMovimientoParaSalida")]
        MovimientoResponseDto ObtenerDetalleMovimientoParaSalida(int idDetalleMovimiento);

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "EliminarMovimientoDetalle")]
        int EliminarMovimientoDetalle(int idMovimientoDetalle);

        #endregion

        #region REPORTES

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerGuiaRemision")]
        GuiaRemisionResponseDto ObtenerGuiaRemision(int idMovimiento);

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerIndexReporteAlmacenesValorizadosResumido")]
        ReporteAlmacenesValorizadoResumidoResponseDto ObtenerIndexReporteAlmacenesValorizadosResumido();

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaReporteAlmacenesValorizadosResumido")]
        ReporteAlmacenesValorizadoResumidoResponseDto ListaReporteAlmacenesValorizadosResumido(ReporteAlmacenesValorizadosResumidoRequestDto request);

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaReporteAlmacenesValorizadosDetallado")]
        ReporteAlmacenesValorizadoResumidoResponseDto ListaReporteAlmacenesValorizadosDetallado(ReporteAlmacenesValorizadosResumidoRequestDto request);

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerIndexReporteKardex")]
        ReporteKardexResponseDto ObtenerIndexReporteKardex();

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListaReporteKardex")]
        ReporteKardexResponseDto ListaReporteKardex(ReporteKardexRequestDto request);

        #endregion

        #region ARCHIVO

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ListadoArchivos")]
        ArchivosResponseDto ListadoArchivos(string tabla, int id);

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerArchivoPorId")]
        ArchivosResponseDto ObtenerArchivoPorId(string id);

        /// <summary>Método que registra MaestroStopees.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Castañeda</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerArchivoPorId")]
        void GuardarArchivo(ArchivosRequestDto request);

        /// <summary>Método que registra MaestroStopees.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Castañeda</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "EliminarArchivo")]
        int EliminarArchivo(string id);

        #endregion

        #region INVENTARIO
        /// <summary>Invoca al Procedimiento Inventario que lista Inventarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Inventarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>David Godoy Huaman.</CreadoPor></item>
        ///<item><FecCrea>06/09/2021</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerIndexInventarios")]
        InventarioResponseDto ObtenerIndexInventarios(InventarioRequestDto request);


        /// <summary>Método que registra Inventarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy Huaman</CreadoPor></item>
        /// <item><FecCrea>13/09/2021.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerEditorInventario")]
        InventarioResponseDto ObtenerEditorInventario(int IdArticulo = 0);


        /// <summary>Método que registra Inventario.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy Huaman</CreadoPor></item>
        /// <item><FecCrea>14/09/2021.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "RegistrarInventario")]
        int RegistrarInventario(InventarioRequestDto request);

        /// <summary>Método que Actualiza maestro de Inventario.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy Huaman</CreadoPor></item>
        /// <item><FecCrea>14/09/2021.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ActualizarInventario")]
        void ActualizarInventario(InventarioRequestDto request);

        /// <summary>Método que elimina Inventario.</summary>
        /// <param name="objPersona">Entidad con los datos de la Inventario.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy Huaman</CreadoPor></item>
        /// <item><FecCrea>14/09/2021.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "EliminarInventario")]
        void EliminarInventario(InventarioRequestDto request);

        /// <summary>Método que activa Inventario.</summary>
        /// <param name="objPersona">Entidad con los datos de la Inventario.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy Huaman</CreadoPor></item>
        /// <item><FecCrea>23/09/2021.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ActivarInventario")]
        void ActivarInventario(InventarioRequestDto request);

        #endregion

        #region LIQUIDACION

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        [OperationContract, FaultContract(typeof(ServiceErrorResponseType))]
        [WebInvoke(Method = "POST",
                ResponseFormat = WebMessageFormat.Json,
                RequestFormat = WebMessageFormat.Json,
                BodyStyle = WebMessageBodyStyle.Bare,
                UriTemplate = "ObtenerEditorMovimientoLiquidacion")]
        MovimientoLiquidacionResponseDto ObtenerEditorMovimientoLiquidacion(int IdLiquidacion = 0);

        #endregion
    }
}
