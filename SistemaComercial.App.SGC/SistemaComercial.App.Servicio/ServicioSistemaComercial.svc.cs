﻿using SistemaComercial.App.BusinessLayer;
using SistemaComercial.App.DataTypes;
using SistemaComercial.App.DataTypes.Request;
using SistemaComercial.App.DataTypes.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace SistemaComercial.App.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ServicioSistemaComercial" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ServicioSistemaComercial.svc o ServicioSistemaComercial.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ServicioSistemaComercial : IServicioSistemaComercial
    {
        #region ALMACEN

        /// <summary>Invoca al Procedimiento almacenado que lista Almacenes.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Almacenes</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public AlmacenResponseDto ObtenerIndexAlmacenes(AlmacenRequestDto request)
        {
            try
            {
                return AlmacenLogic.ObtenerIndexAlmacenes(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Almacenes.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Almacenes</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public AlmacenResponseDto ListaAlmacenes(AlmacenRequestDto request)
        {
            try
            {
                return AlmacenLogic.ListaAlmacenes(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Almacenes.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public AlmacenResponseDto ObtenerEditorAlmacen(short IdAlmacen = 0)
        {
            try
            {
                return AlmacenLogic.ObtenerEditorAlmacen(IdAlmacen);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Almacenes.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int RegistrarAlmacen(AlmacenRequestDto request)
        {
            try
            {
                return AlmacenLogic.RegistrarAlmacen(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que Actualiza maestro de Almacen.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void ActualizarAlmacen(AlmacenRequestDto request)
        {
            try
            {
                AlmacenLogic.ActualizarAlmacen(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que elimina Almacenes.</summary>
        /// <param name="objPersona">Entidad con los datos de la Almacen.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void EliminarAlmacen(AlmacenRequestDto request)
        {
            try
            {
                AlmacenLogic.EliminarAlmacen(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region UBICACION ALMACEN

        /// <summary>Invoca al Procedimiento almacenado que lista Almacenes.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Almacenes</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public AlmacenUbicacionResponseDto ObtenerIndexUbicacionAlmacen(AlmacenUbicacionRequestDto request)
        {
            try
            {
                return AlmacenLogic.ObtenerIndexUbicacionAlmacen(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento AlmacenUbicacionado que lista AlmacenUbicaciones.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las AlmacenUbicaciones</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public AlmacenUbicacionResponseDto ListaUbicacionesAlmacen(AlmacenUbicacionRequestDto request)
        {
            try
            {
                return AlmacenLogic.ListaUbicacionesAlmacen(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra AlmacenUbicaciones.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public AlmacenUbicacionResponseDto ObtenerEditorUbicacionAlmacen(short IdAlmacenUbicacion = 0)
        {
            try
            {
                return AlmacenLogic.ObtenerEditorUbicacionAlmacen(IdAlmacenUbicacion);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra AlmacenUbicaciones.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int RegistrarUbicacionAlmacen(AlmacenUbicacionRequestDto request)
        {
            try
            {
                return AlmacenLogic.RegistrarUbicacionAlmacen(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que Actualiza maestro de AlmacenUbicacion.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void ActualizarUbicacionAlmacen(AlmacenUbicacionRequestDto request)
        {
            try
            {
                AlmacenLogic.ActualizarUbicacionAlmacen(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que elimina AlmacenUbicaciones.</summary>
        /// <param name="objPersona">Entidad con los datos de la AlmacenUbicacion.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void EliminarUbicacionAlmacen(AlmacenUbicacionRequestDto request)
        {
            try
            {
                AlmacenLogic.EliminarUbicacionAlmacen(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento AlmacenUbicacionado que lista AlmacenUbicaciones.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las AlmacenUbicaciones</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public GenericoResponseDto ListaUbicacionesAlmacen_Combo(short idAlmacen)
        {
            try
            {
                return AlmacenLogic.ListaUbicacionesAlmacen_Combo(idAlmacen);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento AlmacenUbicacionado que lista AlmacenUbicaciones.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las AlmacenUbicaciones</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public GenericoResponseDto ListaUbicaciones_Pisos_Combo(short idAlmacen)
        {
            try
            {
                return AlmacenLogic.ListaUbicaciones_Pisos_Combo(idAlmacen);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public GenericoResponseDto ListaUbicaciones_Racks_Combo(COM_MaeAlmacen_Ubicacion objUbicacion)
        {
            try
            {
                return AlmacenLogic.ListaUbicaciones_Racks_Combo(objUbicacion);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public GenericoResponseDto ListaUbicaciones_Combo_PorRack(COM_MaeAlmacen_Ubicacion objUbicacion)
        {
            try
            {
                return AlmacenLogic.ListaUbicaciones_Combo_PorRack(objUbicacion);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region UNIDAD MEDIDA

        /// <summary>Invoca al Procedimiento UnidadMedidaado que lista UnidadMedida.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las UnidadMedida</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public UnidadMedidaResponseDto ObtenerIndexUnidadMedida(UnidadMedidaRequestDto request)
        {
            try
            {
                return UnidadMedidaLogic.ObtenerIndexUnidadMedida(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento UnidadMedidaado que lista UnidadMedida.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las UnidadMedida</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public UnidadMedidaResponseDto ListaUnidadMedida(UnidadMedidaRequestDto request)
        {
            try
            {
                return UnidadMedidaLogic.ListaUnidadMedida(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra UnidadMedida.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public UnidadMedidaResponseDto ObtenerEditorUnidadMedida(short IdUnidadMedida = 0)
        {
            try
            {
                return UnidadMedidaLogic.ObtenerEditorUnidadMedida(IdUnidadMedida);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra UnidadMedida.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int RegistrarUnidadMedida(UnidadMedidaRequestDto request)
        {
            try
            {
                return UnidadMedidaLogic.RegistrarUnidadMedida(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que Actualiza maestro de UnidadMedida.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void ActualizarUnidadMedida(UnidadMedidaRequestDto request)
        {
            try
            {
                UnidadMedidaLogic.ActualizarUnidadMedida(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que elimina UnidadMedida.</summary>
        /// <param name="objPersona">Entidad con los datos de la UnidadMedida.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void EliminarUnidadMedida(UnidadMedidaRequestDto request)
        {
            try
            {
                UnidadMedidaLogic.EliminarUnidadMedida(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region MARCA

        /// <summary>Invoca al Procedimiento Marcaado que lista Marca.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Marca</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public MarcaResponseDto ObtenerIndexMarca(MarcaRequestDto request)
        {
            try
            {
                return MarcaLogic.ObtenerIndexMarca(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Marcaado que lista Marca.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Marca</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public MarcaResponseDto ListaMarca(MarcaRequestDto request)
        {
            try
            {
                return MarcaLogic.ListaMarca(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Marca.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public MarcaResponseDto ObtenerEditorMarca(short IdMarca = 0)
        {
            try
            {
                return MarcaLogic.ObtenerEditorMarca(IdMarca);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Marca.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int RegistrarMarca(MarcaRequestDto request)
        {
            try
            {
                return MarcaLogic.RegistrarMarca(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que Actualiza maestro de Marca.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void ActualizarMarca(MarcaRequestDto request)
        {
            try
            {
                MarcaLogic.ActualizarMarca(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que elimina Marca.</summary>
        /// <param name="objPersona">Entidad con los datos de la Marca.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void EliminarMarca(MarcaRequestDto request)
        {
            try
            {
                MarcaLogic.EliminarMarca(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region PERSONA

        /// <summary>Invoca al Procedimiento Personaado que lista Personas.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Personas</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public PersonaResponseDto ObtenerIndexPersonas(PersonaRequestDto request)
        {
            try
            {
                return PersonaLogic.ObtenerIndexPersonas(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Personaado que lista Personas.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Personas</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public PersonaResponseDto ListaPersonas(PersonaRequestDto request)
        {
            try
            {
                return PersonaLogic.ListaPersonas(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public PersonaResponseDto ObtenerEditorPersona(int IdPersona = 0)
        {
            try
            {
                return PersonaLogic.ObtenerEditorPersona(IdPersona);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int RegistrarPersona(PersonaRequestDto request)
        {
            try
            {
                return PersonaLogic.RegistrarPersona(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que Actualiza maestro de Persona.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void ActualizarPersona(PersonaRequestDto request)
        {
            try
            {
                PersonaLogic.ActualizarPersona(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que elimina Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la Persona.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void EliminarPersona(PersonaRequestDto request)
        {
            try
            {
                PersonaLogic.EliminarPersona(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Nivels.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Nivels</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public GenericoResponseDto ListaPersona_Autocomplete(MovimientoRequestDto request)
        {
            try
            {
                return PersonaLogic.ListaPersona_Autocomplete(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region PERSONA OFICINA

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public PersonaResponseDto ObtenerEditorPersonaOficina(int IdPersonaOficina = 0)
        {
            try
            {
                return PersonaOficinaLogic.ObtenerEditorPersonaOficina(IdPersonaOficina);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int RegistrarPersonaOficina(PersonaRequestDto request)
        {
            try
            {
                return PersonaOficinaLogic.RegistrarPersonaOficina(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que Actualiza maestro de Persona.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void ActualizarPersonaOficina(PersonaRequestDto request)
        {
            try
            {
                PersonaOficinaLogic.ActualizarPersonaOficina(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que elimina Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la Persona.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int EliminarPersonaOficina(PersonaRequestDto request)
        {
            try
            {
                return PersonaOficinaLogic.EliminarPersonaOficina(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento AlmacenUbicacionado que lista AlmacenUbicaciones.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las AlmacenUbicaciones</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public GenericoResponseDto ListaDirecciones_Combo(int idPersona)
        {
            try
            {
                return PersonaOficinaLogic.ListaDirecciones_Combo(idPersona);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region PERSONA CONTACTO

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public PersonaResponseDto ObtenerEditorPersonaContacto(int IdPersonaContacto = 0)
        {
            try
            {
                return PersonaContactoLogic.ObtenerEditorPersonaContacto(IdPersonaContacto);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int RegistrarPersonaContacto(PersonaRequestDto request)
        {
            try
            {
                return PersonaContactoLogic.RegistrarPersonaContacto(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que Actualiza maestro de Persona.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void ActualizarPersonaContacto(PersonaRequestDto request)
        {
            try
            {
                PersonaContactoLogic.ActualizarPersonaContacto(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que elimina Personas.</summary>
        /// <param name="objPersona">Entidad con los datos de la Persona.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int EliminarPersonaContacto(PersonaRequestDto request)
        {
            try
            {
                return PersonaContactoLogic.EliminarPersonaContacto(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region ARTICULO

        /// <summary>Invoca al Procedimiento Articuloado que lista Articulos.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Articulos</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public ArticuloResponseDto ObtenerIndexArticulos(ArticuloRequestDto request)
        {
            try
            {
                return ArticuloLogic.ObtenerIndexArticulos(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Articuloado que lista Articulos.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Articulos</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public ArticuloResponseDto ListaArticulos(ArticuloRequestDto request)
        {
            try
            {
                return ArticuloLogic.ListaArticulos(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Articulos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public ArticuloResponseDto ObtenerEditorArticulo(int IdArticulo = 0)
        {
            try
            {
                return ArticuloLogic.ObtenerEditorArticulo(IdArticulo);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Articulos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int RegistrarArticulo(ArticuloRequestDto request)
        {
            try
            {
                return ArticuloLogic.RegistrarArticulo(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que Actualiza maestro de Articulo.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void ActualizarArticulo(ArticuloRequestDto request)
        {
            try
            {
                ArticuloLogic.ActualizarArticulo(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que elimina Articulos.</summary>
        /// <param name="objPersona">Entidad con los datos de la Articulo.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void EliminarArticulo(ArticuloRequestDto request)
        {
            try
            {
                ArticuloLogic.EliminarArticulo(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Articuloado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public GenericoResponseDto ListaArticulos_Autocomplete(string filtro)
        {
            try
            {
                return ArticuloLogic.ListaArticulos_Auto(filtro);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region PERFIL

        /// <summary>Invoca al Procedimiento almacenado que lista Perfiles.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Perfiles</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>19/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public PerfilResponseDto ObtenerIndexPerfiles(PerfilRequestDto request)
        {
            try
            {
                return PerfilLogic.ObtenerIndexPerfiles(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Perfiles.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Perfiles</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>19/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public PerfilResponseDto ListaPerfiles(PerfilRequestDto request)
        {
            try
            {
                return PerfilLogic.ListaPerfiles(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Perfiles.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public PerfilResponseDto ObtenerEditorPerfil(short IdPerfil = 0)
        {
            try
            {
                return PerfilLogic.ObtenerEditorPerfil(IdPerfil);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Perfiles.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int RegistrarPerfil(PerfilRequestDto request)
        {
            try
            {
                return PerfilLogic.RegistrarPerfil(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que Actualiza maestro de Perfil.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int ActualizarPerfil(PerfilRequestDto request)
        {
            try
            {
                return PerfilLogic.ActualizarPerfil(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que elimina Perfiles.</summary>
        /// <param name="objPersona">Entidad con los datos de la Perfil.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int EliminarPerfil(PerfilRequestDto request)
        {
            try
            {
                return PerfilLogic.EliminarPerfil(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que devuelve el response para la pantalla index de ruta</summary>
        /// <param name="request">Objeto request</param>
        /// <returns>Response de index para ruta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Wilmer Gómez Prado</CreadoPor></item>
        /// <item><FecCrea>02/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public PerfilResponseDto ListaOpcionesPorPerfil(short idPerfil)
        {
            try
            {
                return PerfilLogic.ListaOpcionesPorPerfil(idPerfil);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que devuelve el response para la pantalla index de ruta</summary>
        /// <param name="request">Objeto request</param>
        /// <returns>Response de index para ruta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Wilmer Gómez Prado</CreadoPor></item>
        /// <item><FecCrea>02/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public PerfilResponseDto ListaOpcionesPorPerfil_Menu(short idPerfil)
        {
            try
            {
                return PerfilLogic.ListaOpcionesPorPerfil_Menu(idPerfil);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que devuelve el response para la pantalla index de ruta</summary>
        /// <param name="request">Objeto request</param>
        /// <returns>Response de index para ruta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Wilmer Gómez Prado</CreadoPor></item>
        /// <item><FecCrea>02/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public int GrabarOpcionesPorPerfil(PerfilRequestDto request)
        {
            try
            {
                return PerfilLogic.GrabarOpcionesPorPerfil(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region USUARIO

        /// <summary>Invoca al Procedimiento almacenado que lista Usuarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Usuarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>19/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public UsuarioResponseDto ObtenerIndexUsuarios(UsuarioRequestDto request)
        {
            try
            {
                return UsuarioLogic.ObtenerIndexUsuarios(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Usuarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Usuarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>19/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public UsuarioResponseDto ListaUsuarios(UsuarioRequestDto request)
        {
            try
            {
                return UsuarioLogic.ListaUsuarios(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Usuarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public UsuarioResponseDto ObtenerEditorUsuario(short IdUsuario = 0)
        {
            try
            {
                return UsuarioLogic.ObtenerEditorUsuario(IdUsuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Usuarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int RegistrarUsuario(UsuarioRequestDto request)
        {
            try
            {
                return UsuarioLogic.RegistrarUsuario(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que Actualiza maestro de Usuario.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int ActualizarUsuario(UsuarioRequestDto request)
        {
            try
            {
                return UsuarioLogic.ActualizarUsuario(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que Actualiza maestro de Usuario.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int CambiarClave(CambiarClaveRequestDto request)
        {
            try
            {
                return UsuarioLogic.CambiarClave(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que elimina Usuarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la Usuario.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int EliminarUsuario(UsuarioRequestDto request)
        {
            try
            {
                return UsuarioLogic.EliminarUsuario(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento almacenado que lista Usuarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Usuarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>19/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public LoginResponseDto Login(LoginRequestDto request)
        {
            try
            {
                return UsuarioLogic.Login(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region CABECERA MOVIMIENTO

        /// <summary>Invoca al Procedimiento Movimientoado que lista Movimientos.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Movimientos</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public MovimientoResponseDto ObtenerIndexMovimientos(MovimientoRequestDto request)
        {
            try
            {
                return MovimientoLogic.ObtenerIndexMovimientos(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Movimientoado que lista Movimientos.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Movimientos</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public MovimientoResponseDto ListaMovimientos(MovimientoRequestDto request)
        {
            try
            {
                return MovimientoLogic.ListaMovimientos(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public MovimientoResponseDto ObtenerEditorMovimiento(int IdMovimiento = 0)
        {
            try
            {
                return MovimientoLogic.ObtenerEditorMovimiento(IdMovimiento);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public MovimientoResponseDto RegistrarMovimiento(MovimientoRequestDto request)
        {
            try
            {
                return MovimientoLogic.RegistrarMovimiento(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que Actualiza maestro de Movimiento.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void ActualizarMovimiento(MovimientoRequestDto request)
        {
            try
            {
                MovimientoLogic.ActualizarMovimiento(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que elimina Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la Movimiento.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void CerrarMovimiento(MovimientoRequestDto request)
        {
            try
            {
                MovimientoLogic.CerrarMovimiento(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que elimina Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la Movimiento.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void EliminarMovimiento(MovimientoRequestDto request)
        {
            try
            {
                MovimientoLogic.EliminarMovimiento(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region DETALLE MOVIMIENTO

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public MovimientoResponseDto ObtenerEditorMovimientoDetalle(int IdMovimiento, int IdMovimientoDetalle = 0)
        {
            try
            {
                return MovimientoLogic.ObtenerEditorMovimientoDetalle(IdMovimiento, IdMovimientoDetalle);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int RegistrarMovimientoDetalle(MovimientoRequestDto request)
        {
            try
            {
                return MovimientoLogic.RegistrarMovimientoDetalle(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que Actualiza maestro de Movimiento.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void ActualizarMovimientoDetalle(MovimientoRequestDto request)
        {
            try
            {
                MovimientoLogic.ActualizarMovimientoDetalle(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Articuloado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public GenericoResponseDto ListaArticulosParaSalida_Auto(string filtro)
        {
            try
            {
                return MovimientoLogic.ListaArticulosParaSalida_Auto(filtro);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Articuloado que lista Descripcion Base.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public GenericoResponseDto ListaConceptosLiquidacion(string iCodPartner, string vTipoMov)
        {
            try
            {
                return MovimientoLogic.ListaConceptosLiquidacion(iCodPartner, vTipoMov);
            }
            catch (Exception)
            {
                throw;
            }
        }

        

        /// <summary>Invoca al Procedimiento DetalleMovimientoado que lista Descripcion Base.</summary>
        /// <param name="idDetalleMovimiento">Parámetros para el filtro de Listar los Descripcion Base</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>Bryan Luna Vasquez.</CreadoPor></item>
        ///<item><FecCrea>12/02/2018</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public MovimientoResponseDto ObtenerDetalleMovimientoParaSalida(int idDetalleMovimiento)
        {
            try
            {
                return MovimientoLogic.ObtenerDetalleMovimientoParaSalida(idDetalleMovimiento);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int EliminarMovimientoDetalle(int idMovimientoDetalle)
        {
            try
            {
                return MovimientoLogic.EliminarMovimientoDetalle(idMovimientoDetalle);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region REPORTES

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public GuiaRemisionResponseDto ObtenerGuiaRemision(int idMovimiento)
        {
            try
            {
                return MovimientoLogic.ObtenerGuiaRemision(idMovimiento);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public ReporteAlmacenesValorizadoResumidoResponseDto ObtenerIndexReporteAlmacenesValorizadosResumido()
        {
            try
            {
                return MovimientoLogic.ObtenerIndexReporteAlmacenesValorizadosResumido();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public ReporteAlmacenesValorizadoResumidoResponseDto ListaReporteAlmacenesValorizadosResumido(ReporteAlmacenesValorizadosResumidoRequestDto request)
        {
            try
            {
                return MovimientoLogic.ListaReporteAlmacenesValorizadosResumido(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public ReporteAlmacenesValorizadoResumidoResponseDto ListaReporteAlmacenesValorizadosDetallado(ReporteAlmacenesValorizadosResumidoRequestDto request)
        {
            try
            {
                return MovimientoLogic.ListaReporteAlmacenesValorizadosDetallado(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ReporteKardexResponseDto ObtenerIndexReporteKardex()
        {
            try
            {
                return MovimientoLogic.ObtenerIndexReporteKardex();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ReporteKardexResponseDto ListaReporteKardex(ReporteKardexRequestDto request)
        {
            try
            {
                return MovimientoLogic.ListaReporteKardex(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region ARCHIVO

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public ArchivosResponseDto ListadoArchivos(string tabla, int id)
        {
            try
            {
                return ArchivosLogic.ListadoArchivos(tabla, id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public ArchivosResponseDto ObtenerArchivoPorId(string id)
        {
            try
            {
                return ArchivosLogic.ObtenerArchivoPorId(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra MaestroStopees.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Castañeda</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void GuardarArchivo(ArchivosRequestDto request)
        {
            try
            {
                ArchivosLogic.GuardarArchivo(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra MaestroStopees.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Castañeda</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int EliminarArchivo(string id)
        {
            try
            {
                return ArchivosLogic.EliminarArchivo(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region INVENTARIO
        /// <summary>Invoca al Procedimiento Inventario que lista Inventarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Inventarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>David Godoy Huaman.</CreadoPor></item>
        ///<item><FecCrea>06/09/2021</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public InventarioResponseDto ObtenerIndexInventarios(InventarioRequestDto request)
        {
            try
            {
                return InventarioLogic.ObtenerIndexInventarios(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Invoca al Procedimiento Inventario que lista Inventarios.</summary>
        /// <param name="objFiltro">Parámetros para el filtro de Listar las Inventarios</param>
        ///<remarks>
        ///<list type = "bullet">
        ///<item><CreadoPor>David Godoy Huaman.</CreadoPor></item>
        ///<item><FecCrea>06/09/2021</FecCrea></item></list>        
        ///<list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public InventarioResponseDto ObtenerEditorInventario(int IdInventario = 0)
        {
            try
            {
                return InventarioLogic.ObtenerEditorInventario(IdInventario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que registra Inventarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy Huaman</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public int RegistrarInventario(InventarioRequestDto request)
        {
            try
            {
                return InventarioLogic.RegistrarInventario(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que Actualiza maestro de Inventarios.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy Huaman</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void ActualizarInventario(InventarioRequestDto request)
        {
            try
            {
                InventarioLogic.ActualizarInventario(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que elimina Inventario.</summary>
        /// <param name="objPersona">Entidad con los datos de la Articulo.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy Huaman</CreadoPor></item>
        /// <item><FecCrea>15/09/2021.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void EliminarInventario(InventarioRequestDto request)
        {
            try
            {
                InventarioLogic.EliminarInventario(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Método que activa Inventario.</summary>
        /// <param name="objPersona">Entidad con los datos de la Inventario.</param>
        /// <returns>Respuesta entero.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>David Godoy Huaman</CreadoPor></item>
        /// <item><FecCrea>23/09/2021.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public void ActivarInventario(InventarioRequestDto request)
        {
            try
            {
                InventarioLogic.ActivarInventario(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region LIQUIDACION

        /// <summary>Método que registra Movimientos.</summary>
        /// <param name="objPersona">Entidad con los datos de la entidad.</param>
        /// <returns>.</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vasquez</CreadoPor></item>
        /// <item><FecCrea>13/02/2018.</FecCrea></item></list>
        /// <list type="bullet">
        /// <item><FecActu>XX/XX/XXXX.</FecActu></item>
        /// <item><Resp>Responsable.</Resp></item>
        /// <item><Mot>Motivo.</Mot></item></list></remarks>
        public MovimientoLiquidacionResponseDto ObtenerEditorMovimientoLiquidacion(int IdLiquidacion = 0)
        {
            try
            {
                return MovimientoLogic.ObtenerEditorMovimientoLiquidacion(IdLiquidacion);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
