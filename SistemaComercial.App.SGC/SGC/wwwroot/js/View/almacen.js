﻿var almacen = {
    idFormGrid: "#almacenGridForm",
    idFormEditor: "#almacenEditorForm",
    updateTargetIdGrid: "#almacen-wrapper",

    grid: {

        load: function () {

            $("#Filtro_Descripcion").keypress(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    baseGrid.find(route.almacen, almacen.idFormGrid, "Refrescar", almacen.updateTargetIdGrid);
                }
            });

            $("#Filtro_Direccion").keypress(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    baseGrid.find(route.almacen, almacen.idFormGrid, "Refrescar", almacen.updateTargetIdGrid);
                }
            });

            $(".btn-exportar").click(function (e) {
                almacen.grid.exportar();
            });

            baseGrid.init(route.almacen, almacen.idFormGrid, almacen.updateTargetIdGrid, almacen.editor.init);
        },

        exportar: function () {
            var form = $(almacen.idFormGrid);
            var url = $("#hdnURLExportar").val();
            form.attr('action', url);
            form.submit();
        },

        paginarParcial: function () {
            $('#tblLista').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "order": [],
                "info": true,
            });
        },

        ubicaciones: function (id) {
            var data = "CodigoAlmacen=" + id;

            var options = {
                onSuccessComplete: function (data) {
                    window.location = data;
                }
            };
            baseGrid.ejecutar(route.almacen, "RedirectUbicaciones", undefined, $(almacen.idFormGrid), undefined, data, undefined, options.onSuccessComplete, "");
        },

        eliminar: function (id) {
            baseGrid.ejecutar(route.almacen, "eliminar", id, almacen.idFormGrid, almacen.updateTargetIdGrid, undefined, "¿Está seguro que desea eliminar este almacén?");
        }
    },

    editor: {

        init: function (id, per) {

            if (per == "True" || per == undefined)
                visibleGrab = true;
            else
                visibleGrab = false;

            var title = (id > 0) ? "MODIFICAR ALMACÉN" : "AGREGAR ALMACÉN";
            baseEditor.init(route.almacen, id, title, dialog.normal, "#Almacen_vNombre", undefined, undefined, undefined, visibleGrab);
        },

        load: function () {
            var formContent = $(almacen.idFormEditor);

            almacen.editor.validation(formContent);
        },

        validation: function (formContent) {
            $(formContent).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    "Almacen.vDescripcion": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Almacen.vAbreviatura": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Almacen.vDireccion": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    }
                }
            })
                .on('success.form.fv', function (e) {
                    e.preventDefault();

                    var id = $(almacen.idFormEditor + " #Almacen_siCodAlm").val();
                    if (id == "" || id == "0")
                        almacen.editor.agregar();
                    else
                        almacen.editor.modificar();
                });
        },


        agregar: function () {
            baseGrid.agregar(route.almacen, almacen.idFormEditor, almacen.idFormGrid, almacen.updateTargetIdGrid);
        },

        modificar: function () {
            baseGrid.modificar(route.almacen, "modificar", almacen.idFormEditor, almacen.idFormGrid, almacen.updateTargetIdGrid);
        }
    },

}