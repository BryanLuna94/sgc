﻿var inventario = {
    idFormGrid: "#inventarioGridForm",
    idFormEditor: "#inventarioEditorForm",
    updateTargetIdGrid: "#inventario-wrapper",

    grid: {

        load: function () {

            $("#Filtro_Descripcion").keypress(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    baseGrid.find(route.inventario, inventario.idFormGrid, "Refrescar", inventario.updateTargetIdGrid);
                }
            });


            baseGrid.init(route.inventario, inventario.idFormGrid, inventario.updateTargetIdGrid, inventario.editor.init);
        },

        exportar: function () {
            var form = $(inventario.idFormGrid);
            var url = $("#hdnURLExportar").val();
            form.attr('action', url);
            form.submit();
        },

        paginarParcial: function () {
            $('#tblLista').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "order": [],
                "info": true,
            });
        },

        eliminar: function (id) {
            baseGrid.ejecutar(route.inventario, "eliminar", id, inventario.idFormGrid, inventario.updateTargetIdGrid, undefined, "¿Está seguro que desea eliminar este inventario?");
        },

        activar: function (id) {
            baseGrid.ejecutar(route.inventario, "activar", id, inventario.idFormGrid, inventario.updateTargetIdGrid, undefined, "¿Está seguro que desea activar este inventario?");
        }
    },

    editor: {

        init: function (id, per) {
            if (per == "True" || per == undefined)
                visibleGrab = true;
            else
                visibleGrab = false;

            var title = (id > 0) ? "MODIFICAR INVENTARIO" : "AGREGAR INVENTARIO";
            baseEditor.init(route.inventario, id, title, dialog.normal, "#inventario_vDescripcion", undefined, undefined, undefined, visibleGrab);
        },

        load: function () {
            
            var formContent = $(inventario.idFormEditor);

            inventario.editor.validation(formContent);
        },

        validation: function (formContent) {
            $(formContent).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    "Inventario.vDescripcion": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Almacen": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Inventario.iCodPartner": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                }
            })
                .on('success.form.fv', function (e) {
                    e.preventDefault();

                    var almacen = $("#Almacen").val();
                    var partner = $("#Inventario_iCodPartner").val();

                    if (almacen == "0" || almacen === "") {
                        mensaje.advertencia("Debe seleccionar un almacén de la lista");
                        return;
                    }

                    if (partner == "0" || partner === "") {
                        mensaje.advertencia("Debe seleccionar un partner de la lista");
                        return;
                    }

                    var id = $(inventario.idFormEditor + " #Inventario_iCodInv").val();
                    if (id == "" || id == "0")
                        inventario.editor.agregar();
                    else
                        inventario.editor.modificar();
                });
        },


        agregar: function () {
            baseGrid.agregar(route.inventario, inventario.idFormEditor, inventario.idFormGrid, inventario.updateTargetIdGrid);
        },

        modificar: function () {
            baseGrid.modificar(route.inventario, "modificar", inventario.idFormEditor, inventario.idFormGrid, inventario.updateTargetIdGrid);
        }
    },

}