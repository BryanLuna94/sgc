﻿var persona = {
    idFormGrid: "#personaGridForm",
    idFormEditor: "#personaEditorForm",
    updateTargetIdGrid: "#persona-wrapper",

    grid: {

        load: function () {

            $("#Filtro_Persona").keypress(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    baseGrid.find(route.persona, persona.idFormGrid, "Refrescar", persona.updateTargetIdGrid);
                }
            });

            $("#Filtro_NroDocumento").keypress(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    baseGrid.find(route.persona, persona.idFormGrid, "Refrescar", persona.updateTargetIdGrid);
                }
            });

            $(".chkb").change(function (e) {
                baseGrid.find(route.programa, programa.idFormGrid, "Refrescar", programa.updateTargetIdGrid);
            });

            $(".btn-exportar").click(function (e) {
                persona.grid.exportar();
            });

            baseGrid.init(route.persona, persona.idFormGrid, persona.updateTargetIdGrid, persona.editor.init);
        },

        exportar: function () {
            var form = $(persona.idFormGrid);
            var url = $("#hdnURLExportar").val();
            form.attr('action', url);
            form.submit();
        },

        paginarParcial: function () {
            $('#tblLista').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "order": [],
                "info": true,
            });
        },

        eliminar: function (id) {
            baseGrid.ejecutar(route.persona, "eliminar", id, persona.idFormGrid, persona.updateTargetIdGrid, undefined, "¿Está seguro que desea eliminar este PERSONA?");
        }
    },

    editor: {

        init: function (id) {
            baseEditor.initComplete(route.persona, id);
        },

        load: function () {
            var formContent = $(persona.idFormEditor); 
            helperFunctions.datepicker(formContent);

            $("#Persona_tiCodTipDocIde").change(function (e) {
                $("#Persona_vNroDocIde").val("");
                persona.editor.mostrarDivTipoDocIdentidad();
            });

            persona.editor.mostrarDivTipoDocIdentidad();
            persona.editor.validation(formContent);
        },

        mostrarDivTipoDocIdentidad: function () {
            var tipodocide, tipodocideRuc, tipodocideDni;

            tipodocide = $("#Persona_tiCodTipDocIde").val();
            tipodocideRuc = $("#hdnTipDocIdeRuc").val();
            tipodocideDni = $("#hdnTipDocIdeDni").val();

            if (tipodocide === tipodocideRuc) {
                $(".div-razsoc").show();
                $(".div-name").hide();
                $("#Persona_vNroDocIde").attr("maxlength", "11");
                $(".lbl-pagweb").text("Página Web :");
                $(".lbl-fecnac").text("Aniversario :");
            } else if (tipodocide === tipodocideDni) {
                $(".div-name").show();
                $(".div-razsoc").hide();
                $("#Persona_vNroDocIde").attr("maxlength", "8");
                $(".lbl-pagweb").text("E-mail :");
                $(".lbl-fecnac").text("Fecha Nacimiento :");
            } else {
                $(".div-razsoc").show();
                $(".div-name").hide();
                $("#Persona_vNroDocIde").attr("maxlength", "15");
                $(".lbl-pagweb").text("Página Web :");
                $(".lbl-fecnac").text("Aniversario :");
            }
        },

        validation: function (formContent) {
            $(formContent).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    "Persona.tiCodTipDocIde": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Persona.siCodPais": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Persona.vNroDocIde": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Persona.vNombreComercial": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    }
                }
            })
                .on('success.form.fv', function (e) {
                    e.preventDefault();

                    var id = $(persona.idFormEditor + " #Persona_iCodPer").val();
                    if (id == "" || id == "0")
                        persona.editor.agregar();
                    else
                        persona.editor.modificar();
                });
        },

        agregar: function () {

            var options = {
                onSuccessComplete: function (data) {
                    $("#Persona_iCodPer").val(data);
                }
            };
            baseGrid.ejecutar(route.persona, "agregar", undefined, undefined, undefined, $(persona.idFormEditor).serialize(), undefined, options.onSuccessComplete, "Registro actualizado satisfactoriamente");
        },

        modificar: function () {

            var options = {
                onSuccessComplete: function (data) {
                    
                }
            };
            baseGrid.ejecutar(route.persona, "modificar", undefined, undefined, undefined, $(persona.idFormEditor).serialize(), undefined, options.onSuccessComplete, "Registro actualizado satisfactoriamente");
        }
    },

}


var personaoficina = {
    idFormGrid: "#personaOficinaGridForm",
    idFormEditor: "#personaOficinaEditorForm",
    updateTargetIdGrid: "#personaOficina-wrapper",

    grid: {

        load: function () {

            $(".btn-exportar").click(function (e) {
                persona.grid.exportar();
            });

            baseGrid.init(route.persona, personaoficina.idFormGrid, personaoficina.updateTargetIdGrid, personaoficina.editor.init);
        },

        exportar: function () {
            var form = $(persona.idFormGrid);
            var url = $("#hdnURLExportar").val();
            form.attr('action', url);
            form.submit();
        },

        paginarParcial: function () {
            $('#tblListaOficina').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "order": [],
                "info": true,
            });
        },

        eliminar: function (id) {
            baseGrid.ejecutar(route.persona, "eliminarOficina", id, personaoficina.idFormGrid, personaoficina.updateTargetIdGrid, undefined, "¿Está seguro que desea eliminar esta oficina?");
        }
    },

    editor: {

        init: function (id, per) {

            var codPer = $(persona.idFormEditor).find("#Persona_iCodPer").val();
            if (codPer == "" || codPer == "0") {
                mensaje.advertencia("Primero debe crear una empresa");
                return;
            }

            if (per == "True" || per == undefined)
                visibleGrab = true;
            else
                visibleGrab = false;

            var title = (id > 0) ? "MODIFICAR OFICINA" : "AGREGAR OFICINA";
            baseEditor.init(route.persona, id, title, dialog.normal, "#Oficina_vDescripcion", undefined, undefined, "EditorOficina", visibleGrab);
        },

        load: function () {
            var formContent = $(personaoficina.idFormEditor);

            var codPer = $(persona.idFormEditor).find("#Persona_iCodPer").val();
            $(formContent).find("#Oficina_iCodPer").val(codPer);

            personaoficina.editor.validation(formContent);
        },

        validation: function (formContent) {
            $(formContent).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    "Oficina.vDescripcion": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Oficina.vDireccion": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Oficina.siCodPais": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    }
                }
            })
                .on('success.form.fv', function (e) {
                    e.preventDefault();

                    var id = $(personaoficina.idFormEditor + " #Oficina_iCodPerOfi").val();
                    if (id == "" || id == "0")
                        personaoficina.editor.agregar();
                    else
                        personaoficina.editor.modificar();
                });
        },

        agregar: function () {
            baseGrid.modificar(route.persona, "agregaroficina", personaoficina.idFormEditor, personaoficina.idFormGrid, personaoficina.updateTargetIdGrid);
        },

        modificar: function () {
            baseGrid.modificar(route.persona, "modificaroficina", personaoficina.idFormEditor, personaoficina.idFormGrid, personaoficina.updateTargetIdGrid);
        }
    },

}


var personacontacto = {
    idFormGrid: "#personaContactoGridForm",
    idFormEditor: "#personaContactoEditorForm",
    updateTargetIdGrid: "#personaContacto-wrapper",

    grid: {

        load: function () {

            $(".btn-exportar").click(function (e) {
                personacontacto.grid.exportar();
            });

            baseGrid.init(route.persona, personacontacto.idFormGrid, personacontacto.updateTargetIdGrid, personacontacto.editor.init);
        },

        exportar: function () {
            var form = $(personacontacto.idFormGrid);
            var url = $("#hdnURLExportar").val();
            form.attr('action', url);
            form.submit();
        },

        paginarParcial: function () {
            $('#tblListaContacto').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "order": [],
                "info": true,
            });
        },

        eliminar: function (id) {
            baseGrid.ejecutar(route.persona, "eliminarContacto", id, personacontacto.idFormGrid, personacontacto.updateTargetIdGrid, undefined, "¿Está seguro que desea eliminar este contacto?");
        }
    },

    editor: {

        init: function (id, per) {

            var codPer = $(persona.idFormEditor).find("#Persona_iCodPer").val();
            if (codPer == "" || codPer == "0") {
                mensaje.advertencia("Primero debe crear una empresa");
                return;
            }

            if (per == "True" || per == undefined)
                visibleGrab = true;
            else
                visibleGrab = false;

            var title = (id > 0) ? "MODIFICAR CONTACTO" : "AGREGAR CONTACTO";
            baseEditor.init(route.persona, id, title, dialog.normal, "#Almacen_vNombre", undefined, undefined, "EditorContacto", visibleGrab);
        },

        load: function () {
            var formContent = $(personacontacto.idFormEditor);

            var codPer = $(persona.idFormEditor).find("#Persona_iCodPer").val();
            $(formContent).find("#Contacto_iCodPer").val(codPer);

            personacontacto.editor.validation(formContent);
        },

        validation: function (formContent) {
            $(formContent).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    "Contacto.vNombre": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    }
                }
            })
                .on('success.form.fv', function (e) {
                    e.preventDefault();

                    var id = $(personacontacto.idFormEditor + " #Contacto_iCodPerCon").val();
                    if (id == "" || id == "0")
                        personacontacto.editor.agregar();
                    else
                        personacontacto.editor.modificar();
                });
        },


        agregar: function () {
            baseGrid.modificar(route.persona, "agregarcontacto", personacontacto.idFormEditor, personacontacto.idFormGrid, personacontacto.updateTargetIdGrid);
        },

        modificar: function () {
            baseGrid.modificar(route.persona, "modificarcontacto", personacontacto.idFormEditor, personacontacto.idFormGrid, personacontacto.updateTargetIdGrid);
        }
    },

}