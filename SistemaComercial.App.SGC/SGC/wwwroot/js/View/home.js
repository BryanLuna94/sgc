﻿var home = {
    idFormConfig: "#configEditorForm",
    updateTargetIdGrid: "#empresario-wrapper",


    layout: {

        ready: function () {
            var formContent = $(home.idFormConfig);
        },

        load: function () {
            var urlActiva = $('.url-menu.active');
            var li = $(urlActiva).closest('li');
            var ul = $(li).closest('ul');
            var liPadre = $(ul).closest('li');
            if (liPadre.length > 0) {
                $(liPadre).find(".waves-effect").addClass("active");
                var url = $(li).find("a").attr("href");
                $("#hdnMnu").val();
                helperFunctions.setSesion("sUrl", url);
                var ulAbuelo = $(liPadre).closest('ul');
                var liAbuelo = $(ulAbuelo).closest('li');
                if (liAbuelo.length > 0) {
                    $(liAbuelo).find(".waves-effect").addClass("active");
                    $(liAbuelo).addClass("active");
                    $(liAbuelo).find(".nav-second-level.collapse").attr("aria-expanded", "true");
                    $(liAbuelo).find(".nav-second-level.collapse").addClass("in");
                }
            } else {
                var urlAnterior = helperFunctions.getSesion("sUrl");
                var link = $('a[href*="' + urlAnterior + '"]');
                var li = $(link).closest('li');
                var ul = $(li).closest('ul');
                var liPadre = $(ul).closest('li');
                $(liPadre).find(".waves-effect").addClass("active");
                $(liPadre).find("ul").addClass("in");
                $(link).addClass("active");
                $(liPadre).addClass("active");
            }

            var urlFotos = $(".img-usu").attr("src");
            var urlCompleta = helperAjax.obtenerUrlAbsoluta(urlFotos);
            $(".img-usu").attr("src", urlCompleta);
        },

    },

}
