﻿var movimiento = {
    idFormGrid: "#movimientoGridForm",
    idFormEditor: "#movimientoEditorForm",
    idFormEditorDetalle: "#movimientoEditorDetalleForm",
    idFormEditorDetalleLiquidacion: "#movimientoEditorDetalleLiquidacionForm",
    idFormEditorArchivo: "#archivosForm",
    idFormEditorArticulo: "#articuloEditorForm",
    updateTargetIdGrid: "#movimiento-wrapper",
    updateTargetIdGridDetalle: "#movimientoDetalle-wrapper",
    updateTargetIdGridDetalleLiquidacion: "#liquidacion-wrapper",
    updateTargetIdGridArchivo: "#archivos-wrapper",

    grid: {

        load: function () {

            var formContent = $(movimiento.idFormGrid);
            helperFunctions.datepicker(formContent);

            $("#Filtro_Correlativo").keypress(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
                }
            });

            $("#Filtro_NumeroDoc").keypress(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
                }
            });

            $("#Filtro_FechaDesde").keypress(function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
                }
            });

            $("#Filtro_FechaHasta").keypress(function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
                }
            });

            $("#Filtro_AlertaId").keypress(function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
                }
            });

            $("#Filtro_GuiaArea").keypress(function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
                }
            });

            $("#Filtro_RMA").keypress(function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
                }
            });

            $("#Filtro_OrdExterior").keypress(function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
                }
            });

            $("#Filtro_FacturaComercial").keypress(function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
                }
            });

            $("#Filtro_CodigoOriginal").keypress(function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
                }
            });

            $("#Filtro_Articulo").keypress(function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
                }
            });

            $("#Filtro_Serie").keypress(function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
                }
            });

            $("#Filtro_Persona").keypress(function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
                }
            });

            $("#Filtro_UsuarioCrea").change(function (e) {
                baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
            });

            $("#Filtro_TipoMovimiento").change(function (e) {
                baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
            });

            $("#Filtro_CodTipoDoc").change(function (e) {
                baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
            });

            $("#Filtro_FechaDesde").change(function (e) {
                baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
            });

            $("#Filtro_FechaHasta").change(function (e) {
                baseGrid.find(route.movimiento, movimiento.idFormGrid, "Refrescar", movimiento.updateTargetIdGrid);
            });

            $(".btn-exportar").click(function (e) {
                movimiento.grid.exportar();
            });

            $(".btn-import").click(function (e) {
                movimiento.editor.init(0, 1, 'IMP');
            });

            $(".btn-devolu").click(function (e) {
                movimiento.editor.init(0, 1, 'DEV');
            });

            $(".btn-compra").click(function (e) {
                movimiento.editor.init(0, 1, 'COM');
            });

            $(".btn-export").click(function (e) {
                movimiento.editor.init(0, 2, 'EXP');
            });

            $(".btn-delive").click(function (e) {
                movimiento.editor.init(0, 2, 'DEL');
            });

            $(".btn-venta").click(function (e) {
                movimiento.editor.init(0, 2, 'VEN');
            });

            baseGrid.init(route.movimiento, movimiento.idFormGrid, movimiento.updateTargetIdGrid, movimiento.editor.init);
        },

        exportar: function () {
            var form = $(movimiento.idFormGrid);
            var url = $("#hdnURLExportar").val();
            form.attr('action', url);
            form.submit();
        },

        paginarParcial: function () {
            $('#tblLista').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "order": [],
                "info": true,
            });
        },

        eliminar: function (id) {
            baseGrid.ejecutar(route.movimiento, "eliminar", id, movimiento.idFormGrid, movimiento.updateTargetIdGrid, undefined, "¿Está seguro que desea eliminar este movimiento?");
        }
    },

    editor: {

        init: function (id, tipo, form) {
            id = id + "|" + tipo + "|" + form;

            var options = {
                onSuccessComplete: function (data) {
                    window.location = data;
                }
            };
            baseGrid.ejecutar(route.movimiento, "RedirectEditor", id, undefined, undefined, undefined, undefined, options.onSuccessComplete, "");
        },

        load: function () {
            var formContent = $(movimiento.idFormEditor);
            var canal1 = $("#MovimientoExt_tiCodCan").val();
            helperFunctions.datepicker(formContent);

            $(".btn-agregar").click(function (e) {
                movimiento.editor.grabar();
            });

            $(".btn-imp").click(function (e) {
                movimiento.editor.imprimirGuia();
            });

            $(".btn-dj").click(function (e) {
                movimiento.editor.imprimirDeclaracionJurada();
            });

            $(".btn-cerrar").click(function (e) {
                movimiento.editor.cerrar();
            });

            $(".btn-adjunto").click(function (e) {
                movimiento.archivos.init();
            });

            $(".select2").select2();

            $("#MovimientoExt_iCodPartner").change(function (e) {
                movimiento.editor.grillaLiquidaciones(e);
            });

            $(".btn-editor").click(function (e) {
                $("#hdnNuevoArt").val("1");
                //var codigoStope = $(formContent).find("#Movimiento_iCodCabMov").val();
                //if (codigoStope === "0") {
                //    return;
                //}

                movimiento.editorDetalle.init(0);

            });

            $(formContent).find(".btn-editorLiquidacion").click(function (e) {
                //var codigoStope = $(formContent).find("#Movimiento_iCodCabMov").val();
                //if (codigoStope === "0") {
                //    return;
                //}

                movimiento.editorDetalleIngLiquidacion.init(0);

            });

            movimiento.editor.canal(1, canal1);

            var options = {
                onSuccessComplete: function (response, data) {
                    response($.map(data, function (item) {
                        return { label: item.Descripcion, value: item.Descripcion, captura: item.Codigo };
                    }))
                },
                select: function (event, ui) {
                    var codigo = ui.item.captura;
                    $("#MovimientoExt_iCodPer").val(codigo);
                }
            };
            baseGrid.autocomplete(route.persona, "ListaPersonas_Auto", "MovimientoDto_PersonaExt", options.onSuccessComplete, options.select);

            //movimiento.editor.validation(formContent);
        },

        cerrar: function () {
            var id = $("#Movimiento_iCodCabMov").val();

            var options = {
                onSuccessComplete: function (data) {
                    window.location = data;
                }
            };

            baseGrid.ejecutar(route.movimiento, "cerrar", id, movimiento.idFormGrid, movimiento.updateTargetIdGrid, undefined, "¿Está seguro que desea cerrar este movimiento?", options.onSuccessComplete);
        },

        grillaLiquidaciones: function (e) {

            var id = e.val;
            var idCodCabMov = $("#Movimiento_iCodCabMov").val();
            var tipoMov = $("#Movimiento_vTipoMov").val();
            id = id + "|" + tipoMov;

            if (idCodCabMov == 0 && (tipoMov == 'IMP' || tipoMov == 'EXP')) {
                baseGrid.find(route.movimiento, undefined, "ListarConceptosLiquidacionPorCab", movimiento.updateTargetIdGridDetalleLiquidacion, undefined, id);
            }

        },

        paginarParcial: function () {
            $('#tblListaDetalle').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "order": [],
                "info": true,
            });
        },

        paginarLiquidacion: function () {
            $('#tblListaLiquidacion').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "order": [],
                "info": true,
            });
        },

        imprimirGuia: function () {
            var id = $("#Movimiento_iCodCabMov").val();
            var url = $("#hdnURLImprimirGuia").val();

            if (id === "0") {
                mensaje.advertencia("Primero debe generar una guía de remisión");
                return;
            }

            window.open(url.toString().replace('__id__', id), '_blank')
        },

        imprimirDeclaracionJurada: function () {
            var id = $("#Movimiento_iCodCabMov").val();
            var url = $("#hdnURLDeclaracionJurada").val();

            if (id === "0") {
                mensaje.advertencia("Primero debe guardar la exportación");
                return;
            }

            window.open(url.toString().replace('__id__', id), '_blank')
        },

        canal: function (tipo, canal) {
            $("#cn" + tipo).removeClass("text-success");
            $("#cn" + tipo).removeClass("text-warning");
            $("#cn" + tipo).removeClass("text-danger");

            if (canal == 1) {
                $("#spcn" + tipo).text("Verde");
                $("#cn" + tipo).addClass("text-success");
            } else if (canal == 2) {
                $("#spcn" + tipo).text("Ambar");
                $("#cn" + tipo).addClass("text-warning");
            } else if (canal == 3) {
                $("#spcn" + tipo).text("Rojo");
                $("#cn" + tipo).addClass("text-danger");
            } else {
                $("#spcn" + tipo).text("Ninguno");
            }

            if (tipo == 1) {
                $("#MovimientoExt_tiCodCan").val(canal);
            } else if (tipo == 2) {
                $("#MovimientoExt_tiCodCanAct").val(canal);
            }
        },

        validation: function (formContent) {
            $(formContent).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    //"Movimiento.tiCodTipMov": {
                    //    validators: {
                    //        notEmpty: {
                    //            message: 'Este campo es obligatorio'
                    //        }
                    //    }
                    //},
                    //"MovimientoDto.FechaRecepcion": {
                    //    validators: {
                    //        notEmpty: {
                    //            message: 'Este campo es obligatorio'
                    //        }
                    //    }
                    //},
                    //"MovimientoDto.HoraRecepcion": {
                    //    validators: {
                    //        notEmpty: {
                    //            message: 'Este campo es obligatorio'
                    //        }
                    //    }
                    //}
                }
            })
                .on('success.form.fv', function (e) {
                    e.preventDefault();

                    movimiento.editor.grabar();
                });
        },

        grabar: function (onSuccess) {

            let result = true;

            var persona = $("#MovimientoExt_iCodPerOperador").val();
            var marca = $("#Movimiento_iCodPer").val();
            var personaExt = $("#MovimientoExt_iCodPartner").val();

            if (personaExt == "0" || personaExt === "") {
                mensaje.advertencia("Debe seleccionar un partner de la lista");
                return false;
            }

            if (marca == "0" || marca === "") {
                mensaje.advertencia("Debe seleccionar una empresa de la lista");
                return false;
            }

            if (persona == "0" || persona === "") {
                mensaje.advertencia("Debe seleccionar un operador logístico/agente aduanero de la lista");
                return false;
            }

            var id = $(movimiento.idFormEditor + " #Movimiento_iCodCabMov").val();
            if (id == "" || id == "0")
                movimiento.editor.agregar(onSuccess);
            else
                movimiento.editor.modificar(onSuccess);

            return result;
        },

        agregar: function (onSuccess) {

            var options = {
                onSuccessComplete: function (data) {
                    $("#Movimiento_iCodCabMov").val(data.iCodCabMov);
                    $("#Movimiento_vCorrelativo").val(data.vCorrelativo);

                    if (onSuccess != undefined) onSuccess();

                    $(".div-cerrar").show();

                }
            };
            baseGrid.ejecutar(route.movimiento, "agregar", undefined, undefined, undefined, $(movimiento.idFormEditor).serialize(), undefined, options.onSuccessComplete, "Registro actualizado satisfactoriamente");
        },

        modificar: function (onSuccess) {

            var options = {
                onSuccessComplete: function (data) {
                    if (onSuccess != undefined) onSuccess();
                }
            };
            baseGrid.ejecutar(route.movimiento, "modificar", undefined, undefined, undefined, $(movimiento.idFormEditor).serialize(), undefined, options.onSuccessComplete, "Registro actualizado satisfactoriamente");
        },

        eliminar: function (id) {
            baseGrid.ejecutar(route.movimiento, "eliminardetalle", id, movimiento.idFormEditorDetalle, movimiento.updateTargetIdGridDetalle, undefined, "¿Está seguro que desea eliminar este movimiento?");
        }
    },

    editorDetalle: {

        init: function (id, per) {

            var options = {
                onSuccessComplete: function (data) {
                    if (per == "True" || per == undefined)
                        visibleGrab = true;
                    else
                        visibleGrab = false;

                    var title = (id > 0) ? "MODIFICAR PRODUCTO" : "AGREGAR PRODUCTO";

                    var idMovCab = $("#Movimiento_iCodCabMov").val();

                    id = idMovCab + "|" + id;

                    baseEditor.init(route.movimiento, id, title, dialog.normal, "#MovimientoDetalleDto_Articulo", undefined, undefined, "EditorDetalle", visibleGrab);
                }
            };

            movimiento.editor.grabar(options.onSuccessComplete);

        },

    },

    editorDetalleIng: {

        load: function () {
            var formContent = $(movimiento.idFormEditorDetalle);

            var options = {
                onSuccessComplete: function (response, data) {
                    response($.map(data, function (item) {
                        return { label: item.Descripcion, value: item.Descripcion, captura: item.Codigo };
                    }))
                },
                select: function (event, ui) {
                    var codigo = ui.item.captura;
                    $("#MovimientoDetalle_iCodArt").val(codigo);
                }
            };
            baseGrid.autocomplete(route.articulo, "ListaArticulos_Auto", "MovimientoDetalleDto_Articulo", options.onSuccessComplete, options.select);

            $("#MovimientoDetalleDto_Articulo").keydown(function (e) {
                if (e.keyCode == 8) {
                    $("#MovimientoDetalle_iCodArt").val("");
                }
            });

            $("#Stock_siCodAlm").change(function (e) {
                movimiento.editorDetalleIng.comboPisos();
            });

            $("#Piso").change(function (e) {
                movimiento.editorDetalleIng.comboRacks();
            });

            $("#Rack").change(function (e) {
                movimiento.editorDetalleIng.comboUbicacion();
            });

            helperFunctions.formatoNumero(".input-align-right");

            movimiento.editorDetalleIng.validation(formContent);
        },

        validation: function (formContent) {
            $(formContent).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    "MovimientoDetalleDto.Articulo": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "MovimientoDetalle.vSerie": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Stock.siCodAlm": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "DetalleStock.iCodEstArt": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "MovimientoDetalle.dCosto": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "MovimientoDetalle.dPeso": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    }
                }
            })
                .on('success.form.fv', function (e) {
                    e.preventDefault();

                    var id = $(movimiento.idFormEditorDetalle + " #MovimientoDetalle_iCodDetMov").val();
                    if (id == "" || id == "0")
                        movimiento.editorDetalleIng.agregar();
                    else
                        movimiento.editorDetalleIng.modificar();
                });
        },

        comboPisos: function () {

            var id = $("#Stock_siCodAlm").val();

            var options = {
                onSuccessComplete: function (data) {
                    $("#div-pisos").html(data);

                    $("#Piso").change(function (e) {
                        movimiento.editorDetalleIng.comboRacks();
                    });
                }
            };

            baseGrid.ejecutar(route.movimiento, "ListarUbicacionesPisos", id, undefined, undefined, undefined, undefined, options.onSuccessComplete, "");
        },

        comboRacks: function () {

            var id = $("#Piso").val();
            var idAlm = $("#Stock_siCodAlm").val();
            id = idAlm + "|" + id;

            var options = {
                onSuccessComplete: function (data) {
                    $("#div-racks").html(data);

                    $("#Rack").change(function (e) {
                        movimiento.editorDetalleIng.comboUbicacion();
                    });
                }
            };

            baseGrid.ejecutar(route.movimiento, "ListarUbicacionesRacks", id, undefined, undefined, undefined, undefined, options.onSuccessComplete, "");
        },

        comboUbicacion: function () {

            var id = $("#Rack").val();
            var idAlm = $("#Stock_siCodAlm").val();
            var idPiso = $("#Piso").val();

            id = idAlm + "|" + idPiso + "|" + id;

            var options = {
                onSuccessComplete: function (data) {
                    $("#div-ubic").html(data);
                }
            };

            baseGrid.ejecutar(route.movimiento, "ListarUbicacionesPorRacks", id, undefined, undefined, undefined, undefined, options.onSuccessComplete, "");
        },

        agregar: function () {

            var articulo = $("#MovimientoDetalle_iCodArt").val();
            var ubicacion = $("#DetalleStock_iCodUbi").val();

            if (articulo == "0" || articulo === "") {
                mensaje.advertencia("Debe seleccionar un producto de la lista");
                return;
            }

            if (ubicacion == "0" || ubicacion === "") {
                mensaje.advertencia("Debe seleccionar la ubicación donde almacenará el producto");
                return;
            }

            baseGrid.modificar(route.movimiento, "agregarDetalle", movimiento.idFormEditorDetalle, undefined, movimiento.updateTargetIdGridDetalle);
        },

        modificar: function () {

            var articulo = $("#MovimientoDetalle_iCodArt").val();
            var ubicacion = $("#DetalleStock_iCodUbi").val();

            if (articulo == "0" || articulo === "") {
                mensaje.advertencia("Debe seleccionar un producto de la lista");
                return;
            }

            if (ubicacion == "0" || ubicacion === "") {
                mensaje.advertencia("Debe seleccionar la ubicación donde almacenará el producto");
                return;
            }

            baseGrid.modificar(route.movimiento, "ActualizarDetalle", movimiento.idFormEditorDetalle, undefined, movimiento.updateTargetIdGridDetalle);
        }
    },

    editorDetalleIngLiquidacion: {

        init: function (id, per) {

            var options = {
                onSuccessComplete: function (data) {
                    if (per == "True" || per == undefined)
                        visibleGrab = true;
                    else
                        visibleGrab = false;

                    var title = (id > 0) ? "MODIFICAR GASTO" : "AGREGAR GASTO";

                    var idMovCab = $("#Movimiento_iCodCabMov").val();

                    id = idMovCab + "|" + id;

                    baseEditor.init(route.movimiento, id, title, dialog.normal, "#Liquidacion_vDescripcion", undefined, undefined, "EditorDetalleLiquidacion", visibleGrab);
                }
            };

            movimiento.editor.grabar(options.onSuccessComplete);


        },

        load: function () {
            var formContent = $(movimiento.idFormEditorDetalleLiquidacion);

            helperFunctions.formatoNumero(".input-align-right");

            movimiento.editorDetalleIngLiquidacion.validation(formContent);
        },

        validation: function (formContent) {
            $(formContent).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    "Liquidacion.vDescripcion": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Liquidacion.dCosto": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                }
            })
                .on('success.form.fv', function (e) {
                    e.preventDefault();

                    var id = $(movimiento.idFormEditorDetalleLiquidacion + " #Liquidacion_iCodLiquidacion").val();
                    if (id == "" || id == "0")
                        movimiento.editorDetalleIngLiquidacion.agregar();
                    else
                        movimiento.editorDetalleIngLiquidacion.modificar();
                });
        },

        agregar: function () {

            baseGrid.modificar(route.movimiento, "agregarDetalleLiquidacion", movimiento.idFormEditorDetalleLiquidacion, undefined, movimiento.updateTargetIdGridDetalleLiquidacion);
        },

        modificar: function () {

            baseGrid.modificar(route.movimiento, "ActualizarDetalleLiquidacion", movimiento.idFormEditorDetalleLiquidacion, undefined, movimiento.updateTargetIdGridDetalleLiquidacion);
        },

        eliminar: function (id) {
            baseGrid.ejecutar(route.movimiento, "EliminarDetalleLiquidacion", id, movimiento.idFormEditorDetalleLiquidacion, movimiento.updateTargetIdGridDetalleLiquidacion, undefined,
                "¿Está seguro que desea eliminar este gasto?");
        }
    },

    editorDetalleSal: {

        load: function () {
            var formContent = $(movimiento.idFormEditorDetalle);

            var options = {
                onSuccessComplete: function (response, data) {
                    response($.map(data, function (item) {
                        return { label: item.Descripcion, value: item.Descripcion, captura: item.Codigo };
                    }))
                },
                select: function (event, ui) {
                    var codigo = ui.item.captura;
                    $("#MovimientoDetalle_iCodDetMovIng").val(codigo);
                    movimiento.editorDetalleSal.obtenerDatosMovDetIng(codigo);
                }
            };
            baseGrid.autocomplete(route.movimiento, "ListaArticulosParaSalida_Auto", "MovimientoDetalleDto_Articulo", options.onSuccessComplete, options.select);

            $("#MovimientoDetalleDto_Articulo").keydown(function (e) {
                if (e.keyCode == 8) {
                    $("#MovimientoDetalle_iCodDetMovIng").val("");
                    $("#MovimientoDetalle_vSerie").val("");
                    $("#MovimientoDetalleDto_Almacen").val("");
                    $("#MovimientoDetalleDto_Ubicacion").val("");
                    $("#MovimientoDetalleDto_EstadoArticulo").val("");
                    $("#MovimientoDetalleDto_CostoIngreso").val("");
                    $("#MovimientoDetalleDto.PrecioIngreso").val("");
                }
            });

            movimiento.editorDetalleSal.validation(formContent);
        },

        obtenerDatosMovDetIng: function (id) {

            var options = {
                onSuccessComplete: function (data) {
                    $("#MovimientoDetalle_vSerie").val(data.Serie);
                    $("#MovimientoDetalleDto_Almacen").val(data.Almacen);
                    $("#MovimientoDetalle_vNombreProducto").val(data.NombreProducto);
                    $("#MovimientoDetalleDto_Ubicacion").val(data.Ubicacion);
                    $("#MovimientoDetalleDto_EstadoArticulo").val(data.EstadoArticulo);
                    $("#MovimientoDetalleDto_CostoIngreso").val(data.CostoIngreso);
                    $("#MovimientoDetalleDto_PrecioIngreso").val(data.PrecioIngreso);
                }
            };
            baseGrid.ejecutar(route.movimiento, "ObtenerDetalleMovimientoParaSalida", id, undefined, undefined, undefined, undefined, options.onSuccessComplete, "");

        },

        validation: function (formContent) {
            $(formContent).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    "MovimientoDetalleDto.Articulo": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    }
                }
            })
                .on('success.form.fv', function (e) {
                    e.preventDefault();

                    var id = $(movimiento.idFormEditorDetalle + " #MovimientoDetalle_iCodDetMov").val();
                    if (id == "" || id == "0")
                        movimiento.editorDetalleSal.agregar();
                    else
                        movimiento.editorDetalleSal.modificar();
                });
        },

        agregar: function () {

            var articulo = $("#MovimientoDetalle_iCodDetMovIng").val();

            if (articulo == "0" || articulo === "") {
                mensaje.advertencia("Debe seleccionar un producto de la lista");
                return;
            }

            baseGrid.modificar(route.movimiento, "agregarDetalle", movimiento.idFormEditorDetalle, undefined, movimiento.updateTargetIdGridDetalle);
        },

        modificar: function () {

            var articulo = $("#MovimientoDetalle_iCodDetMovIng").val();

            if (articulo == "0" || articulo === "") {
                mensaje.advertencia("Debe seleccionar un producto de la lista");
                return;
            }

            baseGrid.modificar(route.movimiento, "ActualizarDetalle", movimiento.idFormEditorDetalle, undefined, movimiento.updateTargetIdGridDetalle);
        }
    },

    archivos: {

        init: function () {

            var title = "ARCHIVOS ADJUNTOS";
            var id = $("#Movimiento_iCodCabMov").val();

            if (id === "0") {
                mensaje.advertencia("Primero debe generar un movimiento para agregar archivos, presione el botón guardar")
                return;
            }

            baseEditor.init(route.movimiento, id, title, dialog.normal, undefined, undefined, undefined, "EditorArchivosCab", false);

        },

        load: function () {
            var formContent = $(movimiento.idFormEditorArchivo);
            movimiento.archivos.validation(formContent);
        },

        validation: function (formContent) {
            $(formContent).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    "Descripcion": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Archivo": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                }
            })
                .on('success.form.fv', function (e) {
                    e.preventDefault();

                    movimiento.archivos.agregarArchivo();
                });
        },

        agregarArchivo: function () {

            var formElement = document.getElementById("archivosForm");
            var data = new FormData(formElement);
            var input = document.getElementById('Archivo');

            data.append('Archivo', input.files[0]);

            var options = {
                onSuccessComplete: function (data) {
                    $("#Descripcion").val("");
                    $(movimiento.updateTargetIdGridArchivo).html(data);
                    $(movimiento.idFormEditorArchivo).data('formValidation').resetForm($(movimiento.idFormEditorArchivo));
                }
            };

            var options = {
                route: route.movimiento,
                metodo: "AgregarArchivoCab",
                data: data,
                processData: false,
                contentType: false,
                messageSuccess: "Registro Satisfactorio",
                updateTargetId: undefined,
                idWrapperEditor: undefined,
                onSuccessComplete: options.onSuccessComplete,
                close: false
            };

            baseGrid.ejecutarAccion(options);
        },

        eliminarArchivo: function (id) {

            var options = {
                onSuccessComplete: function (data) {
                    $(movimiento.updateTargetIdGridArchivo).html(data);
                }
            };
            baseGrid.ejecutar(route.movimiento, "EliminarArchivoCab", id, undefined, undefined, undefined, "¿Está seguro que desea eliminar este archivo?", options.onSuccessComplete);
        }

    },

    archivosDet: {

        init: function (id) {

            var title = "ARCHIVOS ADJUNTOS";
            baseEditor.init(route.movimiento, id, title, dialog.normal, undefined, undefined, undefined, "EditorArchivosDet", false);

        },

        load: function () {
            var formContent = $(movimiento.idFormEditorArchivo);
            movimiento.archivosDet.validation(formContent);
        },

        validation: function (formContent) {
            $(formContent).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    "Descripcion": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Archivo": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                }
            })
                .on('success.form.fv', function (e) {
                    e.preventDefault();

                    movimiento.archivosDet.agregarArchivo();
                });
        },

        agregarArchivo: function () {

            var formElement = document.getElementById("archivosForm");
            var data = new FormData(formElement);
            var input = document.getElementById('Archivo');

            data.append('Archivo', input.files[0]);

            var options = {
                onSuccessComplete: function (data) {
                    $("#Descripcion").val("");
                    $(movimiento.updateTargetIdGridArchivo).html(data);
                    $(movimiento.idFormEditorArchivo).data('formValidation').resetForm($(movimiento.idFormEditorArchivo));
                }
            };

            var options = {
                route: route.movimiento,
                metodo: "AgregarArchivoDet",
                data: data,
                processData: false,
                contentType: false,
                messageSuccess: "Registro Satisfactorio",
                updateTargetId: undefined,
                idWrapperEditor: undefined,
                onSuccessComplete: options.onSuccessComplete,
                close: false
            };

            baseGrid.ejecutarAccion(options);
        },

        eliminarArchivo: function (id) {

            var options = {
                onSuccessComplete: function (data) {
                    $(movimiento.updateTargetIdGridArchivo).html(data);
                }
            };
            baseGrid.ejecutar(route.movimiento, "EliminarArchivoDet", id, undefined, undefined, undefined, "¿Está seguro que desea eliminar este archivo?", options.onSuccessComplete);
        }

    },

    articulo: {

        init: function () {

            var title = "AGREGAR ARTICULO";
            baseEditor.init(route.movimiento, 0, title, dialog.normal, "#Articulo_vDescripcion", undefined, undefined, "editorarticulo");
        },

        load: function () {
            var formContent = $(movimiento.idFormEditorArticulo);

            movimiento.articulo.validation(formContent);
        },

        validation: function (formContent) {
            $(formContent).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    "Articulo.vDescripcion": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Articulo.vCodigoOriginal": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Articulo.siCodUMed": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Articulo.iCodMarca": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                }
            })
                .on('success.form.fv', function (e) {
                    e.preventDefault();

                    movimiento.articulo.agregar();
                });
        },

        agregar: function () {

            var options = {
                onSuccessComplete: function (data) {
                    $("#MovimientoDetalle_iCodArt").val(data.iCodArt);
                    $("#MovimientoDetalleDto_Articulo").val(data.vCodigoOriginal + " - " + data.vDescripcion);
                    $(movimiento.idFormEditorDetalle).data('formValidation').resetField($('#MovimientoDetalleDto_Articulo'));
                }
            };
            baseGrid.ejecutar(route.movimiento, "agregarArticulo", undefined, movimiento.idFormEditorArticulo, movimiento.idFormEditorArticulo, $(movimiento.idFormEditorArticulo).serialize(), undefined, options.onSuccessComplete, undefined, true, undefined, "AGREGAR ARTICULO");
        },

    },
}
