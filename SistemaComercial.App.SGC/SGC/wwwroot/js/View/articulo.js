﻿var articulo = {
    idFormGrid: "#articuloGridForm",
    idFormEditor: "#articuloEditorForm",
    updateTargetIdGrid: "#articulo-wrapper",

    grid: {

        load: function () {

            $("#Filtro_Descripcion").keypress(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    baseGrid.find(route.articulo, articulo.idFormGrid, "Refrescar", articulo.updateTargetIdGrid);
                }
            });

            $("#Filtro_CodigoOriginal").keypress(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    baseGrid.find(route.articulo, articulo.idFormGrid, "Refrescar", articulo.updateTargetIdGrid);
                }
            });

            $("#Filtro_CodigoAlterno").keypress(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    baseGrid.find(route.articulo, articulo.idFormGrid, "Refrescar", articulo.updateTargetIdGrid);
                }
            });

            $("#Filtro_Marca").keypress(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    baseGrid.find(route.articulo, articulo.idFormGrid, "Refrescar", articulo.updateTargetIdGrid);
                }
            });

            $(".btn-exportar").click(function (e) {
                articulo.grid.exportar();
            });

            baseGrid.init(route.articulo, articulo.idFormGrid, articulo.updateTargetIdGrid, articulo.editor.init);
        },

        exportar: function () {
            var form = $(articulo.idFormGrid);
            var url = $("#hdnURLExportar").val();
            form.attr('action', url);
            form.submit();
        },

        paginarParcial: function () {
            $('#tblLista').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "order": [],
                "info": true,
            });
        },

        eliminar: function (id) {
            baseGrid.ejecutar(route.articulo, "eliminar", id, articulo.idFormGrid, articulo.updateTargetIdGrid, undefined, "¿Está seguro que desea eliminar este artículo?");
        }
    },

    editor: {

        init: function (id, per) {

            if (per == "True" || per == undefined)
                visibleGrab = true;
            else
                visibleGrab = false;

            var title = (id > 0) ? "MODIFICAR ARTICULO" : "AGREGAR ARTICULO";
            baseEditor.init(route.articulo, id, title, dialog.normal, "#Articulo_vDescripcion", undefined, undefined, undefined, visibleGrab);
        },

        load: function () {
            var formContent = $(articulo.idFormEditor);

            articulo.editor.validation(formContent);
        },

        validation: function (formContent) {
            $(formContent).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    "Articulo.vDescripcion": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Articulo.vCodigoOriginal": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Articulo.siCodUMed": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                    "Articulo.iCodMarca": {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es obligatorio'
                            }
                        }
                    },
                }
            })
                .on('success.form.fv', function (e) {
                    e.preventDefault();

                    var id = $(articulo.idFormEditor + " #Articulo_iCodArt").val();
                    if (id == "" || id == "0")
                        articulo.editor.agregar();
                    else
                        articulo.editor.modificar();
                });
        },


        agregar: function () {
            baseGrid.agregar(route.articulo, articulo.idFormEditor, articulo.idFormGrid, articulo.updateTargetIdGrid);
        },

        modificar: function () {
            baseGrid.modificar(route.articulo, "modificar", articulo.idFormEditor, articulo.idFormGrid, articulo.updateTargetIdGrid);
        }
    },

}