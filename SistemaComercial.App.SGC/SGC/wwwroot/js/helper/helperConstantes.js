//Parametros de configuración usados para obtener la url
var helperConstantes = {
    urlBase: "http://localhost:2550//",
    nombreAplicativo: "",
    rutaLogin: "Autenticacion/Login"
};

var dialog = {
    normal: BootstrapDialog.SIZE_NORMAL,
    mediano: BootstrapDialog.SIZE_SMALL,
    ancho: BootstrapDialog.SIZE_WIDE,
    grande: BootstrapDialog.SIZE_LARGE,
};

var route = {
    login: {
        area: "",
        controller: "autenticacion",
        idTab: ""
    },
    home: {
        area: "",
        controller: "home",
        idTab: ""
    },
    almacen: {
        area: "",
        controller: "almacen",
        idTab: ""
    },
    unidadmedida: {
        area: "",
        controller: "unidadmedida",
        idTab: ""
    },
    reporte: {
        area: "",
        controller: "reporte",
        idTab: ""
    },
    usuario: {
        area: "",
        controller: "usuario",
        idTab: ""
    },
    perfil: {
        area: "",
        controller: "perfil",
        idTab: ""
    },
    configuracion: {
        area: "",
        controller: "configuracion",
        idTab: ""
    },
    persona: {
        area: "",
        controller: "persona",
        idTab: ""
    },
    articulo: {
        area: "",
        controller: "articulo",
        idTab: ""
    },
    inventario: {
        area: "",
        controller: "inventario",
        idTab: ""
    },
    movimiento: {
        area: "",
        controller: "movimiento",
        idTab: ""
    },
    marca: {
        area: "",
        controller: "marca",
        idTab: ""
    },
    archivos: {
        area: "",
        controller: "archivos",
        idTab: ""
    },
};