﻿using SGC.Models.Movimiento;
using System.Web.Mvc;
using SGC.ServicioSistemaComercial;
using System.Threading.Tasks;
using System.ServiceModel;
using SGC.Helper;
using System.Web.UI.WebControls;
using SistemaComercial.App.Utility;
using System.Web.Routing;
using System.Data;
using System.IO;
using SGC.Models.Archivo;
using System;
using SGC.Models.Articulo;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using CrystalDecisions.Shared;

namespace SGC.Controllers
{
    [Atributos.SessionExpireFilter]
    public class MovimientoController : Controller
    {
        ServicioSistemaComercialClient _ServicioSistemaComercial = new ServicioSistemaComercialClient();

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        [Atributos.AccesoAutorizadoFilter]
        public async Task<ActionResult> Ingresos(MovimientoGridViewModel model)
        {
            try
            {
                //Construimos el modelo
                var tipoMov = System.Convert.ToByte(Constantes.Tablas.TipoMovimiento.INGRESO);
                model = await ObtenerModelGrid(model, tipoMov);

                //Retornamos vista con modelo
                return View(model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        [Atributos.AccesoAutorizadoFilter]
        public async Task<ActionResult> Salidas(MovimientoGridViewModel model)
        {
            try
            {
                //Construimos el modelo
                var tipoMov = System.Convert.ToByte(Constantes.Tablas.TipoMovimiento.SALIDA);
                model = await ObtenerModelGrid(model, tipoMov);

                //Retornamos vista con modelo
                return View(model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        private async Task<MovimientoGridViewModel> ObtenerModelGrid(MovimientoGridViewModel model, byte tipo)
        {
            model.Filtro = model.Filtro ?? new MovimientoFiltroDto();
            //Construimos el request 
            var request = new MovimientoRequestDto
            {
                Filtro = model.Filtro
            };

            request.Filtro.TipoMovimiento = tipo;

            //Invocamos al servicio
            var response = await _ServicioSistemaComercial.ObtenerIndexMovimientosAsync(request);

            //Construimos el modelo
            model = new MovimientoGridViewModel(response)
            {
                Filtro = request.Filtro
            };
            //Retornamos el modelo
            return model;

        }

        /// <summary>Método que actualiza la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> Refrescar(MovimientoGridViewModel model)
        {
            try
            {
                //Construimos el modelo
                model = await ObtenerModelGrid(model, 1);

                //Retornamos vista con modelo
                return PartialView("_Index.Grid", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public ActionResult RedirectEditor(string Id)
        {
            SesionModel.FiltrosMovimiento = new MovimientoGridViewModel
            {
                ParamEditor = Id
            };

            var host = GeneralController.RutaRaiz(Request);
            var ruta = host + "Movimiento/Editor";
            return Json(ruta);
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> Editor()
        {
            var paramEditor = SesionModel.FiltrosMovimiento.ParamEditor;
            if (paramEditor == null)
            {
                return RedirectToAction("Ingresos");
            }
            SesionModel.FiltrosMovimiento = null;

            var id = Funciones.Check.Int32(paramEditor.Split('|')[0]);
            var tipo = Funciones.Check.Int32(paramEditor.Split('|')[1]);
            var form = Funciones.Check.Cadena(paramEditor.Split('|')[2]);
            string vista;

            var response = await _ServicioSistemaComercial.ObtenerEditorMovimientoAsync(id);
            var tipoMovIngreso = System.Convert.ToByte(Constantes.Tablas.TipoMovimiento.INGRESO);
            var tipoMovSalida = System.Convert.ToByte(Constantes.Tablas.TipoMovimiento.SALIDA);
            vista = string.Empty;

            //Construimos el modelo
            var model = new MovimientoEditorViewModel(response, form);

            if (tipo == 0)
            {
                switch (model.Movimiento.vTipoMov)
                {
                    case "IMP":
                        vista = "Ingresos._EditorImportacion";
                        break;
                    case "DEV":
                        vista = "Ingresos._EditorDevolucion";
                        break;
                    case "COM":
                        vista = "Ingresos._EditorCompra";
                        break;
                    case "EXP":
                        vista = "Salidas._EditorExportacion";
                        break;
                    case "DEL":
                        vista = "Salidas._EditorDelivery";
                        break;
                    case "VEN":
                        vista = "Salidas._EditorVentas";
                        break;
                    default:
                        break;
                }
            }
            else
            {
                if (tipo == 1)
                {
                    model.Movimiento.tiCodTipMov = tipoMovIngreso;
                }
                else
                {
                    model.Movimiento.tiCodTipMov = tipoMovSalida;
                }
                switch (form)
                {
                    case "IMP":
                        vista = "Ingresos._EditorImportacion";
                        break;
                    case "DEV":
                        vista = "Ingresos._EditorDevolucion";
                        break;
                    case "COM":
                        vista = "Ingresos._EditorCompra";
                        break;
                    case "EXP":
                        vista = "Salidas._EditorExportacion";
                        break;
                    case "DEL":
                        vista = "Salidas._EditorDelivery";
                        break;
                    case "VEN":
                        vista = "Salidas._EditorVentas";
                        break;
                    default:
                        break;
                }
            }
            return View(vista, model);
        }

        /// <summary>Método que graba la Movimiento</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Agregar(MovimientoEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new MovimientoRequestDto
                {
                    MovimientoCabecera = model.Movimiento,
                    MovimientoCabeceraExt = model.MovimientoExt,
                    MovimientoCabeceraDto = model.MovimientoDto,
                    Factura = model.Factura,
                    Guia = model.Guia,
                    ListaLiquidacion = model.ListaLiquidacion
                };

                request.MovimientoCabecera.siCodUsuCre = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                var objMov = await _ServicioSistemaComercial.RegistrarMovimientoAsync(request);

                //Refrescamos la pagina con los registros actuales
                return Json(objMov.MovimientoCabecera, JsonRequestBehavior.AllowGet);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que actualiza la Movimiento</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Modificar(MovimientoEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new MovimientoRequestDto
                {
                    MovimientoCabecera = model.Movimiento,
                    MovimientoCabeceraExt = model.MovimientoExt,
                    MovimientoCabeceraDto = model.MovimientoDto,
                    Factura = model.Factura,
                    Guia = model.Guia,
                    ListaLiquidacion = model.ListaLiquidacion
                };

                request.MovimientoCabecera.siCodUsuAct = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                await _ServicioSistemaComercial.ActualizarMovimientoAsync(request);

                //Refrescamos la pagina con los registros actuales
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que elimina la Movimiento</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Eliminar(int id)
        {
            try
            {
                //Construimos el request
                var request = new MovimientoRequestDto
                {
                    MovimientoCabecera = new COM_CabMovArt
                    {
                        iCodCabMov = id,
                        siCodUsuAct = 1
                    }
                };

                //Invocamos al servicio
                await _ServicioSistemaComercial.EliminarMovimientoAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("Refrescar");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que elimina la Movimiento</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Cerrar(int id)
        {
            try
            {
                //Construimos el request
                var request = new MovimientoRequestDto
                {
                    MovimientoCabecera = new COM_CabMovArt
                    {
                        iCodCabMov = id,
                        siCodUsuAct = 1
                    }
                };

                //Invocamos al servicio
                await _ServicioSistemaComercial.CerrarMovimientoAsync(request);

                //Refrescamos la pagina con los registros actuales
                var host = GeneralController.RutaRaiz(Request);
                var ruta = host + "Movimiento/Ingresos";
                return Json(ruta);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que exporta a archivo excel el listado de modo pago garitas</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Archivo excel</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>19/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public async Task<FileResult> Exportar(MovimientoGridViewModel model)
        {
            model.Filtro = model.Filtro ?? new MovimientoFiltroDto();
            //Construimos el request 
            var request = new MovimientoRequestDto
            {
                Filtro = model.Filtro
            };

            //Invocamos al servicio
            var response = await _ServicioSistemaComercial.ListaMovimientosAsync(request);

            // set the data source
            System.Data.DataTable gridview = new System.Data.DataTable("Grid");

            //Agregar Columna a Grilla
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Codigo", "Codigo"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("TipoMov", "TipoMov"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("TipoDoc", "TipoDoc"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("NumeroDoc", "NumeroDoc"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Persona", "Persona"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Usuario Creación", "UsuarioCrea"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Usuario Actualización", "UsuarioActualiza"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Fecha Creación", "FechaCrea"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Fecha Actualización", "FechaActualiza"));

            //LlenarGrilla
            foreach (var item in response.ListaMovimientos) gridview.Rows.Add(item.Codigo, item.TipoMov, item.TipoDoc, item.NumeroDoc, item.Persona, item.UsuarioCrea, item.UsuarioActualiza, item.FechaCrea, item.FechaActualiza);

            return new GeneralController().ExportarExcel(gridview, Response, "Movimiento");
        }

        /// <summary>Método que elimina la Stope</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        //[HttpPost]
        public async Task<ActionResult> ListarConceptosLiquidacionPorCab(string id = "")
        {
            try
            {
                var iCodPartner = Convert.ToString(id.Split('|')[0]);
                var vTipoMov = Convert.ToString(id.Split('|')[1]);

                //Invocamos al servicio
                var response = await _ServicioSistemaComercial.ListaConceptosLiquidacionAsync(iCodPartner, vTipoMov);

                var listaLiquidacion = new List<COM_Liquidacion>();
                foreach (var item in response.ListaGenerica)
                {
                    listaLiquidacion.Add(new COM_Liquidacion { vDescripcion = item.Descripcion, dCosto = 0 });
                }

                var model = new MovimientoEditorViewModel
                {
                    ListaLiquidacion = listaLiquidacion
                };

                //Refrescamos la pagina con los registros actuales
                return PartialView("_Editor.LiquidacionGrid", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que elimina la Stope</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        //[HttpPost]
        public async Task<ActionResult> ListarUbicacionesPisos(short id = 0)
        {
            try
            {
                //Invocamos al servicio
                var response = await _ServicioSistemaComercial.ListaUbicaciones_Pisos_ComboAsync(id);

                var responseMov = new MovimientoResponseDto
                {
                    ListaPisos = response.ListaGenerica
                };

                var model = new MovimientoDetalleViewModel(responseMov);

                //Refrescamos la pagina con los registros actuales
                return PartialView("_Editor.DetalleIng.ComboPisos", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que elimina la Stope</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        //[HttpPost]
        public async Task<ActionResult> ListarUbicacionesRacks(string id = "")
        {
            try
            {
                var alm = Convert.ToInt16(id.Split('|')[0]);
                var piso = Convert.ToByte(id.Split('|')[1]);

                var objUbicacion = new COM_MaeAlmacen_Ubicacion
                {
                    siCodAlm = alm,
                    tiPiso = piso
                };
                //Invocamos al servicio
                var response = await _ServicioSistemaComercial.ListaUbicaciones_Racks_ComboAsync(objUbicacion);

                var responseMov = new MovimientoResponseDto
                {
                    ListaRacks = response.ListaGenerica
                };

                var model = new MovimientoDetalleViewModel(responseMov);

                //Refrescamos la pagina con los registros actuales
                return PartialView("_Editor.DetalleIng.ComboRacks", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que elimina la Stope</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        //[HttpPost]
        public async Task<ActionResult> ListarUbicacionesPorRacks(string id = "")
        {
            try
            {
                var alm = Convert.ToInt16(id.Split('|')[0]);
                var piso = Convert.ToByte(id.Split('|')[1]);
                var rack = Convert.ToString(id.Split('|')[2]);

                var objUbicacion = new COM_MaeAlmacen_Ubicacion
                {
                    siCodAlm = alm,
                    tiPiso = piso,
                    vRack = rack
                };

                //Invocamos al servicio
                var response = await _ServicioSistemaComercial.ListaUbicaciones_Combo_PorRackAsync(objUbicacion);

                var responseMov = new MovimientoResponseDto
                {
                    ListaUbicaciones = response.ListaGenerica
                };

                var model = new MovimientoDetalleViewModel(responseMov);

                //Refrescamos la pagina con los registros actuales
                return PartialView("_Editor.DetalleIng.ComboUbicacion", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que elimina la Stope</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        //[HttpPost]
        public async Task<ActionResult> ListarDireccionesPersona(int id = 0)
        {
            try
            {
                //Invocamos al servicio
                var response = await _ServicioSistemaComercial.ListaDirecciones_ComboAsync(id);

                var responseMov = new MovimientoResponseDto
                {
                    ListaDirecciones = response.ListaGenerica
                };

                var model = new MovimientoEditorViewModel(responseMov, "1");

                //Refrescamos la pagina con los registros actuales
                return PartialView("_Editor.ComboDirecciones", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        #region DETALLE

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> EditorDetalle(string Id)
        {
            int idCab;
            int idDet;
            int codigoTipoMovIngreso;
            var arreglo = Id.Split(new char[] { '|' });

            idCab = Funciones.Check.Int32(arreglo[0]);
            idDet = Funciones.Check.Int32(arreglo[1]);
            codigoTipoMovIngreso = Funciones.Check.Int32(Constantes.Tablas.TipoMovimiento.INGRESO);

            var response = await _ServicioSistemaComercial.ObtenerEditorMovimientoDetalleAsync(idCab, idDet);

            //Construimos el modelo
            var model = new MovimientoDetalleViewModel(response);

            if (response.MovimientoCabecera.tiCodTipMov == codigoTipoMovIngreso)
            {
                return PartialView("_Editor.DetalleIng", model);
            }
            else
            {
                return PartialView("_Editor.DetalleSal", model);
            }
        }

        /// <summary>Método que actualiza la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> RefrescarDetalle(int Id)
        {
            try
            {
                var response = await _ServicioSistemaComercial.ObtenerEditorMovimientoAsync(Id);

                //Construimos el modelo
                var model = new MovimientoEditorViewModel(response, "1");

                //Retornamos vista con modelo
                return PartialView("_Editor.Grid", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que graba la UnidadMedida</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> AgregarDetalle(MovimientoDetalleViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new MovimientoRequestDto
                {
                    DetalleStock = model.DetalleStock,
                    MovimientoDetalle = model.MovimientoDetalle,
                    CabeceraStock = model.Stock,
                    MovimientoCabecera = model.Movimiento
                };

                //Invocamos al servicio
                await _ServicioSistemaComercial.RegistrarMovimientoDetalleAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarDetalle", new RouteValueDictionary(
                            new { controller = "Movimiento", action = "RefrescarDetalle", Id = model.Movimiento.iCodCabMov }));
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que actualiza la UnidadMedida</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> ActualizarDetalle(MovimientoDetalleViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new MovimientoRequestDto
                {
                    DetalleStock = model.DetalleStock,
                    MovimientoDetalle = model.MovimientoDetalle,
                    CabeceraStock = model.Stock,
                    MovimientoCabecera = model.Movimiento
                };

                //Invocamos al servicio
                await _ServicioSistemaComercial.ActualizarMovimientoDetalleAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarDetalle", new RouteValueDictionary(
                            new { controller = "Movimiento", action = "RefrescarDetalle", Id = model.Movimiento.iCodCabMov }));
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que elimina la UnidadMedida</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> EliminarDetalle(int id)
        {
            try
            {
                //Invocamos al servicio
                var entidad = await _ServicioSistemaComercial.EliminarMovimientoDetalleAsync(id);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarDetalle", new RouteValueDictionary(
                            new { controller = "Movimiento", action = "RefrescarDetalle", Id = entidad }));
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> ListaArticulosParaSalida_Auto(string term)
        {
            var response = await _ServicioSistemaComercial.ListaArticulosParaSalida_AutoAsync(term);

            //retornar consulta como jsonv  
            return Json(response.ListaGenerica, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> ObtenerDetalleMovimientoParaSalida(int id)
        {
            var response = await _ServicioSistemaComercial.ObtenerDetalleMovimientoParaSalidaAsync(id);

            //retornar consulta como jsonv  
            return Json(response.MovimientoDetalleDto, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region GUIA REMISION

        ///// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        ///// <param name="model">Modelo del request</param>
        ///// <returns>Modelo para la vista de al grilla</returns>
        ///// <remarks><list type="bullet">
        ///// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        ///// <item><FecCrea>16/01/2018</FecCrea></item>
        ///// </list>
        ///// <list type="bullet" >
        ///// <item><FecActu>XXXX</FecActu></item>
        ///// <item><Resp>XXXX</Resp></item>
        ///// <item><Mot>XXXX</Mot></item></list></remarks>
        public async Task<ActionResult> _GuiaRemision(int id)
        {
            try
            {
                var response = await _ServicioSistemaComercial.ObtenerGuiaRemisionAsync(id);
                var dt = MovimientoGuiaRemisionViewModel.DTReporteGuiaRemision(response);
                var dtDet = MovimientoGuiaRemisionViewModel.DTReporteGuiaRemisionDet(response);
                return ExportarGuiaRemision(dt, dtDet, "RptGuiaRemision", "RptGuiaRemision");
            }
            catch
            {
                throw;
            }

        }

        ///// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        ///// <param name="model">Modelo del request</param>
        ///// <returns>Modelo para la vista de al grilla</returns>
        ///// <remarks><list type="bullet">
        ///// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        ///// <item><FecCrea>16/01/2018</FecCrea></item>
        ///// </list>
        ///// <list type="bullet" >
        ///// <item><FecActu>XXXX</FecActu></item>
        ///// <item><Resp>XXXX</Resp></item>
        ///// <item><Mot>XXXX</Mot></item></list></remarks>
        public ActionResult ExportarGuiaRemision(DataTable dtCab, DataTable dtDet, string reporte, string nombre)
        {
            try
            {
                ReportDocument rd = new ReportDocument();

                rd.Load(Path.Combine(Request.MapPath(Request.ApplicationPath) + @"\Reportes\Salida\" + reporte + ".rpt"));
                DataSet ds = new DataSet();
                ds.Tables.Add(dtCab);
                ds.Tables.Add(dtDet);
                rd.SetDataSource(ds);
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);

                return File(stream, "application/pdf");
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region ARCHIVOS

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> EditorArchivosCab(int id)
        {
            string tabla = "MOVIMIENTOCAB";

            var response = await _ServicioSistemaComercial.ListadoArchivosAsync(tabla, id);

            //Construimos el modelo
            var model = new ArchivosGridViewModel(response);
            model.Codigo = id;

            return PartialView("_Archivos", model);
        }

        /// <summary>Método que actualiza la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> RefrescarArchivosCab(int id)
        {
            try
            {
                string tabla = "MOVIMIENTOCAB";

                var response = await _ServicioSistemaComercial.ListadoArchivosAsync(tabla, id);

                //Construimos el modelo
                var model = new ArchivosGridViewModel(response);
                model.Codigo = id;
                //Retornamos vista con modelo
                return PartialView("_Archivos.Grid", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que graba la Movimiento</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> AgregarArchivoCab(ArchivosGridViewModel model)
        {
            try
            {
                string tabla = "MOVIMIENTOCAB";

                //Construimos el request
                var request = new ArchivosRequestDto
                {
                    Archivo = new GEN_MaeArchivo
                    {
                        Codigo = model.Codigo.ToString(),
                        Tabla = tabla,
                        Descripcion = model.Descripcion
                    }
                };

                if (model.Archivo != null)
                {
                    Stream str = model.Archivo.InputStream;
                    BinaryReader Br = new BinaryReader(str);
                    Byte[] foto = Br.ReadBytes((Int32)str.Length);
                    request.Archivo.Archivo = foto;
                    request.Archivo.Extension = model.Archivo.ContentType;
                    request.Archivo.Nombre = model.Archivo.FileName;
                }

                //Invocamos al servicio
                await _ServicioSistemaComercial.GuardarArchivoAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarArchivosCab", new RouteValueDictionary(
                            new { controller = "Movimiento", action = "RefrescarDetalle", id = model.Codigo }));
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que graba la Movimiento</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> EliminarArchivoCab(string id)
        {
            try
            {
                //Invocamos al servicio
                var codigo = await _ServicioSistemaComercial.EliminarArchivoAsync(id);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarArchivosCab", new RouteValueDictionary(
                            new { controller = "Movimiento", action = "RefrescarDetalle", id = codigo }));
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> EditorArchivosDet(int id)
        {
            string tabla = "MOVIMIENTODET";

            var response = await _ServicioSistemaComercial.ListadoArchivosAsync(tabla, id);

            //Construimos el modelo
            var model = new ArchivosGridViewModel(response);
            model.Codigo = id;

            return PartialView("_ArchivosDet", model);
        }

        /// <summary>Método que actualiza la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> RefrescarArchivosDet(int id)
        {
            try
            {
                string tabla = "MOVIMIENTODET";

                var response = await _ServicioSistemaComercial.ListadoArchivosAsync(tabla, id);

                //Construimos el modelo
                var model = new ArchivosGridViewModel(response);
                model.Codigo = id;
                //Retornamos vista con modelo
                return PartialView("_ArchivosDet.Grid", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que graba la Movimiento</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> AgregarArchivoDet(ArchivosGridViewModel model)
        {
            try
            {
                string tabla = "MOVIMIENTODET";

                //Construimos el request
                var request = new ArchivosRequestDto
                {
                    Archivo = new GEN_MaeArchivo
                    {
                        Codigo = model.Codigo.ToString(),
                        Tabla = tabla,
                        Descripcion = model.Descripcion
                    }
                };

                if (model.Archivo != null)
                {
                    Stream str = model.Archivo.InputStream;
                    BinaryReader Br = new BinaryReader(str);
                    Byte[] foto = Br.ReadBytes((Int32)str.Length);
                    request.Archivo.Archivo = foto;
                    request.Archivo.Extension = model.Archivo.ContentType;
                    request.Archivo.Nombre = model.Archivo.FileName;
                }

                //Invocamos al servicio
                await _ServicioSistemaComercial.GuardarArchivoAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarArchivosDet", new RouteValueDictionary(
                            new { controller = "Movimiento", action = "RefrescarDetalle", id = model.Codigo }));
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que graba la Movimiento</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> EliminarArchivoDet(string id)
        {
            try
            {
                //Invocamos al servicio
                var codigo = await _ServicioSistemaComercial.EliminarArchivoAsync(id);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarArchivosDet", new RouteValueDictionary(
                            new { controller = "Movimiento", action = "RefrescarDetalle", id = codigo }));
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region ARTICULO

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> EditorArticulo()
        {
            var response = await _ServicioSistemaComercial.ObtenerEditorArticuloAsync(0);

            //Construimos el modelo
            var model = new ArticuloEditorViewModel(response);

            //Retornamos vista con modelo
            return PartialView("_EditorArticulo", model);
        }

        /// <summary>Método que graba la Articulo</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> AgregarArticulo(ArticuloEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new ArticuloRequestDto
                {
                    Articulo = model.Articulo
                };

                request.Articulo.siCodUsuCre = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                var id = await _ServicioSistemaComercial.RegistrarArticuloAsync(request);
                var articulo = await _ServicioSistemaComercial.ObtenerEditorArticuloAsync(id);

                //Refrescamos la pagina con los registros actuales
                return Json(articulo.Articulo, JsonRequestBehavior.AllowGet);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region LIQUIDACION

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> EditorDetalleLiquidacion(string Id)
        {
            int idCab;
            int idDet;
            int codigoTipoMovIngreso;
            var arreglo = Id.Split(new char[] { '|' });

            idCab = Funciones.Check.Int32(arreglo[0]);
            idDet = Funciones.Check.Int32(arreglo[1]);

            var response = await _ServicioSistemaComercial.ObtenerEditorMovimientoLiquidacionAsync(idDet);

            //Construimos el modelo
            var model = new MovimientoLiquidacionViewModel(response);
            model.Liquidacion.iCodCabMov = idCab;

            return PartialView("_Editor.DetalleIngLiquidacion", model);
        }

        /// <summary>Método que actualiza la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> RefrescarDetalleLiquidacion(int Id)
        {
            try
            {
                var response = await _ServicioSistemaComercial.ObtenerEditorMovimientoAsync(Id);

                //Construimos el modelo
                var model = new MovimientoEditorViewModel(response, "1");

                //Retornamos vista con modelo
                return PartialView("_Editor.LiquidacionGrid", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region GUIA REMISION

        ///// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        ///// <param name="model">Modelo del request</param>
        ///// <returns>Modelo para la vista de al grilla</returns>
        ///// <remarks><list type="bullet">
        ///// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        ///// <item><FecCrea>16/01/2018</FecCrea></item>
        ///// </list>
        ///// <list type="bullet" >
        ///// <item><FecActu>XXXX</FecActu></item>
        ///// <item><Resp>XXXX</Resp></item>
        ///// <item><Mot>XXXX</Mot></item></list></remarks>
        public async Task<ActionResult> _DeclaracionJurada(int id)
        {
            try
            {
                var response = await _ServicioSistemaComercial.ObtenerEditorMovimientoAsync(id);
                var dt = MovimientoDeclaracionJuradaViewModel.DTReporteDeclaracion(response);
                return ExportarDeclaracionJurada(dt, "RptDeclaracionJuradaExp");
            }
            catch
            {
                throw;
            }

        }

        ///// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        ///// <param name="model">Modelo del request</param>
        ///// <returns>Modelo para la vista de al grilla</returns>
        ///// <remarks><list type="bullet">
        ///// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        ///// <item><FecCrea>16/01/2018</FecCrea></item>
        ///// </list>
        ///// <list type="bullet" >
        ///// <item><FecActu>XXXX</FecActu></item>
        ///// <item><Resp>XXXX</Resp></item>
        ///// <item><Mot>XXXX</Mot></item></list></remarks>
        public ActionResult ExportarDeclaracionJurada(DataTable dt, string reporte)
        {
            try
            {
                ReportDocument rd = new ReportDocument();

                rd.Load(Path.Combine(Request.MapPath(Request.ApplicationPath) + @"\Reportes\Salida\" + reporte + ".rpt"));

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                rd.SetDataSource(ds);
                rd.SetParameterValue(0, "Flash Global Logistics, Inc., con dirección en 2935 Ramco Street, Suite 100, West Sacramento CA 95691, USA");
                rd.SetParameterValue(1, "0.00");

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);

                return File(stream, "application/pdf");
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }



        #endregion
    }
}