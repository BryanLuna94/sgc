﻿using SGC.Models.Almacen;
using System.Web.Mvc;
using SGC.ServicioSistemaComercial;
using System.Threading.Tasks;
using System.ServiceModel;
using SGC.Helper;
using System.Web.UI.WebControls;

namespace SGC.Controllers
{
    [Atributos.SessionExpireFilter]
    public class AlmacenController : Controller
    {
        ServicioSistemaComercialClient _ServicioSistemaComercial = new ServicioSistemaComercialClient();

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        [Atributos.AccesoAutorizadoFilter]
        public async Task<ActionResult> Index(AlmacenGridViewModel model)
        {
            try
            {
                model = SesionModel.FiltrosAlmacen;
                SesionModel.FiltrosAlmacen = null;

                //Construimos el modelo
                model = await ObtenerModelGrid(model);

                //Retornamos vista con modelo
                return View(model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        private async Task<AlmacenGridViewModel> ObtenerModelGrid(AlmacenGridViewModel model)
        {
            model.Filtro = model.Filtro ?? new AlmacenFiltroDto();
            //Construimos el request 
            var request = new AlmacenRequestDto
            {
                Filtro = model.Filtro
            };

            //Invocamos al servicio
            var response = await _ServicioSistemaComercial.ObtenerIndexAlmacenesAsync(request);

            //Construimos el modelo
            model = new AlmacenGridViewModel(response)
            {
                Filtro = request.Filtro
            };
            //Retornamos el modelo
            return model;

        }

        /// <summary>Método que actualiza la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> Refrescar(AlmacenGridViewModel model)
        {
            try
            {
                //Construimos el modelo
                model = await ObtenerModelGrid(model);

                //Retornamos vista con modelo
                return PartialView("_Index.Grid", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> Editor(short Id = 0)
        {
            var response = await _ServicioSistemaComercial.ObtenerEditorAlmacenAsync(Id);

            //Construimos el modelo
            var model = new AlmacenEditorViewModel(response);

            //Retornamos vista con modelo
            return PartialView("_Editor", model);
        }

        /// <summary>Método que graba la Almacen</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Agregar(AlmacenEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new AlmacenRequestDto
                {
                    Almacen = model.Almacen
                };

                request.Almacen.siCodUsuCre = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                await _ServicioSistemaComercial.RegistrarAlmacenAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("Refrescar");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que actualiza la Almacen</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Modificar(AlmacenEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new AlmacenRequestDto
                {
                    Almacen = model.Almacen
                };

                request.Almacen.siCodUsuAct = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                await _ServicioSistemaComercial.ActualizarAlmacenAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("Refrescar");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que elimina la Almacen</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Eliminar(short id)
        {
            try
            {
                //Construimos el request
                var request = new AlmacenRequestDto
                {
                    Almacen = new COM_MaeAlmacen
                    {
                        siCodAlm = id,
                        siCodUsuAct = 1
                    }
                };

                //Invocamos al servicio
                await _ServicioSistemaComercial.EliminarAlmacenAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("Refrescar");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que exporta a archivo excel el listado de modo pago garitas</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Archivo excel</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>19/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public async Task<FileResult> Exportar(AlmacenGridViewModel model)
        {
            model.Filtro = model.Filtro ?? new AlmacenFiltroDto();
            //Construimos el request 
            var request = new AlmacenRequestDto
            {
                Filtro = model.Filtro
            };

            //Invocamos al servicio
            var response = await _ServicioSistemaComercial.ListaAlmacenesAsync(request);

            // set the data source
            System.Data.DataTable gridview = new System.Data.DataTable("Grid");

            //Agregar Columna a Grilla
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Codigo", "Codigo"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Descripcion", "Descripcion"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Abreviatura", "Abreviatura"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Direccion", "Direccion"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Usuario Creación", "UsuarioCrea"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Usuario Actualización", "UsuarioActualiza"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Fecha Creación", "FechaCrea"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Fecha Actualización", "FechaActualiza"));

            //LlenarGrilla
            foreach (var item in response.ListaAlmacenes) gridview.Rows.Add(item.Codigo, item.Descripcion, item.Abreviatura, item.Direccion, item.UsuarioCrea, item.UsuarioActualiza, item.FechaCrea, item.FechaActualiza);

            return new GeneralController().ExportarExcel(gridview, Response, "Almacen");
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public ActionResult RedirectUbicaciones(AlmacenGridViewModel model)
        {
            SesionModel.FiltrosAlmacen = model;

            //Retornamos vista con modelo
            return Json("/Almacen/Ubicaciones");
        }

        #region UBICACIONES

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> Ubicaciones()
        {
            try
            {
                //validamos sesion
                var modeloAlmacen = SesionModel.FiltrosAlmacen;
                if (modeloAlmacen.CodigoAlmacen == 0)
                {
                    return RedirectToAction("Index", "Almacen");
                }

                //Construimos el modelo
                var model = new UbicacionAlmacenGridViewModel { Filtro = new AlmacenUbicacionFiltroDto { CodigoAlmacen = modeloAlmacen.CodigoAlmacen } };
                model = await ObtenerModelGridUbicacion(model);

                //Retornamos vista con modelo
                return View(model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        private async Task<UbicacionAlmacenGridViewModel> ObtenerModelGridUbicacion(UbicacionAlmacenGridViewModel model)
        {
            var request = new AlmacenUbicacionRequestDto();

            if (model.Filtro == null)
            {
                var sesionAlmacen = SesionModel.FiltrosAlmacen;
                request = new AlmacenUbicacionRequestDto
                {
                    Filtro = new AlmacenUbicacionFiltroDto
                    {
                        CodigoAlmacen = sesionAlmacen.CodigoAlmacen
                    }
                };
            }
            else
            {
                request = new AlmacenUbicacionRequestDto
                {
                    Filtro = model.Filtro
                };
            }

            //Invocamos al servicio
            var response = await _ServicioSistemaComercial.ObtenerIndexUbicacionAlmacenAsync(request);

            //Construimos el modelo
            model = new UbicacionAlmacenGridViewModel(response);
            model.Filtro = request.Filtro;
            //Retornamos el modelo
            return model;

        }

        /// <summary>Método que actualiza la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> RefrescarUbicacion(UbicacionAlmacenGridViewModel model)
        {
            try
            {
                //Construimos el modelo
                model = await ObtenerModelGridUbicacion(model);

                //Retornamos vista con modelo
                return PartialView("_Ubicaciones.Grid", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> EditorUbicacion(short Id = 0)
        {
            var response = await _ServicioSistemaComercial.ObtenerEditorUbicacionAlmacenAsync(Id);

            //Construimos el modelo
            var model = new UbicacionAlmacenEditorViewModel(response);

            //Retornamos vista con modelo
            return PartialView("_EditorUbicaciones", model);
        }

        /// <summary>Método que graba la Almacen</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> AgregarUbicacion(UbicacionAlmacenEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new AlmacenUbicacionRequestDto
                {
                    AlmacenUbicacion = model.Ubicacion
                };

                request.AlmacenUbicacion.siCodUsuCre = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                await _ServicioSistemaComercial.RegistrarUbicacionAlmacenAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarUbicacion");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que actualiza la Almacen</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> ModificarUbicacion(UbicacionAlmacenEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new AlmacenUbicacionRequestDto
                {
                    AlmacenUbicacion = model.Ubicacion
                };

                request.AlmacenUbicacion.siCodUsuAct = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                await _ServicioSistemaComercial.ActualizarUbicacionAlmacenAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarUbicacion");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que elimina la Almacen</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> EliminarUbicacion(int id)
        {
            try
            {
                //Construimos el request
                var request = new AlmacenUbicacionRequestDto
                {
                    AlmacenUbicacion = new COM_MaeAlmacen_Ubicacion
                    {
                        iCodUbi = id,
                        siCodUsuAct = 1
                    }
                };

                //Invocamos al servicio
                await _ServicioSistemaComercial.EliminarUbicacionAlmacenAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarUbicacion");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que exporta a archivo excel el listado de modo pago garitas</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Archivo excel</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>19/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public async Task<FileResult> ExportarUbicacion(UbicacionAlmacenGridViewModel model)
        {
            model.Filtro = model.Filtro ?? new AlmacenUbicacionFiltroDto();
            //Construimos el request 
            var request = new AlmacenUbicacionRequestDto
            {
                Filtro = model.Filtro
            };

            //Invocamos al servicio
            var response = await _ServicioSistemaComercial.ListaUbicacionesAlmacenAsync(request);

            // set the data source
            System.Data.DataTable gridview = new System.Data.DataTable("Grid");

            //Agregar Columna a Grilla
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Codigo", "Codigo"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Almacen", "Almacen"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Piso", "Piso"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Rack", "Rack"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Fila", "Fila"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Columna", "Columna"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Usuario Creación", "UsuarioCrea"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Usuario Actualización", "UsuarioActualiza"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Fecha Creación", "FechaCrea"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Fecha Actualización", "FechaActualiza"));

            //LlenarGrilla
            foreach (var item in response.ListaUbicacionesAlmacen) gridview.Rows.Add(item.Codigo, item.Almacen, item.Piso, item.Rack, item.Fila, item.Columna, item.UsuarioCrea, item.UsuarioActualiza, item.FechaCrea, item.FechaActualiza);

            return new GeneralController().ExportarExcel(gridview, Response, "Almacen");
        }

        #endregion
    }
}