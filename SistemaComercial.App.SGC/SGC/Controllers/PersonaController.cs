﻿using SGC.Models.Persona;
using System.Web.Mvc;
using SGC.ServicioSistemaComercial;
using System.Threading.Tasks;
using System.ServiceModel;
using SGC.Helper;
using System.Web.UI.WebControls;
using System.Web.Routing;

namespace SGC.Controllers
{
    public class PersonaController : Controller
    {
        ServicioSistemaComercialClient _ServicioSistemaComercial = new ServicioSistemaComercialClient();

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        [Atributos.AccesoAutorizadoFilter]
        public async Task<ActionResult> Search(PersonaGridViewModel model)
        {
            try
            {
                //model = SesionModel.FiltrosPersona;
                //SesionModel.FiltrosPersona = null;

                //Construimos el modelo
                model = await ObtenerModelGrid(model);

                //Retornamos vista con modelo
                return View("Index",model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        private async Task<PersonaGridViewModel> ObtenerModelGrid(PersonaGridViewModel model)
        {
            model.Filtro = model.Filtro ?? new PersonaFiltroDto();
            //Construimos el request 
            var request = new PersonaRequestDto
            {
                Filtro = model.Filtro
            };

            //Invocamos al servicio
            var response = await _ServicioSistemaComercial.ObtenerIndexPersonasAsync(request);

            //Construimos el modelo
            model = new PersonaGridViewModel(response)
            {
                Filtro = request.Filtro
            };
            //Retornamos el modelo
            return model;

        }

        /// <summary>Método que actualiza la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> Refrescar(PersonaGridViewModel model)
        {
            try
            {
                //Construimos el modelo
                model = await ObtenerModelGrid(model);

                //Retornamos vista con modelo
                return PartialView("_Index.Grid", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> Editor(int Id = 0)
        {
            var response = await _ServicioSistemaComercial.ObtenerEditorPersonaAsync(Id);

            //Construimos el modelo
            var model = new PersonaEditorViewModel(response);

            //Retornamos vista con modelo
            return View("_Editor", model);
        }

        /// <summary>Método que graba la Persona</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Agregar(PersonaEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new PersonaRequestDto
                {
                    Persona = model.Persona
                };

                request.Persona.siCodUsuCre = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                var id = await _ServicioSistemaComercial.RegistrarPersonaAsync(request);

                //Refrescamos la pagina con los registros actuales
                return Json(id, JsonRequestBehavior.AllowGet);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que actualiza la Persona</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Modificar(PersonaEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new PersonaRequestDto
                {
                    Persona = model.Persona
                };

                request.Persona.siCodUsuAct = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                await _ServicioSistemaComercial.ActualizarPersonaAsync(request);

                //Refrescamos la pagina con los registros actuales
                return Json("1", JsonRequestBehavior.AllowGet);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que elimina la Persona</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Eliminar(int id)
        {
            try
            {
                //Construimos el request
                var request = new PersonaRequestDto
                {
                    Persona = new COM_TabPersona
                    {
                        iCodPer = id,
                        siCodUsuAct = 1
                    }
                };

                //Invocamos al servicio
                await _ServicioSistemaComercial.EliminarPersonaAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("Refrescar");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que exporta a archivo excel el listado de modo pago garitas</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Archivo excel</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>19/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public async Task<FileResult> Exportar(PersonaGridViewModel model)
        {
            model.Filtro = model.Filtro ?? new PersonaFiltroDto();
            //Construimos el request 
            var request = new PersonaRequestDto
            {
                Filtro = model.Filtro
            };

            //Invocamos al servicio
            var response = await _ServicioSistemaComercial.ListaPersonasAsync(request);

            // set the data source
            System.Data.DataTable gridview = new System.Data.DataTable("Grid");

            //Agregar Columna a Grilla
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Codigo", "Codigo"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("NroDocumento", "NroDocumento"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("RazonSocial", "RazonSocial"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("NombreComercial", "NombreComercial"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Usuario Creación", "UsuarioCrea"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Usuario Actualización", "UsuarioActualiza"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Fecha Creación", "FechaCrea"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Fecha Actualización", "FechaActualiza"));

            //LlenarGrilla
            foreach (var item in response.ListaPersonas) gridview.Rows.Add(item.Codigo, item.NroDocumento, item.RazonSocial, item.NombreComercial, item.UsuarioCrea, item.UsuarioActualiza, item.FechaCrea, item.FechaActualiza);

            return new GeneralController().ExportarExcel(gridview, Response, "Persona");
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public ActionResult RedirectUbicaciones(PersonaGridViewModel model)
        {
            //SesionModel.FiltrosPersona = model;

            //Retornamos vista con modelo
            var host = GeneralController.RutaRaiz(Request);
            var ruta = host + "Persona/Ubicaciones";
            return Json(ruta);
        }

        public async Task<ActionResult> ListaPersonas_Auto(string term)
        {
            var arreglo = term.Split(new char[] { '|' });
            MovimientoRequestDto request = new MovimientoRequestDto
            {
                MovimientoCabeceraDto = new MovimientoListaDto
                {
                    Persona = arreglo[0]
                },
                MovimientoCabecera = new COM_CabMovArt()
            };
            var response = await _ServicioSistemaComercial.ListaPersona_AutocompleteAsync(request);

            //retornar consulta como jsonv  
            return Json(response.ListaGenerica, JsonRequestBehavior.AllowGet);
        }

        #region OFICINA

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> EditorOficina(int Id = 0)
        {
            var response = await _ServicioSistemaComercial.ObtenerEditorPersonaOficinaAsync(Id);

            //Construimos el modelo
            var model = new PersonaOficinaEditorViewModel(response);

            //Retornamos vista con modelo
            return PartialView("_Editor.Oficina.Editor", model);
        }

        /// <summary>Método que actualiza la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> RefrescarOficina(int Id)
        {
            try
            {
                var response = await _ServicioSistemaComercial.ObtenerEditorPersonaAsync(Id);

                //Construimos el modelo
                var model = new PersonaEditorViewModel(response);

                //Retornamos vista con modelo
                return PartialView("_Editor.Oficina.Grid", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que graba la Persona</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> AgregarOficina(PersonaOficinaEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new PersonaRequestDto
                {
                    OficinaPersona = model.Oficina
                };

                request.OficinaPersona.siCodUsuCre = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                await _ServicioSistemaComercial.RegistrarPersonaOficinaAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarOficina", new RouteValueDictionary(
                            new { controller = "Persona", action = "RefrescarOficina", Id = request.OficinaPersona.iCodPer }));
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que actualiza la Persona</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> ModificarOficina(PersonaOficinaEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new PersonaRequestDto
                {
                    OficinaPersona = model.Oficina
                };

                request.OficinaPersona.siCodUsuAct = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                await _ServicioSistemaComercial.ActualizarPersonaOficinaAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarOficina", new RouteValueDictionary(
                           new { controller = "Persona", action = "RefrescarOficina", Id = request.OficinaPersona.iCodPer }));
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que elimina la Persona</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> EliminarOficina(int id)
        {
            try
            {
                //Construimos el request
                var request = new PersonaRequestDto
                {
                    OficinaPersona = new COM_TabPersona_Oficina
                    {
                        iCodPerOfi = id,
                        siCodUsuAct = 1
                    }
                };

                //Invocamos al servicio
                var codigoPersona = await _ServicioSistemaComercial.EliminarPersonaOficinaAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarOficina", new RouteValueDictionary(
                           new { controller = "Persona", action = "RefrescarOficina", Id = codigoPersona }));
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que exporta a archivo excel el listado de modo pago garitas</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Archivo excel</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>19/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public async Task<FileResult> ExportarOficina(int id)
        {
            //Invocamos al servicio
            var response = await _ServicioSistemaComercial.ObtenerEditorPersonaAsync(id);

            // set the data source
            System.Data.DataTable gridview = new System.Data.DataTable("Grid");

            //Agregar Columna a Grilla
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Codigo", "Codigo"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Descripcion", "Descripcion"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Direccion", "Direccion"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Usuario Creación", "UsuarioCrea"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Usuario Actualización", "UsuarioActualiza"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Fecha Creación", "FechaCrea"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Fecha Actualización", "FechaActualiza"));

            //LlenarGrilla
            foreach (var item in response.ListaOficinas) gridview.Rows.Add(item.Codigo, item.Descripcion, item.Direccion, item.UsuarioCrea, item.UsuarioActualiza, item.FechaCrea, item.FechaActualiza);

            return new GeneralController().ExportarExcel(gridview, Response, "OficinasEmpresa");
        }

        #endregion

        #region CONTACTO

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> EditorContacto(int Id = 0)
        {
            var response = await _ServicioSistemaComercial.ObtenerEditorPersonaContactoAsync(Id);

            //Construimos el modelo
            var model = new PersonaContactoEditorViewModel(response);

            //Retornamos vista con modelo
            return PartialView("_Editor.Contacto.Editor", model);
        }

        /// <summary>Método que actualiza la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> RefrescarContacto(int Id)
        {
            try
            {
                var response = await _ServicioSistemaComercial.ObtenerEditorPersonaAsync(Id);

                //Construimos el modelo
                var model = new PersonaEditorViewModel(response);

                //Retornamos vista con modelo
                return PartialView("_Editor.Contacto.Grid", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que graba la Persona</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> AgregarContacto(PersonaContactoEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new PersonaRequestDto
                {
                    ContactoPersona = model.Contacto
                };

                request.ContactoPersona.siCodUsuCre = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                await _ServicioSistemaComercial.RegistrarPersonaContactoAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarContacto", new RouteValueDictionary(
                            new { controller = "Persona", action = "RefrescarContacto", Id = request.ContactoPersona.iCodPer }));
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que actualiza la Persona</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> ModificarContacto(PersonaContactoEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new PersonaRequestDto
                {
                    ContactoPersona = model.Contacto
                };

                request.ContactoPersona.siCodUsuAct = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                await _ServicioSistemaComercial.ActualizarPersonaContactoAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarContacto", new RouteValueDictionary(
                           new { controller = "Persona", action = "RefrescarContacto", Id = request.ContactoPersona.iCodPer }));
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que elimina la Persona</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> EliminarContacto(int id)
        {
            try
            {
                //Construimos el request
                var request = new PersonaRequestDto
                {
                    ContactoPersona = new COM_TabPersona_Contacto
                    {
                        iCodPerCon = id,
                        siCodUsuAct = 1
                    }
                };

                //Invocamos al servicio
                var codigoPersona = await _ServicioSistemaComercial.EliminarPersonaContactoAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("RefrescarContacto", new RouteValueDictionary(
                           new { controller = "Persona", action = "RefrescarContacto", Id = codigoPersona }));
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que exporta a archivo excel el listado de modo pago garitas</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Archivo excel</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>19/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public async Task<FileResult> ExportarContacto(int id)
        {
            //Invocamos al servicio
            var response = await _ServicioSistemaComercial.ObtenerEditorPersonaAsync(id);

            // set the data source
            System.Data.DataTable gridview = new System.Data.DataTable("Grid");

            //Agregar Columna a Grilla
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Codigo", "Codigo"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Nombre", "Nombre"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Numero", "Numero"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Usuario Creación", "UsuarioCrea"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Usuario Actualización", "UsuarioActualiza"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Fecha Creación", "FechaCrea"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Fecha Actualización", "FechaActualiza"));

            //LlenarGrilla
            foreach (var item in response.ListaContactos) gridview.Rows.Add(item.Codigo, item.Nombre, item.Numero, item.UsuarioCrea, item.UsuarioActualiza, item.FechaCrea, item.FechaActualiza);

            return new GeneralController().ExportarExcel(gridview, Response, "ContactosEmpresa");
        }

        #endregion

    }
}