﻿using SGC.Models.Reporte;
using System.Web.Mvc;
using SGC.ServicioSistemaComercial;
using System.Threading.Tasks;
using System.ServiceModel;
using SGC.Helper;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

namespace SGC.Controllers
{
    [Atributos.SessionExpireFilter]
    public class ReporteController : Controller
    {
        ServicioSistemaComercialClient _ServicioSistemaComercial = new ServicioSistemaComercialClient();

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> AlmacenValorizado()
        {
            try
            {
                //Construimos el modelo
                var response = await _ServicioSistemaComercial.ObtenerIndexReporteAlmacenesValorizadosResumidoAsync();

                var model = new ReporteAlmacenValorizadoViewModel(response);

                //Retornamos vista con modelo
                return View(model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public async Task<ActionResult> _AlmacenValorizadoRes(ReporteAlmacenValorizadoViewModel model)
        {
            try
            {
                var request = new ReporteAlmacenesValorizadosResumidoRequestDto
                {
                    Filtro = model.Filtro
                };

                var response = await _ServicioSistemaComercial.ListaReporteAlmacenesValorizadosResumidoAsync(request);
                var dt = ReporteAlmacenValorizadoViewModel.DTReporteAlmacenResumido(response);
                //string subtitulo = string.Format("Cuadro de Stopes Programados del año {0}", model.Filtro.Anio.ToString());
                return ExportarPDFAlmacenValorizado(dt, "ReporteAlmacenValorizadoRes", "ReporteAlmacenValorizadoRes", "");
            }
            catch
            {
                throw;
            }

        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public async Task<ActionResult> _AlmacenValorizadoDet(ReporteAlmacenValorizadoViewModel model)
        {
            try
            {
                var request = new ReporteAlmacenesValorizadosResumidoRequestDto
                {
                    Filtro = model.Filtro
                };

                var response = await _ServicioSistemaComercial.ListaReporteAlmacenesValorizadosDetalladoAsync(request);
                var dt = ReporteAlmacenValorizadoViewModel.DTReporteAlmacenDetallado(response);
                //string subtitulo = string.Format("Cuadro de Stopes Programados del año {0}", model.Filtro.Anio.ToString());
                return ExportarPDFAlmacenValorizado(dt, "ReporteAlmacenValorizadoDet", "ReporteAlmacenValorizadoDet", "");
            }
            catch
            {
                throw;
            }

        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public ActionResult ExportarPDFAlmacenValorizado(DataTable dt, string reporte,
            string nombre, string subtitulo)
        {
            try
            {
                ReportDocument rd = new ReportDocument();

                rd.Load(Path.Combine(Request.MapPath(Request.ApplicationPath) + @"\Reportes\Salida\" + reporte + ".rpt"));
                rd.SetDataSource(dt);
                //rd.DataDefinition.FormulaFields["pSubtitulo"].Text = "'" + subtitulo + "'";
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);

                return File(stream, "application/pdf");
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }

        #region KARDEX

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> Kardex()
        {
            try
            {
                //Construimos el modelo
                var response = await _ServicioSistemaComercial.ObtenerIndexReporteKardexAsync();

                var model = new ReporteKardexViewModel(response);

                //Retornamos vista con modelo
                return View(model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public async Task<ActionResult> _Kardex(ReporteKardexViewModel model)
        {
            try
            {
                var request = new ReporteKardexRequestDto
                {
                    Filtro = model.Filtro
                };

                var response = await _ServicioSistemaComercial.ListaReporteKardexAsync(request);
                var dt = ReporteKardexViewModel.DTReporteKardex(response);
                //string subtitulo = string.Format("Cuadro de Stopes Programados del año {0}", model.Filtro.Anio.ToString());
                return ExportarPDFKardex(dt, "ReporteKardex", "ReporteKardex", model);
            }
            catch
            {
                throw;
            }

        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public ActionResult ExportarPDFKardex(DataTable dt, string reporte,
            string nombre, ReporteKardexViewModel model)
        {
            try
            {
                ReportDocument rd = new ReportDocument();

                rd.Load(Path.Combine(Request.MapPath(Request.ApplicationPath) + @"\Reportes\Salida\" + reporte + ".rpt"));
                rd.SetDataSource(dt);
                rd.DataDefinition.FormulaFields["fPeriodo"].Text = "'" + model.Filtro.Anio.ToString() + " " + model.Mes + "'";
                rd.DataDefinition.FormulaFields["falmacen"].Text = "'" + model.Almacen + "'";
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);

                return File(stream, "application/pdf");
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }

        #endregion
    }
}