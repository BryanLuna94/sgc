﻿using SGC.Models.Usuario;
using System.Web.Mvc;
using SGC.ServicioSistemaComercial;
using System.Threading.Tasks;
using System.ServiceModel;
using SGC.Helper;
using System.Web.UI.WebControls;
using System.IO;
using System;

namespace SGC.Controllers
{
    [Atributos.SessionExpireFilter]
    public class UsuarioController : Controller
    {
        ServicioSistemaComercialClient _ServicioSistemaComercial = new ServicioSistemaComercialClient();

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        [Atributos.AccesoAutorizadoFilter]
        public async Task<ActionResult> Index(UsuarioGridViewModel model)
        {
            try
            {
                //Construimos el modelo
                model = await ObtenerModelGrid(model);

                //Retornamos vista con modelo
                return View(model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        private async Task<UsuarioGridViewModel> ObtenerModelGrid(UsuarioGridViewModel model)
        {
            model.Filtro = model.Filtro ?? new UsuarioFiltroDto();
            //Construimos el request 
            var request = new UsuarioRequestDto
            {
                Filtro = model.Filtro
            };

            //Invocamos al servicio
            var response = await _ServicioSistemaComercial.ObtenerIndexUsuariosAsync(request);

            //Construimos el modelo
            model = new UsuarioGridViewModel(response);

            //Retornamos el modelo
            return model;

        }

        /// <summary>Método que actualiza la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> Refrescar(UsuarioGridViewModel model)
        {
            try
            {
                //Construimos el modelo
                model = await ObtenerModelGrid(model);

                //Retornamos vista con modelo
                return PartialView("_Index.Grid", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> Editor(short Id = 0)
        {
            var response = await _ServicioSistemaComercial.ObtenerEditorUsuarioAsync(Id);

            //Construimos el modelo
            var model = new UsuarioEditorViewModel(response);

            //Retornamos vista con modelo
            return PartialView("_Editor", model);
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> Perfil()
        {
            var codigoUsuarioLogueado = SesionModel.DatosSesion.Usuario.Codigo;
            var response = await _ServicioSistemaComercial.ObtenerEditorUsuarioAsync(codigoUsuarioLogueado);

            //Construimos el modelo
            var model = new UsuarioEditorViewModel(response);

            //Retornamos vista con modelo
            return PartialView("_EditorReadOnly", model);
        }

        public FileContentResult ObtenerImagen(short siCodUsu = 0)
        {
            var response = _ServicioSistemaComercial.ObtenerEditorUsuario(siCodUsu);
            if (response.Archivo != null)
            {
                return File(response.Archivo.Archivo, response.Archivo.Longitud.ToString());
            }
            else
            {
                return null;
            }
        }

        /// <summary>Método que graba la Usuario</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Agregar(UsuarioEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new UsuarioRequestDto
                {
                    Usuario = model.Usuario
                };

                request.Usuario.siCodUsuAct = SesionModel.DatosSesion.Usuario.Codigo;

                if (model.Archivo != null)
                {
                    Stream str = model.Archivo.InputStream;
                    BinaryReader Br = new BinaryReader(str);
                    Byte[] foto = Br.ReadBytes((Int32)str.Length);
                    request.Archivo = foto;
                }

                //Invocamos al servicio
                await _ServicioSistemaComercial.RegistrarUsuarioAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("Refrescar");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que actualiza la Usuario</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Modificar(UsuarioEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new UsuarioRequestDto
                {
                    Usuario = model.Usuario
                };

                if (model.Archivo != null)
                {
                    Stream str = model.Archivo.InputStream;
                    BinaryReader Br = new BinaryReader(str);
                    Byte[] foto = Br.ReadBytes((Int32)str.Length);
                    request.Archivo = foto;
                }

                request.Usuario.siCodUsuAct = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                await _ServicioSistemaComercial.ActualizarUsuarioAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("Refrescar");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public ActionResult EditorClave()
        {
            //Construimos el modelo
            var model = new UsuarioCambiarClaveViewModel();

            //Retornamos vista con modelo
            return PartialView("_Editor.CambiarClave", model);
        }

        /// <summary>Método que actualiza la Usuario</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> CambiarClave(UsuarioCambiarClaveViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new CambiarClaveRequestDto
                {
                    ClaveAnterior = model.ClaveAnterior,
                    ClaveNueva = model.ClaveNueva,
                    CodigoUsuario = SesionModel.DatosSesion.Usuario.Codigo
                };

                //Invocamos al servicio
                await _ServicioSistemaComercial.CambiarClaveAsync(request);

                //Refrescamos la pagina con los registros actuales
                return Json("ok", JsonRequestBehavior.AllowGet);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que elimina la Usuario</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Eliminar(short id)
        {
            try
            {
                //Construimos el request
                var request = new UsuarioRequestDto
                {
                    Usuario = new GEN_TabUsuario
                    {
                        siCodUsu = id,
                        siCodUsuAct = SesionModel.DatosSesion.Usuario.Codigo,
                    }
                };

                //Invocamos al servicio
                await _ServicioSistemaComercial.EliminarUsuarioAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("Refrescar");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que exporta a archivo excel el listado de modo pago garitas</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Archivo excel</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>19/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public async Task<FileResult> Exportar(UsuarioGridViewModel model)
        {
            model.Filtro = model.Filtro ?? new UsuarioFiltroDto();
            //Construimos el request 
            var request = new UsuarioRequestDto
            {
                Filtro = model.Filtro
            };

            //Invocamos al servicio
            var response = await _ServicioSistemaComercial.ListaUsuariosAsync(request);

            // set the data source
            System.Data.DataTable gridview = new System.Data.DataTable("Grid");

            //Agregar Columna a Grilla
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Codigo", "Codigo"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Nombre", "Nombre"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Usuario", "Usuario"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Perfil", "Perfil"));

            //LlenarGrilla
            foreach (var item in response.ListaUsuarios) gridview.Rows.Add(item.Codigo, item.Nombre, item.Usuario, item.Perfil);

            return new GeneralController().ExportarExcel(gridview, Response, "Usuario");
        }
    }
}