﻿using SGC.Models.Movimiento;
using System.Web.Mvc;
using SGC.ServicioSistemaComercial;
using System.Threading.Tasks;
using System.ServiceModel;
using SGC.Helper;
using System.Web.UI.WebControls;
using SistemaComercial.App.Utility;
using System.Web.Routing;
using System.Data;
//using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using SGC.Models.Archivo;
using System;
namespace SGC.Controllers
{
    [Atributos.SessionExpireFilter]
    public class ArchivosController : Controller
    {
        ServicioSistemaComercialClient _ServicioSistemaComercial = new ServicioSistemaComercialClient();

        [HttpGet]
        public async Task<FileResult> Descargar(string id)
        {
            var response = await _ServicioSistemaComercial.ObtenerArchivoPorIdAsync(id);

            return File(response.Archivo.Archivo, response.Archivo.Extension, response.Archivo.Nombre);
        }
    }
}