﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGC.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PaginaNoEncontrada()
        {
            return View();
        }

        public ActionResult ErrorInterno()
        {
            return View();
        }

        public ActionResult AccesoNoAutorizado()
        {
            return View();
        }
    }
}