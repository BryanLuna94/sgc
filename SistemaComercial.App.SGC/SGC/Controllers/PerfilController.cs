﻿using SGC.Models.Perfil;
using System.Web.Mvc;
using SGC.ServicioSistemaComercial;
using System.Threading.Tasks;
using System.ServiceModel;
using SGC.Helper;
using System.Web.UI.WebControls;

namespace SGC.Controllers
{
    [Atributos.SessionExpireFilter]
    public class PerfilController : Controller
    {
        ServicioSistemaComercialClient _ServicioSistemaComercial = new ServicioSistemaComercialClient();

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        [Atributos.AccesoAutorizadoFilter]
        public async Task<ActionResult> Index(PerfilGridViewModel model)
        {
            try
            {
                //Construimos el modelo
                model = await ObtenerModelGrid(model);

                //Retornamos vista con modelo
                return View(model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        private async Task<PerfilGridViewModel> ObtenerModelGrid(PerfilGridViewModel model)
        {
            model.Filtro = model.Filtro ?? new PerfilFiltroDto();
            //Construimos el request 
            var request = new PerfilRequestDto
            {
                Filtro = model.Filtro
            };

            //Invocamos al servicio
            var response = await _ServicioSistemaComercial.ObtenerIndexPerfilesAsync(request);

            //Construimos el modelo
            model = new PerfilGridViewModel(response);

            //Retornamos el modelo
            return model;

        }

        /// <summary>Método que actualiza la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>16/01/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> Refrescar(PerfilGridViewModel model)
        {
            try
            {
                //Construimos el modelo
                model = await ObtenerModelGrid(model);

                //Retornamos vista con modelo
                return PartialView("_Index.Grid", model);
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> Editor(short Id = 0)
        {
            var response = await _ServicioSistemaComercial.ObtenerEditorPerfilAsync(Id);

            //Construimos el modelo
            var model = new PerfilEditorViewModel(response);

            //Retornamos vista con modelo
            return PartialView("_Editor", model);
        }

        /// <summary>Método que graba la Perfil</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Agregar(PerfilEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new PerfilRequestDto
                {
                    Perfil = model.Perfil
                };

                request.Perfil.siCodUsuAct = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                await _ServicioSistemaComercial.RegistrarPerfilAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("Refrescar");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que actualiza la Perfil</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Modificar(PerfilEditorViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new PerfilRequestDto
                {
                    Perfil = model.Perfil
                };

                request.Perfil.siCodUsuAct = SesionModel.DatosSesion.Usuario.Codigo;

                //Invocamos al servicio
                await _ServicioSistemaComercial.ActualizarPerfilAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("Refrescar");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que elimina la Perfil</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpPost]
        public async Task<ActionResult> Eliminar(short id)
        {
            try
            {
                //Construimos el request
                var request = new PerfilRequestDto
                {
                    Perfil = new GEN_MaePerfil
                    {
                        siCodPer = id,
                        siCodUsuAct = SesionModel.DatosSesion.Usuario.Codigo
                    }
                };

                //Invocamos al servicio
                await _ServicioSistemaComercial.EliminarPerfilAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("Refrescar");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Método que exporta a archivo excel el listado de modo pago garitas</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Archivo excel</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Bryan Luna Vásquez</CreadoPor></item>
        /// <item><FecCrea>19/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        public async Task<FileResult> Exportar(PerfilGridViewModel model)
        {
            model.Filtro = model.Filtro ?? new PerfilFiltroDto();
            //Construimos el request 
            var request = new PerfilRequestDto
            {
                Filtro = model.Filtro
            };

            //Invocamos al servicio
            var response = await _ServicioSistemaComercial.ListaPerfilesAsync(request);

            // set the data source
            System.Data.DataTable gridview = new System.Data.DataTable("Grid");

            //Agregar Columna a Grilla
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Codigo", "Codigo"));
            gridview.Columns.Add(HelperFunciones.CrearColumnaGrid("Nombre", "Nombre"));

            //LlenarGrilla
            foreach (var item in response.ListaPerfiles) gridview.Rows.Add(item.Codigo, item.Nombre);

            return new GeneralController().ExportarExcel(gridview, Response, "Perfil");
        }

        /// <summary>Método que devuelve el modelo para la vista de la grilla</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Modelo para la vista de al grilla</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Wilmer Gómez Prado</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>
        [HttpGet]
        public async Task<ActionResult> EditorOpciones(short Id = 0)
        {
            var response = await _ServicioSistemaComercial.ListaOpcionesPorPerfilAsync(Id);

            //Construimos el modelo
            var model = new PerfilOpcionesViewModel(response);
            model.IdPerfil = Id;
            //Retornamos vista con modelo
            return PartialView("_EditorOpciones", model);
        }

        /// <summary>Método que graba la Perfil</summary>
        /// <param name="model">Modelo del request</param>
        /// <returns>Vista Refrescar</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Wilmer Gómez Prado</CreadoPor></item>
        /// <item><FecCrea>05/02/2018</FecCrea></item>
        /// </list>
        /// <list type="bullet" >
        /// <item><FecActu>XXXX</FecActu></item>
        /// <item><Resp>XXXX</Resp></item>
        /// <item><Mot>XXXX</Mot></item></list></remarks>

        public async Task<ActionResult> AgregarOpciones(PerfilOpcionesViewModel model)
        {
            try
            {
                //Construimos el request
                var request = new PerfilRequestDto
                {
                    IdPerfil = model.IdPerfil,
                    ListaOpciones = model.ListaOpciones
                };

                //Invocamos al servicio
                await _ServicioSistemaComercial.GrabarOpcionesPorPerfilAsync(request);

                //Refrescamos la pagina con los registros actuales
                return RedirectToAction("Refrescar");
            }
            catch (FaultException<ServiceErrorResponseType> ex)
            {
                //Como existe excepción de lógica de negocio, lo enviamos al Vehiculo para ser procesado por este
                return Json(HelperJson.ConstruirJson(EnumTipoNotificacion.Advertencia, ex.Detail.Message), JsonRequestBehavior.AllowGet);
            }
        }
    }
}