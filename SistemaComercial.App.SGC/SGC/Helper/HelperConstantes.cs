﻿namespace SGC.Helper
{
    public class HelperConstantes
    {
        public const string GSesionUsuario = "SessionUsuario";
        public const string GSesionDatos = "DatosSesion";
        public const string GSesionStope = "StopeSesion";
        public const string GSesionMovDiario = "MovDiarioSesion";
        public const string GSesionActividad = "ActividadSesion";
        public const string GSesionFiltrosPrograma = "FiltrosProgramaSesion";
        public const string GSesionFiltrosDetallePrograma = "FiltrosDetalleProgramaSesion";
        public const string GSesionFiltrosAlmacen = "FiltrosAlmacenSesion";
        public const string GSesionFiltrosMovimiento = "FiltrosMovimiento";

        public const string GIdMonedaSoles = "001";
        public const string GIdEstadoFinalizado = "003";
        public const string GIdValorTileSbs = "INTERES_SBS";
        public const string GIdConceptoCombustible = "2301030101";

        public const string GTipoUsuarioCas = "CAS";
        public const string GTipoUsuarioCap = "CAP";
        public const string GTipoUsuarioExterno = "EXT";

        public const decimal GImpuestoMulti = 0.18M;
        public const decimal GImpuestoDiv = 1.18M;

        public class Permisos
        {
            public const int PUEDE_CREAREDITAR_UNIDADESMEDIDA = 11;
            public const int PUEDE_ELIMINAR_UNIDADESMEDIDA = 12;
            public const int PUEDE_CREAREDITAR_ALMACEN = 13;
            public const int PUEDE_ELIMINAR_ALMACEN = 14;
            public const int PUEDE_CREAREDITAR_EMPRESA= 15;
            public const int PUEDE_ELIMINAR_EMPRESA = 16;
            public const int PUEDE_CREAREDITAR_ARTICULO = 17;
            public const int PUEDE_ELIMINAR_ARTICULO = 18;
            public const int PUEDE_CREAREDITAR_MOVIMIENTO = 19;
            public const int PUEDE_ELIMINAR_MOVIMIENTO = 20;
            public const int PUEDE_CREAREDITAR_PERFIL = 24;
            public const int PUEDE_ELIMINAR_PERFIL = 25;
            public const int PUEDE_CREAREDITAR_USUARIO = 26;
            public const int PUEDE_ELIMINAR_USUARIO = 27;
            public const int PUEDE_CREAREDITAR_MARCA = 29;
            public const int PUEDE_ELIMINAR_MARCA = 30;

            public const int PUEDE_CREAREDITAR_INVENTARIO = 31;
            public const int PUEDE_ELIMINAR_INVENTARIO = 32;
        }
    }
}
