﻿using System.Web;
using SGC.Models;
using SGC.Models.Almacen;
using SGC.Models.Movimiento;

namespace SGC.Helper
{
    public class SesionModel
    {
        private static DatosSesionViewModel _DatosSesion;
        public static DatosSesionViewModel DatosSesion
        {
            get
            {
                var session = (DatosSesionViewModel)HttpContext.Current.Session[HelperConstantes.GSesionDatos];
                if (session == null) session = new DatosSesionViewModel();
                _DatosSesion = session;
                return _DatosSesion;
            }
            set
            {
                HttpContext.Current.Session[HelperConstantes.GSesionDatos] = value;
            }
        }

        private static AlmacenGridViewModel _FiltrosAlmacen;
        public static AlmacenGridViewModel FiltrosAlmacen
        {
            get
            {
                var session = (AlmacenGridViewModel)HttpContext.Current.Session[HelperConstantes.GSesionFiltrosAlmacen];
                if (session == null) session = new AlmacenGridViewModel();
                _FiltrosAlmacen = session;
                return _FiltrosAlmacen;
            }
            set
            {
                if (value == null)
                {
                    HttpContext.Current.Session.Remove(HelperConstantes.GSesionFiltrosAlmacen);
                }
                else
                {
                    HttpContext.Current.Session[HelperConstantes.GSesionFiltrosAlmacen] = value;
                }
            }
        }

        private static MovimientoGridViewModel _FiltrosMovimiento;
        public static MovimientoGridViewModel FiltrosMovimiento
        {
            get
            {
                var session = (MovimientoGridViewModel)HttpContext.Current.Session[HelperConstantes.GSesionFiltrosMovimiento];
                if (session == null) session = new MovimientoGridViewModel();
                _FiltrosMovimiento = session;
                return _FiltrosMovimiento;
            }
            set
            {
                if (value == null)
                {
                    HttpContext.Current.Session.Remove(HelperConstantes.GSesionFiltrosMovimiento);
                }
                else
                {
                    HttpContext.Current.Session[HelperConstantes.GSesionFiltrosMovimiento] = value;
                }
            }
        }

    }
}