﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Usuario
{
    public class UsuarioCambiarClaveViewModel
    {
        public string ClaveAnterior { get; set; }
        public string ClaveNueva { get; set; }
        public string ClaveConfirma { get; set; }
        public short CodigoUsuario { get; set; }
    }
}