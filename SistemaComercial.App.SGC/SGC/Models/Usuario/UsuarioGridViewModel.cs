﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Usuario
{
    public class UsuarioGridViewModel
    {
        public UsuarioGridViewModel() { }

        public UsuarioGridViewModel(UsuarioResponseDto response)
        {
            ListaUsuarios = response.ListaUsuarios;

            LstPerfiles = new List<SelectListItem>();

            if (response.ListaPerfiles != null) foreach (GenericoListaDto item in response.ListaPerfiles) LstPerfiles.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
        }

        public List<UsuarioListaDto> ListaUsuarios { get; set; }
        public List<SelectListItem> LstPerfiles { get; set; }
        public UsuarioFiltroDto Filtro { get; set; }
    }
}