﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;
using System.Web;

namespace SGC.Models.Usuario
{
    public class UsuarioEditorViewModel
    {
        public UsuarioEditorViewModel() { }

        public UsuarioEditorViewModel(UsuarioResponseDto response)
        {
            Usuario = response.Usuario;
            ExisteImagen = "0";
            CodigoUsuario = (Usuario != null) ? Usuario.siCodUsu : (short)0;
            Foto = new ArchivoListaDto();
            LstPerfiles = new List<SelectListItem>();

            if (response.Archivo != null) Foto = response.Archivo;
            if (response.ListaPerfiles != null) foreach (GenericoListaDto item in response.ListaPerfiles) LstPerfiles.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
        }

        public short CodigoUsuario { get; set; }
        public GEN_TabUsuario Usuario { get; set; }
        public ArchivoListaDto Foto { get; set; }
        public List<SelectListItem> LstPerfiles { get; set; }
        public string RutaImagen { get; set; }
        public string ExisteImagen { get; set; }
        public HttpPostedFileBase Archivo { get; set; }
    }
}