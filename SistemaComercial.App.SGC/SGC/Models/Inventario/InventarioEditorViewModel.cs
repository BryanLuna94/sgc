﻿using SGC.ServicioSistemaComercial;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SGC.Models.Inventario
{
    public class InventarioEditorViewModel
    {
        public InventarioEditorViewModel() { }

        public InventarioEditorViewModel(InventarioResponseDto response)
        {
            Inventario = response.Inventario;
            if (response.Inventario != null) Almacen = response.Inventario.siCodAlm;

            LstAlmacenes = new List<SelectListItem>();
            LstEmpresasPartner = new List<SelectListItem>();

            if (response.ListaAlmacenes != null) foreach (GenericoListaDto item in response.ListaAlmacenes) LstAlmacenes.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaEmpresasPartner != null) foreach (GenericoListaDto item in response.ListaEmpresasPartner) LstEmpresasPartner.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
        }

        public COM_TabInventario Inventario { get; set; }
        public List<SelectListItem> LstAlmacenes { get; set; }
        public List<SelectListItem> LstEmpresasPartner { get; set; }
        public InventarioFiltroDto Filtro { get; set; }
        public short Almacen { get; set; }
    }
}