﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Inventario
{
    public class InventarioGridViewModel
    {
        public InventarioGridViewModel() { }

        public InventarioGridViewModel(InventarioResponseDto response)
        {
            ListaInventarios = response.ListaInventarios;
            LstAlmacenes = new List<SelectListItem>();
            LstEstados = new List<SelectListItem>();

            if (response.ListaAlmacenes != null) foreach (GenericoListaDto item in response.ListaAlmacenes) LstAlmacenes.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaEstados != null) foreach (GenericoListaDto item in response.ListaEstados) LstEstados.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
        }

        public List<InventarioListaDto> ListaInventarios { get; set; }
        public InventarioFiltroDto Filtro { get; set; }
        public List<SelectListItem> LstAlmacenes { get; set; }
        public List<SelectListItem> LstEstados { get; set; }


    }
}