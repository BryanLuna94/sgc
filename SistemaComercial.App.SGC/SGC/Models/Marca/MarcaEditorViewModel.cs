﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Marca
{
    public class MarcaEditorViewModel
    {
        public MarcaEditorViewModel() { }

        public MarcaEditorViewModel(MarcaResponseDto response)
        {
            Marca = response.Marca;
        }

        public COM_MaeMarca Marca { get; set; }
    }
}