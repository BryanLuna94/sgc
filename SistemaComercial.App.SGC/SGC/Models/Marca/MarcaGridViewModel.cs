﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Marca
{
    public class MarcaGridViewModel
    {
        public MarcaGridViewModel() { }

        public MarcaGridViewModel(MarcaResponseDto response)
        {
            ListaMarcas = response.ListaMarcas;
        }

        public List<MarcaListaDto> ListaMarcas { get; set; }
        public MarcaFiltroDto Filtro { get; set; }
        public short CodigoMarca { get; set; }
        public string Marca { get; set; }
    }
}