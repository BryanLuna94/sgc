﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Almacen
{
    public class AlmacenEditorViewModel
    {
        public AlmacenEditorViewModel() { }

        public AlmacenEditorViewModel(AlmacenResponseDto response)
        {
            Almacen = response.Almacen;
        }

        public COM_MaeAlmacen Almacen { get; set; }
    }
}