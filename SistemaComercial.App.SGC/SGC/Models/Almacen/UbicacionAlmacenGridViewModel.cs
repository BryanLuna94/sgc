﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Almacen
{
    public class UbicacionAlmacenGridViewModel
    {
        public UbicacionAlmacenGridViewModel() { }

        public UbicacionAlmacenGridViewModel(AlmacenUbicacionResponseDto response)
        {
            ListaUbicaciones = response.ListaUbicacionesAlmacen;
            AlmacenDto = response.AlmacenDto ?? new AlmacenListaDto();
        }

        public List<AlmacenUbicacionListaDto> ListaUbicaciones { get; set; }
        public AlmacenUbicacionFiltroDto Filtro { get; set; }
        public AlmacenListaDto AlmacenDto { get; set; }
    }
}