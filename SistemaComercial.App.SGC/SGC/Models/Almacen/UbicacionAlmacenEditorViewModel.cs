﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;


namespace SGC.Models.Almacen
{
    public class UbicacionAlmacenEditorViewModel
    {
        public UbicacionAlmacenEditorViewModel() { }

        public UbicacionAlmacenEditorViewModel(AlmacenUbicacionResponseDto response)
        {
            Ubicacion = response.AlmacenUbicacion ?? new COM_MaeAlmacen_Ubicacion();

            LstUnidadesMedida = new List<SelectListItem>();

            if (response.ListaUnidadesMedida != null) foreach (GenericoListaDto item in response.ListaUnidadesMedida) LstUnidadesMedida.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
        }

        public COM_MaeAlmacen_Ubicacion Ubicacion { get; set; }
        public List<SelectListItem> LstUnidadesMedida { get; set; }
    }
}