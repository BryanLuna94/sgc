﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Almacen
{
    public class AlmacenGridViewModel
    {
        public AlmacenGridViewModel() { }

        public AlmacenGridViewModel(AlmacenResponseDto response)
        {
            ListaAlmacenes = response.ListaAlmacenes;
        }

        public List<AlmacenListaDto> ListaAlmacenes { get; set; }
        public AlmacenFiltroDto Filtro { get; set; }
        public short CodigoAlmacen { get; set; }
        public string Almacen { get; set; }
    }
}