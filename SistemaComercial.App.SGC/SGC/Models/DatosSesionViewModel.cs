﻿using System;
using System.Collections.Generic;
using System.Linq;
using SGC.ServicioSistemaComercial;

namespace SGC.Models
{
    public class DatosSesionViewModel
    {
        public LoginListaDto Usuario { get; set; }
        public List<OpcionListaDto> PermisosUsuario { get; set; }
        public ArchivoListaDto FotoUsuario { get; set; }
    }
}