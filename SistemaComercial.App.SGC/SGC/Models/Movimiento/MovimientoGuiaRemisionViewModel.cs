﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;
using System;
using System.Data;

namespace SGC.Models.Movimiento
{
    public class MovimientoGuiaRemisionViewModel
    {


        public static DataTable DTReporteGuiaRemision(GuiaRemisionResponseDto response)
        {
            DataTable dt = new DataTable("tbGuiaRemisionCab");
            dt.Columns.Add("Serie", typeof(string));
            dt.Columns.Add("Numero", typeof(string));
            dt.Columns.Add("Cliente", typeof(string));
            dt.Columns.Add("Ruc", typeof(string));
            dt.Columns.Add("DireccionLlegada", typeof(string));
            dt.Columns.Add("Fecha", typeof(string));
            dt.Columns.Add("MotivoTraslado", typeof(string));
            dt.Columns.Add("Marca", typeof(string));
            dt.Columns.Add("Origen", typeof(string));
            dt.Columns.Add("Destino", typeof(string));
            dt.Columns.Add("RMA", typeof(string));
            dt.Columns.Add("Estado", typeof(string));
            dt.Columns.Add("NOrden", typeof(string));
            dt.Columns.Add("Peso", typeof(string));
            dt.Columns.Add("DireccionPartida", typeof(string));

            dt.Rows.Add(response.GuiaRemisionCab.Serie,
                response.GuiaRemisionCab.Numero,
                response.GuiaRemisionCab.Cliente,
                response.GuiaRemisionCab.Ruc,
                response.GuiaRemisionCab.DireccionLlegada,
                response.GuiaRemisionCab.Fecha,
                response.GuiaRemisionCab.MotivoTraslado,
                response.GuiaRemisionCab.Marca,
                response.GuiaRemisionCab.Origen,
                response.GuiaRemisionCab.Destino,
                response.GuiaRemisionCab.RMA,
                response.GuiaRemisionCab.Estado,
                response.GuiaRemisionCab.NOrden,
                response.GuiaRemisionCab.Peso,
                response.GuiaRemisionCab.DireccionPartida);

            return dt;
        }

        public static DataTable DTReporteGuiaRemisionDet(GuiaRemisionResponseDto response)
        {
            DataTable dt = new DataTable("tbGuiaRemisionDet");
            dt.Columns.Add("Cantidad", typeof(int));
            dt.Columns.Add("Descripcion", typeof(string));
            dt.Columns.Add("PartNumber", typeof(string));
            dt.Columns.Add("Serie", typeof(string));
            dt.Columns.Add("Codigo", typeof(string));

            var cont = 0;
            foreach (var item in response.ListaGuiaRemisionDet)
            {
                dt.Rows.Add(item.Cantidad, item.Descripcion, item.PartNumber, item.Serie, item.Codigo);
                cont++;
            }

            return dt;
        }
    }


}