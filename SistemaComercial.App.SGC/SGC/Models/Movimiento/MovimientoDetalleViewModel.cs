﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Movimiento
{
    public class MovimientoDetalleViewModel
    {
        public MovimientoDetalleViewModel() { }

        public MovimientoDetalleViewModel(MovimientoResponseDto response)
        {
            Movimiento = response.MovimientoCabecera;
            MovimientoDetalle = response.MovimientoDetalle;
            Stock = response.Stock;
            DetalleStock = response.DetalleStock ?? new COM_DetStock();
            MovimientoDetalleDto = response.MovimientoDetalleDto ?? new DetalleMovimientoListaDto();
            MovimientoDetalle = response.MovimientoDetalle ?? new COM_DetMovArt();

            LstAlmacenes = new List<SelectListItem>();
            LstEstadosArticulos = new List<SelectListItem>();
            LstPisos = new List<SelectListItem>();
            LstRacks = new List<SelectListItem>();
            LstUbicaciones = new List<SelectListItem>();
            Piso = response.Piso;
            Rack = response.Rack;

            if (response.ListaAlmacenes != null) foreach (GenericoListaDto item in response.ListaAlmacenes) LstAlmacenes.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaEstadosArticulo != null) foreach (GenericoListaDto item in response.ListaEstadosArticulo) LstEstadosArticulos.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaPisos != null) foreach (GenericoListaDto item in response.ListaPisos) LstPisos.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaRacks != null) foreach (GenericoListaDto item in response.ListaRacks) LstRacks.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaUbicaciones != null) foreach (GenericoListaDto item in response.ListaUbicaciones) LstUbicaciones.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });

        }

        public COM_TabStock Stock { get; set; }
        public COM_DetStock DetalleStock { get; set; }
        public COM_CabMovArt Movimiento { get; set; }
        public COM_DetMovArt MovimientoDetalle { get; set; }
        public DetalleMovimientoListaDto MovimientoDetalleDto { get; set; }
        public List<SelectListItem> LstAlmacenes { get; set; }
        public List<SelectListItem> LstEstadosArticulos { get; set; }
        public List<SelectListItem> LstPisos { get; set; }
        public List<SelectListItem> LstRacks { get; set; }
        public List<SelectListItem> LstUbicaciones { get; set; }
        public string Piso { get; set; }
        public string Rack { get; set; }
    }
}