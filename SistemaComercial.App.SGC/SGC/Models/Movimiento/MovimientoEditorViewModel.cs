﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;
using System.Linq;
using SistemaComercial.App.Utility;


namespace SGC.Models.Movimiento
{
    public class MovimientoEditorViewModel
    {
        public MovimientoEditorViewModel() { }

        public MovimientoEditorViewModel(MovimientoResponseDto response, string form)
        {
            TipoMov = (string.IsNullOrEmpty(form) || form == "undefined") ? response.MovimientoCabecera.vTipoMov : form;
            Movimiento = response.MovimientoCabecera ?? new COM_CabMovArt();
            MovimientoExt = response.MovimientoCabecera_Ext;
            MovimientoDto = response.MovimientoCabeceraDto;
            Factura = response.Factura;
            Guia = response.Guia;
            if (MovimientoDto == null)
            {
                MovimientoDto = new MovimientoListaDto
                {
                    FechaRecepcion = System.DateTime.Now.ToShortDateString(),
                    HoraRecepcion = System.DateTime.Now.ToShortTimeString(),
                };

                if (TipoMov == Constantes.Tablas.TipoMov.IMPORTACION)
                {
                    Factura = new COM_CabFactura
                    {
                        sdFecha = System.DateTime.Now
                    };
                }
                else if (TipoMov == Constantes.Tablas.TipoMov.DEVOLUCION)
                {
                    MovimientoExt = new COM_CabMovArt_Ext
                    {
                        sdFechaETA = System.DateTime.Now
                    };
                }
                else if (TipoMov == Constantes.Tablas.TipoMov.DELIVERY)
                {
                    MovimientoExt = new COM_CabMovArt_Ext
                    {
                        sdFechaETA = System.DateTime.Now
                    };
                    Guia = new COM_CabGuia();
                }
                else if(TipoMov == Constantes.Tablas.TipoMov.EXPORTACION)
                {
                    Factura = new COM_CabFactura
                    {
                        sdFecha = System.DateTime.Now
                    };
                    Guia = new COM_CabGuia();
                }

            }
            ListaMovimientoDetalle = response.ListaMovimientoDetalle;
            ListaLiquidacion = response.ListaLiquidacion;

            LstTiposMov = new List<SelectListItem>();
            _LstTiposDoc = new List<SelectListItem>();
            LstTiposDoc = new List<SelectListItem>();
            LstDirecciones = new List<SelectListItem>();
            LstEmpresas = new List<SelectListItem>();
            LstEmpresasPartner = new List<SelectListItem>();
            LstTipoDocFactura = new List<SelectListItem>();
            LstMonedas = new List<SelectListItem>();
            LstOficinas = new List<SelectListItem>();

            if (response.ListaTiposMov != null) foreach (GenericoListaDto item in response.ListaTiposMov) LstTiposMov.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaTiposDoc != null) foreach (GenericoListaDto item in response.ListaTiposDoc) _LstTiposDoc.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaDirecciones != null) foreach (GenericoListaDto item in response.ListaDirecciones) LstDirecciones.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaEmpresas != null) foreach (GenericoListaDto item in response.ListaEmpresas) LstEmpresas.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaEmpresasPartner != null) foreach (GenericoListaDto item in response.ListaEmpresasPartner) LstEmpresasPartner.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaMonedas != null) foreach (GenericoListaDto item in response.ListaMonedas) LstMonedas.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaOficinas != null) foreach (GenericoListaDto item in response.ListaOficinas) LstOficinas.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });

            switch (TipoMov)
            {
                case Constantes.Tablas.TipoMov.COMPRA:
                    LstTiposDoc.Add(_LstTiposDoc.FirstOrDefault(x => x.Value == Constantes.Tablas.TipoDocumento.FACTURA));
                    LstTiposDoc.Add(_LstTiposDoc.FirstOrDefault(x => x.Value == Constantes.Tablas.TipoDocumento.BOLETAVENTA));
                    break;
                case Constantes.Tablas.TipoMov.IMPORTACION:
                    LstTipoDocFactura.Add(_LstTiposDoc.FirstOrDefault(x => x.Value == Constantes.Tablas.TipoDocumento.FACTURA));
                    LstTipoDocFactura.Add(_LstTiposDoc.FirstOrDefault(x => x.Value == Constantes.Tablas.TipoDocumento.BOLETAVENTA));
                    break;
                case Constantes.Tablas.TipoMov.DEVOLUCION:
                    LstTiposDoc.Add(_LstTiposDoc.FirstOrDefault(x => x.Value == Constantes.Tablas.TipoDocumento.GUIAREMISION));
                    LstTiposDoc.Add(_LstTiposDoc.FirstOrDefault(x => x.Value == Constantes.Tablas.TipoDocumento.OTROS));
                    break;
                case Constantes.Tablas.TipoMov.VENTA:
                    LstTipoDocFactura.Add(_LstTiposDoc.FirstOrDefault(x => x.Value == Constantes.Tablas.TipoDocumento.FACTURA));
                    LstTipoDocFactura.Add(_LstTiposDoc.FirstOrDefault(x => x.Value == Constantes.Tablas.TipoDocumento.BOLETAVENTA));
                    break;
                case Constantes.Tablas.TipoMov.DELIVERY:
                    LstTipoDocFactura.Add(_LstTiposDoc.FirstOrDefault(x => x.Value == Constantes.Tablas.TipoDocumento.FACTURA));
                    LstTipoDocFactura.Add(_LstTiposDoc.FirstOrDefault(x => x.Value == Constantes.Tablas.TipoDocumento.BOLETAVENTA));
                    break;
                case Constantes.Tablas.TipoMov.EXPORTACION:
                    LstTipoDocFactura.Add(_LstTiposDoc.FirstOrDefault(x => x.Value == Constantes.Tablas.TipoDocumento.FACTURA));
                    LstTipoDocFactura.Add(_LstTiposDoc.FirstOrDefault(x => x.Value == Constantes.Tablas.TipoDocumento.BOLETAVENTA));
                    break;
                default:
                    break;
            }
        }

        public COM_CabMovArt Movimiento { get; set; }
        public MovimientoListaDto MovimientoDto { get; set; }
        public COM_CabMovArt_Ext MovimientoExt { get; set; }
        public List<DetalleMovimientoListaDto> ListaMovimientoDetalle { get; set; }
        public List<SelectListItem> LstTiposMov { get; set; }
        public List<SelectListItem> _LstTiposDoc { get; set; }
        public List<SelectListItem> LstTiposDoc { get; set; }
        public List<SelectListItem> LstMonedas { get; set; }
        public List<SelectListItem> LstTipoDocFactura { get; set; }
        public List<SelectListItem> LstDirecciones { get; set; }
        public List<SelectListItem> LstEmpresas { get; set; }
        public List<SelectListItem> LstEmpresasPartner { get; set; }
        private string TipoMov { get; set; }
        public COM_CabFactura Factura { get; set; }
        public COM_CabGuia Guia { get; set; }
        public List<COM_Liquidacion> ListaLiquidacion { get; set; }
        public List<SelectListItem> LstOficinas { get; set; }

    }
}