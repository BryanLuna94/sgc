﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Movimiento
{
    public class MovimientoGridViewModel
    {
        public MovimientoGridViewModel() { }

        public MovimientoGridViewModel(MovimientoResponseDto response)
        {
            ListaMovimientos = response.ListaMovimientos;

            LstTiposMov = new List<SelectListItem>();
            LstTiposDoc = new List<SelectListItem>();
            LstUsuarios = new List<SelectListItem>();

            if (response.ListaTiposMov != null) foreach (GenericoListaDto item in response.ListaTiposMov) LstTiposMov.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaTiposDoc != null) foreach (GenericoListaDto item in response.ListaTiposDoc) LstTiposDoc.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaUsuarios != null) foreach (GenericoListaDto item in response.ListaUsuarios) LstUsuarios.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
        }

        public MovimientoFiltroDto Filtro { get; set; }
        public List<MovimientoListaDto> ListaMovimientos { get; set; }
        public List<SelectListItem> LstTiposMov { get; set; }
        public List<SelectListItem> LstTiposDoc { get; set; }
        public List<SelectListItem> LstUsuarios { get; set; }
        public string ParamEditor { get; set; }
    }
}