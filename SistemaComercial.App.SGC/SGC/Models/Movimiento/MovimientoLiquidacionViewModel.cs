﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Movimiento
{
    public class MovimientoLiquidacionViewModel
    {
        public MovimientoLiquidacionViewModel() { }

        public MovimientoLiquidacionViewModel(MovimientoLiquidacionResponseDto response)
        {
            Liquidacion = response.Liquidacion ?? new COM_Liquidacion();
        }

        public COM_Liquidacion Liquidacion { get; set; }
    }
}