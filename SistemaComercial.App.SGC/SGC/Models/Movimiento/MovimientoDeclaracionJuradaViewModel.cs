﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;
using System;
using System.Data;

namespace SGC.Models.Movimiento
{
    public class MovimientoDeclaracionJuradaViewModel
    {

        public static DataTable DTReporteDeclaracion(MovimientoResponseDto response)
        {
            DataTable dt = new DataTable("tbDetalleDeclaracion");
            dt.Columns.Add("DAM", typeof(string));
            dt.Columns.Add("Serie", typeof(string));
            dt.Columns.Add("Cantidad", typeof(string));
            dt.Columns.Add("Codigo", typeof(string));
            dt.Columns.Add("Descripcion", typeof(string));
            dt.Columns.Add("ValorFOB", typeof(string));
            dt.Columns.Add("ValorUSD", typeof(string));

            var cont = 0;
            foreach (var item in response.ListaMovimientoDetalle)
            {
                dt.Rows.Add("", item.Serie, 1, item.Codigo, item.NombreProducto, item.CostoIngreso, item.CostoIngreso);
                cont++;
            }

            return dt;
        }
    }


}