﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;
using System.Data;

namespace SGC.Models.Reporte
{
    public class ReporteAlmacenValorizadoViewModel
    {
        public ReporteAlmacenValorizadoViewModel() { }

        public ReporteAlmacenValorizadoViewModel(ReporteAlmacenesValorizadoResumidoResponseDto response)
        {
            LstMarcas = new List<SelectListItem>();

            if (response.ListaMarcas != null) foreach (GenericoListaDto item in response.ListaMarcas) LstMarcas.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
        }

        public List<SelectListItem> LstMarcas { get; set; }
        public ReporteAlmacenesValorizadoResumidoFiltroDto Filtro { get; set; }
        public string Marca { get; set; }
        public string Articulo { get; set; }

        public static DataTable DTReporteAlmacenResumido(ReporteAlmacenesValorizadoResumidoResponseDto response)
        {
            DataTable dt = new DataTable("Grid");
            dt.Columns.Add("Almacen", typeof(string));
            dt.Columns.Add("Suma", typeof(decimal));

            var cont = 0;
            foreach (var item in response.ListaAlmacenesValorizados)
            {
                dt.Rows.Add(item.Almacen, item.Suma);
                cont++;
            }

            return dt;
        }

        public static DataTable DTReporteAlmacenDetallado(ReporteAlmacenesValorizadoResumidoResponseDto response)
        {
            DataTable dt = new DataTable("Grid");
            dt.Columns.Add("Almacen", typeof(string));
            dt.Columns.Add("CodigoOriginal", typeof(string));
            dt.Columns.Add("Articulo", typeof(string));
            dt.Columns.Add("Serie", typeof(string));
            dt.Columns.Add("Costo", typeof(decimal));

            var cont = 0;
            foreach (var item in response.ListaAlmacenesValorizadosDet)
            {
                dt.Rows.Add(item.Almacen, item.CodigoOriginal, item.Articulo, item.Serie, item.Costo);
                cont++;
            }

            return dt;
        }
    }
}