﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;
using System.Data;
using System.Linq;

namespace SGC.Models.Reporte
{
    public class ReporteKardexViewModel
    {
        public ReporteKardexViewModel() { }

        public ReporteKardexViewModel(ReporteKardexResponseDto response)
        {
            LstMarcas = new List<SelectListItem>();
            LstEmpresas = new List<SelectListItem>();
            LstMeses = new List<SelectListItem>();
            LstAlmacenes = new List<SelectListItem>();

            Filtro = new ReporteKardexFiltroDto
            {
                Anio = (int)System.DateTime.Now.Year,
                IdMes = (int)System.DateTime.Now.Month,
            };

            if (response.ListaMarcas != null) foreach (GenericoListaDto item in response.ListaMarcas) LstMarcas.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaEmpresas != null) foreach (GenericoListaDto item in response.ListaEmpresas) LstEmpresas.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaMeses != null) foreach (GenericoListaDto item in response.ListaMeses) LstMeses.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaAlmacenes != null) foreach (GenericoListaDto item in response.ListaAlmacenes) LstAlmacenes.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });

            Mes = LstMeses.Single(x => x.Value == Filtro.IdMes.ToString()).Text;
        }

        public List<SelectListItem> LstMarcas { get; set; }
        public List<SelectListItem> LstEmpresas { get; set; }
        public List<SelectListItem> LstMeses { get; set; }
        public List<SelectListItem> LstAlmacenes { get; set; }
        public ReporteKardexFiltroDto Filtro { get; set; }
        public string Marca { get; set; }
        public string Articulo { get; set; }
        public string Mes { get; set; }
        public string Almacen { get; set; }

        public static DataTable DTReporteKardex(ReporteKardexResponseDto response)
        {
            DataTable dt = new DataTable("Grid");
            dt.Columns.Add("TipoMov", typeof(string));
            dt.Columns.Add("TipoDoc", typeof(string));
            dt.Columns.Add("NumeroDoc", typeof(string));
            dt.Columns.Add("UnMed", typeof(string));
            dt.Columns.Add("Codigo", typeof(string));
            dt.Columns.Add("Fecha", typeof(string));
            dt.Columns.Add("Cantidad", typeof(int));
            dt.Columns.Add("Articulo", typeof(string));
            dt.Columns.Add("Empresa", typeof(string));

            var cont = 0;
            foreach (var item in response.ListaKardex)
            {
                dt.Rows.Add(item.TipoMov, item.TipoDoc, item.NumeroDoc, item.UnMed, item.Codigo, item.Fecha, item.Cantidad, item.Articulo, item.Empresa);
                cont++;
            }

            return dt;
        }
    }
}