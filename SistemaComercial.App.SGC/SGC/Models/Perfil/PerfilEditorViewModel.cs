﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Perfil
{
    public class PerfilEditorViewModel
    {
        public PerfilEditorViewModel() { }

        public PerfilEditorViewModel(PerfilResponseDto response)
        {
            Perfil = response.Perfil;
        }

        public GEN_MaePerfil Perfil { get; set; }
    }
}