﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Perfil
{
    public class PerfilOpcionesViewModel
    {
        public PerfilOpcionesViewModel() { }

        public PerfilOpcionesViewModel(PerfilResponseDto response)
        {
            ListaOpciones = response.ListaOpciones;
        }

        public List<OpcionesPorPerfilListaDto> ListaOpciones { get; set; }
        public short IdPerfil { get; set; }
    }
}