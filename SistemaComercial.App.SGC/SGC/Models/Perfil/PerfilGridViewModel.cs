﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Perfil
{
    public class PerfilGridViewModel
    {
        public PerfilGridViewModel() { }

        public PerfilGridViewModel(PerfilResponseDto response)
        {
            ListaPerfiles = response.ListaPerfiles;
        }

        public List<PerfilListaDto> ListaPerfiles { get; set; }
        public PerfilFiltroDto Filtro { get; set; }
    }
}