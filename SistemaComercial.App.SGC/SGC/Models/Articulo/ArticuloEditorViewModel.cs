﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Articulo
{
    public class ArticuloEditorViewModel
    {
        public ArticuloEditorViewModel() { }

        public ArticuloEditorViewModel(ArticuloResponseDto response)
        {
            Articulo = response.Articulo;

            LstUnidadMedida = new List<SelectListItem>();
            LstMarca = new List<SelectListItem>();

            if (response.ListaUnidadesMedida != null) foreach (GenericoListaDto item in response.ListaUnidadesMedida) LstUnidadMedida.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaMarcas != null) foreach (GenericoListaDto item in response.ListaMarcas) LstMarca.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
        }

        public COM_TabArticulo Articulo { get; set; }
        public List<SelectListItem> LstUnidadMedida { get; set; }
        public List<SelectListItem> LstMarca { get; set; }
    }
}