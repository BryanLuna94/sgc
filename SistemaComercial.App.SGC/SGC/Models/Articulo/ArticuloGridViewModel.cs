﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Articulo
{
    public class ArticuloGridViewModel
    {
        public ArticuloGridViewModel() { }

        public ArticuloGridViewModel(ArticuloResponseDto response)
        {
            ListaArticulos = response.ListaArticulos;
        }

        public List<ArticuloListaDto> ListaArticulos { get; set; }
        public ArticuloFiltroDto Filtro { get; set; }

    }
}