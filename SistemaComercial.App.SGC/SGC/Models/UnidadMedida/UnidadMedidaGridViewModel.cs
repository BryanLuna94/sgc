﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.UnidadMedida
{
    public class UnidadMedidaGridViewModel
    {
        public UnidadMedidaGridViewModel() { }

        public UnidadMedidaGridViewModel(UnidadMedidaResponseDto response)
        {
            ListaUnidadesMedida = response.ListaUnidadesMedida;
        }

        public List<UnidadMedidaListaDto> ListaUnidadesMedida { get; set; }
        public UnidadMedidaFiltroDto Filtro { get; set; }
        public short CodigoUnidadMedida { get; set; }
        public string UnidadMedida { get; set; }
    }
}