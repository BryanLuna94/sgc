﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.UnidadMedida
{
    public class UnidadMedidaEditorViewModel
    {
        public UnidadMedidaEditorViewModel() { }

        public UnidadMedidaEditorViewModel(UnidadMedidaResponseDto response)
        {
            UnidadMedida = response.UnidadMedida;
        }

        public COM_MaeUnidadMedida UnidadMedida { get; set; }
    }
}