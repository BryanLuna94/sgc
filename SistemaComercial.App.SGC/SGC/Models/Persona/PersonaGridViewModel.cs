﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Persona
{
    public class PersonaGridViewModel
    {
        public PersonaGridViewModel() { }

        public PersonaGridViewModel(PersonaResponseDto response)
        {
            ListaPersonas = response.ListaPersonas;
        }

        public List<PersonaListaDto> ListaPersonas { get; set; }
        public PersonaFiltroDto Filtro { get; set; }
        public short CodigoUnidadMedida { get; set; }
        public string UnidadMedida { get; set; }
    }
}