﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Persona
{
    public class PersonaEditorViewModel
    {
        public PersonaEditorViewModel() { }

        public PersonaEditorViewModel(PersonaResponseDto response)
        {
            Persona = response.Persona ?? new COM_TabPersona();
            ListaContactos = response.ListaContactos;
            ListaOficinas = response.ListaOficinas;

            LstPaises = new List<SelectListItem>();
            LstTipDocIdentidad = new List<SelectListItem>();

            if (response.ListaPaises != null) foreach (GenericoListaDto item in response.ListaPaises) LstPaises.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
            if (response.ListaTipDocIdentidad != null) foreach (GenericoListaDto item in response.ListaTipDocIdentidad) LstTipDocIdentidad.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
        }

        public COM_TabPersona Persona { get; set; }
        public List<PersonaContactoListaDto> ListaContactos { get; set; }
        public List<PersonaOficinaListaDto> ListaOficinas { get; set; }
        public List<SelectListItem> LstPaises { get; set; }
        public List<SelectListItem> LstTipDocIdentidad { get; set; }
    }
}