﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Persona
{
    public class PersonaContactoEditorViewModel
    {
        public PersonaContactoEditorViewModel() { }

        public PersonaContactoEditorViewModel(PersonaResponseDto response)
        {
            Contacto = response.ContactoPersona ?? new COM_TabPersona_Contacto();
        }

        public COM_TabPersona_Contacto Contacto { get; set; }
    }
}