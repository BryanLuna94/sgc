﻿using System.Collections.Generic;
using SGC.ServicioSistemaComercial;
using System.Web.Mvc;

namespace SGC.Models.Persona
{
    public class PersonaOficinaEditorViewModel
    {
        public PersonaOficinaEditorViewModel() { }

        public PersonaOficinaEditorViewModel(PersonaResponseDto response)
        {
            Oficina = response.OficinaPersona ?? new COM_TabPersona_Oficina();

            LstPaises = new List<SelectListItem>();

            if (response.ListaPaises != null) foreach (GenericoListaDto item in response.ListaPaises) LstPaises.Add(new SelectListItem { Value = item.Codigo, Text = item.Descripcion });
        }

        public COM_TabPersona_Oficina Oficina { get; set; }
        public List<SelectListItem> LstPaises { get; set; }
    }
}