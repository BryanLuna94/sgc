USE BDCOMERCIAL
GO
  
--------------------------------------------------------------------------------------------------------------  
-- Inserta inventario   
-- Input          : 
-- Creado por     : David Godoy  
-- Fec Creaci�n   : 14/09/2021  
--------------------------------------------------------------------------------------------------------------  
-- Modificado por : XXXXX  
-- Fec Modificado : XXXXX  
-- Motivo         : XXXXX  
--------------------------------------------------------------------------------------------------------------  
CREATE PROC [dbo].[usp_COM_TabInventario_Insertar]  
(  
   @pvDescripcion    VARCHAR(200), 
   @piCodAlm   SMALLINT, 
   @psiCodUsuCre     SMALLINT,  
   @piCodInv         INT OUTPUT 
)  
AS  
BEGIN  
  
   SET NOCOUNT ON  
  
   DECLARE @dtFechaHoy DATETIME   
     
   SET @dtFechaHoy = GETDATE()  
  
   INSERT dbo.COM_TabInventario  
   (  
	  siCodAlm,
      vDescripcion,
	  sdFechaIni,
	  sdFechaFin,
	  estado,
	  siCodUsuCre,
	  sFechaCre
   )  
   VALUES  
   (  
	  @piCodAlm,
      @pvDescripcion ,
	  @dtFechaHoy,
	  DATEADD(DD,60,@dtFechaHoy),
	  1,
	  @psiCodUsuCre,
	  @dtFechaHoy
   )  
  
   SET @piCodInv = IDENT_CURRENT( 'COM_TabInventario' )  
  
   SET NOCOUNT OFF  
  
END  
  
  