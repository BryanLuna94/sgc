USE BDCOMERCIAL  
GO
  
--------------------------------------------------------------------------------------------------------------  
-- Elimina inventario   
-- Input          : 
-- Output         : --               
-- Creado por     : David Godoy Huaman 
-- Fec Creaci�n   : 15/09/2021  
--------------------------------------------------------------------------------------------------------------  
-- Modificado por : XXXXX  
-- Fec Modificado : XXXXX  
-- Motivo         : XXXXX  
--------------------------------------------------------------------------------------------------------------  
CREATE PROC [dbo].[usp_COM_TabInventario_Eliminar]  
(  
   @piCodInv      INT
)  
AS  
BEGIN  
  
   SET NOCOUNT ON  
  
   DECLARE  @dtFechaHoy DATETIME,  
            @bEliminado INT  
     
   SET @dtFechaHoy = GETDATE()  
   SET @bEliminado = 2  
  
   UPDATE dbo.COM_TabInventario 
   SET   
      estado     = @bEliminado
   WHERE  
      iCodInv = @piCodInv  
  
   SET NOCOUNT OFF  
  
END  
  
  