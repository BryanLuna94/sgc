USE BDCOMERCIAL
GO
--------------------------------------------------------------------------------------------------------------
-- Listado art�culo 
-- Input          : @pvDescripcion         Descripci�n del Inventario
--					@piCodAlm	Codigo de almacen
--					@piEstado Estado de Inventario
-- Output         : --             
-- Creado por     : David godoy
-- Fec Creaci�n   : 07/09/2021
--------------------------------------------------------------------------------------------------------------
-- Modificado por : XXXXX
-- Fec Modificado : XXXXX
-- Motivo         : XXXXX
--------------------------------------------------------------------------------------------------------------
CREATE PROC [dbo].[usp_COM_TabInventario_Listar]
(
   @pvDescripcion    VARCHAR(100),
   @piCodAlm   SMALLINT,
   @piEstado   INT
)
AS
BEGIN

   SET NOCOUNT ON

   SET @pvDescripcion      = ISNULL(@pvDescripcion,'')
   SET @piCodAlm       = ISNULL(@piCodAlm,0)
   SET @piEstado       = ISNULL(@piEstado,0)

   SELECT 
      INV.iCodInv,
	  INV.vDescripcion,
	  IIF(INV.estado=1,'ACTIVO','INACTIVO') AS Estado
   FROM 
      dbo.COM_TabInventario INV WITH(NOLOCK) 
   WHERE 
      INV.vDescripcion LIKE '%' + @pvDescripcion + '%'
	  AND (INV.siCodAlm = @piCodAlm OR @piCodAlm = 0)
	  AND (INV.estado = @piEstado OR @piEstado=0)

   ORDER BY 1 DESC
      
   SET NOCOUNT OFF

END

