USE BDCOMERCIAL  
GO
  
--------------------------------------------------------------------------------------------------------------  
-- Actualiza art�culo   
-- Input          :  
-- Output         : --               
-- Creado por     : David Godoy Huaman
-- Fec Creaci�n   : 14/09/2021  
--------------------------------------------------------------------------------------------------------------  
-- Modificado por : XXXXX  
-- Fec Modificado : XXXXX  
-- Motivo         : XXXXX  
--------------------------------------------------------------------------------------------------------------  
CREATE PROC [dbo].[usp_COM_TabInventario_Actualizar]  
(  
   @piCodInv         INT,  
   @pvDescripcion    VARCHAR(200) 
)  
AS  
BEGIN  
  
   SET NOCOUNT ON  
  
   DECLARE @dtFechaHoy DATETIME   
     
   SET @dtFechaHoy = GETDATE()  
  
   UPDATE dbo.COM_TabInventario 
   SET    
      vDescripcion      = @pvDescripcion,   
      sFechaCre         = @dtFechaHoy  
   WHERE  
      iCodInv = @piCodInv  
  
   SET NOCOUNT OFF  
  
END  
  
  