USE BDCOMERCIAL
GO
--------------------------------------------------------------------------------------------------------------  
-- Valida existe inventario   
-- Input          : @pvDescripcion         Descripci�n de inventario  
-- Output         : @piExiste              Variable existe o no               
-- Creado por     : David Godoy  
-- Fec Creaci�n   : 14/09/2021  
--------------------------------------------------------------------------------------------------------------  
-- Modificado por : XXXXX  
-- Fec Modificado : XXXXX  
-- Motivo         : XXXXX  
--------------------------------------------------------------------------------------------------------------  
CREATE PROC [dbo].[usp_COM_TabInventario_ValidaExiste]  
(  
   @pvDescripcion    VARCHAR(100),  
   @piExiste         SMALLINT OUTPUT  
)  
AS  
BEGIN  
  
   SET NOCOUNT ON  
  
   DECLARE  @bEliminado BIT  
  
   SET @bEliminado = 0  
  
   SELECT @piExiste = ISNULL(COUNT(*),0)  
   FROM   
      dbo.COM_TabInventario WITH(NOLOCK)  
   WHERE   
            
      (vDescripcion = @pvDescripcion) 
        
   SET NOCOUNT OFF  
  
END  
  