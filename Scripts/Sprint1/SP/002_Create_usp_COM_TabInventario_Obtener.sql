USE [BDCOMERCIAL]
GO

--------------------------------------------------------------------------------------------------------------
-- Obtener el maestro 
-- Input          : 
-- Output         : --             
-- Creado por     : David Godoy
-- Fec Creación   : 13/09/2021
--------------------------------------------------------------------------------------------------------------
-- Modificado por : XXXXX
-- Fec Modificado : XXXXX
-- Motivo         : XXXXX
--------------------------------------------------------------------------------------------------------------
CREATE PROC [dbo].[usp_COM_TabInventario_Obtener]
(
   @piCodInv      INT
)
AS
BEGIN

   SET NOCOUNT ON

   DECLARE  @bEliminado BIT

   SET @bEliminado = 0

   SELECT 
      iCodInv , siCodAlm   , vDescripcion 
   FROM 
      dbo.COM_TabInventario WITH(NOLOCK)
   WHERE 
      iCodInv = @piCodInv
      
   SET NOCOUNT OFF

END


