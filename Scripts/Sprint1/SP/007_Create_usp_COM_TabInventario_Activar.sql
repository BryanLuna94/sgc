USE BDCOMERCIAL  
GO
  
--------------------------------------------------------------------------------------------------------------  
-- Activar inventario   
-- Input          : 
-- Output         : --               
-- Creado por     : David Godoy Huaman 
-- Fec Creaci�n   : 23/09/2021  
--------------------------------------------------------------------------------------------------------------  
-- Modificado por : XXXXX  
-- Fec Modificado : XXXXX  
-- Motivo         : XXXXX  
--------------------------------------------------------------------------------------------------------------  
CREATE PROC [dbo].[usp_COM_TabInventario_Activar]  
(  
   @piCodInv      INT
)  
AS  
BEGIN  
  
   SET NOCOUNT ON  
  
   DECLARE  @dtFechaHoy DATETIME,  
            @bActivar INT  
     
   SET @dtFechaHoy = GETDATE()  
   SET @bActivar = 1  
  
   UPDATE dbo.COM_TabInventario 
   SET   
      estado     = @bActivar
   WHERE  
      iCodInv = @piCodInv  
  
   SET NOCOUNT OFF  
  
END  
  
  