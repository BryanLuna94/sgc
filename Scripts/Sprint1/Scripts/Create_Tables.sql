USE [BDCOMERCIAL]
GO

CREATE TABLE [dbo].[COM_TabInventario](
	[iCodInv] [int] IDENTITY(1,1) NOT NULL,
	[siCodAlm] [smallint] NOT NULL,
	[vDescripcion] [varchar](200) NOT NULL,
	[sdFechaIni] [smalldatetime] NOT NULL,
	[sdFechaFin] [smalldatetime] NOT NULL,
	[siCodUsuCre] [smallint] NOT NULL,
	[estado] [int] NOT NULL,
 CONSTRAINT [PK_COM_TabInventario] PRIMARY KEY CLUSTERED 
(
	[iCodInv] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[COM_TabInventario]  WITH CHECK ADD  CONSTRAINT [FK_COM_TabInventario_COM_MaeAlmacen] FOREIGN KEY([siCodAlm])
REFERENCES [dbo].[COM_MaeAlmacen] ([siCodAlm])
GO

ALTER TABLE [dbo].[COM_TabInventario] CHECK CONSTRAINT [FK_COM_TabInventario_COM_MaeAlmacen]
GO

---------------------
INSERT [dbo].[GEN_TabParametro] ([iCodTab], [vTabla], [iCodPar], [vParametro], [vParametro2]) VALUES (6, N'ESTADO INVENTARIO', 1, N'ACTIVO', NULL)
GO
INSERT [dbo].[GEN_TabParametro] ([iCodTab], [vTabla], [iCodPar], [vParametro], [vParametro2]) VALUES (6, N'ESTADO INVENTARIO', 2, N'INACTIVO', NULL)
GO
